// AUTHOR(s): Martin Alexander Wilhelmsen,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#ifndef EQ_GENERATOR_STREAM
#define EQ_GENERATOR_STREAM

#include <boost/network/protocol/http/server.hpp>
#include <boost/network/utils/thread_pool.hpp>

#include <arpa/inet.h>

/// Options for EQGeneratorStream
struct EQGeneratorStreamConfig {
	enum options {
		NO_HTTP_CHUNKING = 0x0,
		HTTP_CHUNKING = 0x1,
		HTTP_WEBSOCKET_FRAME = 0x2
	};
};

/// Simple helper class which generators can send data over a http connection.
///  Handles errors and callbacks.
///
/// The generator must implement:
///   std::string operator()() - data to send.
///   bool done() - check if the generator is done.
///   bool available() - check if there is data available in the generator right now.
///   void post(boost::function<void()>) - function to reschedule this generator.
///     when the functor is called, this generator will be added to the pool again.
///     It can be called right away, or at a smarter time i.e. when data actually is
///     available.
///   void outputClosed() - notification that the EQGeneratorStream is terminating,
///     i.e., the client terminated the connection.
///
/// Use makeGeneratorStream to construct as it creates a shared_ptr and this class uses
/// shared_from_this
///
template <EQGeneratorStreamConfig::options config, class server, class T>
class EQGeneratorStream
: public boost::enable_shared_from_this<EQGeneratorStream<config, server, T>> {
	typename server::connection_ptr conn_;
	T gen_;
	bool done_;

	void cbConnectionHandler(boost::system::error_code const &ec) {
		if (ec.value() == 0) {
			generateAndTransfer();
		} else {
			gen_->destroyCallback();
		}
	}

	void generateAndTransfer() {
		// No more data. Then we are done. :-D
		if (gen_->done()) {
			done_ = true;
			if((config & EQGeneratorStreamConfig::HTTP_CHUNKING) != 0) {
				// end the chunking
				conn_->write("0\r\n\r\n");
			}
			return;
		}

		// Generator has no available data
		//  - reschedule this function using generator's post function
		if (!gen_->available()) {
			auto shared_this = this->shared_from_this();
			gen_->post([shared_this, this]() {
					this->conn_->thread_pool().post(boost::bind(
							&EQGeneratorStream<config, server, T>::generateAndTransfer,
							shared_this));
					});


			return;
		}

		// Write data
		//  - when done, call back to cbConnectionHandler on errors and send complete
		if ((config & EQGeneratorStreamConfig::HTTP_WEBSOCKET_FRAME) != 0) {
			std::string data = (*gen_)();
			unsigned char frame[4];

			frame[0] = 0x81;
			frame[1] = 126;

			if(data.size() <= 125 || data.size() >= 0xFFFF) {
				std::cerr << "Unsupported packed size" << std::endl;

				return;
			}

			uint16_t * f2ptr = (uint16_t*) &frame[2];
			*f2ptr = htons((uint16_t)data.size());

			std::string message((const char*)&frame, sizeof frame);
			message += data;

			conn_->write(
					message,
					boost::bind(&EQGeneratorStream<config, server, T>::cbConnectionHandler,
						this->shared_from_this(), _1));
		}
		else if ((config & EQGeneratorStreamConfig::HTTP_CHUNKING) == 0) {
			conn_->write(
					(*gen_)(),
					boost::bind(&EQGeneratorStream<config, server, T>::cbConnectionHandler,
						this->shared_from_this(), _1));
		} else {
			std::string data = (*gen_)();
			std::stringstream ss;

			// stupid copying.
			ss << std::hex << std::uppercase << data.size(); // << "\r\n";

			std::string toSend = ss.str() + "\r\n" + data + "\r\n";

			conn_->write(
					toSend,
					boost::bind(&EQGeneratorStream<config, server, T>::cbConnectionHandler,
						this->shared_from_this(), _1));
		}
	}

	public:
	EQGeneratorStream(typename server::connection_ptr conn, T gen) :
		conn_(conn), gen_(gen), done_(false) {}
	~EQGeneratorStream(){ if(!done_) gen_->outputClosed(); }
	void run() { generateAndTransfer(); }
};

template <EQGeneratorStreamConfig::options config, class connection_ptr, class T>
boost::shared_ptr<EQGeneratorStream<config, typename connection_ptr::element_type, T>>
makeGeneratorStream(connection_ptr conn, T gen) {
	return boost::make_shared<
		EQGeneratorStream<config, typename connection_ptr::element_type, T>>(conn, gen);
}

#endif
