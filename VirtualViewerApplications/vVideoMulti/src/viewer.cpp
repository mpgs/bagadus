// AUTHOR(s): Ragnar Langseth, Vamsidhar Reddy Gaddam,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <viewer.h>

Viewer::Viewer(ViewOutputIface * output, int devId, bool liveMode, int inW, int inH,
		double fov, double scale, float previewScale){
	m_controlStream = NULL;
	m_outputStream = output;
	m_liveMode = liveMode;
	m_inW = inW;
	m_inH = inH;
	m_devId = devId;
	m_previewScale = previewScale;
	m_matVals.fov = fov;
	m_matVals.scale = scale;
	m_framesProcessed = 0;
	m_previewEnc = NULL;
	m_curPreviewBuffer = NULL;
    m_jpegGpuBuffer = NULL;
    m_jpegWidth = 0;
    m_jpegHeight = 0;
    m_jpegInterval = 0;
    m_jpegCurrentFrameActive = false;
    m_jpegGenerator = new ImgGenerator(devId);
	pthread_mutex_init(&m_matrixMutex, NULL);
	pthread_mutex_init(&m_previewMutex, NULL);
	cudaSafe(cudaSetDevice(m_devId));
	cudaSafe(cudaMallocHost((void**)&m_cpuMatrix, 9*sizeof(float)));
	cudaSafe(cudaMalloc((void**)&m_gpuMatrix, 9*sizeof(float)));
	cudaSafe(cudaStreamCreate(&m_stream));
	initCuViewer();
}

Viewer::~Viewer(){
	cudaSafe(cudaSetDevice(m_devId));
	cudaSafe(cudaFreeHost(m_cpuMatrix));
	cudaSafe(cudaFree(m_gpuMatrix));
	cudaSafe(cudaStreamDestroy(m_stream));
	cudaSafe(cudaFree(m_tileBytemap));
    if(m_jpegGpuBuffer) cudaSafe(cudaFree(m_jpegGpuBuffer));
    delete m_jpegGenerator;
	pthread_mutex_destroy(&m_matrixMutex);
	pthread_mutex_destroy(&m_previewMutex);
}

void Viewer::attachControlStream(ViewControlIface * controller){
	m_controlStream = controller;
}

void Viewer::attachPreviewStream(ViewOutputIface * encoder, unsigned int generateInterval){
	pthread_mutex_lock(&m_previewMutex);
	m_previewEnc = encoder;
	m_previewFrameInterval = generateInterval;
	m_curPreviewBuffer = NULL;
	pthread_mutex_unlock(&m_previewMutex);
}

void Viewer::setJpegConfig(uint32_t frameInterval, uint32_t width, uint32_t height){
    //This reduces the chance of segfault if we are changing dimension (DOES NOT REMOVE IT)
    //Should be a lock or something added, or simply remove the possibilty of changing live.
    //I'd write "temporary fix", but that'll just jinx it into permanent...
    while(m_jpegCurrentFrameActive) usleep(100); 

    if(frameInterval <= 0){
        if(m_jpegGpuBuffer) cudaSafe(cudaFree(m_jpegGpuBuffer));
        m_jpegGpuBuffer = NULL;
        m_jpegInterval = 0;
        return;
    }

    if(width != m_jpegWidth || height != m_jpegHeight){
        cudaSafe(cudaSetDevice(m_devId));
        if(m_jpegGpuBuffer) cudaSafe(cudaFree(m_jpegGpuBuffer));
        cudaSafe(cudaMalloc((void**)&m_jpegGpuBuffer, width*height*3));
    }
    m_jpegInterval = frameInterval;
    m_jpegWidth = width;
    m_jpegHeight = height;
    m_jpegCurrentFrameActive = false;
}

bool Viewer::getJpeg(std::vector<unsigned char> &output){
    if(!m_jpegInterval) return false; //Not available if not initialized
    return m_jpegGenerator->getLastJpeg(output);
}

void Viewer::view(uint8_t * gpuInBufY, uint8_t * gpuInBufUV, size_t strideY, size_t strideUV){
	//Request a buffer to place output
	m_curBuffer = m_liveMode ?
		m_outputStream->getNextBuffer() : m_outputStream->getNextBufferBlocking();

	pthread_mutex_lock(&m_previewMutex);
	if(m_previewEnc){
		m_curPreviewBuffer = (m_framesProcessed % m_previewFrameInterval == 0) ?
			m_previewEnc->getNextBuffer() : NULL;
	}
	pthread_mutex_unlock(&m_previewMutex);

    m_jpegCurrentFrameActive = m_jpegInterval &&
           (m_jpegCurrentFrameActive || m_framesProcessed % m_jpegInterval == 0);

	if(m_curBuffer != NULL){
		pthread_mutex_lock(&m_matrixMutex);
		//Update matrix if it has been changed
		if(m_controlStream){
			m_controlStream->updateMatrix(m_framesProcessed, m_matVals.focal,
					m_matVals.theta_x, m_matVals.theta_y, m_matVals.theta_z);
			m_matVals.fillMat(m_cpuMatrix, m_curBuffer->width, m_curBuffer->height);
			cudaSafe(cudaMemcpyAsync(m_gpuMatrix, m_cpuMatrix, 9*sizeof(float),
						cudaMemcpyHostToDevice, m_stream));
		}else if(m_framesProcessed == 0){
			m_matVals.fillMat(m_cpuMatrix, m_curBuffer->width, m_curBuffer->height);
			cudaSafe(cudaMemcpyAsync(m_gpuMatrix, m_cpuMatrix, 9*sizeof(float),
						cudaMemcpyHostToDevice, m_stream));
		}
		pthread_mutex_unlock(&m_matrixMutex);	
		//Perform virtual view projection
		makeView(m_curBuffer, gpuInBufY, gpuInBufUV, strideY, strideUV);
		if(m_curPreviewBuffer){
			makePreviewWindow(m_curPreviewBuffer, m_curBuffer->width, m_curBuffer->height);
		}
        if(m_jpegCurrentFrameActive){
            makePreviewWindowBGR(m_curBuffer->width, m_curBuffer->height);
        }
	}
	//Advance frame. This is oriented towards the matrix update, so we don't need to wait for
	//the async view to complete before advancing.
	m_framesProcessed++;
}

void Viewer::endView(){
	if(m_curBuffer != NULL){
		cudaSafe(cudaStreamSynchronize(m_stream));

        if(m_jpegCurrentFrameActive){
            m_jpegGenerator->pushRaw((const uint8_t*)m_jpegGpuBuffer,
                    m_jpegWidth, m_jpegHeight, m_jpegWidth*3);
            m_jpegCurrentFrameActive = false;
        }

		//Deliver output
		m_outputStream->bufferReady();
		m_curBuffer = NULL;

		pthread_mutex_lock(&m_previewMutex);
		if(m_previewEnc && m_curPreviewBuffer){
			m_previewEnc->bufferReady();
			m_curPreviewBuffer = NULL;
		}
		pthread_mutex_unlock(&m_previewMutex);
	}
}

void ViewerMatrixValues::fillMat(float * mat, int outW, int outH){
	//NB, dont do what I did and think "outW/H should be unsigned..."

	mat[0] = (-cos(theta_y)*cos(theta_z) -
			sin(theta_x)*sin(theta_y)*sin(theta_z))/focal;

	mat[1] = (cos(theta_z)*sin(theta_z)*sin(theta_y) -
			cos(theta_y)*sin(theta_z))/(focal*scale);

	mat[2] = ((cos(theta_y)*(scale*outW*cos(theta_z) +
					outH*sin(theta_z))) +
			(sin(theta_y)*(-2.0*focal*scale*cos(theta_x) +
						   sin(theta_x)*(-outH*cos(theta_z) +
							   scale*outW*sin(theta_z)))))/(2*focal*scale);


	mat[3] = (cos(theta_x)*sin(theta_z))/focal;

	mat[4] = -((cos(theta_x)*cos(theta_z))/(focal*scale));

	mat[5] = (outH*cos(theta_x)*cos(theta_z) -
			2*focal*scale*sin(theta_x) -
			scale*outW*cos(theta_x)*sin(theta_z))/(2*focal*scale);


	mat[6] = 	(-cos(theta_z)*sin(theta_y) +
			cos(theta_y)*sin(theta_x)*sin(theta_z))/focal;

	mat[7] = -((cos(theta_y)*cos(theta_z)*sin(theta_x) +
				sin(theta_y)*sin(theta_z))/(focal*scale));

	mat[8] =     (2*focal*scale*cos(theta_x)*cos(theta_y) +
			sin(theta_y)*(scale*outW*cos(theta_z) +
				outH*sin(theta_z)) +
			cos(theta_y)*sin(theta_x)*(outH*cos(theta_z) -
				scale*outW*sin(theta_z)))/(2*focal*scale);
}

