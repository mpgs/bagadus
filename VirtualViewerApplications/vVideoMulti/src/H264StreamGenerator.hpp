// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <outStream.h>
#include <utils.h>

#include <cstdlib>
#include <string>
#include <deque>
#include <pthread.h>
#include <boost/function.hpp>

#include <viewGenerator.h>

class H264StreamGenerator : public OutStreamIface {

	private:
		bool m_gotKeyframe = false;
		bool m_gotFrame = false;
		bool m_ended = false;
	   	bool m_clientClosed = false;
		std::string m_curFrame;
		boost::function<void()> m_callback;
		ViewGenerator * m_stream;
		pthread_mutex_t m_frameLock, m_cbLock;
		pthread_cond_t m_condFull;

	public:
		H264StreamGenerator(std::shared_ptr<ViewGenerator> stream) :
			m_callback(NULL), m_stream(stream.get())
	{
		pthread_mutex_init(&m_frameLock, NULL);
		pthread_mutex_init(&m_cbLock, NULL);
		pthread_cond_init(&m_condFull, NULL);
		stream->addSlave(this);
	}	
		~H264StreamGenerator(){
			if(!m_ended) m_stream->removeSlave(this);

			LOG_D("Terminating client stream...");
		}

		void endStream() {
			m_ended = true;
			pthread_mutex_lock(&m_cbLock);
			if(m_callback){
				m_callback();
				m_callback = NULL;
			}
			pthread_mutex_unlock(&m_cbLock);
		}

		void outputClosed(){
			pthread_mutex_lock(&m_frameLock);
			m_clientClosed = true;
			pthread_cond_signal(&m_condFull);
			pthread_mutex_unlock(&m_frameLock);
		}

		bool available(){ return m_gotFrame; }
		bool done() { return m_ended; }

		void pushData(void * data, size_t byteCount, bool keyframe){
			if(!m_gotKeyframe){
				if(!keyframe) return; //First frame delivered should be a keyframe
			   	m_gotKeyframe = true;
			}

			pthread_mutex_lock(&m_frameLock);
			while(!m_clientClosed && m_gotFrame) pthread_cond_wait(&m_condFull, &m_frameLock);

			if(m_clientClosed){
				pthread_mutex_unlock(&m_frameLock);
				return;
			}

			m_curFrame = std::string((char*)data, byteCount);
			m_gotFrame = true;
			
			pthread_mutex_unlock(&m_frameLock);

			pthread_mutex_lock(&m_cbLock);
			if(m_callback){
				m_callback();
				m_callback = NULL;
			}
			pthread_mutex_unlock(&m_cbLock);
		}

		std::string operator()(){
			std::string ret;
			pthread_mutex_lock(&m_frameLock);
			ret = m_curFrame;
			m_gotFrame = false;
			pthread_cond_signal(&m_condFull);
			pthread_mutex_unlock(&m_frameLock);
			return ret;
		}

		void destroyCallback(){
			pthread_mutex_lock(&m_cbLock);
			m_callback = NULL;
			pthread_mutex_unlock(&m_cbLock);
		}

		void post(boost::function<void()> fn){
			pthread_mutex_lock(&m_cbLock);
			m_callback = fn;
			pthread_mutex_unlock(&m_cbLock);
		}
};

