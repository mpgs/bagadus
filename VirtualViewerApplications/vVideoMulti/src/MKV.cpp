// AUTHOR(s): Asgeir Mortensen, Martin Alexander Wilhelmsen,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <vector>
#include <iostream>

#include "MKV.hpp"
#include "EbmlIDs.h"

#define LITERALU64(hi,lo) ((((uint64_t)hi)<<32)|lo)

#define MAP_SZ ((size_t)(1<<28))
#define MKV_HEAD ((size_t)(1<<13))

struct framerate_t {
	int num, den;
};


typedef off_t EbmlLoc;

enum buffer_indexes {
	MKV_HEADER = 0,
	MKV_BUFFER = 1
};

// Source vpxenc.
typedef enum stereo_format {
	STEREO_FORMAT_MONO       = 0,
	STEREO_FORMAT_LEFT_RIGHT = 1,
	STEREO_FORMAT_BOTTOM_TOP = 2,
	STEREO_FORMAT_TOP_BOTTOM = 3,
	STEREO_FORMAT_RIGHT_LEFT = 11
} stereo_format_t;

struct buffers {
	unsigned char *org;
	unsigned char *pos;
	size_t size;
	size_t used;
};

struct EbmlGlobal {
	int debug;

	// Replaced filestream with buffer.
	size_t buffer;

	struct buffers buffers[2];
	size_t last_size;
	int frames_in;

	int64_t last_pts_ms;
	framerate_t framerate;

	/* These pointers are to the start of an element */
	off_t    position_reference;
	off_t    seek_info_pos;
	off_t    segment_info_pos;
	off_t    track_pos;

	// asgeirom - Nope, won't need this for livestreams.
	// off_t    cue_pos;

	/* asgeirom - We're not going to close clusters
	 * If we did we would have to wait for a GOP/cluster before sending data
	 */

	// WARNING: cluster_pos does not contain the correct value!
	off_t    cluster_pos;

	/* This pointer is to a specific element to be serialized */
	off_t    track_id_pos;

	/* These pointers are to the size field of the element */
	EbmlLoc  startSegment;
	EbmlLoc  startCluster;

	uint32_t cluster_timecode;
	int      cluster_open;

	// asgeirom - We could generate cues, but we don't want the stream saved
	struct cue_entry *cue_list;
	unsigned int      cues;
};

static void Ebml_Write(struct EbmlGlobal *glob, const void *src, unsigned long len)
{
	memcpy(glob->buffers[glob->buffer].pos, src, (size_t)len);
	glob->buffers[glob->buffer].pos += (size_t)len;
}

/** asgeirom ifi uio no
 * DEFINE rewritting to add unroll to the loop,
 * given x restricted for better gcc optimization.
 */
#define WRITE_BUFFER(s) \
	for(i = len-1; i>=0; i--) { \
		*(x++) = (char)(*(const s *)buffer_in >> (i * CHAR_BIT)); \
	}

/** asgeirom ifi uio no
 * Buffers are size_t size, not int! Changed parameter to size_t.
 * Changed return value to ssize_t for error handling.
 * Org. Source: vpxenc.c
 */
#pragma GCC push_options
#pragma GCC optimize ("unroll-loops")
static void Ebml_Serialize(struct EbmlGlobal *glob, const void *buffer_in, size_t buffer_size, unsigned long len)
{
	int i;
	unsigned char *x = glob->buffers[glob->buffer].pos;
	//fprintf(stderr, "%x %x\n", x, glob->buffers[glob->buffer].org);
	switch (buffer_size) {
		case 1:
			WRITE_BUFFER(int8_t)
				break;
		case 2:
			WRITE_BUFFER(int16_t)
				break;
		case 4:
			WRITE_BUFFER(int32_t)
				break;
		case 8:
			WRITE_BUFFER(int64_t)
				break;
		default:
			break;
	}
	// asgeirom - If mmap throws a fit, we won't have changed the pointer.
	glob->buffers[glob->buffer].pos += (size_t)len;
}
#pragma GCC pop_options
#undef WRITE_BUFFER


/** asgeirom ifi uio no
 * This is not my work(!), it's from Matroska (as far as I can tell),
 *  but it were supplied with the vpx libary.
 *
 * Source: EbmlWriter.c
 */
static void Ebml_WriteID(struct EbmlGlobal *glob, unsigned long class_id)
{
	int len;

	if (class_id >= 0x01000000)
		len = 4;
	else if (class_id >= 0x00010000)
		len = 3;
	else if (class_id >= 0x00000100)
		len = 2;
	else
		len = 1;

	Ebml_Serialize(glob, (void *)&class_id, sizeof(class_id), len);
}

static void Ebml_WriteLen(struct EbmlGlobal *glob, int64_t val)
{
	/* TODO check and make sure we are not > than 0x0100000000000000LLU */
	unsigned char size = 8; /* size in bytes to output */

	/* mask to compare for byte size */
	int64_t minVal = 0xff;

	for (size = 1; size < 8; size ++) {
		if (val < minVal)
			break;

		minVal = (minVal << 7);
	}

	val |= (((uint64_t)0x80) << ((size - 1) * 7));

	Ebml_Serialize(glob, (void *) &val, sizeof(val), size);
}

static void Ebml_WriteString(struct EbmlGlobal *glob, const char *str)
{
	const size_t size_ = strlen(str);
	const uint64_t  size = size_;
	Ebml_WriteLen(glob, size);
	/* TODO: it's not clear from the spec whether the nul terminator
	 * should be serialized too.  For now we omit the null terminator.
	 */
	Ebml_Write(glob, str, (unsigned long)size);
}

static void Ebml_SerializeString(struct EbmlGlobal *glob, unsigned long class_id, const char *s)
{
	Ebml_WriteID(glob, class_id);
	Ebml_WriteString(glob, s);
}

// Souce: vpxen.c
/* Need a fixed size serializer for the track ID. libmkv provides a 64 bit
 * one, but not a 32 bit one.
 */
static void Ebml_SerializeUnsigned32(struct EbmlGlobal *glob, unsigned long class_id, uint64_t ui)
{
	unsigned char sizeSerialized = 4 | 0x80;
	Ebml_WriteID(glob, class_id);
	Ebml_Serialize(glob, &sizeSerialized, sizeof(sizeSerialized), 1);
	Ebml_Serialize(glob, &ui, sizeof(ui), 4);
}

static void Ebml_SerializeUnsigned(struct EbmlGlobal *glob, unsigned long class_id, unsigned long ui)
{
	unsigned char size = 8; /* size in bytes to output */
	unsigned char sizeSerialized = 0;
	unsigned long minVal;

	Ebml_WriteID(glob, class_id);
	minVal = 0x7fLU; /* mask to compare for byte size */

	for (size = 1; size < 4; size ++) {
		if (ui < minVal) {
			break;
		}

		minVal <<= 7;
	}

	sizeSerialized = 0x80 | size;
	Ebml_Serialize(glob, &sizeSerialized, sizeof(sizeSerialized), 1);
	Ebml_Serialize(glob, &ui, sizeof(ui), size);
}


	static void
Ebml_StartSubElement(struct EbmlGlobal *glob, EbmlLoc *ebmlLoc,
		unsigned long class_id)
{
	/* todo this is always taking 8 bytes, this may need later optimization */
	/* this is a key that says length unknown */
	uint64_t unknownLen = LITERALU64(0x01FFFFFF, 0xFFFFFFFF);
	const struct buffers *buff = glob->buffers + glob->buffer;

	Ebml_WriteID(glob, class_id);
	*ebmlLoc = buff->pos - buff->org;
	Ebml_Serialize(glob, &unknownLen, sizeof(unknownLen), 8);
}

	static void
Ebml_EndSubElement(struct EbmlGlobal *glob, EbmlLoc *ebmlLoc)
{
	struct buffers *buff = glob->buffers + glob->buffer;
	off_t pos;
	uint64_t size;

	/* Save the current stream pointer */
	pos = buff->pos - buff->org;

	/* Calculate the size of this element */
	size = pos - *ebmlLoc - 8;
	size |= LITERALU64(0x01000000, 0x00000000);
	/* Seek back to the beginning of the element and write the new size */
	buff->pos = buff->org + *ebmlLoc;

	Ebml_Serialize(glob, &size, sizeof(size), 8);

	/* Reset the stream pointer */
	buff->pos = buff->org + pos;
}


static void Ebml_SerializeFloat(struct EbmlGlobal *glob, unsigned long class_id, double d) {
	unsigned char len = 0x88;

	Ebml_WriteID(glob, class_id);
	Ebml_Serialize(glob, &len, sizeof(len), 1);
	Ebml_Serialize(glob,  &d, sizeof(d), 8);
}
static void Ebml_SerializeUnsigned64(struct EbmlGlobal *glob, unsigned long class_id, uint64_t ui) {
	unsigned char sizeSerialized = 8 | 0x80;
	Ebml_WriteID(glob, class_id);
	Ebml_Serialize(glob, &sizeSerialized, sizeof(sizeSerialized), 1);
	Ebml_Serialize(glob, &ui, sizeof(ui), 8);
}
	static void
Ebml_SerializeBinary(struct EbmlGlobal *glob, unsigned long class_id, unsigned long bin)
{
	int size;
	for (size = 4; size > 1; size--) {
		if (bin & 0x000000ff << ((size - 1) * 8))
			break;
	}
	Ebml_WriteID(glob, class_id);
	Ebml_WriteLen(glob, size);
	Ebml_WriteID(glob, bin);
}

	static void
write_webm_seek_element(struct EbmlGlobal *ebml, unsigned long id, off_t pos)
{
	const struct buffers *header = ebml->buffers + ebml->buffer;
	uint64_t offset = pos - ebml->position_reference;
	EbmlLoc start = header->pos - header->org;;
	Ebml_StartSubElement(ebml, &start, Seek);
	Ebml_SerializeBinary(ebml, SeekID, id);
	Ebml_SerializeUnsigned64(ebml, SeekPosition, offset);
	Ebml_EndSubElement(ebml, &start);
}

struct MKVImpl {
	unsigned int width_, height_;
	unsigned int fps_num_, fps_den_;

	EbmlGlobal glob;

	struct buffers *header;
	struct buffers *container;

	double timestamp_ms_;

	MKVImpl(unsigned int width, unsigned int height, unsigned int fps)
		: width_(width), height_(height), fps_num_(1000), fps_den_(fps*1000), timestamp_ms_(0.0)
	{
		memset(&glob, 0, sizeof glob);

		header = &glob.buffers[MKV_HEADER];
		container = &glob.buffers[MKV_BUFFER];

		header->org = new unsigned char[MKV_HEAD];
		header->pos = header->org;
		header->size = MKV_HEAD;

		glob.buffer = MKV_HEADER;

		EbmlLoc start;
		Ebml_StartSubElement(&glob, &start, EBML);
		Ebml_SerializeUnsigned(&glob, EBMLVersion, 1);
		Ebml_SerializeUnsigned(&glob, EBMLReadVersion, 1);
		Ebml_SerializeUnsigned(&glob, EBMLMaxIDLength, 4);
		Ebml_SerializeUnsigned(&glob, EBMLMaxSizeLength, 8);
		Ebml_SerializeString(&glob, DocType, "matroska");
		Ebml_SerializeUnsigned(&glob, DocTypeVersion, 2);
		Ebml_SerializeUnsigned(&glob, DocTypeReadVersion, 2);
		Ebml_EndSubElement(&glob, &start);

		Ebml_StartSubElement(&glob, &glob.startSegment, Segment);
		glob.position_reference = header->pos - header->org;
		{
			EbmlLoc start;
			Ebml_StartSubElement(&glob, &start, SeekHead);
			write_webm_seek_element(&glob, Tracks, glob.track_pos);
			write_webm_seek_element(&glob, Info,   glob.segment_info_pos);
			Ebml_EndSubElement(&glob, &start);

			EbmlLoc startInfo;
			glob.segment_info_pos = header->pos - header->org;
			Ebml_StartSubElement(&glob, &startInfo, Info);
			Ebml_SerializeUnsigned(&glob, TimecodeScale, 1000000);
			Ebml_SerializeString(&glob, MuxingApp, "Bagadus");
			Ebml_EndSubElement(&glob, &startInfo);

			EbmlLoc trackStart;
			glob.track_pos = header->pos - header->org;
			Ebml_StartSubElement(&glob, &trackStart, Tracks);
			//+
			{
				unsigned int trackNumber = 1;

				EbmlLoc start;
				Ebml_StartSubElement(&glob, &start, TrackEntry);
				//+
				Ebml_SerializeUnsigned(&glob, TrackNumber, trackNumber);
				glob.track_id_pos = header->pos - header->org;

				/// WARNING: This UID needs to change when we have more than one stream
				Ebml_SerializeUnsigned32(&glob, TrackUID, 0xDEADBEEF);
				Ebml_SerializeUnsigned(&glob, TrackType, 1);
				Ebml_SerializeString(&glob, CodecID, "V_MPEG4/ISO/AVC");
				Ebml_SerializeBinary(&glob, FlagForced, 1);
				{
					unsigned int pixelWidth  = width_;
					unsigned int pixelHeight = height_;
					float        frameRate   = (float)fps_den_ / fps_num_;

					EbmlLoc videoStart = header->pos - header->org;
					Ebml_StartSubElement(&glob, &videoStart, Video);
					//+
					Ebml_SerializeUnsigned(&glob, MinCache, 0);
					Ebml_SerializeUnsigned(&glob, MaxCache, 0);

					Ebml_SerializeUnsigned(&glob, PixelWidth, pixelWidth);
					Ebml_SerializeUnsigned(&glob, PixelHeight, pixelHeight);
					Ebml_SerializeUnsigned(&glob, StereoMode, STEREO_FORMAT_MONO);
// 					Ebml_SerializeFloat(&glob, FrameRate, 1);
					Ebml_SerializeFloat(&glob, FrameRate, frameRate);
					Ebml_EndSubElement(&glob, &videoStart);
					//-
				}
				Ebml_EndSubElement(&glob, &start); /* Track Entry */
				//-
			}
// 			//+
// 			{
// 				unsigned int trackNumber = 2;
// 
// 				EbmlLoc start;
// 				Ebml_StartSubElement(&glob, &start, TrackEntry);
// 				//+
// 				Ebml_SerializeUnsigned(&glob, TrackNumber, trackNumber);
// 				glob.track_id_pos = header->pos - header->org;
// 
// 				/// WARNING: This UID needs to change when we have more than one stream
// 				Ebml_SerializeUnsigned32(&glob, TrackUID, 0xDEADBEEE);
// 				Ebml_SerializeUnsigned(&glob, TrackType, 3);
// 				Ebml_SerializeString(&glob, CodecID, "V_UNCOMPRESSED");
// 				Ebml_SerializeBinary(&glob, FlagEnabled, 0);
// 				Ebml_EndSubElement(&glob, &start); /* Track Entry */
// 				//-
// 			}
			Ebml_EndSubElement(&glob, &trackStart);
		}
	}

	~MKVImpl() {
		delete[] header->org;
	}

	void getHeader(std::vector<unsigned char> &data) {
		size_t sz = header->pos - header->org;
		data.resize(sz);
		memcpy(&data[0], header->org, sz);
	}

	template<typename U, typename V>
		void muxFrame(U &in, V &out) {
			const int CLUSTER_MAX_HEADER_SIZE = 64;

			glob.buffer = MKV_BUFFER;
			struct buffers *buffer = &glob.buffers[MKV_BUFFER];

			// reserve memory for buffer
			out.resize(CLUSTER_MAX_HEADER_SIZE + in.size());
			buffer->pos = buffer->org = (unsigned char*)out.data();

			container->pos = container->org;

			glob.cluster_open = 1;
			glob.cluster_timecode = timestamp_ms_;
			glob.cluster_pos = buffer->pos - buffer->org;

			timestamp_ms_ += (1000.0f * fps_num_) / fps_den_;

			Ebml_StartSubElement(&glob, &glob.startCluster, Cluster);
			Ebml_SerializeUnsigned(&glob, Timecode, glob.cluster_timecode);

			unsigned long  block_length;
			unsigned char  track_number;
			unsigned short block_timecode = 0;
			unsigned char  flags;

			Ebml_WriteID(&glob, SimpleBlock);
			block_length = (unsigned long)in.size() + 4;
			block_length |= 0x10000000;
			Ebml_Serialize(&glob, &block_length, sizeof(block_length), 4);

			track_number = 1;
			track_number |= 0x80;
			Ebml_Write(&glob, &track_number, 1);

			Ebml_Serialize(&glob, &block_timecode, sizeof(block_timecode), 2);

			flags = 0;
			Ebml_Write(&glob, &flags, 1);
			Ebml_Write(&glob, in.data(), in.size());

			// resize buffer to actual size
			out.resize(buffer->pos - buffer->org);
		}
	template<typename V>
		void fillCluster(int track, size_t n, V &out) {
			const int CLUSTER_MAX_HEADER_SIZE = 64;

			glob.buffer = MKV_BUFFER;
			struct buffers *buffer = &glob.buffers[MKV_BUFFER];

			// reserve memory for buffer
			out.resize(CLUSTER_MAX_HEADER_SIZE + n);
			buffer->pos = buffer->org = (unsigned char*)out.data();

			container->pos = container->org;

// 			timestamp_ms_ += 1000 * ((float)fps_num_ / fps_den_);

			glob.cluster_open = 1;
			glob.cluster_timecode = timestamp_ms_;
			glob.cluster_pos = buffer->pos - buffer->org;

			Ebml_StartSubElement(&glob, &glob.startCluster, Cluster);
			Ebml_SerializeUnsigned(&glob, Timecode, glob.cluster_timecode);

			unsigned long  block_length;
			unsigned char  track_number;
			unsigned short block_timecode = 0;
			unsigned char  flags;

			Ebml_WriteID(&glob, SimpleBlock);
			block_length = (unsigned long)n;
			block_length |= 0x10000000;
			Ebml_Serialize(&glob, &block_length, sizeof(block_length), 4);

			track_number = track;
			track_number |= 0x80;
			Ebml_Write(&glob, &track_number, 1);

			Ebml_Serialize(&glob, &block_timecode, sizeof(block_timecode), 2);

			flags = 0;
			Ebml_Write(&glob, &flags, 1);
			buffer->pos += n;

			// resize buffer to actual size
			out.resize(buffer->pos - buffer->org);
		}
};

//
// Public interface
//

MKV::MKV(unsigned int width, unsigned int height, unsigned int fps) {
	impl_ = new MKVImpl(width, height, fps);
}

MKV::~MKV() {
	delete impl_;
}

void MKV::getHeader(std::vector<unsigned char> &out) {
	impl_->getHeader(out);
}

template<typename U, typename V>
void MKV::muxFrame(U &in, V &out) {
	impl_->muxFrame(in, out);
}

template<typename V>
void MKV::fillCluster(int track, size_t n, V &out) {
	impl_->fillCluster(track, n, out);
}


template void MKV::muxFrame(std::string &in, std::string &out);
template void MKV::muxFrame(std::vector<unsigned char> &in, std::string &out);
template void MKV::muxFrame(std::vector<unsigned char> &in, std::vector<unsigned char> &out);
template void MKV::muxFrame(std::string &in, std::vector<unsigned char> &out);

template void MKV::fillCluster(int track, size_t n, std::vector<unsigned char> &out);
template void MKV::fillCluster(int track, size_t n, std::string &out);

/*
   void useMuxer__________() {
   std::string a1, a2;
   std::vector<unsigned char> b1, b2;

   MKV c(1000,1000,10);

   c.muxFrame(a1, a2);
   c.muxFrame(a1, b1);
   c.muxFrame(b1, b2);
   c.muxFrame(b1, a1);
   }
   */

