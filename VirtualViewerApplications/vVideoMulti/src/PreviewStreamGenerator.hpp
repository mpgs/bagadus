// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <streamGenerator.h>

class PreviewStreamGenerator : public OutStreamIface , public StreamGeneratorIface {
	private:
		Viewer * m_viewer;
		ViewEnc m_enc;
		StreamStats m_stats;
		pthread_mutex_t m_slaveLock;
		std::vector<OutStreamIface*> m_slaves;
	public:
		PreviewStreamGenerator(Viewer * viewer, int devId, unsigned int frameInterval,
				StreamStats stats) :
			m_viewer(viewer), m_stats(stats)
	{
		pthread_mutex_init(&m_slaveLock, NULL);
		m_enc.init(stats.fps, stats.width, stats.height, devId, stats.bitrate*1000, stats.fps*3);
		m_enc.attachOutputStream(this);
		viewer->attachPreviewStream(&m_enc, frameInterval);
	}

		~PreviewStreamGenerator(){
			m_viewer->attachPreviewStream(NULL, 0); //Reset
			m_enc.terminate();
		}

		StreamStats getStats(){ return m_stats;	}

		void pushData(void * data, size_t byteCount, bool keyframe){
			pthread_mutex_lock(&m_slaveLock);
			for(auto it : m_slaves) it->pushData(data, byteCount, keyframe);
			pthread_mutex_unlock(&m_slaveLock);
		}
		void endStream() {
			pthread_mutex_lock(&m_slaveLock);
			for( auto &it : m_slaves) it->endStream();
			pthread_mutex_unlock(&m_slaveLock);
		}
		void addSlave(OutStreamIface * slave){
			pthread_mutex_lock(&m_slaveLock);
			m_stats.viewers++;
			m_slaves.push_back(slave);
			pthread_mutex_unlock(&m_slaveLock);
			m_enc.forceKeyframe();
		}
		void removeSlave(OutStreamIface * slave){
			pthread_mutex_lock(&m_slaveLock);
			m_stats.viewers--;
			m_slaves.erase(std::remove(m_slaves.begin(), m_slaves.end(), slave), m_slaves.end());
			pthread_mutex_unlock(&m_slaveLock);
		}
		void requestKeyframe(){	m_enc.forceKeyframe(); }
};

