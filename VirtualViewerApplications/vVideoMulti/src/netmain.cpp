// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>
#include <memory>
#include <string>
#include <fstream>
#include <streambuf>

#define BOOST_NETWORK_NO_LIB
#include <boost/network/protocol/http/server.hpp>
#include <boost/network/utils/thread_pool.hpp>
#include <boost/generator_iterator.hpp>
#include <boost/thread.hpp>
#include <boost/regex.hpp>
#include <boost/bind.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/function.hpp>

#include <utils.h>

#include "Websocket.hpp"
#include "EQGeneratorStream.hpp"
#include "PostReader.hpp"
#include "PipeWriter.hpp"

#include <panoDefines.h>
#include <viewInput.h>
#include <streamGenerator.h>
#include <viewGenerator.h>

#include "SlaveStreamGenerator.hpp"

namespace http = boost::network::http;
namespace utils = boost::network::utils;

const int port = 8089;
const unsigned int bitrateDefault = 0;
const unsigned int previewScaleDefault = 35;
const unsigned int outWidthDefault = 1280;
const unsigned int outHeightDefault = 720;

class HTTPHandler {
	public:
		typedef http::async_server<HTTPHandler> server;

		std::map<std::string, std::shared_ptr<StreamGeneratorIface> > m_streams;
		pthread_mutex_t m_streamLock;
		std::map<boost::regex, boost::function<void(server::request const &,
				server::connection_ptr, boost::smatch &)>> m_sites;

		ViewInput * m_inputSource;
		int m_fps;
		PipeWriter m_pipeWriter;

		HTTPHandler(ViewInput * inputSource, int fps, int pipe, std::string loopDir) :
			m_inputSource(inputSource), m_fps(fps), m_pipeWriter(PipeWriter(pipe, loopDir)) {
				m_sites[boost::regex("^/stream/([[:xdigit:]]+)"
						"((\\?(width|height|rate|preview)=\\d+)*)")] =
					boost::bind(&HTTPHandler::stream, this, _1, _2, _3);

				m_sites[boost::regex("^/$")] =
					boost::bind(&HTTPHandler::index, this, _1, _2, _3);

				m_sites[boost::regex("^/watch$")] =
					boost::bind(&HTTPHandler::watch, this, _1, _2, _3);

				m_sites[boost::regex("^/jpeg/([[:xdigit:]]+)$")] = 
					boost::bind(&HTTPHandler::jpeg, this, _1, _2, _3);

				m_sites[boost::regex("^/path/([[:xdigit:]]+)$")] = 
					boost::bind(&HTTPHandler::pathPost, this, _1, _2, _3);

				m_sites[boost::regex("^/terminate/([[:xdigit:]]+)$")] = 
					boost::bind(&HTTPHandler::terminateRequest, this, _1, _2, _3);

				m_sites[boost::regex("^/listStreams$")] =
					boost::bind(&HTTPHandler::listStreams, this, _1, _2, _3);

				m_sites[boost::regex("^/watch/([[:xdigit:]]+)$")] =
					boost::bind(&HTTPHandler::attach, this, _1, _2, _3);

				m_sites[boost::regex("^/preview/([[:xdigit:]]+)$")] =
					boost::bind(&HTTPHandler::preview, this, _1, _2, _3);

				m_sites[boost::regex("^/video$")] =
					boost::bind(&HTTPHandler::videoPost, this, _1, _2, _3);

				m_sites[boost::regex("^/websocket/([[:xdigit:]]+)$")] =
					boost::bind(&HTTPHandler::websocket, this, _1, _2, _3);
				pthread_mutex_init(&m_streamLock, NULL);
		}

		//Looks up a stream-ID and returns the stream, or NULL
        inline std::shared_ptr<StreamGeneratorIface> lookup(std::string id){
            pthread_mutex_lock(&m_streamLock);
            auto it = m_streams.find(id);
			if(it == m_streams.end()){
				pthread_mutex_unlock(&m_streamLock);
				return NULL;
			}else{
				pthread_mutex_unlock(&m_streamLock);
				return it->second;
			}
        }

		//Looks up a stream-ID and returns the stream, cast to type 'StreamT'
		//If the stream is not of type 'StreamT', NULL is returned.
		//Therefore, in order to check for existance you have to lookup the base interface
        template <typename StreamT> inline std::shared_ptr<StreamT> lookup(std::string id){
			return std::dynamic_pointer_cast<StreamT>(lookup(id));
        }

		void operator()(server::request const &request,
				server::connection_ptr conn) {
			for (auto &it : m_sites) {
				std::string tmp;
				boost::smatch what;
				if (boost::regex_match(request.destination, what, it.first)) {
// 					std::cout << "REQUEST ARRIVED (" << what.size() << " tokens): ";
// 					for(unsigned int i=0; i< what.size(); ++i){
// 						std::cout << what[i] << " ";
// 					}
// 					std::cout << std::endl;
					it.second(request, conn, what);
					return;
				}
			}

			simplePage(conn, server::connection::not_found, "404: Unknown url");
		}

		void simpleResponse(server::connection_ptr conn,
				server::connection::status_t status) {
			std::vector<server::response_header> cheaders = {
				{ "Connection", "close" },
			};
			conn->set_status(status);
			conn->set_headers(boost::make_iterator_range(cheaders.begin(), cheaders.end()));
		}

		void simplePage(server::connection_ptr conn,
				server::connection::status_t status, std::string s) {
			std::vector<server::response_header> cheaders = {
				{ "Connection", "close" }, { "Content-Type", "text/html" },
			};
			conn->set_status(status);
			conn->set_headers(boost::make_iterator_range(cheaders.begin(), cheaders.end()));
			conn->write(s);
		}

		void index(server::request const &request, server::connection_ptr conn,
				boost::smatch &regexResult) {
			std::ifstream f("html/index.html");
			std::string html((std::istreambuf_iterator<char>(f)),
					std::istreambuf_iterator<char>());

			simplePage(conn, server::connection::ok, html);
		}

		void terminateStream(std::string id){
			pthread_mutex_lock(&m_streamLock);
			m_streams.erase(id);
			pthread_mutex_unlock(&m_streamLock);
		}

		void jpeg(server::request const &request,
				server::connection_ptr conn, boost::smatch &matches){

			std::string id = matches[1];

			auto stream = lookup(id);

			if(stream){
                std::vector<unsigned char> jpg;
                if(stream->getJpeg(jpg)){
                    std::vector<server::response_header> cheaders = {
                        { "Connection", "close" },
                        { "Content-Type", "image/jpeg" },
                        { "Cache-Control", "no-cache, no-store, must-revalidate" },
                        { "Pragma", "no-cache" },
                        { "Expires", "0" },
                        { "Content-Type", "image/jpeg" },
                    };
                    conn->set_status(server::connection::ok);
                    conn->set_headers(boost::make_iterator_range(cheaders.begin(), cheaders.end()));
                    conn->write(std::string(jpg.begin(), jpg.end()));
                }else{
                    simplePage(conn, server::connection::not_found, "Temporarily unavailable");
                }
			}else{
				simplePage(conn, server::connection::not_found, "Unknown stream");
			}
		}

		void terminateRequest(server::request const &request,
				server::connection_ptr conn, boost::smatch &matches){

			std::string id = matches[1];

			//We only allow deletion of 'ViewGenerators', not owned by a single client
			auto stream = lookup<ViewGenerator>(id);
			if(stream){
				//Since we release the lock, in theory,
				//someone else could delete the stream in the meantime, but this does not matter
				pthread_mutex_lock(&m_streamLock);
				m_streams.erase(id);
				pthread_mutex_unlock(&m_streamLock);
			}

			if(stream){
				simplePage(conn, server::connection::ok, "");
			}else{
				simplePage(conn, server::connection::not_found, "Unknown stream");
			}
		}

		void videoPost(server::request const &request,
				server::connection_ptr conn, boost::smatch &matches){
			if(request.method != "POST") {
				LOG_W("Received non-post data on /video/ url");
				return;
			}

			auto reader = boost::make_shared<PostReader<server>>(
					request, conn, [conn, request, this]
					(bool err, std::unordered_map<std::string, PostReaderData> &data) {

					if(err) {
						this->simplePage(conn, server::connection::ok, 
							"AYAYAYA, got some errors while parsing");
					} else {
						PostReaderData content;
						content = data["file"];

						if(content.p != 0) {
							while(!m_pipeWriter.pushData(content.p, content.l)) usleep(500000);
						} else {
							LOG_W("Received empty video-post");
						}
					}
					simplePage(conn, server::connection::ok, "");
				}
			);

			if((*reader)()) {
				return;
			} else {
				simplePage(conn, server::connection::ok, "Error");
			}
		}

		void parsePathPostData(std::unordered_map<std::string, PostReaderData> &data, 
				unsigned int &width, unsigned int &height,
				float &previewScale, unsigned int &bitrate){
			PostReaderData content;

			//Set some defaults, then override them if possible
			width = 1920;
			height = 1080;
			previewScale = 30.0f;
			bitrate = 0;

			content = data["width"];
			if(content.p != 0) {
				try{
					width = boost::lexical_cast<unsigned int>(std::string(content.p,content.l));
				} catch( boost::bad_lexical_cast const& ) { /* Stitck to default */ }
			}

			content = data["height"];
			if(content.p != 0) {
				try{
					height = boost::lexical_cast<unsigned int>(std::string(content.p,content.l));
				} catch( boost::bad_lexical_cast const& ) { /* Stitck to default */ }
			}

			content = data["bitrate"];
			if(content.p != 0) {
				try{
					bitrate = boost::lexical_cast<unsigned int>(std::string(content.p,content.l));
				} catch( boost::bad_lexical_cast const& ) { /* Stitck to default */ }
			}

			content = data["preview"];
			if(content.p != 0) {
				try{
					previewScale = boost::lexical_cast<float>(std::string(content.p,content.l));
				} catch( boost::bad_lexical_cast const& ) { /* Stitck to default */ }
			}

			previewScale /= 100.0f;
		}

		void pathPost(server::request const &request,
				server::connection_ptr conn, boost::smatch &matches){
			if(request.method != "POST") {
				LOG_W("Received non-post data on /path/ url");
				return;
			}

			std::string id = matches[1];

			// Create a reader with will read the HTTP post body from the connection.
			//  It will then run the callback as demonstrated below.
			auto reader = boost::make_shared<PostReader<server>>(
					request, conn, [conn, request, this, id]
					(bool err, std::unordered_map<std::string, PostReaderData> &data) {

					if(err) {
						this->simplePage(conn, server::connection::ok, 
							"AYAYAYA, got some errors while parsing");
					} else {
						
                        auto stream = lookup(id);

						if(!stream){
							//First time seeing this pid, need to create the stream

							unsigned int width, height, bitrate;
							float previewScale;
							parsePathPostData(data, width, height, previewScale, bitrate);

                            stream = std::dynamic_pointer_cast<StreamGeneratorIface>(
                                std::make_shared<ViewGenerator>(id, m_inputSource,
                                    width, height, previewScale, m_fps, bitrate));

							LOG_D("Starting stream: %s %dx%d (%.2fMbit/s, preview=%f)",
									id.data(), width, height, bitrate/1000.0f, previewScale);

							pthread_mutex_lock(&m_streamLock);
							m_streams[id] = stream;
							pthread_mutex_unlock(&m_streamLock);
						}

						PostReaderData content;
						content = data["path"];
						if(content.p != 0) {
							std::string contentData(content.p, content.l);
							stream->applyCommandData(contentData);
						} else {
							LOG_W("Received empty path-post");
						}
					}
				}
			);

			if((*reader)()) {
				bool sent = false;
				auto headers = request.headers;
				for(auto &it : headers){
					if( ! it.name.compare("Expect")){
						conn->set_status((server::connection::status_t)100);
						std::vector<server::response_header> cheaders = {{ "Connection", "close" }};
						conn->set_headers(boost::make_iterator_range(cheaders.begin(), cheaders.end()));
						sent = true;
						break;
					}
				}
				if(!sent) simpleResponse(conn, server::connection::ok);
			} else {
				simplePage(conn, server::connection::ok, "Error");
			}
		}

		void websocket(server::request const &request,server::connection_ptr conn,
				boost::smatch &regexResult){
			
			if(regexResult.size() <= 1) return;

			struct WebsocketData {
				double pingSentAt;
				double pingRecvAt;
				double latency;
				std::string id;
			};
			auto wsData = boost::make_shared<WebsocketData>();
			wsData->id = regexResult[1];

			auto ws = boost::make_shared<Websocket<server>>(request, conn,
                [this, wsData](Websocket<server> *ws, std::string & msg) {
                timeval tv;
                gettimeofday(&tv, NULL);

                if(msg == "PING") {
                    wsData->pingSentAt = tv.tv_sec + tv.tv_usec / double(1000000);

                    ws->send("PING");
                }else if(msg == "PONG") {
                    wsData->pingRecvAt = tv.tv_sec + tv.tv_usec / double(1000000);
                    wsData->latency = wsData->pingRecvAt - wsData->pingSentAt;

                    double playTime;
                    auto stream = lookup<StreamGenerator>(wsData->id);

                    if(stream){
                        playTime = stream->getCurrentTime();
                        std::stringstream ss;
                        ss << "F " << playTime;
                        ss << " L " << (wsData->latency * 1000);
                        ws->send(ss.str());
                    }
                }else if(msg.size() > 0) {

                    auto stream = lookup(wsData->id);

                    //Try to parse it as commands
                    if(stream) stream->applyCommandData(msg);
                }

                return true;
			});

			LOG_D("Starting websocket: %s", regexResult[0].str().c_str());
			if (!ws->run())
				simplePage(conn, server::connection::not_found, "Websocket failed");
		}

		void listStreams(server::request const &request, server::connection_ptr conn,
				boost::smatch &regexResult) {

			std::ostringstream ss;
			pthread_mutex_lock(&m_streamLock);
			for (auto &it : m_streams) {
				StreamStats stats = it.second->getStats();
				ss << stats.id << " " << stats.width << " " << stats.height << " ";
				ss << stats.fps << " " << (long) stats.bitrate << " " << stats.viewers << "\n";
			}
			pthread_mutex_unlock(&m_streamLock);

			simplePage(conn, server::connection::ok, ss.str());
		}

		void watch(server::request const &request, server::connection_ptr conn,
				boost::smatch &regexResult) {

			std::ostringstream ss;
			std::ifstream f("html/watch_index.html");
			copy(std::istreambuf_iterator<char>(f),
					std::istreambuf_iterator<char>(),
					std::ostreambuf_iterator<char>(ss));

			simplePage(conn, server::connection::ok, ss.str());
		}

		void attach(server::request const &request, server::connection_ptr conn,
				boost::smatch &regexResult) {

			std::vector<server::response_header> cheaders = {
				{ "Connection", "close" }, { "Content-Type", "video/webm" },
			};

			std::string id = regexResult[1];

			//Test if the stream already exists. If so, connect a slave
            auto s = lookup(id);

			if(s){
				conn->set_status(server::connection::ok);
				conn->set_headers(boost::make_iterator_range(cheaders.begin(), cheaders.end()));
				auto ns = std::make_shared<SlaveStreamGenerator>(s.get(), SLAVES_ALLOW_SKIPS);
				makeGeneratorStream<EQGeneratorStreamConfig::NO_HTTP_CHUNKING>(conn, ns)->run();
				LOG_D("Attaching slave to %s", id.data());
			}

			if(!s){
				simplePage(conn, server::connection::not_found, "Stream does not exist");
			}
		}

		void preview(server::request const &request, server::connection_ptr conn,
				boost::smatch &regexResult) {
			std::vector<server::response_header> cheaders = {
				{ "Connection", "close" }, { "Content-Type", "video/webm" },
			};

			std::string id = regexResult[1];

            auto stream = lookup(id);

			if(!stream){
				simplePage(conn, server::connection::not_found, "404, stream does not exist");
				return;
			}

			auto s = stream->getPreview();

			conn->set_status(server::connection::ok);
			conn->set_headers(boost::make_iterator_range(cheaders.begin(), cheaders.end()));

			makeGeneratorStream<EQGeneratorStreamConfig::NO_HTTP_CHUNKING>(conn, s)->run();
		}

		void stream(server::request const &request, server::connection_ptr conn,
				boost::smatch &regexResult) {

			if(regexResult.size() <= 1) return;

			std::vector<server::response_header> cheaders = {
				{ "Connection", "close" }, { "Content-Type", "video/webm" },
			};

			std::string id = regexResult[1];

			{

                auto s = lookup(id);
				if(s){
					simplePage(conn, server::connection::not_found, "Stream already exists");
					return;
				}
			}

			conn->set_status(server::connection::ok);
			conn->set_headers(boost::make_iterator_range(cheaders.begin(), cheaders.end()));

			unsigned int width, height, bitrate, previewScale;
			//defaults
			width = outWidthDefault;
			height = outHeightDefault;
			bitrate = bitrateDefault;
			previewScale = previewScaleDefault;

			//Parse additional options
			if(regexResult.size() > 1){
				std::string str = regexResult[2].str();
				if(str[0] == '?') str.erase(0, 1);
				std::vector<std::string> results;
				size_t pos = 0;
				std::string token;
				while ((pos = str.find("?")) != std::string::npos) {
					token = str.substr(0, pos);
					results.push_back(token);
					str.erase(0, pos + 1);
				}
				results.push_back(str);
				for(unsigned int i = 0; i<results.size(); ++i){
					boost::regex streamOptionRegex("(width|height|rate|preview)=(\\d+)");
					boost::smatch what;
					if(boost::regex_match(results[i], what, streamOptionRegex)){
						try {
							if(what[1] == "width")
								width = boost::lexical_cast<unsigned int>(what[2]);
							else if(what[1] == "height")
								height = boost::lexical_cast<unsigned int>(what[2]);
							else if(what[1] == "rate")
								bitrate = boost::lexical_cast<unsigned int>(what[2]);
							else if(what[1] == "preview")
								previewScale = boost::lexical_cast<unsigned int>(what[2]);
						} catch( boost::bad_lexical_cast const& ) {
							LOG_W("Caught lexical-cast exception when parsing %s as uint.",
									what[2].str().c_str());
						}
					}else{
						LOG_D("Unknown regex-string %s", results[i].c_str());
					}
				}

			}

			//Sanitize input
			if(width > 3440) width = 3440;
			if(height > 1700) height = 1700;
			if(previewScale > 100) previewScale = 100;
			if(bitrate > 40000) bitrate = 40000;

			width = (width+31)&~31; //Pad to 32byte alignment
			height = (height+7)&~7; //Pad to 8byte alignment

			//If the bitrate is high enough, we may mux at a higher fps to force chrome into
			//showing the frames immidiately. This gives a lot lower latency, but may look
			//a bit choppy. Looks worse the lower the bitrate, or greater variation in bitrate.
			unsigned int muxFps = this->m_fps;
			if(STREAM_ENABLE_FAST_MUXING && bitrate >= 6000) muxFps *= 5;

			auto s = std::make_shared<StreamGenerator>(id, m_inputSource, width, height,
					previewScale / 100.0,
					boost::bind(&HTTPHandler::terminateStream,this,_1),
					this->m_fps,muxFps,bitrate);

			pthread_mutex_lock(&m_streamLock);
			m_streams[id] = std::dynamic_pointer_cast<StreamGeneratorIface>(s);
			pthread_mutex_unlock(&m_streamLock);
			
			makeGeneratorStream<EQGeneratorStreamConfig::NO_HTTP_CHUNKING>(conn, s)->run();
		}
};

int main(int argc, char ** argv){


	if(argc < 2){
		LOG_E("Incorrect number of arguments. give me ffmpegs input-pipe!");
		return -1;
	}
	int ff_fd = open(argv[1], O_WRONLY);
	if(ff_fd < 0){
		LOG_E("Failed to open pipe for writing: %s", argv[1]);
		exit(EXIT_FAILURE);
	}

	//Ignore broken pipes
	signal(SIGPIPE,SIG_IGN);   

	std::stringstream sport;
	if(argc > 2) sport << argv[2];
	else sport << port;
	
	std::string loopDir;
	//TODO Get a fucking cmdline parser added...
	if(argc > 3)  loopDir = std::string(argv[3]);
	else if(PANORAMA_WIDTH == 4096 && PANORAMA_HEIGHT == 1680) loopDir = "defaultvid/";

	int fd = STDIN_FILENO;
	int fps = PANORAMA_FPS;
	ViewInput inputstream = ViewInput(PANORAMA_WIDTH,PANORAMA_HEIGHT);

	HTTPHandler handler = HTTPHandler(&inputstream, fps, ff_fd, loopDir);
	http::async_server<HTTPHandler>::options options(handler);

	if(argc > 4){
		fd = open(argv[4], O_RDONLY);
		if(fd < 0){
			LOG_E("Failed to open pipe: %s", argv[3]);
			exit(EXIT_FAILURE);
		}
	}

	options.address("0.0.0.0").port(sport.str())
		.io_service(boost::make_shared<boost::asio::io_service>())
		.thread_pool(boost::make_shared<boost::network::utils::thread_pool>(20))
		.reuse_address(true);

	http::async_server<HTTPHandler> instance(options);
	boost::thread ht([&]() { instance.run(); });

	LOG_D("Server up and running...");

	inputstream.runReader(fd);

	exit(EXIT_SUCCESS);
}
