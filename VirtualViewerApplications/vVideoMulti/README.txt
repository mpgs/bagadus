# Author: Ragnar Langseth
# Copyright University of Oslo Norway.
# 	and
# Simula Research laboratory Norway AS
#	from Jun 2014 and onwards

#DISCLAIMER
Fair warning, this was written by multiple people,
with different coding styles and comfortableness with c++.
Therefore, there is an ugly mix of c++ style inlined .hpp files,
more standard c-style header files and something in between.

Compilation requirements listed in Makefile.
Additionally, it may be useful to have these programs to provide input to the system:

ffmpeg, built with libx264 and gpl (e.g., ./configure --enable-shared --enable-pic --enable-libx264 --enable-gpl)

curl, for uploading video files.

Chromium are the only tested browsers.

METHOD 1:
Run a network client, which accepts POSTs containing h264 inputvideo (static resolution) and stream GET requests.
Clients can connect to this server IP (port 8089), chrome only, and watch/control video:

ffmpeg -loglevel quiet -f h264 -re -i - -f rawvideo -pix_fmt yuv422p - < dataPipe | ./multicastStreamingServer dataPipe 8089 path/to/video/source

This will loop through all '*.h264' files in 'path/to/video/source' alphabetically.
(Without this option, it will either hang or loop 'defaultvid/' if it exists.)

Alternatively, (or at the same time), you may feed input video from a different machine / terminal, using curl:

for f in /expData/*.h264; do curl -F "file=@$f" -L http://127.0.0.1:8089/video ; sleep 1; done


You may then start a new unique stream with http://127.0.0.1:8089 or view any of the current active streams with http://127.0.0.1:8089/watch


To use the Bagadus pipeline as input directly, you may run the pipeline as such, replacing localhost:port with your webserver:

Distributed Version: bin/BagadusProcessor -f 30 -c >(while read f; do curl -F "file=@$f" -L http://127.0.0.1:8089/video; done)
Singlemachine Version: bin/SingleMachine -f 30 -p >(while read f; do curl -F "file=@$f" -L http://127.0.0.1:8089/video; done)

NB!!! Make sure that your input video resolution is correct, relating to defines in 'inc/panoDefines.h'.
Recompile if updated.

Browser accepts multiple inputs at the same time:
mouse-movement & left/right click for zoom
Keys: move = w,a,s,d  zoom = q,e or 1,2,3,4
Gamepad: move with left stick, zoom with y-axis of right stick

METHOD 2:
Run locally, reading input at real-time speed from some directory,
control the stream using one pipe and direct output into vlc through another pipe

ffmpeg -loglevel quiet -f concat -re -i <(for f in /expData/*.h264; do echo file $f ; done) -f rawvideo -pix_fmt yuv422p - | ./localMultiView <(../virtualVideo/build/mouseMove 1024 640) >( vlc --file-caching=67 --demux=h264 --h264-fps 30 - -q)

or you may start X streams at once by appending more input/output: "<(../virtualVideo/build/mouseMove 1024 640) >( vlc --file-caching=67 --demux=h264 --h264-fps 30 - -q)"

