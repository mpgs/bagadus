// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#pragma once
#include "utils.h"
#include <time.h>
#include <queue>
#include <deque>
#include <string>

/* ***********************************
 * Command parsing syntax / semantics:
 * ***********************************
 * A command consists of an operand, operator, value and an optional arrayIndex.
 * A list of commands is separated by a delimiter.
 * The values are normalized between 0 and 100 (see the code .cpp for this mapping)
 * All (non-delimiter) whitespace characters are ignored when parsing.
 * 
 * SIMPLE EXAMPLES:
 * x = 5.5;
 * x=25;y+75; f =50
 * x > 1; y < 3.14159; f > .14159
 * f<1;x-5;y<6;
 *
 * ARRAY INDICES:
 * Commands may also include an array index, which determines when the command is executed.
 * F ex, this will queue a command to be executed on the 100th frame processed and the 110th:
 * f[100] = 20; f[110] < 1;
 * This way, you may preload a long video sequence and create complex motions with few commands.
 * Commands currently must come in the correct order, and commands without an array index will
 * be delayed by previous commands (with indices).
 * If an array index is too low (frame already processed) it will be applied immidiately.
 * F ex:
 * x[10] = 5; y = 50; f = 50; //all executed on frame 10.
 * x[20] = 5; y[10] = 50; f = 50; //all executed on frame 20.
 *
 * VARIABLE MOTION OPERATOR:
 * '<' and '>' will apply that value to all frames, as a constant motion.
 * F ex, instead of writing this:
 * x[10]+1; x[11]+1; x[12]+1; ..... x[24]+1;
 * you may create the exact same result with this:
 * x[10]>1; x[25]>0;
 * Note that this motion must be set to 0 to stop, or it will keep happening untill it reaches
 * the boundry. The motion is automatically set to 0 if the camera goes outside [0..100] range.
 * It is very practical for button-down, button-up style of
 * input, or as a way to create smooth motions with few commands.
 */
const std::string CONTROL_COMMAND_DELIMITERS = ";\n\r";
enum Operand {
    VAR_TX, // x (horizontal axis, leftmost = 0, rightmost = 100)
    VAR_TY, // y (vertical axis, top = 0)
    VAR_FO, // f (zoom, zoomed all the way out = 0)
    VAR_IX  // i (modify the frame index counter. NB! Only OP_SET allowed)
};
enum Operator {
    OP_SET,      // = (assign the variable directly with an absolute value)
    OP_INC,      // + (increase the variable, think '+=')
    OP_DEC,      // - (decrease, '-=')
    OP_STEP_INC, // > (assign a positive motion to the variable for every frame. See above)
    OP_STEP_DEC  // < (negative motion, see above)
};

struct Command {
    Command(Operand var, Operator op, double value, long framenum) :
        var(var),op(op),value(value),framenum(framenum) { }

    Operand var;
    Operator op;
    double value;
    long framenum;
};

struct Coordinate {
	Coordinate() : x(50.0),y(50.0),f(50.0) { }
	Coordinate(double dx, double dy, double df) : x(dx),y(dy),f(df) { }
	double x, y, f;
};

class ViewState {
	public:
		/* These two functions contain a lot of values / constants that are tweaked
		 * from alfheim deployment. All dependencies to alfheim pano should be isolated here.
		 * Alternative panoramas should work, but may not have optimal normalization mapping */
		ViewState(double fov, int width, int height);
		/* Fill the four output variables with correct values */
		void getState(double &tx, double &ty, double &tz, double &tf);
		/* Convert a set of normalized coordinates to viewer-input */
		void setState(const Coordinate &pos);
	private:
		double m_x, m_y, m_f;
		double m_stepX, m_stepY, m_stepF;
		double m_minX, m_minY, m_minF;
};

class ViewControlIface {
	public:
		virtual ~ViewControlIface() { }
		virtual void updateMatrix(long frameNum, double &focal,
				double &theta_x, double &theta_y, double &theta_z) = 0;
};

class ViewControl : public ViewControlIface{
	public:
		ViewControl(double fieldOfView, int inWidth, int inHeight, bool absCoords=false)
			: m_absCoords(absCoords), m_state(ViewState(fieldOfView, inWidth, inHeight)),
			m_pos( (absCoords ? Coordinate(0,0,1000) : Coordinate(50,30,50)) ),
			m_movement(Coordinate(0,0,0)), m_lastFrameReset(0)
		{
			if(!absCoords) m_state.setState(m_pos);
			pthread_mutex_init(&m_queueLock, NULL);
		}

		/* Called by viewer before a new frame gets projected.
		 * Should only contain fairly quick operations.
		 * */
		void updateMatrix(long frameNum, double &focal,
			double &theta_x, double &theta_y, double &theta_z);

		/* Parse a sequence of commands. Parses 'len' characters, or untill '\0'.
		 * If parts of the input cannot be parsed (incorrect syntax/semantics), it will ignore
		 * everything untill the next delimiter.
		 * String will be temporarily mutated, but restored to original state before returning.
		 * */
		void parseCommands(char * str, int len);

		/* Fills two variables with the last processed frame, and its CLOCK_MONOTONIC timestamp */
		void getLastFrameTime(long &fnum, struct timespec &time){
			time = m_lastFrameTimestamp;
			fnum = m_lastFrameNum;
		}

	protected:
		bool m_absCoords;
		ViewState m_state;

		Coordinate m_pos;
		Coordinate m_movement;
		long m_lastFrameReset;

		pthread_mutex_t m_queueLock;

        std::deque<Command> m_commandQueue;

		//Some debug variables
		struct timespec m_lastFrameTimestamp;
		long m_lastFrameNum;

		void parse(char *str, int ln);

		void parseSetOp(Operand var, double value, long frameNum);
		void parseIncOp(Operand var, double value);
		void parseStepOp(Operand var, double value);

		void updateSteps();

};

class ViewNetworkControl : public ViewControl {
	public:
		ViewNetworkControl(int inWidth, int inHeight, double fov, bool absCoords=false) :
            ViewControl(fov, inWidth, inHeight, absCoords), m_curFrame(0) { }

		void updateMatrix(long frameNum, double &focal, double &theta_x,
				double &theta_y, double &theta_z);
		long getCurFrame() { return m_curFrame; }
	private:
		long m_curFrame;
};

/* This class is just an extension to ViewControl, where one thread is reading input
 * commands from a pipe, untill terminated. */
class ThreadedViewControl : public ViewControl {
	public:
		ThreadedViewControl(double fov, int width, int height, int pipe,
				void (* terminateCallback)(void*) = NULL, void* terminateCbParam = NULL)
			:
				ViewControl(fov, width, height), m_pipe(pipe), m_terminate(false),
				m_terminateCallback(terminateCallback), m_terminateCbParam(terminateCbParam)
	{
		pthread_create(&m_controlThread, NULL, &runControllerThread, this);
	}
		void terminate();
		
	private:
		int m_pipe;
		bool m_terminate;
        pthread_t m_controlThread;

		void (* m_terminateCallback)(void*);
		void * m_terminateCbParam;

		void runController();
		static void * runControllerThread(void *p);
};

