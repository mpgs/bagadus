// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#pragma once

#include <panoDefines.h>
#include <outStream.h>
#include <viewInput.h>
#include <viewEnc.h>
#include <viewer.h>
#include <viewControl.h>
#include <utils.h>
#include <streamGenerator.h>

#include <cstdlib>
#include <string>
#include <atomic>
#include <deque>
#include <pthread.h>
#include <boost/function.hpp>

/* Class responsible for generating and controlling a video stream,
 * but does not receive the actual output. Several output sources can 'subscribe' to output */
class ViewGenerator : public StreamGeneratorIface {
	public:
		ViewGenerator(std::string id,
				ViewInput * inputSource,
				unsigned int outWidth,
				unsigned int outHeight,
				float previewScale,
				unsigned int fps,
				unsigned int targetBitrate, //Kbit/s
				boost::function<void(std::string)> terminateCb = NULL,
				unsigned int inWidth = PANORAMA_WIDTH,
				unsigned int inHeight = PANORAMA_HEIGHT,
				double fov = PANORAMA_FOV,
				double scale = PANORAMA_SCALE )
			:
				m_id(id), m_devId(inputSource->getLeastUtilizedDevice()),
				m_inputSrc(inputSource), m_enc(ViewEnc()),
				//WARN liveMode determines if frames are skipped if clients are too slow.
				//If false, every frame is guaranteed received, but not necessaily real-time
				m_viewer(Viewer(&m_enc, m_devId, STREAM_LIVE_MODE, inWidth, inHeight, fov,scale,previewScale)),
				m_controller(ViewControl(fov, inWidth, inHeight, STREAM_ABSOLUTE_COORDS)),
                m_stats(id, fps, outWidth, outHeight, targetBitrate),
				m_inWidth(inWidth), m_inHeight(inHeight),
                m_terminateCallback(terminateCb), m_previewStream(NULL), m_numSlaves(0)
		{
			pthread_mutex_init(&m_previewLock, NULL);
			//1 keyframe / sec
			m_enc.init(fps, outWidth, outHeight, m_devId, targetBitrate*1000, fps*STREAM_KEYFRAME_INTERVAL);
			m_inputSrc->attachViewer(&m_viewer, m_devId);
			m_viewer.attachControlStream(&m_controller);

			if(STREAM_CREATE_JPEGS){
				m_viewer.setJpegConfig(fps, 320, 136); 
			}
		}

		~ViewGenerator();

		std::shared_ptr<SlaveStreamGenerator> getPreview(); 

		StreamStats getStats(){
			m_stats.bitrate = m_enc.getCurrentBitrate();
			m_stats.viewers = m_numSlaves;
            return m_stats;
        }

        bool getJpeg(std::vector<unsigned char> &output){
            return m_viewer.getJpeg(output);
        }

		//Register an object to be pushed output h264 encoded data
		void addSlave(OutStreamIface * stream) {
			m_numSlaves++;
			m_enc.attachOutputStream(stream);
			m_enc.forceKeyframe();
		}

		void removeSlave(OutStreamIface * stream) {
			m_numSlaves--;
			m_enc.detatchOutputStream(stream);
		}

		void requestKeyframe(){
            m_enc.forceKeyframe();
        }

		void applyCommandData(std::string cmds){
			m_controller.parseCommands(&cmds[0], cmds.size());
		}

	private:
		std::string m_id;
		int m_devId;
		
		ViewInput * m_inputSrc;
		ViewEnc m_enc;
		Viewer m_viewer;
		ViewControl m_controller;
		StreamStats m_stats;
		unsigned int m_inWidth, m_inHeight;

		boost::function<void(std::string)> m_terminateCallback;

		std::deque<size_t> m_prevFrameSizes;
		size_t m_accumulatedRate;

		pthread_mutex_t m_previewLock;
		PreviewStreamGenerator * m_previewStream;

		std::atomic<int> m_numSlaves;
};

