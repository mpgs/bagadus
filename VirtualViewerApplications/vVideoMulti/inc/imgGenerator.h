// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#pragma once
#include "utils.h"

#include <pthread.h>
#include <cuda_runtime_api.h>

class ImgGenerator {
	private:
		enum ImgGenState{
			IMG_GEN_NONE,
			IMG_GEN_NEW,
			IMG_GEN_OLD
		};
		int m_devId;
		std::vector<unsigned char> m_lastGenerated;
		ImgGenState m_state;
		uint8_t * m_cpuBuffer, * m_gpuBuffer;
		uint32_t m_lastWidth, m_lastHeight;
		cudaStream_t m_cudaStream;
		pthread_mutex_t m_lock;

	public:
		ImgGenerator(int devId) :
			m_devId(devId), m_state(IMG_GEN_NONE), m_cpuBuffer(NULL), m_gpuBuffer(NULL)
	{
		pthread_mutex_init(&m_lock, NULL);
		cudaSafe(cudaSetDevice(m_devId));
		cudaSafe(cudaStreamCreate(&m_cudaStream));
	}
		~ImgGenerator(){
			//Ensure that no copy is ongoing
			cudaSafe(cudaStreamSynchronize(m_cudaStream));
			cudaSafe(cudaSetDevice(m_devId));
			cudaSafe(cudaStreamDestroy(m_cudaStream));
			if(m_cpuBuffer) cudaSafe(cudaFreeHost(m_cpuBuffer));
			if(m_gpuBuffer) cudaSafe(cudaFree(m_gpuBuffer));
		}

		/* Push a raw video frame, in gpu memory. Pixelformat expected: BGR (packed)
		 * Stride, however, is in bytes. So, normally, 3*width
		 * */
		void pushRaw(const uint8_t *gpuImgData, uint32_t width, uint32_t height, uint32_t stride);

		/* Fetch the previous pushed image, in jpeg format. Output array is resized to fit,
		 * and contains a memcopied set of data. Function varies in time/complexity. First call,
		 * after pushing another frame, will take significantly longer, since it needs to convert
		 * from raw to jpeg format. Subsequent calls is only a memcpy operation.
		 * */
		bool getLastJpeg(std::vector<unsigned char> &output);
};
