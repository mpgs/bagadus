// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#ifndef PANO_DEFINES
#define PANO_DEFINES

#define PANORAMA_FPS 30

#if 1
//As of writing this, these values work OK for Alfheim

#define PANORAMA_WIDTH 4096
#define PANORAMA_HEIGHT 1680
#define PANORAMA_FOV 2.7 //Horizontal field of view of input panorama
#define PANORAMA_SCALE 1.75 //Vertical pixel scaling, rotated cameras=~1.75, regular=1.0
#define CONTROL_AUTO_ZOOM_ENABLED //Automatically zooms based on x/y coordinates (distace to src cameras)

#define PANORAMA_THETA_Z(ty) (-0.32 * (ty - 0.027)) //Z-rotation based on horizontal camera position

#define VIEW_ZOOM_CONSTANT 4.025
#define VIEW_MIN_X_CONSTANT -0.88
#define VIEW_MAX_X_CONSTANT 0.87
#define VIEW_MIN_Y_CONSTANT -0.6
#define VIEW_MAX_Y_CONSTANT 0.45

#elif 0

//As of writing this, these values work OK for Ullevaal

#define PANORAMA_WIDTH 4096
#define PANORAMA_HEIGHT 1680
#define PANORAMA_FOV 2.5 //Horizontal field of view of input panorama
#define PANORAMA_SCALE 1.75 //Vertical pixel scaling, rotated cameras=~1.75, regular=1.0

#define PANORAMA_THETA_Z(ty) (-0.12 * (ty - 0.027)) //Z-rotation based on horizontal camera position

//These somewhat arbitrary constants define the max/min camera values, to give a comfortable range
//These are just empirically found. Correlate to ViewState constructor in ViewControl.cpp
//These can be a bit off, and it will still look ok.
#define VIEW_ZOOM_CONSTANT 5.025
#define VIEW_MIN_X_CONSTANT -0.88
#define VIEW_MAX_X_CONSTANT 0.87
#define VIEW_MIN_Y_CONSTANT -0.6
#define VIEW_MAX_Y_CONSTANT 0.45

#else

//As of writing this, these values work OK for the 3-cam demo

#define PANORAMA_WIDTH 2848
#define PANORAMA_HEIGHT 1864
#define PANORAMA_FOV 2.0 //Horizontal field of view of input panorama
#define PANORAMA_SCALE 1.0 //Vertical pixel scaling, rotated cameras=~1.75, regular=1.0

#define PANORAMA_THETA_Z(ty) (0) //Z-rotation based on horizontal camera position

//TODO These are badly optimized...
#define VIEW_ZOOM_CONSTANT 4.025
#define VIEW_MIN_X_CONSTANT -0.88
#define VIEW_MAX_X_CONSTANT 0.87
#define VIEW_MIN_Y_CONSTANT -0.6
#define VIEW_MAX_Y_CONSTANT 0.45

#endif

#define STREAM_ABSOLUTE_COORDS false //If true, the control values use the direct theta_x/y/z & focal
//from Vamsi's virtual viewer. If false, command values are expected to be normalized to the 0..100% range.

#define STREAM_LIVE_MODE true //Live: If one output / stream is running too slow, skip frames to stay real-time

#define STREAM_CREATE_JPEGS true //Generate Jpegs every second, to allow webserver to provide this.
//Costs a few microsec per stream, once every second. The majority of processing, however, is not done if
//no one actually requests the images.

#define STREAM_KEYFRAME_INTERVAL 5 //sek. Can be 0, if so keyframes are only sent when a new client connects
//Note that the smoothest playback and lowest bitrate is typically achieved with no unnecessary keyframes.

#define SLAVES_ALLOW_SKIPS true //If a slave-client is too slow and loses a frame, the stream will appear
//"broken" untill the next keyframe. AllowSkips: client stream breaking is ok, tough luck!
//DisallowSkips: if skip occurs, client will request a new keyframe. This may be highly problematic if a client
//is consistently too slow (which is likely), as keyframes will be forced continuously and stream bitrate will
//increase exponentially for EVERYONE (making said client probably lose even more frames).
//If the keyframe interval is low, recommended to set this to true. However, if it's designed for a single client
//it should maybe be true.


/////////////// These apply only for streamGenerator, private/owned streams
#define STREAM_ENABLE_BLOATED_MUXING true //Asking for X kbps, bloat stream to X even when lower.
#define STREAM_ENABLE_FAST_MUXING true //Mux MKV at 5x frame rate, forcing browser to show frame immidiately
//This is ignored if the target bitrate is below a threshold. If the bitrate is too low and inconsistent,
//fast muxing will look choppy
///////////////

#endif //ifndef
