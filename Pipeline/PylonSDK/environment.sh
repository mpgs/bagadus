
export LC_ALL="en_US.UTF-8"
export PYLON_ROOT=/opt/pylon4
export GENICAM_ROOT_V2_3=${PYLON_ROOT}/genicam
mkdir -p $HOME/genicam_xml_cache_p4
export GENICAM_CACHE_V2_3=$HOME/genicam_xml_cache_p4
export LD_LIBRARY_PATH=${PYLON_ROOT}/lib64:${GENICAM_ROOT_V2_3}/bin/Linux64_x64:${GENICAM_ROOT_V2_3}/bin/Linux64_x64/GenApi/Generic:$LD_LIBRARY_PATH
