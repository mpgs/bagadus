// AUTHOR(s): Sigurd Ljodal,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <stdlib.h>
#include <string.h>

#include "client.h"
#include "logging.h"

#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <event2/dns.h>
#include <sys/socket.h>

// Callback structure
typedef struct callback
{
    client_cb callback;
    void *userdata;
    enum pkg_type type;

    struct callback *next;
    struct callback *prev;
} callback_t;

// Client structure
struct client {
    struct bufferevent *bev;
    callback_t *callbacks;
};

// Client callback
void client_read(struct bufferevent *bev, void *ptr);
void client_event(struct bufferevent *bev, short events, void *ctx);

/****************************************
 *
 * Client
 *
 ****************************************/
client_t *client_init(struct event_base *base, const char *hostname,
        uint16_t port)
{
    // Allocate memory for a new client
    client_t *c = malloc(sizeof(client_t));
    if (!c) {
        LOG_E("Unable to allocate memory for client.");
        return NULL;
    }

    // Initialize
    c->callbacks = NULL;

    // Initialize the DNS serivce
    struct evdns_base *dns_base = evdns_base_new(base, 1);

    // Initialize the buffer event
    c->bev = bufferevent_socket_new(base, -1, BEV_OPT_CLOSE_ON_FREE);
    bufferevent_setcb(c->bev, client_read, NULL, client_event, c);
    bufferevent_enable(c->bev, EV_READ|EV_WRITE);

    // Connect to the remote host
    bufferevent_socket_connect_hostname(c->bev, dns_base, AF_UNSPEC, hostname,
            port);

    return c;
}

void client_write(client_t *c, void *data, size_t len)
{
    bufferevent_write(c->bev, data, len);
}

int client_register_cb(client_t *c, enum pkg_type type, client_cb callback,
        void *userdata)
{
    // Allocate memory
    callback_t *cb = malloc(sizeof(callback_t));
    if (!cb) {
        LOG_E("Unable to allocate memory for callback struct.");
        return 0;
    }

    // Initialize
    cb->callback = callback;
    cb->userdata = userdata;
    cb->type = type;
    cb->prev = NULL;

    // Add to callbacks list
    if (!c->callbacks) {
        c->callbacks = cb;
        cb->next = NULL;
    } else {
        cb->next = c->callbacks;
        cb->next->prev = cb;
        c->callbacks = cb;
    }

    return 1;
}

void client_free(client_t *c)
{
    // Free the buffer event
    bufferevent_free(c->bev);

    // Free the callback struct
    callback_t *cb = c->callbacks;
    while (cb) {
        callback_t *tmp = cb->next;
        free(cb);
        cb = tmp;
    }

    // Free the client struct
    free(c);
}

/****************************************
 *
 * Callbacks
 *
 ****************************************/
void client_read(struct bufferevent *bev, void *ctx)
{
    // Get the buffer
    struct evbuffer *buf = bufferevent_get_input(bev);

    // Get the client
    client_t *c = ctx;

    // Loop in case more than one package is ready
    for(;;) {
        // Check if there is a header in the buffer
        size_t avail = evbuffer_get_length(buf);
        if (avail < sizeof(struct pkg_header)) return;

        // Get the header
        struct pkg_header header;
        evbuffer_copyout(buf, &header, sizeof(struct pkg_header));

        // Entire package not delivered yet
        if (avail < header.size) return;

        // Read out the actual package
        char pkg[header.size];
        evbuffer_remove(buf, pkg, header.size);

        callback_t *cb = c->callbacks;
        for(;cb;cb = cb->next) {
            if (cb->type == header.type)
                cb->callback((struct pkg_header *)pkg, cb->userdata);
        }
    }
}

void client_event(struct bufferevent *bev, short events, void *ctx)
{
    if (events & BEV_EVENT_CONNECTED) {
        LOG_I("Connected.");
    } else if (events & (BEV_EVENT_ERROR|BEV_EVENT_EOF)) {
        struct event_base *base = bufferevent_get_base(bev);
        if (events & BEV_EVENT_ERROR) {
            int err = bufferevent_socket_get_dns_error(bev);
            if (err)
                LOG_E("DNS failed: %s.", evutil_gai_strerror(err));
        }

        LOG_I("Disconnecting.");

        // Exit the event base loop
        event_base_loopbreak(bufferevent_get_base(bev));
    }
}
