// AUTHOR(s): Sigurd Ljodal,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#ifndef logging_h
#define logging_h

//
// ### Logging
//

//
// ** syslog **
//
// Use syslog for logging all messages.

#ifdef LOG_SYSLOG

#define LOG_E(M, ...)
#define LOG_W(M, ...)
#define LOG_I(M, ...)

#ifdef LOG_DEBUG
#define LOG_D(M, ...)
#else
#define LOG_D(M, ...)
#endif // DEBUG

//
// ** With filepath **
//
// Log to `stderr` and include the file path and line numder.

#elif LOG_FILEPATH

#define LOG_E(M, ...) fprintf(stderr, \
        "[\x1b[01;95mERROR\x1b[0m] " M " (%s:%d) " \
        "\n", ##__VA_ARGS__, __FILE__, __LINE__)

#define LOG_W(M, ...) fprintf(stderr, \
        "[\x1b[01;93mWARN \x1b[0m] " M " (%s:%d) " \
        "\n", ##__VA_ARGS__, __FILE__, __LINE__)

#define LOG_I(M, ...) fprintf(stderr, \
        "[\x1b[01;97mINFO \x1b[0m] " M " (%s:%d) " \
        "\n", ##__VA_ARGS__, __FILE__, __LINE__)

#ifdef LOG_DEBUG
#define LOG_D(M, ...) fprintf(stderr, \
        "[\x1b[01;96mDEBUG\x1b[0m] " M "\n", ##__VA_ARGS__)
#else
#define LOG_D(M, ...)
#endif // DEBUG

//
// ** Default, stderr **
//
// Use the default logging method. This logs to `stderr` and does not include
// file paths or line numbers.

#elif LOG_TIMESTAMP
#ifdef __cplusplus
extern "C" {
void logging_start_timer();
int logging_get_time();
}
#else
void logging_start_timer();
int logging_get_time();
#endif
#define LOG_E(M, ...) fprintf(stderr, \
        "[\x1b[01;95mE %4d\x1b[0m] " M "\n", logging_get_time(), ##__VA_ARGS__)

#define LOG_W(M, ...) fprintf(stderr, \
        "[\x1b[01;93mW %4d\x1b[0m] " M "\n", logging_get_time(), ##__VA_ARGS__)

#define LOG_I(M, ...) fprintf(stderr, \
        "[\x1b[01;97mI %4d\x1b[0m] " M "\n", logging_get_time(), ##__VA_ARGS__)

#ifdef LOG_DEBUG
#define LOG_D(M, ...) fprintf(stderr, \
        "[\x1b[01;96mD %4d\x1b[0m] " M "\n", logging_get_time(), ##__VA_ARGS__)
#else
#define LOG_D(M, ...)
#endif // DEBUG

#else

#define LOG_E(M, ...) fprintf(stderr, \
        "[\x1b[01;95mERROR\x1b[0m] " M "\n", ##__VA_ARGS__)

#define LOG_W(M, ...) fprintf(stderr, \
        "[\x1b[01;93mWARN \x1b[0m] " M "\n", ##__VA_ARGS__)

#define LOG_I(M, ...) fprintf(stderr, \
        "[\x1b[01;97mINFO \x1b[0m] " M "\n", ##__VA_ARGS__)

#ifdef LOG_DEBUG
#define LOG_D(M, ...) fprintf(stderr, \
        "[\x1b[01;96mDEBUG\x1b[0m] " M "\n", ##__VA_ARGS__)
#else
#define LOG_D(M, ...)
#endif // DEBUG

#endif // Log type

//
// ** Dolphin helpers **
//
// Helpers for handling errors from the SISCI API.

#define SCIErrorGoto(ERR, FUNC, GOTO) if (ERR != SCI_ERR_OK) { \
        LOG_E(FUNC" failed - Error code 0x%X", ERR); goto GOTO; }

#define SCIErrorWarn(ERR, FUNC) if (ERR != SCI_ERR_OK) { \
        LOG_W(FUNC" failed - Error code 0x%X", ERR); }

#endif // logging_h
