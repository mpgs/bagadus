// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 


#include "PylGigE.hpp"
#include "PylGigEBasicConfig.hpp"
#include "PylGigEHdrConfig.hpp"
#include "CustomBufferFactory.hpp"

#include <sys/socket.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>

using namespace PylonGigE;
using namespace Pylon;
using namespace Basler_GigECamera;
using namespace Basler_GigECameraParams;

/* ------------------- Constants --------------------------- */

const size_t maxSingleStreamFrameQueue = 2; //How many frames to keep in the output buffer
const int minimumBuffersPerCamera = 4; //Minimum buffers to be allocated with custom factory
const int timestampSyncInterval = 2*1000000;//Usec, syncing camera timestamps with system clock

/* ------------------- Global utility functions -------------------------- */

const void PylonGigE::initializePylon(){
	Pylon::PylonInitialize();
}

const void PylonGigE::terminatePylon(){
	Pylon::PylonTerminate();
}

const void PylonGigE::printAvailableDevices(){
	DeviceInfoList_t devs;
	CTlFactory &ctlFactory = CTlFactory::GetInstance();
	ctlFactory.EnumerateDevices(devs);

	if(devs.size() == 0){
		std::cout << "No devices found!\n";
		return;
	}
	std::cout << "\nListing all available devices:\n";
	for(DeviceInfoList_t::iterator it = devs.begin(); it != devs.end(); ++it){
		String_t devClass = it->GetDeviceClass();
		std::cout << "\nCamera  : " << devClass << " : " << it->GetModelName() << std::endl;
		if( devClass.compare(BaslerGigEDeviceClass) != 0 ){
			std::cout << "Unsupported class!\n";
			continue;
		}
		CBaslerGigEDeviceInfo * i = (CBaslerGigEDeviceInfo*) &(*it);
		std::cout << "MAC     : " << i->GetMacAddress() << std::endl;
		std::cout << "Serial  : " << i->GetSerialNumber() << std::endl;
		std::cout << "IP:port : " << i->GetAddress() << std::endl;
        int mtu;
        std::string ifaceName;
        getIfaceInfo(*i, mtu, ifaceName);
		std::cout << "Iface   : " << ifaceName << "\tMTU: " << mtu << std::endl;
	}
}

void PylonGigE::getIfaceInfo(CBaslerGigEDeviceInfo &info, int &mtu, std::string &name){
    //default
    mtu = 1500;
    name = "unknown";

    //Loop through all software interfaces untill we find one whose address matches the
    //interface registerred in the device info
    struct ifaddrs * ifAddrStruct=NULL;
    getifaddrs(&ifAddrStruct);
    for (struct ifaddrs* ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
        int fd = socket(AF_INET, SOCK_DGRAM, 0);
        struct ifreq ifr;
        ifr.ifr_addr.sa_family = AF_INET;
        strncpy(ifr.ifr_name, ifa->ifa_name, IFNAMSIZ-1);
        ioctl(fd, SIOCGIFADDR, &ifr);
        String_t ifaceIp = inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr);
        if(info.GetInterface().compare(ifaceIp) == 0){
            if (ioctl(fd, SIOCGIFMTU, (caddr_t)&ifr) == 0) {
                mtu = ifr.ifr_mtu;
            }
            name = ifr.ifr_name;
            close(fd);
            return;
        }
        close(fd);
    }
}

/* ------------------- Smaller class functions -------------------------- */

PylonFrame::PylonFrame(Pylon::CGrabResultPtr grab){
	grabResult = grab;
	pixels = (uint8_t*)grab->GetBuffer();
	size = grab->GetImageSize();
	frameCounter = GET_FRAME_COUNTER(grab);
}

PylonStream::PylonStream(){
	pthread_mutex_init(&_bufferMutex, NULL);
	pthread_cond_init(&_bufferCond, NULL);
	_terminate = false;
}

PylonStream::~PylonStream(){
	pthread_mutex_lock(&_bufferMutex);
	_terminate = true;
	//Delete any remaining frames
	while(_buffer.size() > 0){
		PylonFrame *del = _buffer.front();
		_buffer.pop_front();
		delete del;
	}
	pthread_mutex_unlock(&_bufferMutex);
	pthread_mutex_destroy(&_bufferMutex);
	pthread_cond_destroy(&_bufferCond);
}

PylonFrame * PylonStream::getFrame(){

	pthread_mutex_lock(&_bufferMutex);
	if(_terminate){
		pthread_mutex_unlock(&_bufferMutex);
		return NULL;
	}
	while(_buffer.size() == 0){
		pthread_cond_wait(&_bufferCond, &_bufferMutex);
		if(_terminate){
			pthread_mutex_unlock(&_bufferMutex);
			return NULL;
		}
	}
	PylonFrame * ret = _buffer.front();
	_buffer.pop_front();
	pthread_mutex_unlock(&_bufferMutex);
	return ret;
}

int PylonStream::poll(){
	pthread_mutex_lock(&_bufferMutex);
	int ret = (_terminate) ? 0 : _buffer.size();
	pthread_mutex_unlock(&_bufferMutex);
	return ret;
}

void PylonStream::terminateStream(){
	pthread_mutex_lock(&_bufferMutex);
	_terminate = true;
	pthread_cond_signal(&_bufferCond);
	pthread_mutex_unlock(&_bufferMutex);
}

void PylonStream::putFrame(PylonFrame *f){

	while(pthread_mutex_trylock(&_bufferMutex) == EBUSY)
		;//Trying not to context switch here. No one holds this lock for long

	if(_terminate){
		delete f;
		pthread_mutex_unlock(&_bufferMutex);
		return;
	}

	if(_buffer.size() >= maxSingleStreamFrameQueue){
		//Delete oldest element in queue if "full"
		PylonFrame *del = _buffer.front();
		_buffer.pop_front();
		delete del;
	}
	_buffer.push_back(f);
	pthread_cond_signal(&_bufferCond);
	pthread_mutex_unlock(&_bufferMutex);
}

/* ------------------- PylGigE functions -------------------------- */

bool PylGigE::open(std::vector<PylonStream*> &streams,
		const PylonConfig &configuration){

	if(_isOpen){
		LOG_E("Pylon already opened. Returning");
		return false;
	}

	pthread_mutex_init(&_configMutex, NULL);
	pthread_mutex_lock(&_configMutex);

	DeviceInfoList_t devs;
	CTlFactory &ctlFactory = CTlFactory::GetInstance();
	ctlFactory.EnumerateDevices(devs);

	if(devs.size() == 0){
		LOG_E("Unable to open devices, none found");
		pthread_mutex_unlock(&_configMutex);
		return false;
	}
	_cameras.Initialize(devs.size());

	for(size_t i=0; i<devs.size(); ++i){
		String_t devClass = devs[i].GetDeviceClass();
		if( devClass.compare(BaslerGigEDeviceClass) != 0 ) continue;
		try{
			//Code may throw if camera is open by another contex
			_cameras[i].Attach(ctlFactory.CreateDevice(devs[i]));
		}catch(GenICam::GenericException &ex){
			LOG_E("Failed to attach device(s): %s", ex.GetDescription());
			_cameras.DestroyDevice();
			pthread_mutex_unlock(&_configMutex);
			return false;
		}
		PylonStream * ps = new PylonStream();
		streams.push_back(ps);
		_streams.push_back(ps);
	}
	if(streams.size() == 0){
		LOG_E("Unable to open devices, no GigE devices found. %d other devices found.",
				(int)devs.size());
		pthread_mutex_unlock(&_configMutex);
		return false;
	}

	return init(configuration);
}

bool PylGigE::open(const std::vector<std::string> &macAddr, std::vector<PylonStream*> &streams,
		const PylonConfig &configuration){

	if(_isOpen){
		LOG_E("Pylon already opened. Returning");
		return false;
	}

	pthread_mutex_init(&_configMutex, NULL);
	pthread_mutex_lock(&_configMutex);

	DeviceInfoList_t devs;
	CTlFactory &ctlFactory = CTlFactory::GetInstance();
	ctlFactory.EnumerateDevices(devs);

	_cameras.Initialize(macAddr.size());
	for(size_t i=0; i<macAddr.size(); ++i){
		for(size_t j=i; j>0; --j){
			if(!macAddr[i].compare(macAddr[j-1])){
				LOG_E("Cannot open the device %s twice, two identical mac addresses",
						macAddr[i].c_str());
				_cameras.DestroyDevice();
				pthread_mutex_unlock(&_configMutex);
				return false;
			}
		}
		bool found = false;
		for(DeviceInfoList_t::iterator it = devs.begin(); it != devs.end(); ++it){
			String_t devClass = it->GetDeviceClass();
			if(std::string("BaslerGigE").compare(devClass)) continue;
			CBaslerGigEDeviceInfo * dev = (CBaslerGigEDeviceInfo*) &(*it);
			if(!macAddr[i].compare(dev->GetMacAddress())){
				try{
					//Code may throw if camera is open by another contex
					_cameras[i].Attach(ctlFactory.CreateDevice(*dev));
				}catch(GenICam::GenericException &ex){
					LOG_E("Failed to attach device(s): %s", ex.GetDescription());
					_cameras.DestroyDevice();
					pthread_mutex_unlock(&_configMutex);
					return false;
				}
				PylonStream * ps = new PylonStream();
				streams.push_back(ps);
				_streams.push_back(ps);
				found = true;
				break;
			}
		}
		if(!found){
			LOG_E("Could not find device with correct MAC address: %s", macAddr[i].c_str());
			_cameras.DestroyDevice();
			pthread_mutex_unlock(&_configMutex);
			return false;
		}
	}

	return init(configuration);
}

bool PylGigE::init(const PylonConfig &configuration){

	if(!_cameras.IsPylonDeviceAttached()){
		LOG_E("Unable to attach/open camera device(s)");
		pthread_mutex_unlock(&_configMutex);
		return false;
	}

	try{
        for(size_t i=0; i<_cameras.GetSize();++i){
            _cameras[i].GrabCameraEvents = configuration.hdrConf.toggle;
        }
		//Code may throw multiple exceptions
		_cameras.Open();
	}catch(GenICam::GenericException &ex){
		LOG_E("Failed to open device(s): %s", ex.GetDescription());
		_cameras.DestroyDevice();
		pthread_mutex_unlock(&_configMutex);
		return false;
	}
	
	_size = _cameras.GetSize();

	_ticksPerUsec.resize(_size);
	_basePcTime.resize(_size);
	_baseCamTime.clear();
	_baseCamTime.resize(_size,0); //Needs to be initialized to 0

    //Save information on what cameras are connected, used when trying to reconnect
	for(size_t i=0; i<_size; ++i){
		_camDevInfo.push_back((CBaslerGigEDeviceInfo) _cameras[i].GetDeviceInfo());
	}

	if(configuration.exposurePilotHandle >= 0 && configuration.exposurePilotHandle < (int)_size){
		_pilot = configuration.exposurePilotHandle;
	}else{
        //Manual control of exposure
		_pilot = -1;
    }

	for(size_t i=0; i<_size; ++i){
        _cameras[i].GevTimestampControlReset();
		_ticksPerUsec[i] = _cameras[i].GevTimestampTickFrequency() / 1000000;
		_cameras[i].SetBufferFactory(NULL);//Override later if wanted
		_cameras[i].MaxNumBuffer = 12; //Override later if wanted
	}

    if(configuration.hdrConf.toggle){
        _config = new PylGigEHdrConfig(this, configuration);
    }else{
        _config = new PylGigEBasicConfig(this, configuration);
    }
    
    if(!_config->setConfig()){
        _cameras.DestroyDevice();
        pthread_mutex_unlock(&_configMutex);
        return false;
    }

	//Start thread-loops
	_isOpen = true;
    _config->beginExposureLoop();

    //Set real-time priority on grab thread
    pthread_attr_t attr;
    struct sched_param param = {
        .sched_priority = sched_get_priority_max(SCHED_FIFO)
    };
    if(pthread_attr_init(&attr) != 0 ||
            pthread_attr_setschedpolicy(&attr, SCHED_FIFO) != 0 ||
            pthread_attr_setschedparam(&attr, &param) != 0 ||
            pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED) != 0 ||
            pthread_create(&_grabThread, &attr, &grabLoopStarter, this) != 0){

        LOG_W("Unable to initialize pthread attributes with real-time priorities: %s",
                strerror(errno));
        perror("pthread_attr_init / pthread_create");
        if(pthread_create(&_grabThread, NULL, &grabLoopStarter, this) != 0){
            LOG_E("Unable to create thread at all: %s", strerror(errno));
            delete _config;
            pthread_mutex_unlock(&_configMutex);
            return false;
        }
    }

	pthread_mutex_unlock(&_configMutex);
	return true;
}

void PylGigE::setExposure(float newExposureValue, float newGainValue){
    if(_isOpen){
        _config->setExposure(newExposureValue, newGainValue);
    }
}

void PylGigE::adjustWhitebalance(size_t idx, float r, float g, float b){
    if(_isOpen){
        _config->adjustWhitebalance(idx, r, g, b);
    }
}

void PylGigE::close(){
	if(!_isOpen) return;
	
	pthread_mutex_lock(&_configMutex);

	//Terminate threads
    _isOpen = false;
	pthread_mutex_unlock(&_configMutex);

    delete _config;
    pthread_join(_grabThread, NULL);//Based on _isOpen

 	// Destroy all output streams
	for(size_t i=0; i<_size; ++i){
		delete _streams[i];
	}
	_streams.clear();

	_cameras.DestroyDevice();
	pthread_mutex_destroy(&_configMutex);
}

bool PylGigE::useCustomBuffers(const std::vector<void*> &buffers, size_t bufferByteSize){

	if(!_isOpen){
		LOG_E("Cameras must be opened before setting custom buffers");
		return false;
	}

	int buffersPerCamera = buffers.size() / _size;
	if(buffersPerCamera < minimumBuffersPerCamera){
		LOG_E("Too few (%d) buffers allocated. Should be at least %d per camera (%d)",
				(int)buffers.size(), minimumBuffersPerCamera, (int)_size*minimumBuffersPerCamera);
		return false;
	}

	_bufferFactory = new CustomBufferFactory(bufferByteSize, _size);

	for(size_t i=0; i<buffers.size(); ++i){
		_bufferFactory->AddBuffer(buffers[i]);
	}
	if(buffers.size() % _size) buffersPerCamera++;
	for(size_t i=0; i<_size; ++i){
		_cameras[i].SetBufferFactory(_bufferFactory);
		_cameras[i].MaxNumBuffer = buffersPerCamera;
	}

	return true;
}

bool PylGigE::startStreaming(){

	pthread_mutex_lock(&_configMutex);
	if(_cameras.IsGrabbing()){
		pthread_mutex_unlock(&_configMutex);
		return true;
	}
	try{
		_cameras.StartGrabbing();
	}catch(GenICam::GenericException &ex){
		LOG_E("Failed to start streaming: %s", ex.GetDescription());
		pthread_mutex_unlock(&_configMutex);
		for(size_t i=0; i<_size; ++i){
			if(testDisconnected(i)){
				return _cameras.IsGrabbing();
			}
		}
		return false;
	}
	pthread_mutex_unlock(&_configMutex);
	return true;
}

void PylGigE::stopStreaming(){

	pthread_mutex_lock(&_configMutex);
	if(_cameras.IsGrabbing()) _cameras.StopGrabbing();
	pthread_mutex_unlock(&_configMutex);
}

bool PylGigE::testDisconnected(int camIdx){
	try{
		_cameras[camIdx].GevTimestampControlLatch();
	}catch(GenICam::GenericException &ex){
		if(_cameras[camIdx].IsCameraDeviceRemoved()){
			LOG_W("Camera timeout occurred! "
					"Trying to restart all camera streams to keep streaming...");
			if(reconnect()){
				LOG_W("Successfully restarted stream!");
			}else{
				LOG_E("Restarting stream failed. Lost contact with all cameras.");
			}
			return true;
		}else{
			LOG_E("Unknown exception occured with camera %d: %s", camIdx, ex.GetDescription());
		}
	}
	return false;
}

void PylGigE::grabLoop(){
	CGrabResultPtr ptrGrabResult;

	int framesSinceLastGrab[_size];
	for(size_t i=0; i<_size; ++i) framesSinceLastGrab[i]=0;

	while(_isOpen){
		if(_cameras.IsGrabbing()){

			try{
				if(!_cameras.RetrieveResult(1500, ptrGrabResult, TimeoutHandling_ThrowException)){

					//I believe returned false happens when the camera is being terminated, but
					//it takes a while for IsGrabbing() to be fully updated.
					continue;
				}
			}catch(GenICam::RuntimeException& ex){
				bool timeout = false;
				for(size_t i=0; i<_size; ++i){
					if( (timeout = testDisconnected(i))) break;
				}
				if(!timeout){
					LOG_E("Grab timed out while retrieving frame, but camera still connected: %s"
							"\nCommon causes include: "
                            "External trigger signal never received (triggerbox offline), "
                            "forgetting to delete frames "
                            "or too few buffers allocated.", ex.GetDescription());
				}
				continue;
			}catch(GenICam::GenericException& ex){
				LOG_E("Camera exception while retrieving frame: %s", ex.GetDescription());
				continue;
			}
			if(!ptrGrabResult->GrabSucceeded()){
				LOG_W("Grab failed! [#%8x] %s", ptrGrabResult->GetErrorCode(),
						ptrGrabResult->GetErrorDescription().c_str());
				continue;
			}
			EPayloadType grabType = ptrGrabResult->GetPayloadType();
			if(grabType != PayloadType_Image && grabType != PayloadType_ChunkData){
				//This shouldnt happen. Re-evaluate statement upon it happening.
				LOG_W("Received unknown data from grab. PayloadType %d",(int)grabType);
				continue;
			}

			intptr_t camIdx = ptrGrabResult->GetCameraContext();

			bool disconnected = false;
			for(int i=0; i<(int)_size; ++i){
				if(i == camIdx){
					framesSinceLastGrab[i]=0;
				}else if(++framesSinceLastGrab[i] > (int) (_size*3)){
					if( (disconnected = testDisconnected(i))){
						for(int j=0; j<(int)_size; ++j){
							framesSinceLastGrab[j]=0;
							_baseCamTime[j] = 0;
						}
					   	break;
					}
				}
			}
			if(disconnected) continue;

			if(_pilot == camIdx){
				//Updates exposure if it's time
                _config->checkUpdateExposure(ptrGrabResult);
			}

            //dbg
//             uint32_t fnum = GET_FRAME_COUNTER(ptrGrabResult);
//             switch(camIdx){
//                 case 0: if(fnum > 500 && fnum < 900) continue; else break;
//                 case 1: if(fnum > 250 && fnum < 1200) continue; else break;
//                 case 2: if(fnum > 250 && fnum < 700) continue; else break;
//                 default: break;
//             }
            
			PylonFrame * outFrame;
            try{
                outFrame = new PylonFrame(ptrGrabResult);
            }catch(std::bad_alloc &ex){
				LOG_E("Mem alloc threw exception in grab thread: %s", ex.what());
                continue; //Not sure what to do here... cross fingers, hope for the best
			}

			//Handle timestamp
			int64_t time = (int64_t)(ptrGrabResult->GetTimeStamp());
			if(_baseCamTime[camIdx] == 0 || (time-_baseCamTime[camIdx])
					> (_ticksPerUsec[camIdx] * timestampSyncInterval)){
				//We want to match the time of the camera with the time on the PC every couple sek
				//This takes about a millisec to do, shouldn't be done too often
				try{
					_cameras[camIdx].GevTimestampControlLatch();
					gettimeofday(&_basePcTime[camIdx],NULL);
					_baseCamTime[camIdx] = _cameras[camIdx].GevTimestampValue();
				}catch(GenICam::GenericException &ex){
					LOG_E("Exception while updating timestamp: %s",ex.GetDescription());
				}
			}
			int64_t usec = (int64_t)(_basePcTime[camIdx].tv_usec) +
				( (time-_baseCamTime[camIdx]) / _ticksPerUsec[camIdx]);
			if(usec < 0){
                
				outFrame->timeStamp.tv_usec = usec+1000000;
				outFrame->timeStamp.tv_sec = _basePcTime[camIdx].tv_sec - 1;
			}else{
				outFrame->timeStamp.tv_usec = usec % 1000000;
				outFrame->timeStamp.tv_sec = _basePcTime[camIdx].tv_sec + (usec / 1000000);
			}

            _config->addExposure(outFrame);

			//DBG
// 			fprintf(stderr,"F: %2d:%4d  ::: %8ld.%7ld\n",(int)camIdx,outFrame->frameCounter,
// 					outFrame->timeStamp.tv_sec, outFrame->timeStamp.tv_usec);

			_streams[camIdx]->putFrame(outFrame);
		}else{
			//We're not grabbing, but we likely will soon. Lets just sleep a few millisec
			usleep(4000);
		}
	}
}

bool PylGigE::reconnect(){
	pthread_mutex_lock(&_configMutex);
	if(_bufferFactory){
		_bufferFactory = _bufferFactory->clone();
		//I know what you're thinking, but this isn't a memory leak. The old factory will
		//automatically suicide as we close the cameras. But we wish to reuse the buffers
		//There is a small possibility of problems with the pixel data, but that's unlikely
	}
    _cameras.DestroyDevice();
    for(int s=1; s>0; s = sleep(s)) ;
    _config->reconnect();
    LOG_D("Destroyed devices... Starting to re-create cameras");
    //Lets assume some moron pulled the ethernet cable out.
    //Lets give him some time to put it back.
    //Edit: Wait forever, at a longer interval
    for(int tries=0; ; tries++){
        try{
            for(size_t i=0; i<_size; ++i){
                _cameras[i].Attach( CTlFactory::GetInstance().CreateDevice(_camDevInfo[i]) );
            }
            break;//Success
        }catch(GenICam::GenericException &ex){
            LOG_W("Error when re-attaching cameras for reconnect.\n"
                    "Exception: %s", ex.GetDescription());
            
            //Sleep a little and try again
            //Start by trying frequently, otherwise we try with a long interval
            if( tries < 10){
                for(int s=6; s>0; s = sleep(s)) ;
            }else{
                for(int s=60; s>0; s = sleep(s)) ;
            }
        }
    }
	if(!_cameras.IsPylonDeviceAttached()){
		LOG_E("Error, cameras failed to re-attach during reconnect.");
		pthread_mutex_unlock(&_configMutex);
		return false;
	}
	try{
        for(size_t i=0; i<_cameras.GetSize();++i)
            _cameras[i].GrabCameraEvents = _config->_configuration.hdrConf.toggle;
		_cameras.Open();
	}catch(GenICam::GenericException &ex){
		LOG_E("Error when trying to reconnect cameras. Exception: %s", ex.GetDescription());
		pthread_mutex_unlock(&_configMutex);
		return false;
	}
    if(!_config->setConfig()){
        pthread_mutex_unlock(&_configMutex);
        return false;
    }
	for(size_t i=0; i<_size; ++i){
		_cameras[i].SetBufferFactory(_bufferFactory);//It's ok if this is NULL
	}
	try{
		_cameras.StartGrabbing();
	}catch(GenICam::GenericException &ex){
		LOG_E("Error when trying to reconnect cameras. Exception: %s", ex.GetDescription());
		pthread_mutex_unlock(&_configMutex);
		return false;
	}
	pthread_mutex_unlock(&_configMutex);
	return true;
}

/* ------------------- Helper functions --------------- */

void * PylonGigE::grabLoopStarter(void * param){
	((PylGigE *)param)->grabLoop();
	pthread_exit(0);
	return NULL;
}

