// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "PylGigEHdrConfig.hpp"

using namespace PylonGigE;
using namespace Pylon;
using namespace Basler_GigECamera;
using namespace Basler_GigECameraParams;

/* ------------------- Constants --------------------------- */
const int exposureSafeMarginUsec = 4000; 
// const int cameraHeartbeatInterval = 3000;//ms
const int cameraHeartbeatInterval = 20000;//ms
const float defaultMaximumFramerate = 50.0f; 

/* ------------------- Helper functions --------------- */
struct ExpoThreadParam {
    PylGigEHdrConfig * obj;
    int idx;
};

void * exposureLoopStarterHdr(void * param){
    ExpoThreadParam * p = (ExpoThreadParam *)param;
    p->obj->exposureLoop(p->idx);
    delete p;
	pthread_exit(0);
	return NULL;
}

void * exposureUpdateLoopStarterHdr(void * param){
    ((PylGigEHdrConfig *)param)->exposureUpdateLoop();
	pthread_exit(0);
	return NULL;
}

/* ------------- Public member functions ------------------- */

PylGigEHdrConfig::~PylGigEHdrConfig(){
    if(!_isOpen) return;

    for(size_t i=0; i<_size; ++i){
        pthread_mutex_lock(&_sleepMutex[i]);
    }
    pthread_mutex_lock(&_exposureSleepMutex);
    _isOpen = false;
    pthread_cond_signal(&_exposureSleepCond);
    pthread_mutex_unlock(&_exposureSleepMutex);
    for(size_t i=0; i<_size; ++i){
        pthread_cond_signal(&_sleepCond[i]);
        pthread_mutex_unlock(&_sleepMutex[i]);
    }
    for(size_t i=0; i<_size; ++i){

        try{
            _gige->_cameras[i].DeregisterCameraEventHandler(this, "FrameStartEventData");
            _gige->_cameras[i].EventSelector = EventSelector_FrameStart;
            _gige->_cameras[i].EventNotification = EventNotification_Off;
        }catch(GenICam::GenericException &ex) { }

        pthread_join(_threads[i], NULL);
        pthread_mutex_destroy(&_sleepMutex[i]);
        pthread_cond_destroy(&_sleepCond[i]);
    }
    pthread_join(_exposureSleepThread, NULL);
    pthread_mutex_destroy(&_exposureSleepMutex);
    pthread_cond_destroy(&_exposureSleepCond);
    delete _sleepMutex;
    delete _sleepCond;
    delete _threads;
    delete _frameCounter;
}

void PylGigEHdrConfig::OnCameraEvent( Pylon::CBaslerGigEInstantCamera& camera,
        intptr_t idx, GenApi::INode* pNode){

    pthread_mutex_lock(&_sleepMutex[idx]);
    _frameCounter[idx]++;
    pthread_cond_signal(&_sleepCond[idx]);
    pthread_mutex_unlock(&_sleepMutex[idx]);
}

bool PylGigEHdrConfig::beginExposureLoop(){
    _currentIdx = 0;
    _currentExposure[0] = 2000.0f;
    _currentExposure[1] = 5000.0f;
    _framesUntillNextExposureUpdate = 10;

    _threads = new pthread_t[_size];
    _sleepMutex = new pthread_mutex_t[_size];
    _sleepCond = new pthread_cond_t[_size];
    _frameCounter = new int[_size];

    _isOpen = true;

    _exposureNewIdx = -1;
    pthread_mutex_init(&_exposureSleepMutex, NULL);
    pthread_cond_init(&_exposureSleepCond, NULL);
    pthread_create(&_exposureSleepThread, NULL, exposureUpdateLoopStarterHdr, (void*)this); 

    //Set real-time priority on thread setting hdr
    pthread_attr_t attr;
    bool realtime = true;
    struct sched_param param = {
        .sched_priority = sched_get_priority_max(SCHED_FIFO)
    };
    if(pthread_attr_init(&attr) != 0 ||
            pthread_attr_setschedpolicy(&attr, SCHED_FIFO) != 0 ||
            pthread_attr_setschedparam(&attr, &param) != 0 ||
            pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED) != 0){
        LOG_W("Unable to initialize pthread attributes with real-time priorities: %s",
                strerror(errno));
        realtime = false;
    }
    for(size_t i=0; i<_size; ++i){
        _frameCounter[i] = 0;
        pthread_mutex_init(&_sleepMutex[i], NULL);
        pthread_cond_init(&_sleepCond[i], NULL);
        ExpoThreadParam * param = new ExpoThreadParam;
        param->obj = this;
        param->idx = i;
        if(realtime){
            if(pthread_create(&_threads[i], &attr, exposureLoopStarterHdr, (void*)param) != 0){
                LOG_W("Unable to create thread with real-time priorities: %s",
                        strerror(errno));
                pthread_create(&_threads[i], NULL, exposureLoopStarterHdr, (void*)param); 
            } 
        }else{
            pthread_create(&_threads[i], NULL, exposureLoopStarterHdr, (void*)param); 
        }
    }

    return true;
}

void PylGigEHdrConfig::reconnect(){
    _currentIdx = 0;
    _framesUntillNextExposureUpdate = 20;
    for(int i=0; i<(int)_size; ++i) _frameCounter[i] = 0;
}

bool PylGigEHdrConfig::setConfig(){

    if(_configuration.exposureContinuousAuto){
        LOG_W("Continuous auto exposure not supported with HDR."
                "Requires pilot camera or manual approach.");
    }
    
    for(size_t i=0; i<_size; ++i)
        if( !applyConfig(i) ) return false;
    return true;
}

//This is called on pilot camera only
void PylGigEHdrConfig::checkUpdateExposure(CGrabResultPtr framePtr){

//     static uint64_t prevTime = 0;
//     int diff = (int) ((framePtr->GetTimeStamp()-prevTime)*8/1000);
//     LOG_D("%8d",diff);
//     prevTime = framePtr->GetTimeStamp();

    --_framesUntillNextExposureUpdate;
    switch(_pilotState){
        case HdrState_INITIALIZED:
            if(_framesUntillNextExposureUpdate != 0) break;
            try{
                if(_autoTarget[0] < 50)
                    _gige->_cameras[_pilot].AutoTargetValue = 50;
                else
                    _gige->_cameras[_pilot].AutoTargetValue = _autoTarget[0];
                _gige->_cameras[_pilot].ExposureTimeAbs =
                    _gige->_cameras[_pilot].AutoExposureTimeAbsUpperLimit() / 2;
                _gige->_cameras[_pilot].GainSelector = GainSelector_All;
                _gige->_cameras[_pilot].GainAuto = GainAuto_Once;
                _gige->_cameras[_pilot].ExposureAuto = ExposureAuto_Once;
                _pilotState = HdrState_GET_DARK_GAIN;
                _framesUntillNextExposureUpdate = 5;
            }catch(GenICam::GenericException &ex){
                LOG_W("Setting initial hdr config failed: %s", ex.GetDescription());
                _framesUntillNextExposureUpdate = 20;
            }
            break;
        case HdrState_GET_DARK_GAIN:
            try{
                GainAutoEnums gEnum = _gige->_cameras[_pilot].GainAuto();
                if(gEnum != GainAuto_Once || _framesUntillNextExposureUpdate == 0){
                    if(gEnum == GainAuto_Once)
                        _gige->_cameras[_pilot].GainAuto = GainAuto_Off;
                    _currentGain = _gige->_cameras[_pilot].GainRaw();
                    _gige->_cameras[_pilot].ExposureAuto = ExposureAuto_Once;
                    _framesUntillNextExposureUpdate = 5;
                    _pilotState = HdrState_GET_DARK_EXPO;
                }
            }catch(GenICam::GenericException &ex){
                LOG_W("Reading dark gain failed: %s", ex.GetDescription());
                _framesUntillNextExposureUpdate = _configuration.exposureUpdateFrequency;
            }
            break;
        case HdrState_GET_DARK_EXPO:
            try{
                ExposureAutoEnums eEnum = _gige->_cameras[_pilot].ExposureAuto();
                if(eEnum != ExposureAuto_Once || _framesUntillNextExposureUpdate == 0){

                    if(eEnum == ExposureAuto_Once)
                        _gige->_cameras[_pilot].ExposureAuto = ExposureAuto_Off;
                    _currentExposure[0] = _gige->_cameras[_pilot].ExposureTimeAbs();

                    if(!setAutoFunctionRegion(_aois[1],_gige->_cameras[_pilot], false)){
                        LOG_W("Setting light auto function region failed");
                        _framesUntillNextExposureUpdate = _configuration.exposureUpdateFrequency;
                        break;
                    }
                    if(_autoTarget[1] < 50)
                        _gige->_cameras[_pilot].AutoTargetValue = 50;
                    else
                        _gige->_cameras[_pilot].AutoTargetValue = _autoTarget[1];
                    _gige->_cameras[_pilot].ExposureAuto = ExposureAuto_Once;

                    //Now we have a gain + exp value to broadcast
                    pthread_mutex_lock(&_exposureSleepMutex);
                    _exposureNewIdx = 0;
                    pthread_cond_signal(&_exposureSleepCond);
                    pthread_mutex_unlock(&_exposureSleepMutex);

                    _framesUntillNextExposureUpdate = 5;
                    _pilotState = HdrState_GET_LIGHT_EXPO;
                }
            }catch(GenICam::GenericException &ex){
                LOG_W("Reading dark expo failed: %s", ex.GetDescription());
                _framesUntillNextExposureUpdate = _configuration.exposureUpdateFrequency;
            }
            break;
        case HdrState_GET_LIGHT_EXPO:
            try{
                ExposureAutoEnums eEnum = _gige->_cameras[_pilot].ExposureAuto();
                if(eEnum != ExposureAuto_Once || _framesUntillNextExposureUpdate == 0){

                    if(eEnum == ExposureAuto_Once)
                        _gige->_cameras[_pilot].ExposureAuto = ExposureAuto_Off;
                    _currentExposure[1] = _gige->_cameras[_pilot].ExposureTimeAbs();

                    //Now we have a gain + exp value to broadcast
                    pthread_mutex_lock(&_exposureSleepMutex);
                    _exposureNewIdx = 1;
                    pthread_cond_signal(&_exposureSleepCond);
                    pthread_mutex_unlock(&_exposureSleepMutex);

                    _framesUntillNextExposureUpdate = _configuration.exposureUpdateFrequency / 2;
                    _pilotState = HdrState_UPDATE_DARK;
                }
            }catch(GenICam::GenericException &ex){
                LOG_W("Reading light expo failed: %s", ex.GetDescription());
                _framesUntillNextExposureUpdate = _configuration.exposureUpdateFrequency;
            }
            break;
        case HdrState_UPDATE_DARK:
            if(_framesUntillNextExposureUpdate == 0){

                int frameCnt = GET_FRAME_COUNTER(framePtr);
                int idx = frameCnt & 1;
                if(idx == 0){
                    //We need an actual dark frame here, this is a light frame
                    _framesUntillNextExposureUpdate = 1;
                    break;
                }
                double avrg = getAverageGrayscale(framePtr, 0);
                float newVal = _currentExposure[0] * _autoTarget[0] / avrg;
//                 LOG_D("DARK: %f : %f", avrg, _autoTarget[0]);
                _currentExposure[0] = std::min<float>(
                        _gige->_cameras[_pilot].AutoExposureTimeAbsUpperLimit(),
                        std::max<float>(newVal,
                            _gige->_cameras[_pilot].AutoExposureTimeAbsLowerLimit()));

                pthread_mutex_lock(&_exposureSleepMutex);
                _exposureNewIdx = 0;
                pthread_cond_signal(&_exposureSleepCond);
                pthread_mutex_unlock(&_exposureSleepMutex);
                _framesUntillNextExposureUpdate = _configuration.exposureUpdateFrequency / 2;
                _pilotState = HdrState_UPDATE_LIGHT;
            }
            break;
        case HdrState_UPDATE_LIGHT:
            if(_framesUntillNextExposureUpdate == 0){
                int frameCnt = GET_FRAME_COUNTER(framePtr);
                int idx = frameCnt & 1;
                if(idx == 1){
                    //We need an actual light frame here, this is a dark frame
                    _framesUntillNextExposureUpdate = 1;
                    break;
                }
                double avrg = getAverageGrayscale(framePtr, 1);
                float newVal = _currentExposure[1] * _autoTarget[1] / avrg;
//                 LOG_D("LIGHT: %f : %f", avrg, _autoTarget[1]);
                _currentExposure[1] = std::min<float>(
                        _gige->_cameras[_pilot].AutoExposureTimeAbsUpperLimit(),
                        std::max<float>(newVal,
                            _gige->_cameras[_pilot].AutoExposureTimeAbsLowerLimit()));

                pthread_mutex_lock(&_exposureSleepMutex);
                _exposureNewIdx = 1;
                pthread_cond_signal(&_exposureSleepCond);
                pthread_mutex_unlock(&_exposureSleepMutex);
                _framesUntillNextExposureUpdate = _configuration.exposureUpdateFrequency / 2;
                _pilotState = HdrState_UPDATE_DARK;
            }
            break;
        case HdrState_UNINITIALIZED:
        default: break;
    }
}

void PylGigEHdrConfig::setExposure(float newExposureValue, float newGainValue){

    //Since we have 2 exposure values to control, but want the function to look the same from
    //the outside, we separate between the two exposure values by adding/subtracting 1000
    //from gain, which will never occur naturally
    if(newGainValue > 1000){
       _currentExposure[1] = newExposureValue;
       _currentGain = newGainValue - 1000;
    }else{
        _currentExposure[0] = newExposureValue;
        _currentGain = newGainValue;
    }
	LOG_D("Updated exposure and gain: [%.1f,%.1f : %.1f] ",_currentExposure[0],
            _currentExposure[1],_currentGain);
    for(size_t i=0; i<_size; ++i){
        try{
            if(GenApi::IsWritable(_gige->_cameras[i].GainRaw)){
                _gige->_cameras[i].GainRaw = _currentGain;
            }
        }catch(GenICam::GenericException &ex){
            LOG_W("Setting gain value caused exception: %s", ex.GetDescription());
        }
    }
}

void PylGigEHdrConfig::addExposure(PylonFrame *f){
    f->darkExposure = (f->frameCounter&1);
    f->exposureTime = (uint32_t) _currentExposure[ (f->frameCounter&1)^1 ];
}

/* ------------- Private member functions ------------------ */

void PylGigEHdrConfig::exposureUpdateLoop(){
    while(true){
        pthread_mutex_lock(&_exposureSleepMutex);
        while( _isOpen && _exposureNewIdx == -1){
            pthread_cond_wait(&_exposureSleepCond, &_exposureSleepMutex);
        }
        if( !_isOpen ) break;
		float cE = _currentExposure[_exposureNewIdx];
		float cG = _currentGain + (1000*_exposureNewIdx);
        _exposureNewIdx = -1;
        pthread_mutex_unlock(&_exposureSleepMutex);


		setExposure(cE, cG);
		if(_configuration.exposureCallback){
			_configuration.exposureCallback(cE,cG);
		}
    }
    pthread_mutex_unlock(&_exposureSleepMutex);
}

void PylGigEHdrConfig::exposureLoop(int idx){
	CBaslerGigEInstantCamera &cam = _gige->_cameras[idx];
    int prevFrameCounter = 0;
    while(true){
        pthread_mutex_lock(&_sleepMutex[idx]);
        while( _isOpen && prevFrameCounter == _frameCounter[idx]){
            pthread_cond_wait(&_sleepCond[idx], &_sleepMutex[idx]);
        }
        if( !_isOpen ) break;
        prevFrameCounter = _frameCounter[idx];
        pthread_mutex_unlock(&_sleepMutex[idx]);

        try{
            if(GenApi::IsWritable(cam.ExposureTimeAbs)){
                cam.ExposureTimeAbs = _currentExposure[prevFrameCounter & 1];
            }
        }catch(GenICam::GenericException &ex){
            LOG_W("Set exposure value caused exception: %s", ex.GetDescription());
        }
    }
    pthread_mutex_unlock(&_sleepMutex[idx]);
}

bool PylGigEHdrConfig::applyConfig(size_t idx){
    
    if(!applySharedConfig(idx)) return false;

	CBaslerGigEInstantCamera &cam = _gige->_cameras[idx];

	//Set triggermode
	try{
        if(_configuration.triggerMode){
            LOG_W("It is not possible, due to Pylon being strange / bugged,"
                    "to use triggerbox with HDR mode."
                    "Turning triggerMode off and relying on fps");
            //So, this is a bit funny. If you reduce the exposure time by X, the next frame will
            //be pushed back by X and vice versa. This means that, with triggerbox, you lose
            //the next frame every single time you REDUCE the exposure time compared to the
            //previous value. Seems like a clear bug...
            _configuration.triggerMode = false;
        }
        cam.TriggerSelector = TriggerSelector_AcquisitionStart;
        cam.TriggerMode = TriggerMode_Off;
        cam.TriggerSelector = TriggerSelector_FrameStart;
        cam.TriggerMode = TriggerMode_Off;
	}catch(GenICam::GenericException &ex){
		LOG_E("Failed to set triggermode. Exception: %s", ex.GetDescription());
		return false;
	}
	
	//Determine auto function region of interest
    if((int)idx == _pilot){
        if(!checkValidAoi(_configuration.hdrConf.light, _configuration.hdrConf.dark)){
            return false;
        }
        if(!setAutoFunctionRegion(_configuration.hdrConf.dark, cam, true)){
            return false;
        }
        _aois[0] = _configuration.hdrConf.dark;
        _aois[1] = _configuration.hdrConf.light;
        _autoTarget[0] = _configuration.hdrConf.targetDark;
        _autoTarget[1] = _configuration.hdrConf.targetLight;
        _pilotState = HdrState_INITIALIZED;
    }

    // Check if the device supports events.
    if ( !IsAvailable( cam.EventSelector)) {
        LOG_E("This camera device doesn't support events");
        return false;
    }
    try{
        //Set up event grabbing
        cam.RegisterCameraEventHandler(this, "FrameStartEventData", idx,
                RegistrationMode_ReplaceAll, Cleanup_None);
        cam.EventSelector = EventSelector_FrameStart;
        cam.EventNotification = EventNotification_GenICamEvent;
    }catch(GenICam::GenericException &ex){
        LOG_E("Exception occurred when configuring event grabbing: %s", ex.GetDescription());
        return false;
    }

    return true;
}

bool PylGigEHdrConfig::checkValidAoi(AreaOfInterest &aoi1, AreaOfInterest &aoi2){
    if(aoi1.centerX + (aoi1.scaleX*0.5f) > 0.99f ||
            aoi1.centerX - (aoi1.scaleX*0.5f) < 0.01f ||
            aoi1.centerY + (aoi1.scaleY*0.5f) > 0.99f ||
            aoi1.centerY - (aoi1.scaleY*0.5f) < 0.01f){
        LOG_E("Auto update region is outside image boundaries");
        return false;
    }
    if(aoi2.centerX + (aoi2.scaleX*0.5f) > 0.99f ||
            aoi2.centerX - (aoi2.scaleX*0.5f) < 0.01f ||
            aoi2.centerY + (aoi2.scaleY*0.5f) > 0.99f ||
            aoi2.centerY - (aoi2.scaleY*0.5f) < 0.01f){
        LOG_E("Auto update region is outside image boundaries");
        return false;
    }
    if(aoi1.scaleX != aoi2.scaleX || aoi1.scaleY != aoi2.scaleY){
        LOG_E("Auto update regions for HDR must be of the same size");
        return false;
    }
    return true;
}

bool PylGigEHdrConfig::setAutoFunctionRegion(AreaOfInterest &aoi, CBaslerGigEInstantCamera &cam,
        bool first){
    int aoiW = (int) (_configuration.width * aoi.scaleX);
    int aoiH = (int) (_configuration.height * aoi.scaleY);
    int aoiOx = (int) (_configuration.width * aoi.centerX - (aoiW/2));
    int aoiOy = (int) (_configuration.height * aoi.centerY - (aoiH/2));
    try{
        cam.AutoFunctionAOISelector = AutoFunctionAOISelector_AOI1;
        //Later on, we don't want to change the dimensions, just the offset
        if(first){
            cam.AutoFunctionAOIOffsetX = 0; //Reset it to top-left corner before trying to set dim
            cam.AutoFunctionAOIOffsetY = 0;
            cam.AutoFunctionAOIWidth = aoiW;
            cam.AutoFunctionAOIHeight = aoiH;
        }
        cam.AutoFunctionAOIOffsetX = aoiOx;
        cam.AutoFunctionAOIOffsetY = aoiOy;
    }catch(GenICam::GenericException &ex){
        LOG_E("Exception occurred when configuring exposure AOI: %s", ex.GetDescription());
        return false;
    }
    return true;
}

double PylGigEHdrConfig::getAverageGrayscale(CGrabResultPtr image, int aoi){
    int aoiW = (int) (image->GetWidth() * _aois[aoi].scaleX);
    aoiW = (aoiW>>1)<<1;
    int aoiH = (int) (image->GetHeight() * _aois[aoi].scaleY);
    int aoiOx = (int) (image->GetWidth() * _aois[aoi].centerX - (aoiW/2));
    int aoiOy = (int) (image->GetHeight() * _aois[aoi].centerY - (aoiH/2));
    
    double numPixels = aoiH*aoiW;
    double accumulate = 0;
    uint8_t * pixels = (uint8_t*) image->GetBuffer();

    aoiH += aoiOy;
    aoiW += aoiOx;
    
    int stride = image->GetImageSize() / image->GetHeight();
    int pixelSize = stride / image->GetWidth();

    for(int y=aoiOy; y<aoiH; ++y){
        for(int x=aoiOx; x<aoiW; ++x){
            accumulate += pixels[y*stride + x*pixelSize];
        }
    }

    return accumulate / (numPixels);
}
