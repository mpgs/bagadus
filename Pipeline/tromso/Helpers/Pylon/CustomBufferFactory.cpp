// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "CustomBufferFactory.hpp"

#include <cstdlib>
#include <iostream>
#include <cstdio>

using namespace PylonGigE;

CustomBufferFactory::CustomBufferFactory(size_t bufSize, size_t numClients) {
	_size = bufSize;
	_clients = numClients;
	_activeClients = numClients;
	pthread_spin_init(&_lock, PTHREAD_PROCESS_PRIVATE);
}

void CustomBufferFactory::AllocateBuffer(size_t bufferSize, void **pCreatedBuffer,
		intptr_t &bufferContext){
	if(bufferSize > _size){
		LOG_E("Custom buffer pool frames too small, requesting %d bytes instead of %d",
				(int)bufferSize, (int)_size);
		*pCreatedBuffer = NULL;
		return;
	}
	pthread_spin_lock(&_lock);
	*pCreatedBuffer = _freeBuffers.back();
	_freeBuffers.pop_back();
	bufferContext = _usedBuffers.size();
	_usedBuffers.push_back(*pCreatedBuffer);
	pthread_spin_unlock(&_lock);
}

void CustomBufferFactory::FreeBuffer(void *pCreatedBuffer, intptr_t bufferContext){
	pthread_spin_lock(&_lock);
	_freeBuffers.push_back(pCreatedBuffer);
	for (std::vector<void*>::iterator it = _usedBuffers.begin(); it != _usedBuffers.end(); ++it){
		if(*it == pCreatedBuffer){
			_usedBuffers.erase(it);
			break;
		}
	}
	pthread_spin_unlock(&_lock);
}

void CustomBufferFactory::AddBuffer(void * buf){
	_freeBuffers.push_back(buf);
}

void CustomBufferFactory::DestroyBufferFactory(){
	pthread_spin_lock(&_lock);
	if(--_activeClients == 0){
		//Goodbye cruel world
		_usedBuffers.clear();
		_freeBuffers.clear();
		pthread_spin_unlock(&_lock);
		pthread_spin_destroy(&_lock);
		delete this; 
	}else{
		pthread_spin_unlock(&_lock);
	}
}

CustomBufferFactory * CustomBufferFactory::clone(){

	pthread_spin_lock(&_lock);
	CustomBufferFactory * newFact = new CustomBufferFactory(_size, _clients);
	for(size_t i=0; i<_usedBuffers.size(); ++i){
		newFact->AddBuffer(_usedBuffers[i]);
	}
	for(size_t i=0; i<_freeBuffers.size(); ++i){
		newFact->AddBuffer(_freeBuffers[i]);
	}
	pthread_spin_unlock(&_lock);

	return newFact;
}

