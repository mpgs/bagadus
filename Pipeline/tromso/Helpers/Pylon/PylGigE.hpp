// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#pragma once

/* README
 * Pylon version used during development: 3.2.1
 * To edit the code, you may need documentation found in current version tar-ball,
 * located in the repository under PylonSDK.
 * You may also need the ace GigE Users Manual found at baslerweb.com
 */

#include <cstdlib>
#include <string>
#include <iostream>
#include <iomanip>
#include <deque>
#include <vector>
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>
#include <sched.h>
#include <cerrno>
#include <sys/types.h>

#include <logging.h>

//Pylon includes
#include <pylon/PylonIncludes.h>
// #include <pylon/gige/PylonGigEIncludes.h>
#include <pylon/gige/GigETransportLayer.h>
#include <pylon/gige/BaslerGigEInstantCamera.h>
#include <pylon/private/DeviceSpecificInstantCameraArray.h>

//This first function is deprecated in pylon v4, but the same behavior for GigE cams
#ifdef PYL_VER_3
#define GET_FRAME_COUNTER( p ) p->GetFrameNumber()
#else
#define GET_FRAME_COUNTER( p ) p->GetBlockID()
#endif

namespace PylonGigE{

    /* IMPORTANT!
     * This function must be called before creating any camera objects.
     * It only needs to be called once per process. */
    const void initializePylon();

    /* This function must be called before terminating the process. */
    const void terminatePylon();

    /* Prints all connected cameras, their model / MAC / Serialnumber */
    const void printAvailableDevices();

    /* Determine name and MTU on the network interface, based on a connected camera */
    void getIfaceInfo(Pylon::CBaslerGigEDeviceInfo &info, int &mtu, std::string &name);

    struct AreaOfInterest{
        AreaOfInterest(float x, float y, float sx, float sy) :
            centerX(x),centerY(y),scaleX(sx),scaleY(sy) { }
        AreaOfInterest() :
            centerX(0.5f),centerY(0.5f),scaleX(0.9f),scaleY(0.9f) { }
        float centerX; //0.01f .. 0.99f
        float centerY; //0.01f .. 0.99f
        float scaleX;  //0.01f .. 0.99f
        float scaleY;  //0.01f .. 0.99f
    };//end AreaOfInterest

    struct HdrConfig {
        HdrConfig() :
            toggle(false),
            dark (AreaOfInterest(0.60f,0.5f,0.3f,0.9f)), targetDark (50),
            light(AreaOfInterest(0.20f,0.5f,0.3f,0.9f)), targetLight(70) 
        { }
        bool toggle;//Activate/De-activate. Everything else is ignored if false
        AreaOfInterest dark;//Expected dark region. scaleX/Y must be identical to light 
        int targetDark;//Auto target value for dark region. 50..200
        AreaOfInterest light;//Expected light region. scaleX/Y must be identical to dark
        int targetLight;//Auto target value for light region. 50..200
    };//end HdrConfig

    /* Configuration parameters for the configure-function.
    */
    struct PylonConfig {
        PylonConfig() :
            width(2040), height(1080), triggerMode(true), fps(50), pixelFormat("BayerGR8"),
            autoTargetValue(64), whiteBalanceOn(true), exposureContinuousAuto(false),
            indoorLighting(false), outsideDaylight(false), exposurePilotHandle(0),
            exposureUpdateFrequency(60), exposureCallback(NULL), autoUpdateRegion(AreaOfInterest()),
            hdrConf(HdrConfig())
        {}
        int width, height;// 0 = device-maximum
        bool triggerMode;//true=using triggerbox, false=ignore trigger signals
        float fps;//0=disable fps-cap, will capture at maximum fps.
        //if triggerMode is true, the output frame rate will be whatever the
        //triggerbox is configured with, NOT necessarily 'fps'
        //
        //The fps will then be treated as suggestion to help estimate
        //the required bandwidth / exposure duration etc
        //
        //If the triggerbox is configured higher than max, the output will be halved.
        //f ex, maximum=52fps, trigger=60 --> 30 fps. ex2: trigger=140 --> 35fps
        std::string pixelFormat; //List of available names printed if invalid.
        //Common formats: "Mono8", "BayerGR8", "YUV422_YUYV_Packed"

        int autoTargetValue; //Gray-value, between 50 and 200, to aim for during exposure updates.
        //128 is usually quite a bit too bright. Default should be ok, but may depend on scene

        bool whiteBalanceOn; // Alternative is no white balancing. continuous auto exposure gives
        // autoWhiteBalance. Otherwise, set presets with lighting parameters

        bool exposureContinuousAuto; // Alternative is manual.
        // If true, next parameters are ignored

        //-----------MANUAL AUTO EXPOSURE ONLY----------
        bool indoorLighting; //Use 'tungsten' light source / incandescent indoor light

        bool outsideDaylight; //Determines the "warmth" of the light source.
        //true=6500K, false=5000K. Ignored if indoor

        int exposurePilotHandle; // Use one camera-handle as exposure-pilot. -1 or invalid handle
        // means external, manual, config only. Corresponds to the
        // index in the array of MAC-addresses if that is used

        size_t exposureUpdateFrequency; // Number of frames between each exposure update
        // Under 1 second is not recommended

        void (*exposureCallback)(float exposure, float gain); //Callback function, called when
        //exposure value gets updated. Other cameras are automatically updated, and should not
        //need to be manually set afterwards
        //---------------------------------------------
        AreaOfInterest autoUpdateRegion; //Determines a rectangle region of the image, used for
        //performing auto-exposure, -whitebalance and -gain

        HdrConfig hdrConf; //Configuration of HDR mode. Default is OFF
    }; //end PylonConfig


    /* Container for the output frame.
     * IMPORTANT: This must be deleted when no longer needed, as soon as possible, or
     * the cameras will fail to allocate new frames and no new frames will arrive.
     * The driver uses a limited number of buffers, use 'delete frameptr' to allow the
     * pixel-buffer to be re-used.
     */
    class PylonFrame {
        public:
            PylonFrame(Pylon::CGrabResultPtr grab);
            ~PylonFrame() {}

            uint8_t * pixels; //Pointer to raw pixel data

            size_t size; //Size, in bytes, of the image data

            uint32_t frameCounter; //1-4294967295 since startStreaming().
            //This may reset if the camera times out and is restarted during the streaming.

            struct timeval timeStamp; //Time of the start of the frames exposure, adjusted to
            //the local machine clock.

            uint32_t exposureTime;//Duration (usec) of the frame exposure / shutter speed.

            bool darkExposure;//Only set for HDR, to identify which setting was used
        private:
            Pylon::CGrabResultPtr grabResult;
    };//end PylonFrame

    /* Buffered stream of frames from one camera context.
     * Only a few frames are kept buffered, e.g., 2 last frames, before they are discarded
     * if not explicitly fetched with getFrame().
     */
    class PylonStream {
        public:
            PylonStream();
            ~PylonStream();

            //This function will hang, without a timeout, untill a new frame arrives or
            //the stream is explicitly terminated.
            //The returned frame pointer MUST be deleted as soon as possible once no longer
            //needed. See 'class PylonFrame' above.
            //Camera does not need to be streaming and no notification is sent at stopStreaming()
            PylonFrame * getFrame();
            //Immidiately returns the number of frames in the buffer.
            int poll();
            //This can be used to wake up threads waiting for frames, but is NOT required.
            //Doesn't stop the camera if its running, just stops accepting frames
            //and poll() / getFrame will immidately return 0/NULL.
            //Can be used to signal end of stream to other threads.
            void terminateStream();

            //Insert a frame into the stream. Should only be used by the PylGigE-object.
            void putFrame(PylonFrame *f);
        private:
            pthread_mutex_t _bufferMutex;
            pthread_cond_t _bufferCond;
            std::deque<PylonFrame*> _buffer;
            bool _terminate;
    };//end PylonStream

    class CustomBufferFactory;

    class PylGigEConfig;
    class PylGigEBasicConfig;
    class PylGigEHdrConfig;

    class PylGigE {
        public:
            PylGigE();
            ~PylGigE();

            /* Open and configure all devices with matching MAC-address.
             * Input: 
             *	macAddr: list of cameras to open, using camera MAC addresses.
             *	configuration: the struct containing parameters to set.
             * Output:
             *	streams: Pointers to a simple stream-buffer, one for each camera, to be used when
             *	extracting frames. Indices correspond to input mac addr.
             * Returns: true on success, false on failure.
             */
            bool open(const std::vector<std::string> &macAddr, std::vector<PylonStream*> &streams,
                    const PylonConfig &configuration = PylonConfig());

            /* Open all available devices in undefined order.
             * Input:
             *	configuration: the struct containing parameters to set.
             * Output:
             *	streams: Pointers to a simple stream-buffer, one for each camera, to be used when
             *	extracting frames.
             * Returns: true on success
             */
            bool open(std::vector<PylonStream*> &streams,
                    const PylonConfig &configuration = PylonConfig());

            /* Closes all opened devices. Cleans up everything and resets the
             * PylGigE-object to it's original state, ready for 'open' to be called.
             * All handles are invalidated. All returned buffers are invalidated,
             * potentially freed. Generated streams will be deleted and can no longer be used.
             * All returned PylonFrames should also be deleted before calling this function. 
             */
            void close();

            /* Provide a pre-allocated buffer-pool.
             * Cameras must be opened and must not be streaming.
             * The bufferByteSize of each buffer must be >= width * height * bytesPerPixel + 128.
             * The number of buffers should be more than 5 * number of cameras.
             * The pointers provided by 'buffers' must be successfully
             * allocated, and not deallocated while the cameras are
             * streaming, or while there are un-released frames. Do not provide the same
             * buffers to multiple 'useCustomBuffers'-calls.
             * Cameras must be open and must not be streaming.
             * Contains no error checking with regard to size of the buffer, compared
             * to the required size of a frame, but an error message will be printed.
             *
             * Input:
             *	buffers        : Array of pointers to pre-allocated buffers.
             *	bufferByteSize : Size of each buffer
             * Returns: true on success, false if too few buffers
             */
            bool useCustomBuffers(const std::vector<void*> &buffers, size_t bufferByteSize);

            /* Begin to continuously capture frames from all opened cameras.
             * Will begin fetching frames immediately. Call this once done
             * with other initialization, ready to capture frames.
             * If function returns false, it usually means that cameras have timed out
             * and must be closed, re-opened and re-configured before trying again.
             * Returns: true on success
             */
            bool startStreaming();

            /* Stops capturing frames. Does not de-allocate or reset configuration,
             * you can re-start the stream with startStreaming().
             */
            void stopStreaming();

            /* Apply a given exposure and gain configuration to all open cameras.
             * May give warning messages if stream is stopped while setting exposure, but
             * does not crash as a result.
             */
            void setExposure(float newExposureValue, float newGainValue);

            /* Provide RGB multipliers to slightly adjust white balance of a single camera.
             * f ex, 1.01 : 1.002 : 1.012 (will slightly boost all colors).
             * NB: Effect will be a little odd on a camera that performs auto-exposure (i.e., pilotcam)
             */
            void adjustWhitebalance(size_t idx, float r, float g, float b);


            //Don't touch!
            //Internal thread-loop
            void grabLoop();
        private:

            friend class PylGigEConfig;
            friend class PylGigEBasicConfig;
            friend class PylGigEHdrConfig;

            Pylon::CDeviceSpecificInstantCameraArrayT<Pylon::CBaslerGigEInstantCamera> _cameras;
            std::vector<Pylon::CBaslerGigEDeviceInfo> _camDevInfo;
            std::vector<PylonStream*> _streams; 
            CustomBufferFactory * _bufferFactory;

            size_t _size;
            bool _isOpen;

            pthread_t _grabThread;

            //Timing variables
            std::vector<int64_t> _ticksPerUsec; 
            std::vector<struct timeval> _basePcTime;
            std::vector<int64_t> _baseCamTime;

            //Configuration variables
            int _pilot;
            PylGigEConfig *_config;

            pthread_mutex_t _configMutex;

            bool init(const PylonConfig &conf);

            bool reconnect();

            bool testDisconnected(int camIdx);

    };//end PylGigE

    class PylGigEConfig {
        public:
            PylGigEConfig(PylGigE * master, const PylonConfig &conf)
                : _gige(master), _configuration(conf), _size(master->_size), _pilot(master->_pilot)
            { }
            virtual ~PylGigEConfig() { };
            virtual bool beginExposureLoop() = 0;
            virtual bool setConfig() = 0;
            virtual void checkUpdateExposure(Pylon::CGrabResultPtr) = 0;
            virtual void setExposure(float newExposureValue, float newGainValue) = 0;
            virtual void addExposure(PylonFrame *f) = 0;
            virtual void reconnect() { };//Not implemented

            void adjustWhitebalance(size_t idx, float r, float g, float b);
        protected:
            bool applySharedConfig(size_t idx);

            friend class PylGigE;

            PylGigE * _gige;
            PylonConfig _configuration;
            size_t _size;
            int _pilot;
            float _currentGain;
    };//end PylGigEConfig

    //Private functions to start internal threads
    void * grabLoopStarter(void * param);

}; //end namespace

