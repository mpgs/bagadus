// AUTHOR(s): Sigurd Ljodal,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#ifndef protocol_h
#define protocol_h

enum pkg_type
{
    pkg_type_record,
    pkg_type_exposure,
    pkg_type_config,
    pkg_type_shutdown,
    pkg_type_whitebalance
};

struct pkg_header
{
    // Size of the package (including header)
    uint32_t size;
    // Package type, see emum pkg_type
    uint32_t type;
    // TODO Should probably have some magic identifier here
} __attribute__ ((__packed__));

struct pkg_record
{
    // All packages start with a header
    struct pkg_header header;

    // Then comes the actual package data
    uint8_t record;
} __attribute__ ((__packed__));

struct pkg_exposure
{
    // All packages start with a header
    struct pkg_header header;

    // Then comes the actual package data
    float exposure;
    float gain;
} __attribute__ ((__packed__));

struct pkg_config
{
    // All packages start with a header
    struct pkg_header header;

    // Dolphin node id
    int node_id;
    // Exposure base multiplication factor
    int baseExposure;
    // Recording frame rate
    int fps;
	int width;
	int height;
	// bool, turns on hdr mode 
	int hdr_mode;
	
    // Size of dolphin buffers. TODO Remember to set this to 2x frame size for hdr
    int buffer_size;

	// One camera is globally the "pilot", controlling exposure
	int pilot_cam;

} __attribute__ ((__packed__));

struct pkg_whitebalance
{
    // All packages start with a header
    struct pkg_header header;

    // Then comes the actual package data
    int cam_idx;
    float red;
    float green;
    float blue;
} __attribute__ ((__packed__));


#endif
