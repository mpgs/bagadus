// AUTHOR(s): Sigurd Ljodal,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "server.h"
#include "logging.h"

#include <event2/event.h>
#include <event2/listener.h>
#include <event2/buffer.h>
#include <event2/bufferevent.h>

//
// ** Logging defines **
//

// Connection struct
typedef struct connection
{
    struct bufferevent *bev;
    struct connection  *next;
    struct connection  *prev;
} connection_t;

// Callback struct
typedef struct callback
{
    server_cb       callback; // Function to call
    void            *userdata; // User data to send with callback
    enum pkg_type   type;

    struct callback *next;
    struct callback *prev;
} callback_t;

// Server struct
struct server
{
    connection_t            *connections;
    struct evconnlistener   *listener;

    uint32_t                client_count;

    // Package to be sent whenever a client connects
    struct pkg_config       *connect_pkg;

    // Information for the ready callback
    uint32_t                ready_count;
    server_ready_cb         ready_cb;
    void                    *ready_ctx;

    // Schedule callbacks
    callback_t              *callbacks;
};

// Server callbacks
static void server_accept(struct evconnlistener *listener, evutil_socket_t fd,
        struct sockaddr *address, int socklen, void *ctx);
static void server_error(struct evconnlistener *listener, void *ctx);

// Buffer event callbacks
static void server_read(struct bufferevent *bev, void *ctx);
static void server_event(struct bufferevent *bev, short events, void *ctx);

/****************************************
 *
 * Server
 *
 ****************************************/
server_t *server_init(struct event_base *evbase, uint16_t port) {
    LOG_D("Using port %d", port);

    // Allocate
    server_t *s = malloc(sizeof(server_t));
    if (!s) {
        LOG_E("Unable to allocate memory for the server.");
        return NULL;
    }

    // Initialize
    s->callbacks    = NULL;
    s->connections  = NULL;
    s->connect_pkg  = NULL;
    s->client_count = 0;
    s->ready_count  = 0;
    s->ready_cb     = NULL;
    s->ready_ctx    = NULL;

    // Set up the socket
    struct sockaddr_in sin;
    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = 0;
    sin.sin_port = htons(port);

    // Create a new listener
    s->listener = evconnlistener_new_bind(evbase, server_accept, s,
            LEV_OPT_CLOSE_ON_FREE | LEV_OPT_REUSEABLE, -1,
            (struct sockaddr *) &sin, sizeof(sin));
    if (!s->listener)
    {
        free(s);

        LOG_E("Unable to create socket listener.");
        return NULL;
    }

    evconnlistener_set_error_cb(s->listener, server_error);

    return s;
}

void server_broadcast(server_t *s, void *buf, size_t len) {
    connection_t *c;
    for (c = s->connections; c; c = c->next) {
        bufferevent_write(c->bev, buf, len);
    }
}

void server_shutdown(server_t *s)
{
    // Create shutdown message
    struct pkg_header pkg = {
        .size = sizeof(struct pkg_header),
        .type = pkg_type_shutdown
    };

    // Get the event base
    struct event_base *base = evconnlistener_get_base(s->listener);

    // Send shutdown message to all clients
    connection_t *c = s->connections;
    for (;c;c = c->next) {
        // Flush the buffer
        bufferevent_write(c->bev, &pkg, sizeof(struct pkg_header));
        bufferevent_flush(c->bev, EV_READ | EV_WRITE, BEV_FINISHED);

        // Close the socket
        int fd = bufferevent_getfd(c->bev);
        shutdown(fd, SHUT_RDWR);

        // Free the buffer event
        bufferevent_free(c->bev);

        // Clear memory struct
        free(c);
    }

    // Exit the event base loop
    event_base_loopbreak(base);
}

int server_register_cb(server_t *s, enum pkg_type type, server_cb callback,
        void *userdata)
{
    // Allocate memory
    callback_t *cb = malloc(sizeof(callback_t));
    if (!cb) {
        LOG_E("Unable to allocate memory for callback struct.");
        return 0;
    }

    // Initialize
    cb->callback        = callback;
    cb->userdata        = userdata;
    cb->type            = type;
    cb->prev            = NULL;

    // Add to callbacks list
    if (!s->callbacks) {
        s->callbacks    = cb;
        cb->next        = NULL;
    } else {
        cb->next        = s->callbacks;
        cb->next->prev  = cb;
        s->callbacks    = cb;
    }

    return 1;
}

int server_register_ready_cb(server_t *s, uint32_t count,
        server_ready_cb callback, void *userdata)
{
    s->ready_count  = count;
    s->ready_cb     = callback;
    s->ready_ctx    = userdata;

    return 1;
}

int server_set_config_pkg(server_t *s, struct pkg_config *pkg)
{
    if (s->connect_pkg) {
        LOG_W("Overwriting old connect callback");
    }

    s->connect_pkg = pkg;

    return 1;
}
/****************************************
 *
 * Server callbacks
 *
 ****************************************/
static void server_accept(struct evconnlistener *listener, evutil_socket_t fd,
        struct sockaddr *address, int socklen, void *ctx)
{
    server_t *s = ctx;

    /* We got a new connection! Set up a bufferevent for it. */
    struct event_base *base = evconnlistener_get_base(listener);
    struct bufferevent *bev = bufferevent_socket_new(base, fd,
            BEV_OPT_CLOSE_ON_FREE);
    if (!bev) {
        LOG_E("Unable to create bufferevent");
        return;
    }

    // Set up the buffer
    bufferevent_setcb(bev, server_read, NULL, server_event, ctx);
    bufferevent_enable(bev, EV_READ | EV_WRITE);

    // Store the connection in the linked list
    connection_t *c = malloc(sizeof(connection_t));
    c->bev = bev;
    c->next = s->connections;
    if (s->connections != NULL)
        s->connections->prev = c;
    c->prev = NULL;
    s->connections = c;

    // Count clients
    s->client_count++;

    // If we have a configuration package, send it now
    if (s->connect_pkg) {
        bufferevent_write(bev, s->connect_pkg, sizeof(struct pkg_config));
    }

    // Check if all clients have connected
    if (s->client_count == s->ready_count && s->ready_cb) {
        s->ready_cb(s, s->ready_ctx);
    }
}

static void server_error(struct evconnlistener *listener, void *ctx)
{
    LOG_E("An error occured with the listener.");
}


/****************************************
 *
 * Buffer event callbacks
 *
 ****************************************/
static void server_read(struct bufferevent *bev, void *ctx)
{
    // Get the buffer
    struct evbuffer *buf = bufferevent_get_input(bev);

    // Get the client
    server_t *s = ctx;

    for (;;) {
        size_t avail = evbuffer_get_length(buf);
        if (avail < sizeof(struct pkg_header)) return;

        // Get the header
        struct pkg_header header;
        evbuffer_copyout(buf, &header, sizeof(struct pkg_header));

        // Entire package not delivered yet
        if (avail < header.size) return;

        // Read out the actual package
        char pkg[header.size];
        evbuffer_remove(buf, pkg, header.size);

        // Trigger all registered callbacks for this package type
        callback_t *cb = s->callbacks;
        for(;cb;cb = cb->next) {
            if (cb->type == header.type) cb->callback((struct pkg_header *)pkg,
                    cb->userdata);
        }
    }
}

static void server_event(struct bufferevent *bev, short events, void *ctx)
{
    server_t *s = ctx;
    connection_t *c = s->connections;

    if (events & BEV_EVENT_ERROR)
        LOG_E("Error from bufferevent.");
    if (events & (BEV_EVENT_EOF | BEV_EVENT_ERROR)) {
        LOG_D("Client disconnected.");

        for(;c && c->bev != bev; c = c->next) {}

        if (c) {
            if(c->prev != NULL) {
                c->prev->next = c->next;
            } else {
                s->connections = c->next;
            }
            if (c->next) c->next->prev = c->prev;

            free(c);
            LOG_I("Connection closed.");
        } else {
            LOG_W("Unknown connection closed.");
        }

        bufferevent_free(bev);

        s->client_count--;
    }
}
