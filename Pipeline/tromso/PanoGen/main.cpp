// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 


#include "PanoGen.hpp"

// #define OPPOSITE_ROTATION //Ullevaal mount is rotated 180 degrees different than Alfheim atm

/* 
 * USAGE: ./program inputconfig.txt image1.jpg image2.jpg ...
 *
 * output configfile is staticly defined, if you wish to preserve
 * your input file config then rename it.
 */


class GUIControl{
	public:
		OutputImage * image;
		unsigned int currentActive;
		double rotAmount;
		double resized;

		void rotateImage(cv::Mat &src, cv::Mat &dst){
			cv::Point2f pt(src.rows/2., src.cols/2.);
			cv::Mat r = cv::getRotationMatrix2D(pt, -90, 1.0);
			cv::warpAffine(src, dst, r, cv::Size(src.rows, src.cols));
		}

		void loadConfig(int argc, char ** argv){
			std::ifstream f;
			f.open(std::string(argv[1]), std::ios::in);
			if(!(f.is_open())){
				std::cerr << "Error opening config file " << std::string(argv[1]) << std::endl;
				exit(1);
			}

			resized = 1.0;

			unsigned int outW, outH, inW, inH, numS;
			int xShift, yShift, yOff;
			double fov;

			f >> outW;
			f >> outH;
			f >> xShift;
			f >> yShift;
			f >> inW;
			f >> inH;
			f >> fov;
			f >> numS;

			if(argc < (int)(numS+2)){
				std::cerr << "Too few image arguments for this config file, " << argc-2
					<< " vs " << numS << std::endl;
				exit(1);
			}
			if(argc != (int)(numS+2))
				std::cout << "Warning, config uses fewer images than given\n";

			InputImage * input = new InputImage[numS];
			for(unsigned int i=0; i<numS; i++){
				double a, x, y, z, foc;
				cv::Mat img;
				img	= cv::imread(argv[2+i], CV_LOAD_IMAGE_COLOR);

				f >> a;
				f >> yOff;
				f >> x;
				f >> y;
				f >> z;
				f >> foc;

				if(img.rows != (int)inH || img.cols != (int)inW){
					if(img.rows == (int)inW && img.cols == (int)inH){
						cv::Mat dup;
						cv::transpose(img, dup);
						cv::flip(dup, dup, 1);
						input[i].init(dup, a, yOff, foc, x, y, z, true);
					}else{
						std::cerr << "Image " << std::string(argv[2+i]) <<
							" has different dimension ( " << img.cols << ":"
							<< img.rows << " ) than config file ( " << inW
							<< ":" << inH << " ). Exiting\n";
						exit(1);
					}
				}else{
					input[i].init(img, a, yOff, foc, x, y, z, true);
				}

			}

			image = new OutputImage(input, numS, fov, outW, outH, xShift, yShift, true);

			f.close();
		}

		GUIControl(int argc, char ** argv){
			if(argc < 3){
				std::cerr << "Missing arguments. Need minimum config-file followed by an image.";
				exit(1);
			}

			currentActive = 0;
			rotAmount = 0.04;

			loadConfig(argc, argv);
		}

		GUIControl(OutputImage * img){
			image = img;
			currentActive = 0;
			rotAmount = 0.04;
		}

		bool handleCommand(int key){
			switch(key){
				// STUFF THAT MODIFIES THE MAPPING / PROJECTIONS
				case 'w'   : image->sources[currentActive].updateRotation(-rotAmount, 0, 0);
							 return true;
				case 'a'   : image->sources[currentActive].updateRotation(0, rotAmount, 0);
							 return true;
				case 's'   : image->sources[currentActive].updateRotation(rotAmount, 0, 0);
							 return true;
				case 'd'   : image->sources[currentActive].updateRotation(0, -rotAmount, 0);
							 return true;
				case 'q'   : image->sources[currentActive].updateRotation(0, 0, rotAmount);
							 return true;
				case 'e'   : image->sources[currentActive].updateRotation(0, 0, -rotAmount);
							 return true;

				case 'j'   : image->sources[currentActive].angleOffset -= rotAmount/2;
							 return true;
				case 'l'   : image->sources[currentActive].angleOffset += rotAmount/2;
							 return true;
#ifdef OPPOSITE_ROTATION
				case 'k'   : image->sources[currentActive].yOffset += (int)(rotAmount*256);
							 return true;
				case 'i'   : image->sources[currentActive].yOffset -= (int)(rotAmount*256);
							 return true;
#else
				case 'i'   : image->sources[currentActive].yOffset += (int)(rotAmount*256);
							 return true;
				case 'k'   : image->sources[currentActive].yOffset -= (int)(rotAmount*256);
							 return true;
#endif

				case 'o'   : image->sources[currentActive].focalMultiplier -= (rotAmount/2);
							 return true;
				case 'u'   : image->sources[currentActive].focalMultiplier += (rotAmount/2);
							 return true;

							 //ArrowKeys 
				case 65361 : for(uint32_t i=0; i<image->numSources; ++i)
								 image->sources[i].angleOffset -= rotAmount/2;
							 return true;
				case 65363 : for(uint32_t i=0; i<image->numSources; ++i)
								 image->sources[i].angleOffset += rotAmount/2;
							 return true;
				case 65362 : for(uint32_t i=0; i<image->numSources; ++i)
								 image->sources[i].yOffset += (int)(rotAmount*256);
							 return true;
				case 65364 : for(uint32_t i=0; i<image->numSources; ++i)
								 image->sources[i].yOffset -= (int)(rotAmount*256);
							 return true;

							 //SHIFT + arrowkeys
				case 130897 : image->changeDim(-rotAmount*300, 0);
							  return true;
				case 130899 : image->changeDim(rotAmount*300, 0);
							  return true;
				case 130900 : image->changeDim(0,-rotAmount*200);
							  return true;
				case 130898 : image->changeDim(0,rotAmount*200);
							  return true;


							  // STUFF THAT DOESN'T MODIFY
				case 65505 : return false; //Shift			 
				case 65451 : // numpad + 
				case '+'   : rotAmount *= 2; return false;
				case 65453 : // numpad - 
				case '-'   : rotAmount /= 2; return false;
							 //Resizing. '1' for smaller '2' for larger
				case '1'   : resized /= 1.1; std::cout << "Resized image: " << resized << std::endl;
							 return false;
				case '2'   : resized *= 1.1; std::cout << "Resized image: " << resized << std::endl;
							 return false;
							 //TAB
				case  9    : if(++currentActive==image->numSources) currentActive = 0;
								 return false;

				case 'p'   :  std::cout << "Image " << currentActive+1 << " : Offset " <<
							  image->sources[currentActive].angleOffset << " :  Rotation( "
								  << image->sources[currentActive].rotX << " , "
								  << image->sources[currentActive].rotY << " , "
								  << image->sources[currentActive].rotZ << " ) : Focal "
								  << image->sources[currentActive].focalMultiplier << "\n";
							  return false;


				default    : std::cout << "Keypress: "<< key <<
							 ". Make sure numlock is OFF" << std::endl;
							 return false;
			}

			return true;
		}

		void tweakLoop(){
			bool first = true;
			Clock::time_point start;
			cv::Mat realImg;
			while(true){
				if(first) start = Clock::now();
				image->makeMapping();
				if(first)
					std::cout<< "Map time: "<< sec(Clock::now()-start).count()*1000 << " ms\n";

				bool reMap = false;
				while(!reMap){
					if(first) start = Clock::now();
					image->fillPixels();
					if(first){
						std::cout << "Fill time: "<<sec(Clock::now()-start).count()*1000 <<" ms\n";
						first = false;
					}
					realImg = cv::Mat(image->destHeight,
							image->destWidth, CV_8UC3, image->destImage);
					cv::Mat modifiedImg;
					if(resized <= 1.05 && resized >= 0.95){
						modifiedImg = cv::Mat(image->destHeight, image->destWidth,
								CV_8UC3, image->destImage);
					}else{ 
						cv::resize(realImg, modifiedImg, cv::Size(), resized, resized);
					}

					int fontFace = CV_FONT_HERSHEY_SIMPLEX;
					for(unsigned int i=0; i<image->numSources; i++){
						std::ostringstream n;
						n << i+1;
						if(i == currentActive){
							cv::putText(modifiedImg, n.str(),
									cv::Point((i*2+1)*(modifiedImg.cols/(image->numSources*2)),
										modifiedImg.rows/2), fontFace, 3,
									cv::Scalar(0,0,250,200), 4, 8);
						}else{ 
							cv::putText(modifiedImg, n.str(),
									cv::Point((i*2+1)*(modifiedImg.cols/(image->numSources*2)),
										modifiedImg.rows/2), fontFace, 3,
									cv::Scalar(0,250,250,100), 4, 8);
						}

					}
					std::ostringstream n2;
					n2 << "Mod: " << rotAmount;
					cv::putText(modifiedImg, n2.str(), cv::Point(modifiedImg.cols/2, 40),
							fontFace, 1, cv::Scalar(0,250,250,100), 1, 8);

					cv::imshow("ImageWindow", modifiedImg);
					int k = cv::waitKey(0);

					if(k == 27){ // escape
						image->fillPixels();
						cv::Mat pano = cv::Mat(image->destHeight,
								image->destWidth, CV_8UC3, image->destImage);
						cv::imwrite("Panorama.jpg", pano);
						return;
					}

					reMap = handleCommand(k);
				}

			}
		}

		void saveConfig(char * fileName){
			std::ofstream f;
			std::streambuf * buf;
			f.open(std::string(fileName), std::ios::out);
			bool noFile = !(f.is_open());
			if(noFile){
				std::cerr << "Unable to open file " << fileName <<
					"\nWriting to terminal instead\n\n";
				buf = std::cout.rdbuf();
			}else buf = f.rdbuf();
			std::ostream out(buf);

			out << image->destWidth << " " << image->destHeight << " "
				<< image->xShift << " " << image->yShift << std::endl;
			out << image->srcWidth << " " << image->srcHeight << " "
				<< image->srcFov << " " << image->numSources << std::endl;
			for(unsigned int i=0; i<image->numSources; i++)
				out << image->sources[i].angleOffset << " " << image->sources[i].yOffset
					<< " " << image->sources[i].rotX << " "
					<< image->sources[i].rotY << " " << image->sources[i].rotZ
					<< " " << image->sources[i].focalMultiplier << std::endl;

			if(!noFile)
				f.close();

			image->makeIndividualMap();
		}

};

int main(int argc, char** argv)
{
	/* Bit odd, but easier to let guiControl create
	 * the ouput image from command line args */
	GUIControl control(argc, argv);
	control.tweakLoop();
	char fname[] = OUTPUT_CONFIG_NAME;
	control.saveConfig(fname);

	exit(EXIT_SUCCESS);
}
