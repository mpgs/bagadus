// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "PanoGen.hpp"

void InputImage::createConstants(double radius){
	/* http://mathworld.wolfram.com/RotationMatrix.html
	 * This is just all rotations multipled to one matrix */

	using namespace cv;
	Mat Rx = (Mat_<double>(3, 3) <<
			1,         0,          0,
			0,  cos(rotX), sin(rotX),
			0, -sin(rotX), cos(rotX));

	// Rotation matrices around the Y axis
	Mat Ry = (Mat_<double>(3, 3) <<
			cos(rotY), 0, -sin(rotY),
			0,          1,           0,
			sin(rotY), 0,  cos(rotY));

	// Rotation matrices around the Z axis
	Mat Rz = (Mat_<double>(3, 3) <<
			cos(rotZ), sin(rotZ), 0,
			-sin(rotZ), cos(rotZ), 0,
			0, 0,  1);

	Mat r = (Rx * Ry * Rz);
	for(int i=0; i<3; i++)
		for(int j=0; j<3; j++)
			rotMatrix[j][i] = r.at<double>(j,i);

	focalLength = radius*focalMultiplier;
}

/* For simplicity, these angles may be in degrees */
void InputImage::init(cv::Mat src, double angle, int yOff, double focal, double xR, double yR, double zR, bool inRadians){
	width = src.cols;
	height = src.rows;
	yOffset = yOff;
	focalMultiplier = focal;
	img = new unsigned char[width*height*3];
	memcpy(img, src.data, width*height*3); //hardcoded CV pixelsize 3 BGR format!
	angleOffset = (inRadians) ? angle : angle*M_PI / 180;
	rotX = (inRadians) ? xR : xR*M_PI / 180;
	rotY = (inRadians) ? yR : yR*M_PI / 180;
	rotZ = (inRadians) ? zR : zR*M_PI / 180;
}

void InputImage::updateRotation(double xR, double yR, double zR){
	rotX += xR;
	rotY += yR;
	rotZ += zR;
}

