// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "PanoGen.hpp"

OutputImage::OutputImage(InputImage * input, unsigned int numInput, double fov, int width, int height, int shiftRight, int shiftDown, bool inRadians){
	sources = input;
	numSources = numInput;
	/* If its larger than 2*PI we assume this is meant as degrees */
	srcFov = (inRadians && fov < (M_PI*2)) ? fov : fov * M_PI / 180;
	destWidth = (int)(width / 32.0f) * 32;
	destHeight = (int)(height / 8.0f) * 8;
	srcWidth = input[0].width;
	srcHeight = input[0].height;

	xShift = shiftRight;
	yShift = shiftDown;

	destImage = new unsigned char[3 * width * height];
	map = new DynPixPoint[width * height];
	if(numInput > 1)
		stitches = new int[numInput-1];
	else stitches = NULL;

	curIdx = 0;
	pthread_mutex_init(&idxMutex, 0);
	pthread_barrier_init(&stitcherBarrier, 0, numInput+1);
	stitcherThreads = new pthread_t[numInput];
	for(unsigned int i=0; i<numInput; ++i)
		pthread_create(&stitcherThreads[i], 0, &stitcherThreadStarter, (void*) this);
}

OutputImage::~OutputImage(){
	delete destImage;
	if(stitches) delete stitches;
	delete map;
}

int OutputImage::getId(){
	pthread_mutex_lock(&idxMutex);
	int a = curIdx++;
	pthread_mutex_unlock(&idxMutex);
	return a;
}

void OutputImage::changeDim(double w, double h){
	destWidth += ((int)(w/32))*32;
	destHeight += ((int)(h/8))*8;
	delete destImage;
	delete map;
	destImage = new unsigned char[3 * destWidth * destHeight];
	map = new DynPixPoint[destHeight * destWidth];
}

/* This is the main function for projecting a vertical column on the output image,
 * from a single source image. Correct source image must be identified first */
void OutputImage::projectX(InputImage * in, DynPixPoint * mapping, int dstX, int stride, int minX){
	float horizontalAngle = ((dstX+xShift) - (destWidth/2) ) / radius;
	/* Set up our coordinate system so that the center
	 * of the image is at (x,y,z)=(0,0,focalLength) */
	horizontalAngle -= in->angleOffset;
	if(horizontalAngle == 0)
		horizontalAngle = 0.000001;//0 yields NaN

	/* Carhesian coordinates for the cylindrical surface */
	float cX, cY, cZ;
	cX = radius * sin(horizontalAngle);
	cZ = radius * cos(horizontalAngle);
	for(int dstY=0; dstY<destHeight; dstY++){

		cY = (dstY+yShift) - (destHeight/2);

		/* Lets rotate the cylinder according to the rotation of the
		 * spesific image. It's easier than rotating the image.
		 * However, it comes at a cost. Once this is done, we can no
		 * longer use the radius, which significantly slows down
		 * computations. */
		double cRotatedX, cRotatedY, cRotatedZ;

		cRotatedX = cX * in->rotMatrix[0][0] +
			cY * in->rotMatrix[0][1] +
			cZ * in->rotMatrix[0][2];

		cRotatedY = cX * in->rotMatrix[1][0] +
			cY * in->rotMatrix[1][1] +
			cZ * in->rotMatrix[1][2];

		cRotatedZ = cX * in->rotMatrix[2][0] +
			cY * in->rotMatrix[2][1] +
			cZ * in->rotMatrix[2][2];
		/* Now we have a vector from 0,0,0 to cRotatedX/Y/Z.
		 * The input image will (or won't) intersect with this
		 * vector somewhere. */
		double iX, iY, iZ;

		/* The only known point is Z, how far away we place the image. */
		iZ = in->focalLength;

		/* Find horizontal angle */
		double alpha = atan(cRotatedX / cRotatedZ);
		iX = tan(alpha) * iZ;

		/* Find the vertical angle */
		/* However, to do this with standard trig, we need another
		 * edge (close or hyp). The close edge is the same as
		 * the hyp of previous triangle.
		 * Finding iY is damn slow computationally speaking */
		double beta = atan( (cRotatedY*sin(alpha)) / cRotatedX);
		iY = tan(beta) * sqrt(iX*iX+iZ*iZ);

		/* Finally, map this to image space */
		float imgX = iX + srcWidth/2;
		float imgY = iY + srcHeight/2 + in->yOffset;

		/* Are we within source image bounds? */
#ifdef CROP_FOR_BAYER
		if(ceil(imgX+2) >= srcWidth || imgX < 2 || ceil(imgY+2) >= srcHeight || imgY < 2)
#else
		if(ceil(imgX) >= srcWidth || imgX < 0 || ceil(imgY) >= srcHeight || imgY < 0)
#endif
		{
			/* Not within bounds, output will be black/undefined */
			mapping[ dstY*stride + (dstX-minX) ].x = -1;
			mapping[ dstY*stride + (dstX-minX) ].y = -1;
		}else{
			mapping[ dstY*stride + (dstX-minX) ].x = imgX;
			mapping[ dstY*stride + (dstX-minX) ].y = imgY;
		}
	}
}

void OutputImage::makeIndividualMap(){
	FILE * f;
	if(!(f = fopen(OUTPUT_DYN_MAP_NAME, "wb"))){
		std::cerr << "Unable to open output file: " << OUTPUT_DYN_MAP_NAME << " for writing. Cannot dump the map to file.\n";
		return;
	}

	fprintf(f,"%d %d \n %d %d %d", destWidth, destHeight, srcWidth, srcHeight, numSources);

	for(unsigned int i=0; i<numSources; ++i){
		int maxX = (i==(numSources-1)) ? destWidth : stitches[i]+DYNAMIC_STITCH_AREA_HALF;
		int minX = (i)?stitches[i-1]-DYNAMIC_STITCH_AREA_HALF:0;
		fprintf(f,"\n %d %d \n", minX, maxX);
	}
	for(unsigned int i=0; i<numSources; ++i){
		InputImage * in = &sources[i];
		int maxX = (i==(numSources-1)) ? destWidth : stitches[i]+DYNAMIC_STITCH_AREA_HALF;
		int minX = (i)?stitches[i-1]-DYNAMIC_STITCH_AREA_HALF:0;
		int stride = maxX-minX;

		DynPixPoint * individualMap = new DynPixPoint[stride*destHeight];

		for(int dstX = minX; dstX<maxX; ++dstX){
			projectX(in, individualMap, dstX, stride, minX);
		}
		fwrite((void *) individualMap, sizeof(DynPixPoint), stride*destHeight, f);
		delete individualMap;
	}
	fclose(f);
}

unsigned char * OutputImage::fillPixels(){
	if(!destImage)
		destImage = new unsigned char[3 * destWidth * destHeight];

	unsigned char * dst = destImage;
	for(int y=0; y<destHeight; y++){
		int target = 0;
		for(int x=0; x<destWidth; x++, dst+=3){
			if(x >= stitches[target] && target != (int)(numSources-1)) target++;
			DynPixPoint *p = &(map[destWidth*y + x]);
			if(p->x < 0){
				dst[0] = 0;
				dst[1] = 0;
				dst[2] = 0;
			}else{
				int px = (int)round(p->x);
				int py = (int)round(p->y);
				unsigned char * src = &sources[target].img[(py * srcWidth + px)*3];
				dst[0] = src[0];
				dst[1] = src[1];
				dst[2] = src[2];
			}
		}
	}
	return destImage;
}

void OutputImage::makeMapping(){
	/* Radius determines a lot of how the panorama will look. */
	radius = srcWidth / (2 * tan( srcFov/2 ));
	for(unsigned int i=1; i<numSources; ++i){
		float a = (sources[i-1].angleOffset - sources[i].angleOffset) / 2 + sources[i].angleOffset;
		/* THIS MUST BE AN EVEN NUMBER, so rounding down to nearest even number here */
		stitches[i-1] = (((int) (a * radius) + destWidth/2)>>1)<<1;
	}

	pthread_barrier_wait(&stitcherBarrier);
	pthread_barrier_wait(&stitcherBarrier);
}

void OutputImage::runStitcherThread(){
	int id = getId();

	InputImage * in = &sources[id];
	int * maxX = (id==(int)(numSources-1)) ? &destWidth : &stitches[id];

	while(true){

		pthread_barrier_wait(&stitcherBarrier);
		if(!maxX) break;

		in->createConstants(radius);
		for(int dstX = (id)?stitches[id-1]:0; dstX<*maxX; ++dstX){
			projectX(in, map, dstX, destWidth, 0);
		}
		pthread_barrier_wait(&stitcherBarrier);
	}
}

void * stitcherThreadStarter(void * p){
	OutputImage *param = (OutputImage *) p;
	param->runStitcherThread();
	pthread_exit(0);
}

#ifdef __APPLE__
int pthread_barrier_init(pthread_barrier_t *barrier,
		const pthread_barrierattr_t *attr, unsigned int count)
{
    if(count == 0)
    {
        errno = EINVAL;
        return -1;
    }
    if(pthread_mutex_init(&barrier->mutex, 0) < 0)
    {
        return -1;
    }
    if(pthread_cond_init(&barrier->cond, 0) < 0)
    {
        pthread_mutex_destroy(&barrier->mutex);
        return -1;
    }
    barrier->tripCount = count;
    barrier->count = 0;

    return 0;
}

int pthread_barrier_destroy(pthread_barrier_t *barrier)
{
    pthread_cond_destroy(&barrier->cond);
    pthread_mutex_destroy(&barrier->mutex);
    return 0;
}

int pthread_barrier_wait(pthread_barrier_t *barrier)
{
    pthread_mutex_lock(&barrier->mutex);
    ++(barrier->count);
    if(barrier->count >= barrier->tripCount)
    {
        barrier->count = 0;
        pthread_cond_broadcast(&barrier->cond);
        pthread_mutex_unlock(&barrier->mutex);
        return 1;
    }
    else
    {
        pthread_cond_wait(&barrier->cond, &(barrier->mutex));
        pthread_mutex_unlock(&barrier->mutex);
        return 0;
    }
}
#endif
