// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <iostream>
#include <cstdlib>
#include "../Modules/hdr/utils/defines.h"

#include "../Modules/hdr/radiancemapper/radiancemapper_calibrate.h"

#include "../Modules/hdr/weightfunction/weightfunction.h"
#include "../Modules/hdr/weightfunction/weightfunction_simple.h"
#include "../Modules/hdr/utils/image.h"


void readImage(RadianceMapperCalibrate* rm, char* path, float exposure)
{
	Image* img_1 = new Image();
	img_1->loadPng(path);
	img_1->setExposureTime(exposure);
	img_1->setAperture(2.8f);
	rm->addImage(img_1);
}


int main(int argc, char** argv)
{
	if(argc < 5 || (argc - 1) % 2 != 0){
		fprintf(stderr, "Missing Argument. At least two images with: path, exposure time in usek\n");
		exit(EXIT_FAILURE);
	}


	RadianceMapperCalibrate* radiance_mapper = new RadianceMapperCalibrate();
	WeightFunction* wf = new WeightFunctionSimple(0,255);
	radiance_mapper->setWeightFunction(wf);
	//load images
	for(int i = 1; i < argc; i += 2)
	{
		readImage(radiance_mapper, argv[i], atof(argv[i + 1]) );
	}

	//calibrate
	radiance_mapper->calculateResponseFunction(1000);

	//write result
	radiance_mapper->writeResponseFunctionToFile("calibrated_response_function.txt");

}
