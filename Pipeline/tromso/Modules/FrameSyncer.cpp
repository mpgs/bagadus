// AUTHOR(s): Sigurd Ljodal,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "FrameSyncer.hpp"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

#include "Helpers/logging.h"

//
// ### Constructor
//

// Constructor, perhaps do stuff here.
FrameSyncer::FrameSyncer(size_t frame_size, uint8_t cams, unsigned int id)
{
    this->cams          = cams;
    this->frame_size    = frame_size;
    this->buffer_size   = frame_size;

    this->max_wait      = {0, (1000*1000)/(int)fps};
    this->first_max_wait = {0, (1500*1000)/(int)fps};
    this->threshold     = (750 * 1000) / fps;

#if FRAMESYNCER_USE_DOLPHIN
    // Initialize segment ids
    this->sid_rw        = id;
    this->sid_headers   = id+1;
    this->sid_frames    = id+2;
#endif
}

//
// ### Setup
//

bool FrameSyncer::setHDRMode(bool hdr)
{
    // If already initialized, we can't change to HDR mode as buffers has to
    // be reallocated and that is handled in `initialize`.
    if (initialized) return false;

    this->hdr = hdr;

    // If we are running in HDR mode, the buffer size is two times the size of
    // a single frame. Otherwise it is the same size as a single frame.
    this->buffer_size = hdr ? frame_size * 2 : frame_size;

    return true;
}

// Configure the syncer. This function performes different things depending
// on the compile flags.
bool FrameSyncer::initialize(uint8_t *r_ptr, uint8_t *w_ptr,
        struct header **h_ptr, void **f_ptr)
{
    if (initialized) {
        LOG_D("Already initialized");
        return true;
    }

    if (cams == 0) {
        LOG_E("Camera count is set to 0");
        return false;
    }

    if (fps == 0) {
        LOG_E("Frame rate is set to 0");
        return false;
    }

#if FRAMESYNCER_USE_CUDA
    LOG_E("Cuda support is not yet implemented");
    return false;
#endif

#if FRAMESYNCER_USE_DOLPHIN
    if (r_ptr || w_ptr || h_ptr || f_ptr) {
        LOG_W("Pointers were passed to FrameSyncer::initalize, "
                "these are ignored because it is compiled for Dolphin use.");
    }

    // Allocate memory for the arrays of dolphin descriptors
    sd_frames = new sci_desc_t[cams];

    seg_frames = new sci_local_segment_t[cams];
    map_frames = new sci_map_t[cams];
    map_headers = new sci_map_t[cams];

    // Allocate memory for the header and frame arrays
    headers = new volatile struct header *[cams];
    frames = new volatile void *[cams];

    sci_error_t err;

    // Get the size of a page, for use when allocating segments.
    long page_size = (unsigned int)sysconf(_SC_PAGESIZE);
    if (page_size < 0) {
        LOG_E("Unable to get page size");
        goto open_failure;
    }

    // Open a new SISCI virtual device
    SCIOpen(&sd_rw, NO_FLAGS, &err);
    SCIErrorGoto(err, "SCIOpen", open_failure);

    SCIOpen(&sd_headers, NO_FLAGS, &err);
    SCIErrorGoto(err, "SCIOpen", open_failure);

    for (int i = 0; i < cams; i++) {
        SCIOpen(&sd_frames[i], NO_FLAGS, &err);
        SCIErrorGoto(err, "SCIOpen", open_failure);
    }

    // Create segments all segments needed
    SCICreateSegment(sd_rw, &seg_rw, sid_rw, page_size,
            NULL, NULL, NO_FLAGS, &err);
    SCIErrorGoto(err, "SCICreateSegment", create_failure);

    SCICreateSegment(sd_headers, &seg_headers, sid_headers,
            FRAMESYNCER_BUFFER_COUNT * cams * sizeof(**headers),
            NULL, NULL, NO_FLAGS, &err);
    SCIErrorGoto(err, "SCICreateSegment", create_failure);

    for (int i = 0; i < cams; i++) {
        SCICreateSegment(sd_frames[i], &seg_frames[i], sid_frames+i,
                FRAMESYNCER_BUFFER_COUNT * buffer_size,
                NULL, NULL, NO_FLAGS, &err);
        SCIErrorGoto(err, "SCICreateSegment", create_failure);
    }

    // Map read array into user address space
    read = (volatile uint8_t *)SCIMapLocalSegment(seg_rw, &map_r,
            0, page_size,
            NULL, NO_FLAGS, &err);
    SCIErrorGoto(err, "SCIMapLocalSegment", map_failure);

    // Mape write array into user address space
    write = read + (page_size >> 1);

    // Now map up the header and frame arrays
    for (int i = 0; i < cams; i++) {
        headers[i] = (struct header *)SCIMapLocalSegment(seg_headers,
                &map_headers[i],
                FRAMESYNCER_BUFFER_COUNT * sizeof(**headers) * i,
                FRAMESYNCER_BUFFER_COUNT * sizeof(**headers),
                NULL, SCI_FLAG_READONLY_MAP, &err);
        SCIErrorGoto(err, "SCIMapLocalSegment", map_failure);

        frames[i] = SCIMapLocalSegment(seg_frames[i], &map_frames[i], 0,
                FRAMESYNCER_BUFFER_COUNT * buffer_size,
                NULL, SCI_FLAG_READONLY_MAP, &err);
        SCIErrorGoto(err, "SCIMapLocalSegment", map_failure);
    }

    // Prepare all segments
    SCIPrepareSegment(seg_rw, adapter, NO_FLAGS, &err);
    SCIErrorGoto(err, "SCIPrepareSegment", prepare_failure);

    SCIPrepareSegment(seg_headers, adapter, NO_FLAGS, &err);
    SCIErrorGoto(err, "SCIPrepareSegment", prepare_failure);

    for (int i = 0; i < cams; i++) {
        SCIPrepareSegment(seg_frames[i], adapter, NO_FLAGS, &err);
        SCIErrorGoto(err, "SCIPrepareSegment", prepare_failure);
    }
#else
    if (!r_ptr || !w_ptr || !h_ptr || !f_ptr) {
        return false;
    }

    // Read and write arrays
    read = r_ptr;
    write = w_ptr;

    // Frame and header buffers
    frames = f_ptr;
    headers = h_ptr;
#endif

#if FRAMESYNCER_USE_CUDA

#else
    // Error checking
    frameset.ptr = malloc(cams * buffer_size);
    if (!frameset.ptr) {
        LOG_E("Unable to allocate memory for the frameset");
#if FRAMESYNCER_USE_DOLPHIN
        goto frameset_failure;
#else
        return false;
#endif
    }
#endif

    // Set all read and write locations to zero
    for (int i = 0; i < cams; i++) {
        read[i]  = 0;
        write[i] = 0;
    }

#if FRAMESYNCER_USE_DOLPHIN
    // Everything is ready to be used, so set the segments as available.
    SCISetSegmentAvailable(seg_rw, adapter, NO_FLAGS, &err);
    SCIErrorGoto(err, "SCIPrepareSegment", set_avail_failure);

    SCISetSegmentAvailable(seg_headers, adapter, NO_FLAGS, &err);
    SCIErrorGoto(err, "SCIPrepareSegment", set_avail_failure);

    for (int i = 0; i < cams; i++) {
        SCISetSegmentAvailable(seg_frames[i], adapter, NO_FLAGS, &err);
        SCIErrorGoto(err, "SCIPrepareSegment", set_avail_failure);
    }
#endif

    dropped_camera = new int[cams];
    current_dropped = new uint_fast64_t[cams];

    initialized = true;

    return true;

#if FRAMESYNCER_USE_DOLPHIN
set_avail_failure:
frameset_failure:
prepare_failure:
map_failure:
create_failure:
    SCIClose(sd_rw, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIClose");

    SCIClose(sd_headers, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIClose");

    for (int i = 0; i < cams; i++) {
        SCIClose(sd_frames[i], NO_FLAGS, &err);
        SCIErrorWarn(err, "SCIClose");
    }

open_failure:
    // Clean up allocated memory
    delete[] sd_frames;

    delete[] seg_frames;
    delete[] map_frames;
    delete[] map_headers;

    delete[] frames;
    delete[] headers;

    // TODO Proper cleanup
    return false;
#endif
}

void FrameSyncer::setFrameRate(uint32_t fps)
{
    this->fps               = fps;
    this->max_wait          = {0, (1000*1000)/(int)fps};
    this->first_max_wait    = {0, (1500*1000)/(int)fps};
    this->threshold         = (750 * 1000) / fps;
}

//
// ### Streaming
//

void FrameSyncer::streamStart()
{
    pthread_mutex_lock(&stream_mutex);

#ifndef FRAMEWRITER_KEEP_RUNNING
    // Reset all read and write locations to zero
    for (int i = 0; i < cams; i++) {
        read[i]  = 0;
        write[i] = 0;
    }
#endif

    // Reset deadline, so that we will wait for a new first frame
    deadline = {0, 0};

    for (int i = 0; i < cams; i++) {
        dropped_camera[i] = 0;
        current_dropped[i] = 0;
    }

    // Set streaming state
    streaming = true;
    pthread_mutex_unlock(&stream_mutex);
}

void FrameSyncer::streamStop()
{
    pthread_mutex_lock(&stream_mutex);
    streaming = false;
    pthread_mutex_unlock(&stream_mutex);
}

//
// ### Getting frames
//

// If this returns `NULL`, that means that we have reached the end of the
// current recording and that the callee should stop it's work for now.
struct frame *FrameSyncer::getFrame(struct frame *f)
{
    //LOG_D("FrameSyncer::getFrame - call");
    // Check if we are streaming, and if we are not, return `false`.
    pthread_mutex_lock(&stream_mutex);
    if (!streaming) {
        pthread_mutex_unlock(&stream_mutex);
        //LOG_D("FrameSyncer::getFrame - return null 1");
        return NULL;
    }
    pthread_mutex_unlock(&stream_mutex);

//     struct timespec cpuStart, globStart, cpuEnd, globEnd;
//     clock_gettime(CLOCK_MONOTONIC, &globStart);
//     clock_gettime(CLOCK_THREAD_CPUTIME_ID, &cpuStart);

    // Try to get a frameset (blocking call).
    if (!getFrameset()) {
        //LOG_D("FrameSyncer::getFrame - return null 2");
        return NULL;
    }

    // If a resource wasn't provided, we must allocate memory for a new one.
    if (!f) {
        f = (struct frame *)malloc(sizeof(struct frame));

        f->ptr = malloc(cams * buffer_size);
    }

#if FRAMESYNCER_USE_CUDA
    cuMemcpyDtoH(f->ptr, frameset, cams * buffer_size);
#else
    memcpy(f->ptr, frameset.ptr, cams * buffer_size);
#endif

    // Set the header
    f->hdr = frameset.hdr;

    // When we have delivered a frame, we set the deadline for the next frame.
    if (deadline.tv_sec != 0) {
        timeradd(&deadline, &max_wait, &deadline);
    } else {
//         gettimeofday(&deadline, NULL);
		struct timespec cur_time;
        //Intensionally using MONOTONIC instead of MONOTONIC_RAW,
        //since non-raw is subject to NTP slew correction.
        //However, we don't use realtime clock since that may suddenly jump
		clock_gettime(CLOCK_MONOTONIC, &cur_time);
		deadline = {cur_time.tv_sec, cur_time.tv_nsec/1000};
        timeradd(&deadline, &first_max_wait, &deadline);
    }

//     clock_gettime(CLOCK_MONOTONIC, &globEnd);
//     clock_gettime(CLOCK_THREAD_CPUTIME_ID, &cpuEnd);
//     float t1 = (globEnd.tv_sec * 1.0e3 + (globEnd.tv_nsec * 1.0e-6)) -
//         (globStart.tv_sec * 1.0e3 + (globStart.tv_nsec * 1.0e-6));
//     float t2 = (cpuEnd.tv_sec * 1.0e3 + (cpuEnd.tv_nsec * 1.0e-6)) -
//         (cpuStart.tv_sec * 1.0e3 + (cpuStart.tv_nsec * 1.0e-6));
//     LOG_D("T: %8.2fms CT: %8.2fms", t1, t2);

    // TODO: Remove this
    //LOG_D("FrameSyncer::getFrame - return");
    return f;
}

#ifdef HAVE_CUDA
struct frame *FrameSyncer::getCudaFrame(struct frame *f)
{
    // Check if we are streaming, and if we are not, return `false`.
    pthread_mutex_lock(&stream_mutex);
    if (!streaming) {
        pthread_mutex_unlock(&stream_mutex);
        return NULL;
    }
    pthread_mutex_unlock(&stream_mutex);

    // Try to get a frameset (blocking call).
    if (!getFrameset()) {
        return NULL;
    }

    // If a resource wasn't provided, we must allocate memory for a new one.
    if (!f) {
        f = (struct frame *)malloc(sizeof(struct frame));

        cuMemAlloc(&f->cuPtr, cams * buffer_size);
    }

#if FRAMESYNCER_USE_CUDA
    cuMemcpyDtoD(f->cuPtr, frameset.cuPtr, cams * buffer_size);
#else
    cuMemcpyHtoD(f->cuPtr, frameset.ptr, cams * buffer_size);
#endif

    // Set the header
    f->hdr = frameset.hdr;

    // When we have delivered a frame, we set the deadline for the next frame.
    if (deadline.tv_sec != 0) {
        timeradd(&deadline, &max_wait, &deadline);
    } else {
//         gettimeofday(&deadline, NULL);
		struct timespec cur_time;
		clock_gettime(CLOCK_MONOTONIC, &cur_time);
		deadline = {cur_time.tv_sec, cur_time.tv_nsec/1000};
        timeradd(&deadline, &first_max_wait, &deadline);
    }

    // TODO: Remove this
    return f;
}

struct frame *FrameSyncer::getCudaArrayFrame(struct frame *f)
{
    // Check if we are streaming, and if we are not, return `false`.
    pthread_mutex_lock(&stream_mutex);
    if (!streaming) {
        pthread_mutex_unlock(&stream_mutex);
        return NULL;
    }
    pthread_mutex_unlock(&stream_mutex);

    // Try to get a frameset (blocking call).
    if (!getFrameset()) {
        return NULL;
    }

    // If a resource wasn't provided, we must allocate memory for a new one.
    if (!f) {
        LOG_E("Allocating a new cuda array is currently not "
                "supported");

        return NULL;
    }

#if FRAMESYNCER_USE_CUDA
    cuMemcpyDtoA(f->cuArrPtr, 0, frameset.cuPtr,
            cams * buffer_size);
#else
    cuMemcpyHtoA(f->cuArrPtr, 0, frameset.ptr,
            cams * buffer_size);
#endif
    // Set the header
    f->hdr = frameset.hdr;

    // When we have delivered a frame, we set the deadline for the next frame.
    if (deadline.tv_sec != 0) {
        timeradd(&deadline, &max_wait, &deadline);
    } else {
//         gettimeofday(&deadline, NULL);
		struct timespec cur_time;
		clock_gettime(CLOCK_MONOTONIC, &cur_time);
		deadline = {cur_time.tv_sec, cur_time.tv_nsec/1000};
        timeradd(&deadline, &first_max_wait, &deadline);
    }

    // TODO: Remove this
    return f;
}
#endif

//
// Teardown
//

bool FrameSyncer::terminate()
{
    return true;
}

FrameSyncer::~FrameSyncer()
{
    LOG_I("I quit!");
}

//
// ### Helper functions
//

// This function returns the number of frames currently available for the
// specified camera.
uint8_t FrameSyncer::availableFrames(uint8_t camera)
{
    uint8_t r = read[camera];
    uint8_t w = write[camera];
    //LOG_W("Avail %d - read: %d, write: %d", camera, r, w);
    if (r > w) {
        return FRAMESYNCER_BUFFER_COUNT - r + w;
    } else {
        return w - r;
    }
}

// This function will drop a frame if we have two or more available frames from
// the camera specified.
bool FrameSyncer::dropFrame(uint8_t camera)
{
    if (availableFrames(camera) < 2) {
        // There are fewer than two frames available, so we can't drop a frame.
        return false;
    } else {
        // Get the current location and increase it by one, wrapping around if
        // necessary.
        read[camera] = (read[camera] + 1) % FRAMESYNCER_BUFFER_COUNT;
//         LOG_D("Camera %d read pointer %d", camera, read[camera]);
//         LOG_D("Camera %d write pointer %d", camera, write[camera]);
        return true;
    }
}

bool FrameSyncer::matchFrames()
{
    // We loop until we either have a set of frames, or are unable to get a
    // matching set of frames.
    for (;;) {

        uint8_t ready = 0;
        uint8_t dropped = 0;
        uint8_t available[cams];
        //First, test if we have available frames from all cameras
        for(uint8_t i=0; i<cams; ++i) {
            available[i] = availableFrames(i);
            if(available[i] > 0){
                dropped_camera[i] = 0;
                current_dropped[i] = 0;
                ready++;
            }else{
                //If we've recently dropped a lot of frames from this camera,
                //we start to ignore it and mark this camera as dropped
                if(current_dropped[i] > fps){
                    dropped_camera[i] = 1;
                    ready++;
                    dropped++;
                }
            }
        }

        //If we don't have frames from all cameras, or if every camera
        //has dropped frames, we return false
        if(ready != cams || dropped == cams){
            return false;
        }

        //The camera we are using as the default header should not have
        //dropped status, so we switch to one that isn't
        for(int i=0;i<cams;++i){
            if(!dropped_camera[pilot_hdr_idx]) break;
            pilot_hdr_idx = (pilot_hdr_idx+1)%cams;
        }

        // Get the timestamp from the first camera
        volatile struct timeval *oldest = &headers[pilot_hdr_idx][read[pilot_hdr_idx]].timestamp;
        volatile struct timeval *newest = oldest;

        // Currently, camera 0 is smallest and largest
        uint8_t oldest_cam = 0;

        // Loop through the cameras and find the oldest and newest frame
        for (int i = 0; i < cams; i++) {

            //Ignore dropped cameras
            if(dropped_camera[i]) continue;

            volatile struct timeval *ts = &headers[i][read[i]].timestamp;
            if (time_cmp(oldest, ts) < 0) {
                oldest = ts;
                oldest_cam = i;
            }

            if (time_cmp(newest, ts) > 0) {
                newest = ts;
            }
        }

        // Check the diff between min and max frames
        int_fast64_t diff = time_cmp(oldest, newest);
        if (diff < threshold) {
            return true;
        }

        // If this is the first frame, we should check if it's old.
        if (deadline.tv_sec == 0) {
            struct timeval now;
            gettimeofday(&now, NULL);

            // If the frame is more than a second old, drop it
            if (time_cmp(&now, oldest) > 1000*1000) {
                if (!dropFrame(oldest_cam)) {
                    LOG_D("Dropped old frame from camera %d", oldest_cam);
                    return false;
                } else {
                    continue;
                }
            }
        }

        // Try to drop a frame. If we are not able to drop a frame, it's not
        // possible to get a matching set of frames, and we return `false`.
        if (!dropFrame(oldest_cam)) {
            return false;
        } else {
            LOG_I("Throwing frame - cam %u - diff %ld", oldest_cam, diff);
        }
    }
}

// Returns `true` if a frame should be returned. If `false` is returned, it
// means that streaming has been stopped and we should push `null`.
bool FrameSyncer::getFrameset()
{
    // We also have a struct in which we can store the current time of day
    struct timeval now;

    while( 1 ) {
        // Check if we are streaming, and if we are not, return `false`.
        pthread_mutex_lock(&stream_mutex);
        if (!streaming) {
            pthread_mutex_unlock(&stream_mutex);
            return false;
        }
        pthread_mutex_unlock(&stream_mutex);

        // Loop through the frames and try to match one from each camera
        if (matchFrames()) {
            // We should now update the read pointers so that they point to the
            // frames that make up the set
            copyToSet();

            // We were able to get a set, so break out of the loop and return
            // the set retrieved.
            return true;
        } else {
            //fprintf(stderr, "Unable to get a set of frames\n");


			// Get the current time
			//gettimeofday(&now, NULL);
			struct timespec cur_time;
			clock_gettime(CLOCK_MONOTONIC, &cur_time);
			now = {cur_time.tv_sec, cur_time.tv_nsec/1000};

			if ( ! (deadline.tv_sec == 0 || time_cmp(&deadline, &now) <= 0) ) break;

			usleep(1000);
		}
    }

    for(int i=0; i<cams; ++i){
        if(availableFrames(i) < 1){
            current_dropped[i]++;
        }
    }

    // TODO
    // When we end up here, a timeout occured. This should be reflected in the
    // header of the returned set.
//     LOG_I("Syncer dropping frame. Deadline: %ld.%06ld. Now: %ld.%06ld",
// 			deadline.tv_sec, deadline.tv_usec, now.tv_sec, now.tv_usec);
    LOG_I("Syncer dropping frame. Deadline/Now diff: %.3fms", time_cmp(&deadline, &now)/1000.0);

    timeradd(&frameset.hdr.timestamp, &max_wait, &frameset.hdr.timestamp);
    frameset.hdr.frameNum += 1;

    frameset.hdr.flags |= FRAME_DROPPED;

    return true;
}

void FrameSyncer::copyToSet()
{
#if FRAMESYNCER_USE_DOLPHIN
    memcpy(&frameset.hdr, (const void *)&headers[pilot_hdr_idx][read[pilot_hdr_idx]],
            sizeof(struct header));
#else
    frameset.hdr = headers[pilot_hdr_idx][read[pilot_hdr_idx]];
#endif

    //Set correct size
    frameset.hdr.size *= cams;

    uint8_t *ptr = (uint8_t *)frameset.ptr;

    if (hdr) {
        for (int i = 0; i < cams; i++) {
            
            //ignore dropped cameras, no copy operation is performed since the
            //old data lying in the buffer will be used.
            if(dropped_camera[i]) continue;

            void *src_first = (uint8_t *)frames[i] + (read[i] * buffer_size);
            void *dst_first = ptr + (i * frame_size);

            void *src_second =
                (uint8_t *)frames[i] + (read[i] * buffer_size + frame_size);
            void *dst_second = ptr + (i * frame_size + frame_size * cams);

#if FRAMESYNCER_USE_CUDA
            cuMemcpyDtoD((CUdeviceptr) dst_first, (CUdeviceptr) src_first,
                    frame_size);
            cuMemcpyDtoD((CUdeviceptr) dst_second, (CUdeviceptr) src_second,
                    frame_size);
#else
            memcpy(dst_first, src_first, frame_size);
            memcpy(dst_second, src_second, frame_size);
#endif

            read[i] = (read[i] + 1) % FRAMESYNCER_BUFFER_COUNT;
            //LOG_D("Camera %d read pointer %d", i, read[i]);
            //LOG_D("Camera %d write pointer %d", i, write[i]);
        }
    } else {
        for (int i = 0; i < cams; i++) {
            
            //ignore dropped cameras, no copy operation is performed since the
            //old data lying in the buffer will be used.
            if(dropped_camera[i]) continue;

            void *src = (uint8_t *)frames[i] + (read[i]*buffer_size);
            void *dst = ptr + (i*buffer_size);

#if FRAMESYNCER_USE_CUDA
            cuMemcpyDtoD((CUdeviceptr) dst, (CUdeviceptr) src, buffer_size);
#else
            memcpy(dst, src, buffer_size);
#endif

            read[i] = (read[i] + 1) % FRAMESYNCER_BUFFER_COUNT;
            //LOG_D("Camera %d read pointer %d", i, read[i]);
            //LOG_D("Camera %d write pointer %d", i, write[i]);
        }
    }
}

// Returns the time from t0 to t1, so if t0 is newer a negative value is
// returned, otherwise a positive value is returned. The returned value is the
// number of µs between the timestamps
inline int_fast64_t FrameSyncer::time_cmp(volatile struct timeval *t0,
        volatile struct timeval *t1)
{
    // Calculate the difference between the parts of the structs.
    int_fast64_t sec  = t1->tv_sec  - t0->tv_sec;
    int_fast64_t usec = t1->tv_usec - t0->tv_usec;

    // Return number of micro seconds in difference
    return (sec * 1000 * 1000) + usec;
}
