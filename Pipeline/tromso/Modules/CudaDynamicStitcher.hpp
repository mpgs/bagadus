// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#ifndef CUDA_DYNAMICSTITCHER_HPP
#define CUDA_DYNAMICSTITCHER_HPP

#include "Module.hpp"

#include <pthread.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define GPU_STITCHER_PRINT_DEBUGTIME
// #define DONT_ROTATE //Uncomment if images dont need to be rotated during stitching
// #define USE_STATIC_STITCH //Uncomment to disable dynamic stitching

/* THIS IS CONFIGURED FOR ALFHEIM SCENARIO!
 * Algorithm will usually skip pathfinding in the top and bottom of the image,
 * to reduce unnecessary flickering and reduces runtime. If the length of these
 * arrays differ from the number of seams, or from each other, we use no skipping.
 * This defines the number of pixels to bypass at the top and bottom, for each seam,
 * left to right.  */
#define PATHFINDING_SKIP_TOP {440,300,280,420}
#define PATHFINDING_SKIP_BOT {100,50,50,100}
// #define PATHFINDING_SKIP_TOP {0}
// #define PATHFINDING_SKIP_BOT {0}
// #define PATHFINDING_SKIP_TOP {300,500}
// #define PATHFINDING_SKIP_BOT {50,60}

#define WHITEBALANCE_FREQUENCY 90 //nr of frames to average when testing color diff between overlap

/* These values are all read from the lookup table / map file.
 * Describes the state of the read map-file. Input width/height is
 * what is expected from the camera, opposite of what is read from the file.
 * Unless "DONT_ROTATE" define is
 * active, this means it will be rotated 90 degrees. */
struct ContextVariables{
	int inWidth, inHeight;
	int outWidth, outHeight;
	int numSources;
};

namespace CudaStitcher{

	void *consumerStarter(void * context);

	struct Node{
		/* Cost of stitching across this node */
		float selfCost;
		/* Lowest cost of children nodes, left/center/right */
		int bestChild; /* 0,1,2 */
	};
	struct ImageStitch{
		/* shows horizontal boundries. this spesific image, once projected correctly, will
		 * "cover" all pixels from minX to maxX in the output panorama. This includes overlap
		 * for dynamic seam usage. Stride is maxX-minX, size of projected output (single) image */
		int minX, maxX, stride;

		int skipTop, skipBot;
		
		/* Size/stride of the overlapping area between 2 images.
		 * This is set for image 0-(numSources-1) */
		int stitchW;
		/* Only used during startup to transfer to gpu */
		float2 * cpuMap;

		/* DEVICE POINTERS */
		/* This is the mapping for one image projection.
		 * It is panorama-height tall and stride wide. */
		cudaArray * gpuMap;
		cudaTextureObject_t mapTex;
		/* Temporary storage. Each image is put into its own global memory area before
		 * being memcopied together into a stitched panorama */
		uint8_t * output;
		/* Graph for pathfinding the dynamic seam */
		Node * graph;
		/* The dynamic seam is saved as an array of indices,
		 * saving the x-offset at each y-location */
		int * stitchResult;
		/* Several things can run in paralell for optimal performance */
		cudaStream_t stream[2];
		/* We temporarily write the difference in luminosity to a surface object within the
		 * stitching areas. This allows for easy access when performing blending */
		cudaSurfaceObject_t diff;
		cudaArray * diffMap;
	};

    struct RgbRatio {
        RgbRatio() : r(0.0),g(0.0),b(0.0) { }
        RgbRatio(double red, double green, double blue) : r(red),g(green),b(blue) { }
        RgbRatio(double all) : r(all),g(all),b(all) { }
        RgbRatio(double * all) : r(all[0]),g(all[1]),b(all[2]) { }

        RgbRatio operator+(RgbRatio other) { return RgbRatio(r+other.r,g+other.g,b+other.b); }
        RgbRatio operator-(RgbRatio other) { return RgbRatio(r-other.r,g-other.g,b-other.b); }
        void operator+=(RgbRatio other) { r+=other.r; g+=other.g; b+=other.b; }
        RgbRatio operator/(double val) { return RgbRatio(r/val,g/val,b/val); }

        double r,g,b;
    };
    class WhiteBalanceAdjustment {
        public:
            WhiteBalanceAdjustment(int numSources, void (*cb)(int, RgbRatio), int center)
                : stage(0), frameCounter(-60), numSources(numSources), center(center),
                callback(cb), accumulatedRatios(std::vector<RgbRatio>(numSources-1)) { }

            void add(RgbRatio * rgbRatios);
        private:
            int stage;
            int frameCounter;
            int numSources;
            int center;
            void (* callback)(int, RgbRatio);
            std::vector<RgbRatio> accumulatedRatios;
    };
};

class CudaDynamicStitcher : public FetchModule {
	public:
		CudaDynamicStitcher();
		~CudaDynamicStitcher();
		/* Reads the pre-computed map.
		 * THIS MUST BE DONE BEFORE initializing
		 * Output dimensions etc are placed in the given ContextVariables struct.
		 * Returns false on unrecoverable failure.
		 * */
		bool parseMap(char * mapName, ContextVariables * result,
                void (*whiteBalCb)(int, CudaStitcher::RgbRatio) = NULL, int expoPilot = -1);

		void init(FetchModule *producer);

		/* Specified in CudaModuleInterface.
		 * getResource is the expected call,
		 * getArrResource performs a memcpy and should be avoided */
		struct frame * getCudaFrame(struct frame *buffer);
		struct frame * getCudaArrayFrame(struct frame *buffer);

		void runConsumer();

	private:
		bool initialized, tmpBufferAllocated;
		bool running[2];
		int numSources;
		size_t srcSize, dstSize;
		/* Rembember that these values are how the input data arrives.
		 * These will be reversed several places due to image rotation */
		int srcWidth, srcHeight;
		/* Size of output panorama */
		int dstWidth, dstHeight;
		/* Alternating buffer, 0/1 */
		int inputIdx;
		pthread_barrier_t startWork;
		/* Consumer thread, continously retrieving new input data */
		pthread_t consumer;
		/* Generic stream. Most of the kernel operations are performed in
		 * paralell, using individual streams per image or stitch */
		cudaStream_t stream;
		FetchModule *producer;
		/* Statically allocated input buffers */
		struct frame * inputData[2];
		/* Most kernels use similar config, so placed for convinience */
		dim3 block, grid, edgeGrid;
		/* Contains everything related to the dynamic seam essentially */
		CudaStitcher::ImageStitch * images;

        CudaStitcher::WhiteBalanceAdjustment * whiteBalanceAdjuster;

		/* Device-only data pointers */
		void * tmpCudaPointer;

		/* Called on termination */
		void deleteCudaArrays();
		/* Called on startup */
		void initCudaArrays();
		/* Called per frame, stitching a single set of input to one output */
		void stitchImage(cudaArray * input, uint8_t * output);
};

#endif
