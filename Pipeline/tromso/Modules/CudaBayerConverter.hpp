// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#ifndef CUDA_BAYERCONVERTER_HPP
#define CUDA_BAYERCONVERTER_HPP

#include "Module.hpp"

#include <pthread.h>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cmath>
#include <cstring>

/* Debug define, to allow this module to deliver data directly to encoder.
 * Otherwise, the data is stored as yuv444i (uchar4 Y-U-V-_, interleaved format)
 * If consumer uses getArrResource, this should have no effect.
 * */
// #define BAYER_CONVERT_TO_PLANAR_YUV

#define GPU_BAYER_PRINT_DEBUGTIME

/* There are multiple different algorithms implemented. See Ragnar's thesis for details.
 * gradient_corr, edge_dir, weighted_4 are good options, ranged from cheapest to slowest.
 * Only 1 of these defines should be active! */
// #define DO_NN
// #define DO_BILINEAR
// #define DO_SMOOTH_HUE
// #define DO_GRADIENT_CORRECTED
// #define DO_FULL_WEIGHTED_ADAPTIVE 4 // Use directional weighting on R/B as well
// #define DO_WEIGHTED_ADAPTIVE 4
// #define DO_WEIGHTED_ADAPTIVE 8
// #define DO_ADAPTIVE_BILINEAR
#define DO_EDGE_DIRECTED
// #define DO_HOMOGENEITY_BASED_EDGE_DIRECTED
// #define DO_GRADIENT_BASED_EDGE_DIRECTED

/* Leave this alone at 0 except for testing. It is not realtime suitable! */
#define MEDIAN_FILTER_ITERATIONS 0


class CudaBayerConverter : public FetchModule {
	public:
		/* Doesn't do anything, init-function starts thread and initialized properly */
		CudaBayerConverter();
		~CudaBayerConverter();
		void init(FetchModule *producer, int width, int height, int numSources,
                bool printFinalImgs=false);

		/* See CudaModuleInterface */
		struct frame * getCudaFrame(struct frame *buffer);
		/* getArrRes... is the expected call */
		struct frame * getCudaArrayFrame(struct frame *buffer);

		/* Retrieves the next set of frames from the producer-module */
		void runConsumer();
	private:
		bool initialized, tmpBufferAllocated;
        bool printLastImages;
		size_t frameSize;
		int width, height, numSources;
		FetchModule *producer;
		/* 2 buffers allocated. One for consumer thread to insert next frame, one for
		 * producer-thread as input for computations. Swaps upon each barrier-iteration */
		struct frame * inputData[2];
		/* Determines which input buffer should be used when calling getResource */
		int inputIdx;
		pthread_barrier_t startWork;
		pthread_t consumer;
		cudaStream_t stream;
		/* Allocated first time consumer asks for a non-cudaArray resource.
		 * Writes to this tmp buffer, then copies to the real outputbuffer */
		cudaArray * tmpCudaArray;

		/* Some algorithms need a lot of buffers. Several algorithms dont include
		 * all of these, in which case they arent allocated/used. */
		cudaArray * bayerFirstPassBuffer;
		cudaArray * bayerSecondPassBuffer;
		cudaArray * bayerFirstPassMaskBuffer;
		cudaArray * medianFilterBuffer;
		dim3 block, grid;

		void convertImage(cudaArray * input, cudaArray * output);
		void convertToPlanar(cudaArray * input, void * output); /* Debug function */
		void initCudaArrays();
		void deleteCudaArrays();
		void freeExtraBuffers();
		void allocateExtraBuffers();
		void performInitialPasses();

		void dbgPrintImages(struct frame * f);
};

namespace CudaBayer{
	void *consumerStarter(void * context);
};

#endif

