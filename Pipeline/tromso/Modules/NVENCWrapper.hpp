// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#ifndef NVENCWRAPPER
#define NVENCWRAPPER

#define NVENC_PRINT_DEBUG_TIME

#include <pthread.h>
#include <stdint.h>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <string>
#include <unistd.h>
#include <fcntl.h>

#include "Module.hpp"

//Describes input format. Module may perform conversion from 422p to NV12. Output always NV12
#define X264_FORMAT_422 1
#define X264_FORMAT_420 2
#define X264_FORMAT_NV12 3

class NVENCWrapper {
    public:
        NVENCWrapper() : m_initialized(false) { }
        ~NVENCWrapper();

        /* runAsMain: If this is true, there is no separate thread created for consuming
         * input. Meaning that runConsumer() (a blocking call) will have to be executed
         * from caller. If this is false, a separate thread will begin. */
        bool init(std::string filepath, int width, int height, int fps, float sekPerFile,
                FetchModule *producer, bool runAsMain=false, int format=X264_FORMAT_422, int outputPipe=0);

        /* Update the root recording directory. Affects next created file.
         * Returns false if the path already exists */
        bool setNewPath(std::string path);

        /* Consumer thread, continually calls getResource on producer to give
         * more input to encoder. Use ONLY if runAsMain (see init) is true,
         * else will be called by a separate internal thread. */
        void runConsumer();
    private:
        class NVENCImpl;
        class FileWriter;
        class YUV422ToNV12 {
            public:
                bool init(int width, int height, NVENCImpl *enc);
                void *swapBuffer();
                ~YUV422ToNV12();
            private:
                bool m_busy, m_terminate, m_firstSwap;
                int m_width, m_height;
                NVENCImpl *m_enc;
                pthread_t m_thread;
                pthread_mutex_t m_lock;
                pthread_cond_t m_condIdle, m_condBusy;
                cudaStream_t m_stream;
                void * m_ptrs[2];
                int m_idx;
                static void * launchThread(void *);
                void run();
                void convert(void * output, void * input, size_t outputStride);
        };
        bool m_initialized;
        bool m_runAsMain;
        NVENCImpl * m_enc;
        FileWriter * m_fileWriter;
        YUV422ToNV12 * m_converter;
        FetchModule * m_producer;
        pthread_t m_consumer;
};

#endif
