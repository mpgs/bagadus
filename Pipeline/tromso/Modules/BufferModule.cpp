// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "BufferModule.hpp"
#include "Helpers/logging.h"
#include <errno.h>
#include <string.h>

BufferModule::~BufferModule(){
    if(!initialized) return;

    pthread_join(consumerThread, NULL);
    for(int i=0; i<numBuffers; ++i){
#ifdef HAVE_CUDA
        cudaSafe(cudaFreeHost(circularBuffer[i].ptr));
#else
        free(circularBuffer[i].ptr);
#endif
    }
    delete[] circularBuffer;
}

bool BufferModule::init(FetchModule *producer, size_t frameSize, int numBuffers){
    this->producer = producer;
    this->frameSize = frameSize;
    this->numBuffers = numBuffers;

    terminated = false;
    headIdx = tailIdx = 0;
    circularBuffer = new struct frame[numBuffers];
    for(int i=0; i<numBuffers; ++i){
#ifdef HAVE_CUDA
        cudaError_t err = cudaMallocHost(&circularBuffer[i].ptr, frameSize);
        if(err != cudaSuccess){
            LOG_E("MallocHost failed in buffermodule: %s", cudaGetErrorString(err));
            return false;
        }
#else
        circularBuffer[i].ptr = malloc(frameSize);
        if(circularBuffer[i].ptr == NULL){
            LOG_E("Malloc failed in buffermodule: %s", strerror(errno));
            return false;
        }
#endif
    }

    pthread_cond_init(&condEmpty, NULL);
    pthread_cond_init(&condFull, NULL);
    pthread_mutex_init(&bufferLock, NULL);
    pthread_create(&consumerThread, NULL, startRunConsumer, this);

    initialized = true;
    return true;
}

struct frame * BufferModule::getFrame(struct frame *buffer){
    if(!buffer){
        LOG_E("Buffer==NULL not implemented in BufferModule");
        return NULL;
    }

    pthread_mutex_lock(&bufferLock);
    while(headIdx == tailIdx){
        if(terminated){
            pthread_mutex_unlock(&bufferLock);
            return NULL;
        }
        pthread_cond_wait(&condEmpty, &bufferLock);
    }
    struct frame * src = &circularBuffer[tailIdx % numBuffers];
    pthread_mutex_unlock(&bufferLock);

    buffer->hdr = src->hdr;
    memcpy(buffer->ptr, src->ptr, frameSize);

    pthread_mutex_lock(&bufferLock);
    tailIdx++;
    pthread_cond_signal(&condFull);
    pthread_mutex_unlock(&bufferLock);

    return buffer;
}

void BufferModule::runConsumer(){
    while(true){
        struct frame * ret = producer->getFrame(&circularBuffer[headIdx % numBuffers]);
        if(!ret) break;//EOF

        pthread_mutex_lock(&bufferLock);
        headIdx++;
        pthread_cond_signal(&condEmpty);

        while( (headIdx - tailIdx) == numBuffers )
            pthread_cond_wait(&condFull, &bufferLock);
        
        pthread_mutex_unlock(&bufferLock);
    }
    pthread_mutex_lock(&bufferLock);
    terminated = true;
    pthread_cond_signal(&condEmpty);
    pthread_mutex_unlock(&bufferLock);
}

void * BufferModule::startRunConsumer(void *arg){
    ((BufferModule*)arg)->runConsumer();
    pthread_exit(0);
}
