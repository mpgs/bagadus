// AUTHOR(s): Ragnar Langseth, Sigurd Ljodal,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#ifndef module_hpp
#define module_hpp

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#ifdef __APPLE__
#include "../Helpers/pthread_barrier.c"
#endif

#ifdef HAVE_CUDA
#include <cuda.h>
#include <cuda_runtime.h>

#define cudaSafe(call){ \
    cudaError_t error; \
    if( (error = call) != cudaSuccess){ \
        fprintf(stderr, "Cuda error: %s\n%s : %d\n", \
                cudaGetErrorString(error), __FILE__,__LINE__); \
        exit(EXIT_FAILURE); \
    } \
}
#endif

#define NO_FLAGS 0

// These are the flags that can be used in the `flags` field of the frame
// header struct.
enum {
    FRAME_DROPPED   =   1 << 0x00,
    FRAME_CUDA      =   1 << 0x01,
    FRAME_CUDA_ARR  =   1 << 0x02
};

extern "C" struct header
{
    struct timeval  timestamp;
    uint32_t        frameNum;
    int32_t         expoFirst;
    int32_t         expoSecond;
    uint32_t        flags;

    uint32_t        size;

    //uint32_t        width;
    //uint32_t        height;
} __attribute__ ((packed));

extern "C" struct frame
{
    struct header   hdr;
    union {
        void       *ptr;

#ifdef __cuda_cuda_h__
        // `CUdeviceptr` is defined as a unsigned long long on 64 bit platforms
        // and should therefore be the same size as a pointer.
        CUdeviceptr cuPtr;

        // When using the runtime API, arrays are referenced as a `CUarray`
        // type. This is compatible with the `cudaArray` type of the runtime
        // API. Since `CUarray` is defined as a pointer, it will be the same
        // size as a `cudaArray` pointer.
        CUarray     cuArrPtr;
#endif

#ifdef __CUDA_RUNTIME_H__
        // When using the runtime API, it's preferable to reference Cuda arrays
        // as `cudaArray` pointers.
        cudaArray_t cuArr;
#endif
    };
};


/**
 * Basic usage:
 * In every module, getResource should be called asap.
 * Most modules implement this as
 * A) Waiting call, waiting on a condition variable for new output.
 * B) Computational call, performing the actual computations of that module.
 * This means that these modules expect getResource to be constantly called,
 * with little delay in between. Assume that NO WORK is done when function
 * isn't called (this isnt always accurate, but still a correct assumption).
 *
 * Most modules (all as of writing this) return NULL at end-of-input,
 * at which point each module terminates.
 *
 * If you don't use the header, remember to copy it along anyways, other
 * modules depend on the information stored here.
 *
 * Should be able to handle dynamically allocating the buffer, or writing
 * to a passed-along pre-allocated (non-NULL) buffer.
 * */
class FetchModule {
    public:
        virtual ~FetchModule() {}
        virtual struct frame *getFrame(struct frame *f=NULL) {
            fprintf(stderr,
                    "[\x1b[01;95mERROR\x1b[0m]"
                    "Called unimplemented function! \n");
            exit(EXIT_FAILURE);
            return NULL;
        };

#ifdef HAVE_CUDA
        // Get a frame in Cuda memory.
        virtual struct frame *getCudaFrame(struct frame *f=NULL) {
            fprintf(stderr,
                    "[\x1b[01;95mERROR\x1b[0m]"
                    "Called unimplemented function! \n");
            exit(EXIT_FAILURE);
            return NULL;
        };

        // Get a frame in a Cuda array
        virtual struct frame *getCudaArrayFrame(struct frame *f=NULL) {
            fprintf(stderr,
                    "[\x1b[01;95mERROR\x1b[0m]"
                    "Called unimplemented function! \n");
            exit(EXIT_FAILURE);
            return NULL;
        };
#endif
};

class PushModule {
    public:
        virtual ~PushModule() {}
        virtual void putFrame(struct frame *f, int id) = 0;
};

#endif
