// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "CudaDynamicStitcher.hpp"
#include <sys/time.h>
#include <Helpers/logging.h>

using namespace CudaStitcher;

texture<uchar4, 2, cudaReadModeNormalizedFloat> yuvTex;

#define GIANT_COST 1000000000 //The cost of purchasing giants at the pet store
#define COST_THRESH 10 //Threshold for deciding to ignore a pixel for blending
//Size of blending region, # of pixels on each side of the stitch. Should be multiple of 4!
//Careful increasing this, increases blending runtime exponentially.
//Under no circumstances set this above 40
#define BLEND_AREA_SIZE 20

// #define SHOW_COST /* Im often too lazy to properly use this define */

#include "stitcher/bicubic.cu"

/* Find the median value of an array of 9 elements */
template <typename T>
__device__ T getMedianFiltered9(T * arr){
#define SWAP(i,j) { \
	const T a = min(arr[i], arr[j]); \
	const T b = max(arr[j], arr[i]); \
	arr[i] = a; arr[j] = b; \
}
	SWAP(0,1); SWAP(0,2); SWAP(0,3); SWAP(0,4); SWAP(0,5); SWAP(0,6); SWAP(0,7); SWAP(0,8);
	SWAP(1,2); SWAP(1,3); SWAP(1,4); SWAP(1,5); SWAP(1,6); SWAP(1,7); SWAP(1,8);
	SWAP(2,3); SWAP(2,4); SWAP(2,5); SWAP(2,6); SWAP(2,7); SWAP(2,8);
	SWAP(3,4); SWAP(3,5); SWAP(3,6); SWAP(3,7); SWAP(3,8);
	SWAP(4,5); SWAP(4,6); SWAP(4,7); SWAP(4,8);
	return arr[4];
#undef SWAP
}

__global__ void getColorOffsets(uint8_t * yA, uint8_t * uA, uint8_t * vA,
        uint8_t * yB, uint8_t * uB, uint8_t * vB,
        int strideA, int strideB, int dstW, int dstH, double * rgb){

    __shared__ double diffShared[256][3];

    double rgbAcc[2][3] = { {0,0,0},{0,0,0} };

	for(int idx=threadIdx.x*2; idx < (dstW*dstH); idx += blockDim.x*2){
		int x, y;
		x = (idx%dstW);
		y = (idx/dstW);

        double cbVal,crVal;
        double yVals;
        double rgb[2][3];

        yVals = (double) yA[strideA*y + x] + yA[strideA*y + x + 1];
        cbVal = (double) uA[(strideA/2)*y + x/2] - 128;
        crVal = (double) vA[(strideA/2)*y + x/2] - 128;

        rgb[0][0] = yVals + (2*1.402 * crVal);
        rgb[0][1] = yVals - (2*0.34414 * cbVal) - (2*0.71414 * crVal);
        rgb[0][2] = yVals + (2*1.772 * cbVal);

        yVals = (double) yB[strideB*y + x] + yB[strideB*y + x + 1];
        cbVal = (double) uB[(strideB/2)*y + x/2] - 128;
        crVal = (double) vB[(strideB/2)*y + x/2] - 128;

        rgb[1][0] = yVals + (2*1.402 * crVal);
        rgb[1][1] = yVals - (2*0.34414 * cbVal) - (2*0.71414 * crVal);
        rgb[1][2] = yVals + (2*1.772 * cbVal);

        //Values near the min/max will behave differently, due to clamping etc, and ratios will be skewed
        if(rgb[0][0] > 5 && rgb[0][0] < 250 && rgb[1][0] > 5 && rgb[1][0] < 250){
            rgbAcc[0][0] += rgb[0][0];
            rgbAcc[1][0] += rgb[1][0];
        }
        if(rgb[0][1] > 5 && rgb[0][1] < 250 && rgb[1][1] > 5 && rgb[1][1] < 250){
            rgbAcc[0][1] += rgb[0][1];
            rgbAcc[1][1] += rgb[1][1];
        }
        if(rgb[0][2] > 5 && rgb[0][2] < 250 && rgb[1][2] > 5 && rgb[1][2] < 250){
            rgbAcc[0][2] += rgb[0][2];
            rgbAcc[1][2] += rgb[1][2];
        }
    }

    diffShared[threadIdx.x][0] = (rgbAcc[0][0]) / (rgbAcc[1][0]);
    diffShared[threadIdx.x][1] = (rgbAcc[0][1]) / (rgbAcc[1][1]);
    diffShared[threadIdx.x][2] = (rgbAcc[0][2]) / (rgbAcc[1][2]);
    __syncthreads();

    //->128
    if(!(threadIdx.x & 1)){
        diffShared[threadIdx.x][0] += diffShared[threadIdx.x + 1][0];
        diffShared[threadIdx.x][1] += diffShared[threadIdx.x + 1][1];
        diffShared[threadIdx.x][2] += diffShared[threadIdx.x + 1][2];
    }
    __syncthreads();

    //->64
    if(!(threadIdx.x & 3)){
        diffShared[threadIdx.x][0] += diffShared[threadIdx.x + 2][0];
        diffShared[threadIdx.x][1] += diffShared[threadIdx.x + 2][1];
        diffShared[threadIdx.x][2] += diffShared[threadIdx.x + 2][2];
    }
    __syncthreads();

    //->32
    if(!(threadIdx.x & 7)){
        diffShared[threadIdx.x][0] += diffShared[threadIdx.x + 4][0];
        diffShared[threadIdx.x][1] += diffShared[threadIdx.x + 4][1];
        diffShared[threadIdx.x][2] += diffShared[threadIdx.x + 4][2];
    }
    __syncthreads();

    //->16
    if(!(threadIdx.x & 15)){
        diffShared[threadIdx.x][0] += diffShared[threadIdx.x + 8][0];
        diffShared[threadIdx.x][1] += diffShared[threadIdx.x + 8][1];
        diffShared[threadIdx.x][2] += diffShared[threadIdx.x + 8][2];
    }
    __syncthreads();

    //->8
    if(!(threadIdx.x & 31)){
        diffShared[threadIdx.x][0] += diffShared[threadIdx.x + 16][0];
        diffShared[threadIdx.x][1] += diffShared[threadIdx.x + 16][1];
        diffShared[threadIdx.x][2] += diffShared[threadIdx.x + 16][2];
    }
    __syncthreads();

    //->4
    if(!(threadIdx.x & 63)){
        diffShared[threadIdx.x][0] += diffShared[threadIdx.x + 32][0];
        diffShared[threadIdx.x][1] += diffShared[threadIdx.x + 32][1];
        diffShared[threadIdx.x][2] += diffShared[threadIdx.x + 32][2];
    }
    __syncthreads();

    //->2
    if(!(threadIdx.x & 127)){
        diffShared[threadIdx.x][0] += diffShared[threadIdx.x + 64][0];
        diffShared[threadIdx.x][1] += diffShared[threadIdx.x + 64][1];
        diffShared[threadIdx.x][2] += diffShared[threadIdx.x + 64][2];
    }
    __syncthreads();

    //->1
    if(!threadIdx.x){
        rgb[0] = ( diffShared[0][0] + diffShared[128][0] ) / 256.0;
        rgb[1] = ( diffShared[0][1] + diffShared[128][1] ) / 256.0;
        rgb[2] = ( diffShared[0][2] + diffShared[128][2] ) / 256.0;
    }
}

/* Kernel for creating a the weights for Dijkstra's algorithm
   dstW = images[i].maxW-images[i+1].minX-1;
   inputA/B points to start of stitch in both images. */
__global__ void createCost(Node * graph, uint8_t * inputA, uint8_t * inputB,
	int srcStrideA, int srcStrideB, int dstW, int dstH, int * prevStitch,
	cudaSurfaceObject_t diffSurf){

	for(int idx=(blockIdx.x*blockDim.x + threadIdx.x);
			idx < (dstW*dstH); idx += (blockDim.x*gridDim.x)){
		int x, y;
		x = (idx%dstW);
		y = (idx/dstW);
		if(!y || y==(dstH-1) || x==(dstW-1)){
			/* Avoid going out of bounds. Upper & lower edges all have zero cost,
			we essentially don't care about them. The right edge of the graph is never touched */
			graph[idx].selfCost = 0.0f;
			continue;
		}
		float cost = 0;

		/* Read in 3x3 pixel grid of luminosity of the first image */
		uint8_t arr[3][3];
		float2 gradient;
		for(int i=0; i<3; ++i){
			for(int j=0; j<3; ++j){
				arr[i][j] = inputA[srcStrideA*(y+i-1) + (x+j-1)];
			}
		}
		int diffToWrite = arr[1][1];
		/* Scharr operator for edge-detection (variation of Sobel) */
		gradient.x = 0.3f*arr[0][0] + arr[1][0] + 0.3f*arr[2][0]
			- 0.3f*arr[0][2] - arr[1][2] - 0.3f*arr[2][2];
		gradient.y = 0.3f*arr[0][0] + arr[0][1] + 0.3f*arr[0][2]
			- 0.3f*arr[2][0] - arr[2][1] - 0.3f*arr[2][2];
		for(int i=0; i<3; ++i){
			for(int j=0; j<3; ++j){
				uint8_t tmp = inputB[srcStrideB*(y+i-1) + (x+j-1)];
				/* Increase the cost based on the absolute difference in luminosity */
				cost += 0.05f*fabsf((int)tmp - (int)arr[i][j]);
				/* save the luminosity difference in a temporary array, used for blending */
				arr[i][j] = tmp;
			}
		}
		diffToWrite = ((int)arr[1][1]) - diffToWrite;
		/* Find the edge-difference between this pixel, and the one from the other image.
		The difference in edge gradient is what I refer to "edge-cost" component */
		gradient.x = fabsf(gradient.x - (0.3f*arr[0][0] + arr[1][0] + 0.3f*arr[2][0]
			- 0.3f*arr[0][2] - arr[1][2] - 0.3f*arr[2][2]));
		gradient.y = fabsf(gradient.y - (0.3f*arr[0][0] + arr[0][1] + 0.3f*arr[0][2]
			- 0.3f*arr[2][0] - arr[2][1] - 0.3f*arr[2][2]));
		cost += gradient.x + gradient.y;

		/* Determine if this pixel is OK to use for blending. If its edge-cost is too high, 
		   it means we likely want to avoid using this pixel to judge overall luminosity change. */
		if(gradient.x+gradient.y > COST_THRESH) diffToWrite = GIANT_COST;
		surf2Dwrite(diffToWrite, diffSurf, x * sizeof(int), y);

		/* Adding a small cost to all nodes that werent a part of the previous stitch */
		int prev = prevStitch[y];
		if(prev>=0){
			prev = abs(prev - x);
			cost += (prev>15)?5:(prev/3.0);
		}
		graph[idx].selfCost = cost;
	}
}

/* Kernel for moving all the pixels that belong to the wrong image, compared
   to the initial memcpy operation. Essentially, if a pixel within a seam area
   is located on the *right* side of the seam, we overwrite the previous value 
   This is not a particularily effective method, lot of random branching, but
   I deem the number of affected pixels low enough to not care. Can be optimized!
   In order to ensure logical transfer of downsampled chromatic channels, the kernel
   handles 2 pixels. At least no two kernels will attept to copy the same chroma values.
   srcW = images[i].stride; dstW = dstWidth;
   xOffset = start of the seam area in the output image.
 */
#if __CUDA_ARCH__ >= 300
__global__ __launch_bounds__(128,16) void copyStitched(int * stitch, uint8_t * input,
		uint8_t * output, int xOffset, int srcW, int dstW, int height, int stitchW){
#else
__global__ void copyStitched(int * stitch, uint8_t * input, uint8_t * output,
		int xOffset, int srcW, int dstW, int height, int stitchW){
#endif

	for(int idx=(blockIdx.x*blockDim.x + threadIdx.x)*2;
			idx < (stitchW*height); idx += (blockDim.x*gridDim.x)*2){
		int x,y;

		x = (idx%stitchW);
		y = (idx/stitchW);
		/* Is this pixel on the wrong side of the seam? The copy Y-channel */
		if(x >= stitch[y]){
			output[x+xOffset + y*dstW] = input[x + y*srcW];
		}
		x = ((idx+1)%stitchW);
		y = ((idx+1)/stitchW);
		/* Is this second pixel on the wrong side? Copy YUV-channels */
		if(x >= stitch[y]){
			output[x+xOffset + y*dstW] = input[x + y*srcW];
			x = x>>1;
			output[dstW*height+x+(xOffset>>1)+y*(dstW>>1)] =
				input[srcW*height+x+y*(srcW>>1)];
			output[((dstW*height*3)>>1)+x+(xOffset>>1)+y*(dstW>>1)] =
				input[((srcW*height*3)>>1)+x+y*(srcW>>1)];
		}
	}
}

/* Kernel for performing optional post-processing blending of the seam.
   diffSurf has been pre-computed, containing the difference in luminecence
   for the two images. stitchW = images[i].stitchW. Output points to beginning
   of seam area. */
#if __CUDA_ARCH__ >= 300
__global__ __launch_bounds__(128,16) void performBlend(Node * graph, int * stitchResult,
		uint8_t * output, int stitchW, int dstW, int dstH, cudaSurfaceObject_t diffSurf){
#else
__global__ void performBlend(Node * graph, int * stitchResult, uint8_t * output, int stitchW,
		int dstW, int dstH, cudaSurfaceObject_t diffSurf){
#endif
	for(int y=(blockIdx.x*blockDim.x + threadIdx.x); y < dstH; y += (blockDim.x*gridDim.x)){
		int stitchX = stitchResult[y];
		int x = stitchX - BLEND_AREA_SIZE;
		/* If the seam is placed near one of the edges we will not blend equally to both sides,
		   instead shifting it to fit inside the "window". No pixels outside seam area known */
		if(x<0) x=0;
		if((x + BLEND_AREA_SIZE*2) >= (stitchW-1)) x = stitchW-2-BLEND_AREA_SIZE*2;	

		/* Find the average among the valid elements inside the grid */
		int elements = 0;
		int sum = 0;
		/* This loop is quite slow. Blend grid is BLEND_AREA_SIZE/2 tall and
		   BLEND_AREA_SIZE*2 wide. This yields f ex 10x40=400 reads per pixel. */
		for(int i = (y>BLEND_AREA_SIZE/4)?y-BLEND_AREA_SIZE/4:1;
				i<(dstH-1) && i<(y+BLEND_AREA_SIZE/2); ++i){
			for(int j = x; j<(BLEND_AREA_SIZE*2+x); ++j){
				int diff;
				surf2Dread(&diff, diffSurf, j * sizeof(int), i);
				if(fabsf(diff) < 70){ /* Ignore differences too large */
					sum += diff;
					elements++;
				}
			}
		}

		if(!elements){
			/* Dividing by zero is bad, but no valid entries is very unlikely.
			   In these cases we just dont blend in this Y-location.
			   Happens a few times per panorama frame, probaby in the stands or sky (top) */
			continue; 
		}
		float avrg = (float)sum / elements;

		/* This is essentially just feathering, but using the avrg value instead of individual
		   pixel differences. We want to remove overall differences in light, not perfect blend */
		float gradient = 0;
		float gradientStep = 1.0f/(BLEND_AREA_SIZE*2);
		/* We blend all the pixels in this spesific Y-coordinate, near the seam. */
		for(int j = x; j<(BLEND_AREA_SIZE*2+x); ++j, gradient+=gradientStep){
			/* Read the original value and add/remove a fraction depending on diff */
			int val = output[j + y*dstW];
			float change;
			if(j>=stitchX){
				change = (1-gradient)*avrg*-1;
			}else{
				change = gradient*avrg;
			}
			change += val;
			/* clamping output, as we run the risk of going outside legal range */
			output[j + y*dstW] = min(255,max(0,(int)rintf(change)));
		}
	}
}
/* Debug kernel, for testing and comparing regular feathering to improved blending */
__global__ void performFeathering(Node * graph, int * stitchResult, uint8_t * output, int stitchW,
		int dstW, int dstH, cudaSurfaceObject_t diffSurf){
	for(int y=(blockIdx.x*blockDim.x + threadIdx.x); y < dstH; y += (blockDim.x*gridDim.x)){
		int stitchX = stitchResult[y];
		int x = stitchX - BLEND_AREA_SIZE;
		if(x<0) x=0;
		if((x + BLEND_AREA_SIZE*2) >= (stitchW-1)) x = stitchW-2-BLEND_AREA_SIZE*2;	

		float gradient = 0;
		float gradientStep = 1.0f/(BLEND_AREA_SIZE*2);
		for(int j = x; j<(BLEND_AREA_SIZE*2+x); ++j, gradient+=gradientStep){
			int val = output[j + y*dstW];
			int avrg;
			surf2Dread(&avrg, diffSurf, j * sizeof(int), y);
			if(avrg > 100) avrg = 100;
			if(avrg < -100) avrg = -100;
			float change;
			if(j>=stitchX){
				change = (1-gradient)*avrg*-1;
			}else{
				change = gradient*avrg;
			}
			change += val;
			output[j + y*dstW] = min(255,max(0,(int)rintf(change)));
		}
	}
}

/* Debug kernel that simply replaces all the Y-values with the corresponding graph-weights,
   giving a nice visual representation of the weights for pathfinding */
__global__ void drawCost(Node * graph, uint8_t * output, int srcW, int dstW, int dstH, int xOffset){
	for(int idx=(blockIdx.x*blockDim.x + threadIdx.x);
			idx < (srcW*dstH); idx += (blockDim.x*gridDim.x)){
		int x,y;

		x = (idx%srcW);
		y = (idx/srcW);
		float cost = graph[idx].selfCost*3;
		int locX = x+xOffset;
		output[locX + y*dstW] = (cost>255)?255:cost;
		output[dstW*dstH + (locX>>1) + y*(dstW>>1)] = 128;
		output[dstW*dstH + (dstW>>1)*dstH + (locX>>1) + y*(dstW>>1)] = 128;
	}
}

/* Debug kernel for showing the seam and its boundries */
__global__ void drawStitch(int * stitchResult, uint8_t * output, int dstW, int dstH,
		int xOffset, int xEnd){
	int idx = blockIdx.x*blockDim.x + threadIdx.x;
	if(idx > dstH) return;

	int x = xOffset + stitchResult[idx];
// 	output[idx*dstW + x] = 0;
	output[idx*dstW + xOffset] = 0;
	output[idx*dstW + xEnd] = 0;
// 	x = (x>>1)<<1;
// 	output[idx*dstW + x + 1] = 0;
	output[idx*dstW + x] = 100;
	output[dstW*dstH + idx*(dstW>>1) + (x>>1)] = 0;
	output[dstW*dstH + (dstW>>1)*dstH + idx*(dstW>>1) + (x>>1)] = 200;
}

/* Performs the pathfinding based on precomputed pixel weights.
   This kernel is slooow, but not resource demanding.
   The algorithm uses one thread per x-coordinate within a seam-area ('stride' long),
   calculating the combined cost for this x-coordinate for all y-coordinates.
 */
__global__ void createDynamicSeam(Node * graph, int * stitchResult, int dstH,
		int stride, int searchFrom, int searchTo){
	/* Grid(1) : Block(stitchW) */
	extern __shared__ float3 array[];
	/* We use shared memory to save 2 rows of temporary child-costs.
	   One float3 represents the cost of a nodes 3 children. These
	   values are only needed to decide the best child, in the next
	   iteration of the loop. Therefore, it's enough to store 2 rows */
	float3 * buffers[2];
	buffers[0] = array;
	buffers[1] = &array[stride];
	int bufIdx = 0;

	bool left = !threadIdx.x;
	bool right = (threadIdx.x == (blockDim.x-1));

	if(searchFrom){
		/* Initialize the first values.
		   Ensure that we re-use the first part of the previous seam */
		int prev = stitchResult[searchFrom-1];
		float3 res;
		if(prev == (threadIdx.x-1)) res.x = 0;
		else res.x = GIANT_COST;

		if(prev == (threadIdx.x)) res.y = 0;
		else res.y = GIANT_COST;

		if(prev == (threadIdx.x+1)) res.z = 0;
		else res.z = GIANT_COST;
		buffers[bufIdx][threadIdx.x] = res;
	}else{
		/* Initialize the first values to something */
		buffers[bufIdx][threadIdx.x].x = 0;
		buffers[bufIdx][threadIdx.x].y = 0;
		buffers[bufIdx][threadIdx.x].z = 0;
	}

	__syncthreads();
	for(int y=searchFrom; y<searchTo-1; ++y){

		/* Read in the next node's information, set in previous iteration */
		float3 n = buffers[bufIdx][threadIdx.x];
		bufIdx ^= 1;

		/* If this is the left or right side, this ensures that we never choose
		   an invalid path as the best cost. No node has set this cost */
		if(left) n.x = GIANT_COST;
		if(right) n.z = GIANT_COST;

		/* Find the best child-cost, and set it */
		int best = (n.x<n.y)?((n.x<n.z)?0:2):((n.y<n.z)?1:2);
		Node node;
		node.selfCost = graph[threadIdx.x + y*stride].selfCost;
		float cost = ((float*)&n)[best] + node.selfCost;
		node.bestChild = best;
		graph[threadIdx.x + y*stride] = node;

		/* Insert cost into next node.
		   We are the right child of our left-ward node and vice versa */
		if(!left) buffers[bufIdx][threadIdx.x-1].z = cost;
		buffers[bufIdx][threadIdx.x].y = cost;
		if(!right) buffers[bufIdx][threadIdx.x+1].x = cost;

		/* And here's the reason why this is slow. Needs to sync to ensure all 3 child
		   values are computed before continuing with the next iteration */
		__syncthreads();
	}
	{
		/* Set final best-child */
		float3 n = buffers[bufIdx][threadIdx.x];
		if(left) n.x = GIANT_COST;
		if(right) n.z = GIANT_COST;
		int best = (n.x<n.y)?((n.x<n.z)?0:2):((n.y<n.z)?1:2);
		Node node;
		/* We ignore the real selfCost for simplcity reasons.
		   Essentially, all end-nodes have selfCost==0 */
		node.selfCost = ((float*)&n)[best];
		node.bestChild = best;
		graph[threadIdx.x + (searchTo-1)*stride] = node;
		__syncthreads();
	}
	/* From here on out, we elect one little thread to be the king of all the others.
	   He shall rule over this graph and decide which final node is most worthy.
	   This is quite sequential, but no syncing etc needed. Timed to be a tiny tiny part
	   of the entire kernel. */
	if(!threadIdx.x){
		/* Find best end-node */
		float best = GIANT_COST;
		int bestIdx;
		if(dstH == searchTo){
			bestIdx = 30;
			for(int i=0; i<blockDim.x; ++i){
				Node n = graph[i + stride*(searchTo-1)];
				if(n.selfCost<best){
					bestIdx = i;
					best = n.selfCost;
				}
			}
		}else{
			bestIdx = stitchResult[searchTo-1];
		}
		
		/* Perform a full backwards traversal through the entire graph, following
		   the set best-child, while filling in the new seam. */
		stitchResult[searchTo-1] = bestIdx;
		for(int y=searchTo-2; y>=searchFrom; --y){
			/* Bestchild == 0,1,2. So if its 0, we move one pixel left etc */
			bestIdx += graph[y*stride + bestIdx].bestChild - 1;
			stitchResult[y] = bestIdx;
		}
	}
}

/* Project a single input image, based on its individual lookup table.
   Handles two pixels per iteration, due to the chroma downsampling.
   Input is saved in a single texture, so we multiply image index with height */
#if __CUDA_ARCH__ >= 300
__global__ __launch_bounds__(128,16) void createSingleImage(uint8_t * output,
		cudaTextureObject_t mapTex2, int img, int srcH, int dstH, int minX, int strideX){
#else
__global__ __launch_bounds__(128,8) void createSingleImage(uint8_t * output,
		cudaTextureObject_t mapTex2, int img, int srcH, int dstH, int minX, int strideX){
#endif

	for(int idx=2*(blockIdx.x*blockDim.x + threadIdx.x);
			idx < (strideX*dstH); idx += 2*(blockDim.x*gridDim.x)){

		float uVal, vVal, x, y;
		float2 pix;
		int mapX = idx%strideX;
		int mapY = idx/strideX;
		/* Access the output pixel in the lookup table to find the input x/y coord */
		pix = tex2D<float2>(mapTex2, mapX, mapY);
		if(pix.x>=0){
#ifdef DONT_ROTATE
			x = pix.x;
			y = pix.y + img*srcH;
#else
			/* Image is rotated 90 degrees. Lookup table doesnt know that tho */
			x = pix.y;
			y = srcH - 1 - pix.x + img*srcH;
#endif
// 			float4 yuv = tex2D(yuvTex, rintf(x)+0.5f, rintf(y)+0.5f);
// 			float4 yuv = tex2D(yuvTex, x+0.5f, y+0.5f);
// 			float4 yuv = tex2DBicubic<uchar4,float4>(yuvTex, x+0.5f, y+0.5f);
			/* Fast bicubic spline interpolation for sub-pixel accuracy */
			float4 yuv = tex2DFastBicubic<uchar4,float4>(yuvTex, x+0.5f, y+0.5f);
			output[idx] = rintf(yuv.x*255.0f);
			uVal = yuv.y;
			vVal = yuv.z;
		}else{
			output[idx] = 0;
			uVal = 0.5f; 
			vVal = 0.5f; 
		}
		mapX = (idx+1)%strideX;
		mapY = (idx+1)/strideX;
		pix = tex2D<float2>(mapTex2, mapX, mapY);
		if(pix.x>=0){
#ifdef DONT_ROTATE
			x = pix.x;
			y = pix.y + img*srcH;
#else
			x = pix.y;
			y = srcH - 1 - pix.x + img*srcH;
#endif
// 			float4 yuv = tex2D(yuvTex, rintf(x)+0.5f, rintf(y)+0.5f);
// 			float4 yuv = tex2D(yuvTex, x+0.5f, y+0.5f);
// 			float4 yuv = tex2DBicubic<uchar4,float4>(yuvTex, x+0.5f, y+0.5f);
			float4 yuv = tex2DFastBicubic<uchar4,float4>(yuvTex, x+0.5f, y+0.5f);
			output[idx+1] = rintf(yuv.x*255.0f);
			uVal += yuv.y;
			vVal += yuv.z;
		}else{
			output[idx+1] = 0;
			uVal += 0.5f; 
			vVal += 0.5f; 
		}
// 		uVal = 1; vVal = 1; /* Greyscale */
		/* Write the combined chromatic values */
		output[strideX*dstH + (idx>>1)] = rintf(uVal*127.5f);
		output[(strideX*dstH*3 + idx)>>1] = rintf(vVal*127.5f);
	}
}

#ifdef GPU_STITCHER_PRINT_DEBUGTIME
static float totalTime = 0;
#endif
static int totalFrames = 0;

void CudaDynamicStitcher::stitchImage(cudaArray *input, uint8_t * output){
// 	usleep(20000); //TODO THIS IS ONLY HERE FOR BENCHING/DEBUGGING

#ifdef GPU_STITCHER_PRINT_DEBUGTIME
	struct timeval start_time, stop_time;
	gettimeofday(&start_time, 0);
#endif
	cudaSafe(cudaBindTextureToArray(yuvTex, input));

	/* First project each image seperately into its own array, using the lookup table / map */
	for(int i=0; i<numSources; ++i){
		createSingleImage<<< grid, block, 0, images[i].stream[1] >>>
			(images[i].output, images[i].mapTex,
				i, srcHeight, dstHeight, images[i].minX, images[i].stride);
	}

	/* Copy the Y-channel. For each overlapping stitching area, only one image will be copied
	   to this location. Later on, once we know the dynamic seam, we overwrite all of the
	   pixels that fell on the "wrong" side of the seam, based on this copy operation */
	for(int i=0; i<numSources; ++i){
		int start = i ? images[i-1].maxX : 0;
		int end = images[i].maxX;
		int startLocal = i ? images[i-1].maxX-images[i].minX : 0;
		cudaSafe(cudaMemcpy2DAsync(output+start, dstWidth,
					images[i].output + startLocal, images[i].stride,
					end-start, dstHeight, cudaMemcpyDeviceToDevice, images[i].stream[1]));
	}

	/* Copy the chromatic channels, disregarding the dynamic seam, as before */
	for(int i=0; i<numSources; ++i){
		int start = (i ? images[i-1].maxX : 0)/2;
		int end = images[i].maxX/2;
		int startLocal = (i ? (images[i-1].maxX-images[i].minX) : 0)/2;
		cudaSafe(cudaMemcpy2DAsync(output+dstWidth*dstHeight+start, dstWidth>>1,
					images[i].output + images[i].stride*dstHeight + startLocal,
					images[i].stride>>1, end-start, dstHeight<<1,
					cudaMemcpyDeviceToDevice, images[i].stream[1]));
	}

#ifdef USE_STATIC_STITCH
	/* To create a static seam, we only need to copy the above projections together.
	   First we copy the Y-channel, then the chroma channels */
	for(int i=0; i<numSources; ++i){
		int start = i ? images[i-1].maxX-images[i-1].stitchW/2 : 0;
		int end = i==(numSources-1) ? images[i].maxX : images[i].maxX-images[i].stitchW/2;
		int startLocal = i ? images[i-1].stitchW/2 : 0;
		cudaSafe(cudaMemcpy2DAsync(output+start, dstWidth,
					images[i].output + startLocal, images[i].stride,
					end-start, dstHeight, cudaMemcpyDeviceToDevice, images[i].stream[1]));
	}

	for(int i=0; i<numSources; ++i){
		int start = (i ? images[i-1].maxX-images[i-1].stitchW/2 : 0)/2;
		int end = (i==(numSources-1) ? images[i].maxX : images[i].maxX-images[i].stitchW/2)/2;
		int startLocal = (i ? images[i-1].stitchW/2 : 0)/2;
		cudaSafe(cudaMemcpy2DAsync(output+dstWidth*dstHeight+start, dstWidth>>1,
					images[i].output + images[i].stride*dstHeight + startLocal,
					images[i].stride>>1, end-start, dstHeight<<1,
					cudaMemcpyDeviceToDevice, images[i].stream[1]));
	}
#else

	/*TODO Not sure if what has been done below is 100% safe. Kernels execute in different
	  streams, but when we begin createCost on overlap between image[0-1] these two must both
	  be finished. We only test for image[1], trusting that image[0] was scheduled earlier.
	  The probability of this going to hell seems really fucking low, but I think its
	  theoretically possible.
	 */
	

	/* Create the weights used for pathfinding for the dynamic seam */
	for(int i=0; i<numSources-1; ++i){
		int offset = images[i+1].minX - images[i].minX;
		createCost<<< edgeGrid, block, 0, images[i+1].stream[1] >>>
			(images[i].graph, images[i].output + offset, images[i+1].output,
			 images[i].stride, images[i+1].stride, images[i].stitchW,
			 dstHeight, images[i].stitchResult, images[i].diff);
	}

	/* Perform pathfinding on the created graph, using Dijkstra's algorithm with the
	   weights generated above. These have to run in paralell, as they cost very little
	   resources, but are damn slow due to synchronization. */
	for(int i=0; i<numSources-1; ++i){
		/* You CANNOT skip fullrange pathfinding on the first frame. It
		   will segfault. Allowing it to do fullrange every now and again
		   feels smart, in case something weird happens with the initial one */
		if(totalFrames<5 || !(totalFrames%200)){
			/* Do full search */
			createDynamicSeam<<< 1, images[i].stitchW-1, images[i].stitchW*sizeof(float3)*2,
				images[i+1].stream[1] >>>
					(images[i].graph, images[i].stitchResult, dstHeight, images[i].stitchW,
					 0, dstHeight);
		}else{
			createDynamicSeam<<< 1, images[i].stitchW-1, images[i].stitchW*sizeof(float3)*2,
				images[i+1].stream[1] >>>
					(images[i].graph, images[i].stitchResult, dstHeight, images[i].stitchW,
					 images[i].skipTop, dstHeight-images[i].skipBot);
		}
	}

	/* Now we copy over the pixels that landed on the "wrong" side of the seam, based on
	   the stitchResult and the original 2D memcopies we did earlier */
	for(int i=1; i<numSources; ++i){
		copyStitched<<< 1+((dstHeight*images[i-1].stitchW)>>7), 128, 0, images[i].stream[1] >>>
			(images[i-1].stitchResult, images[i].output, output, images[i].minX,
			 images[i].stride, dstWidth, dstHeight, images[i-1].stitchW);
	}

	/* This is an optional blending step, to reduce the visibility of the seam when there
	   are small differences in luminosity between the two images */
	for(int i=0; i<numSources-1; ++i){
		performBlend<<< 1+(dstHeight+1)>>7, 128, 0, images[i+1].stream[1] >>>
// 		performFeathering<<< 128, (dstHeight+1)>>7, 0, images[i+1].stream[1] >>>
			(images[i].graph, images[i].stitchResult, output+images[i+1].minX,
			 images[i].stitchW, dstWidth, dstHeight, images[i].diff);
	}

// 	cudaSafe(cudaMemset(output+dstWidth*dstHeight,128,dstHeight*dstWidth));

#ifdef SHOW_COST
	for(int i=0; i<numSources; ++i){
		cudaSafe(cudaStreamSynchronize(images[i].stream[1]));
	}
	for(int i=0; i<numSources-1; ++i){
		drawCost<<< (images[i].stitchW*dstHeight)/block.x, block, 0, images[i].stream[1] >>>
			(images[i].graph, output, images[i].stitchW, dstWidth, dstHeight, images[i+1].minX);
	}

	for(int i=0; i<numSources-1; ++i){
		drawStitch<<< 2, dstHeight>>1, 0, images[i].stream[1] >>>
			(images[i].stitchResult, output, dstWidth, dstHeight, images[i+1].minX, images[i].maxX);
	}
#endif //end show cost
#endif //end-else static stitch

#if 1
    //Calculate average color difference in overlapping regions
    double * d_result[numSources-1];
    double h_result[numSources-1][3];

	for(int i=0; i<numSources-1; ++i){
		cudaSafe(cudaStreamSynchronize(images[i].stream[1]));
        cudaSafe(cudaMalloc(&d_result[i], sizeof(double)*3));
	}

	for(int i=0; i<numSources-1; ++i){

        //Searches through half the overlapping pixels, closest 50% to the seam
        //Finds the RGB color differences in this region.

        int off1 = (images[i+1].minX - images[i].minX + images[i].stitchW/4) & ~3;
        int off2 = (images[i].stitchW/4) & ~3;

        uint8_t * y1 = images[i].output + off1;
		uint8_t * u1 = images[i].output + images[i].stride * dstHeight + off1/2;
		uint8_t * v1 = u1 + (images[i].stride * dstHeight)/2;
		
        uint8_t * y2 = images[i+1].output + off2;
		uint8_t * u2 = images[i+1].output + images[i+1].stride * dstHeight + off2/2;
		uint8_t * v2 = u2 + (images[i+1].stride * dstHeight)/2;

        getColorOffsets<<< 1, 256, 0, images[i].stream[1] >>>
            (y1,u1,v1, y2,u2,v2,
             images[i].stride, images[i+1].stride,
             images[i].stitchW/2, dstHeight, d_result[i]);

        cudaSafe(cudaMemcpyAsync(&h_result[i][0], d_result[i], sizeof(double)*3,
                cudaMemcpyDeviceToHost, images[i].stream[1])); 
    }

    RgbRatio result[numSources-1];
	for(int i=0; i<numSources-1; ++i){
		cudaSafe(cudaStreamSynchronize(images[i].stream[1]));
        cudaSafe(cudaFree(d_result[i]));

        result[i] = RgbRatio(h_result[i]);
    }

    whiteBalanceAdjuster->add(result);

#else
	for(int i=0; i<numSources-1; ++i){
		cudaSafe(cudaStreamSynchronize(images[i].stream[1]));
    }
#endif

#ifdef GPU_STITCHER_PRINT_DEBUGTIME
	gettimeofday(&stop_time, 0);
 	float time;
	time = (stop_time.tv_sec * 1.0e3 + stop_time.tv_usec / 1.0e3)
		- (start_time.tv_sec * 1.0e3 + start_time.tv_usec / 1.0e3);
	totalTime += time;
#endif //end print debugtime
	totalFrames++;
}

void CudaDynamicStitcher::initCudaArrays(){

	block = dim3(128);
	grid = dim3( 1 + ((dstWidth*dstHeight)/(block.x*2*numSources)) );
	totalTime = 0;
	totalFrames = 0;

	cudaSafe(cudaStreamCreate(&stream));

	cudaChannelFormatDesc u4byteChannel = cudaCreateChannelDesc<uchar4>();
	cudaSafe(cudaMallocArray(&inputData[0]->cuArr,&u4byteChannel, srcWidth,
				srcHeight*numSources, cudaArraySurfaceLoadStore));
	cudaSafe(cudaMallocArray(&inputData[1]->cuArr,&u4byteChannel, srcWidth,
				srcHeight*numSources, cudaArraySurfaceLoadStore));

	yuvTex.addressMode[0] = cudaAddressModeClamp;
	yuvTex.addressMode[1] = cudaAddressModeClamp;
	yuvTex.filterMode = cudaFilterModeLinear;

	//Setup pathfinding
	bool useDefaultSkip;
	int defaultSkipTop[] = PATHFINDING_SKIP_TOP;
	int defaultSkipBot[] = PATHFINDING_SKIP_BOT;
	useDefaultSkip = (sizeof(defaultSkipTop)/sizeof(*defaultSkipTop)) == (numSources-1) 
		&& (sizeof(defaultSkipBot)/sizeof(*defaultSkipBot)) == (numSources-1);
	if(!useDefaultSkip)
		LOG_W("Stitcher: Limited pathfinding / skipping top&bottom pixels disabled");

	cudaChannelFormatDesc float2Channel = cudaCreateChannelDesc<float2>();
	cudaChannelFormatDesc intChannel = cudaCreateChannelDesc<int>();
	for(int i = 0; i<numSources; ++i){
		images[i].stride = images[i].maxX-images[i].minX;
		if(i<(numSources-1)){
			cudaSafe(cudaMalloc(&images[i].stitchResult, dstHeight*sizeof(int)));
			cudaSafe(cudaMemset(images[i].stitchResult, -1, dstHeight*sizeof(int)));
			images[i].stitchW = images[i].maxX-images[i+1].minX;
			cudaSafe(cudaMalloc(&images[i].graph,
						sizeof(Node)*images[i].stitchW*dstHeight));
			/* There is an ugly hidden assumption that all stitch-areas are equally wide.
			This should always be the case, but if it's not things should still work */
			if(!i) edgeGrid = dim3( 1 + (images[i].stitchW*dstHeight/(block.x*2)) );

			cudaSafe(cudaMallocArray(&images[i].diffMap, &intChannel,
						images[i].stride, dstHeight));
			struct cudaResourceDesc resDesc;
			memset(&resDesc, 0, sizeof(resDesc));
			resDesc.resType = cudaResourceTypeArray;
			resDesc.res.array.array = images[i].diffMap;
			cudaSafe(cudaCreateSurfaceObject(&images[i].diff, &resDesc));

			if(useDefaultSkip){
				images[i].skipTop = defaultSkipTop[i];
				images[i].skipBot = defaultSkipBot[i];
			}else{
				images[i].skipTop = 0;
				images[i].skipBot = 0;
			}

		}

		cudaSafe(cudaMallocArray(&images[i].gpuMap, &float2Channel,
					images[i].stride, dstHeight));
		cudaSafe(cudaMemcpyToArray(images[i].gpuMap, 0, 0, images[i].cpuMap,
					sizeof(float2)*images[i].stride*dstHeight, cudaMemcpyHostToDevice));
		delete images[i].cpuMap;
		cudaSafe(cudaMalloc(&images[i].output, 2*images[i].stride*dstHeight));
		cudaSafe(cudaStreamCreate(&images[i].stream[0]));
		cudaSafe(cudaStreamCreate(&images[i].stream[1]));

		struct cudaResourceDesc resDesc;
		memset(&resDesc, 0, sizeof(resDesc));
		resDesc.resType = cudaResourceTypeArray;
		resDesc.res.array.array = images[i].gpuMap;

		struct cudaTextureDesc texDesc;
		memset(&texDesc, 0, sizeof(texDesc));
		texDesc.normalizedCoords = 0;
		texDesc.readMode = cudaReadModeElementType;
		texDesc.addressMode[0] = cudaAddressModeClamp;
		texDesc.addressMode[ 1] = cudaAddressModeClamp;
		texDesc.filterMode = cudaFilterModePoint;
		cudaSafe(cudaCreateTextureObject(&images[i].mapTex, &resDesc, &texDesc, NULL));
	}
#ifdef GPU_STITCHER_PRINT_DEBUGTIME
	totalTime = 0;
#endif
	totalFrames = 0;
}

void CudaDynamicStitcher::deleteCudaArrays(){
	/* Brutally destroy all the stuff we've worked so hard to create */
	cudaSafe(cudaStreamDestroy(stream));
	if(inputData[0]){
		cudaSafe(cudaFreeArray(inputData[0]->cuArr));
		free(inputData[0]);
	}
	if(inputData[1]){
		cudaSafe(cudaFreeArray(inputData[1]->cuArr));
		free(inputData[1]);
	}
	for(int i = 0; i<numSources; ++i){
		if(i<(numSources-1)){
			cudaSafe(cudaFree(images[i].stitchResult));
			cudaSafe(cudaFree(images[i].graph));
			cudaSafe(cudaDestroySurfaceObject(images[i].diff));
			cudaSafe(cudaFreeArray(images[i].diffMap));
		}
		cudaSafe(cudaFreeArray(images[i].gpuMap));
		cudaSafe(cudaFree(images[i].output));
		cudaSafe(cudaStreamDestroy(images[i].stream[0]));
		cudaSafe(cudaStreamDestroy(images[i].stream[1]));
	}
#ifdef GPU_STITCHER_PRINT_DEBUGTIME
	if(totalFrames)
		LOG_I("Stitch time  : %7.3fms", totalTime/totalFrames);
#endif
}

#ifdef REDEF_CLAMP_Y
#undef CLAMP_Y
#endif
