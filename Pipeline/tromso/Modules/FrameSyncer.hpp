// AUTHOR(s): Sigurd Ljodal,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#ifndef framesyncer_hpp
#define framesyncer_hpp

#include <stdint.h>
#include <sys/time.h>
#include <pthread.h>

#ifdef HAVE_CUDA
#include <cuda.h>
#endif

#if FRAMESYNCER_USE_DOLPHIN
#include <sisci_api.h>
#endif

#include "config.h"

#include "Module.hpp"

class FrameSyncer : public FetchModule {
    public:
        //
        // ### Construction
        //

        // Create a new frame syncer for the given frame rate and number of
        // cameras. Frame rate defaults to 50 and number of cameras to 5. The
        // `id` must be set if multiple syncers with dolphin support are going
        // to be used on the same machine.
        FrameSyncer(size_t frame_size=2040*1080, uint8_t cams=5,
                unsigned int id=0x10);

        //
        // ### Configuration and setup
        //

        // Enable or disable HDR mode. As this is affects buffer sizes, it must
        // be called before initialize is called. Returns `false` if
        // `initialize` has already been called, otherwise `true` is returned.
        bool setHDRMode(bool hdr);

        // Setup the syncer. If pointers are passed, they are used as long as
        // `FRAMESYNCER_USE_DOLPHIN` is not defined. Otherwise the syncer will
        // allocate memory for itself. It is always safe to call setup without
        // pointers.
        bool initialize(uint8_t *r_ptr=NULL, uint8_t *w_ptr=NULL,
                struct header **h_ptr=NULL, void **f_ptr=NULL);

        // Change the frame rate of the syncer. It is safe to call this at any
        // time, but if it is called while another thread is calling `getFrame`
        // it will not take effect until after a frame has been returned.
        void setFrameRate(uint32_t fps);

        //
        // ### Streaming
        //

        void streamStart();
        void streamStop();

        //
        // ### Getting frames
        //
        // These methods provides ways to retreive frames in different formats.
        //
        // All methods allow for a preallocated buffer to be passed in. If
        // a preallocated buffer is provided, the data will be copied into it.
        // Otherwise a new buffer will be allocated and returned, this new
        // buffer must be freed by the callee using the appropriate functions
        // depending on the buffer type.

        // Get a frame in CPU memory.
        struct frame *getFrame(struct frame *f=NULL);

#ifdef HAVE_CUDA
        // Get a frame in Cuda memory.
        struct frame *getCudaFrame(struct frame *f=NULL);

        // Get a frame in a Cuda array
        struct frame *getCudaArrayFrame(struct frame *f=NULL);
#endif

        //
        // ### Teardown
        //

        // Try to terminate the syncer. This releases all resources held by the
        // syncer and makes in non-operative. If this call returns `false` it
        // means that something failed and that all resources are not
        // guaranteed to have been released. If `true` is returned all
        // resources have been released. After a successful call to terminate,
        // `initialize` can be called again to reuse the syncer.
        bool terminate();

        ~FrameSyncer();

    private:
        // Current state of the syncer.
        bool initialized = false;

        // If we are currently streaming or not
        bool streaming = false;

        // The number of cameras that will be used.
        uint8_t cams;

        // The frame rate
        uint32_t fps = 50;

        // HDR mode, changes how the incoming buffers are handled
        bool hdr = false;

        // The size of a single frame.
        size_t frame_size;

        // The size of a single frame buffer. For normal mode this is the same
        // as `frame_size` and in HDR mode it is two times the size.
        size_t buffer_size;

        // This is the maximum number of µs that can be between frames to be
        // considered to belong to the same set.
        int_fast64_t threshold;

        // When a frame is delivered, the deadline for the next frame is noted.
        // Initially this is zero, and the syncer will then wait until it has a
        // full set of frames.
        struct timeval deadline = {0, 0};

        // This is the time interval to wait from when the last frame was
        // delivered before dropping a frame.
        struct timeval first_max_wait = {0, 0};
        struct timeval max_wait = {0, 0};

        //This determines which camera should be used to read header/timestamp from
        int pilot_hdr_idx = 0;

        int * dropped_camera;
        uint_fast64_t * current_dropped;
        

        // These are the current read and write locations of each camera. Only
        // the writer is allowed to modify `write`, and only the reader
        // is allowed to modify `read`. If this is not adhered to,
        // unexpected situations might occur, where the integrety of the
        // circular buffer is no longer guaranteed.
#if FRAMESYNCER_USE_DOLPHIN
        volatile uint8_t *read = NULL;
        volatile uint8_t *write = NULL;
#else
        uint8_t *read = NULL;
        uint8_t *write = NULL;
#endif

        // This is the headers for each of the frames. They must be acessible
        // from the CPU as certain parts of them are used in the syncing
        // process. If `FRAMESYNCER_USE_DOLPHIN` is true, these headers are
        // made available through Dolphin.
#if FRAMESYNCER_USE_DOLPHIN
        volatile struct header **headers = NULL;
#else
        struct header **headers = NULL;
#endif

        // These are the frame buffers. This is where the actual pictura data
        // is stored. This can be on either CPU or GPU. They can also be
        // available through Dolphin.
#if FRAMESYNCER_USE_DOLPHIN
        volatile void **frames = NULL;
#else
        void **frames = NULL;
#endif

        // We keep track of the last frame set delivered, so that we can reuse
        // it if we need to drop a frame.
        struct frame frameset;

#if FRAMESYNCER_USE_DOLPHIN
        // SISCI virtual device descriptors
        sci_desc_t sd_rw;
        sci_desc_t sd_headers;
        sci_desc_t *sd_frames;

        // The adapter to use
        // TODO Make configurable
        unsigned int adapter = 0;

        // Read and write location segment and mapping descriptors
        sci_local_segment_t seg_rw;
        sci_map_t map_r;
        unsigned int sid_rw;

        // Segments for headers and frames
        sci_local_segment_t seg_headers;
        sci_local_segment_t *seg_frames;

        // Maps for headers and frames
        sci_map_t *map_headers;
        sci_map_t *map_frames;

        // Segment ids for headers and frames
        unsigned int sid_headers;
        unsigned int sid_frames;
#endif

        //
        // ### Thread safety
        //

        // Lock for stopping or starting streaming
        pthread_mutex_t stream_mutex = PTHREAD_MUTEX_INITIALIZER;

        //
        // ### Helper functions
        //
        // These are helper functions used when trying to find a set of frames

        // Matches the frames from each camera an tries to find a set. Returns
        // `true` if a set is found and `false` if no set is found.
        bool matchFrames();

        // Calculate the number of frames that are currently buffered for the
        // requested camera.
        uint8_t availableFrames(uint8_t camera);

        // Try to trow away a frame from the requested frame. If the conditions
        // does not allow for a frame to be thrown away `false` is returned. If
        // a frame was thrown away, `true` is retured.
        bool dropFrame(uint8_t camera);

        inline int_fast64_t time_cmp(volatile struct timeval *t0,
                volatile struct timeval *t1);

        // Try to get a set of frames and copy them to the frameset buffer.
        bool getFrameset();

        // Copy the current frames from all cameras into the frameset.
        inline void copyToSet();
};

#endif
