// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "CudaBayerConverter.hpp"

#include <cuda.h>
#include <cuda_runtime.h>
#include <math_functions.h>
#include <Helpers/logging.h>

#ifdef GPU_BAYER_PRINT_DEBUGTIME
#include <sys/time.h>
#endif

#define _USE_MATH_DEFINES

/* Constants from:
 * http://www.fourcc.org/fccyvrgb.php */
// #define Y(rgb) ( 0.29900f*rgb.x + 0.11400f*rgb.z)
// #define U(rgb) ( 128)
// #define V(rgb) ( 128)
#define Y(rgb) ( 0.29900f*rgb.x + 0.58700f*rgb.y + 0.11400f*rgb.z)
#define U(rgb) (-0.16900f*rgb.x - 0.33100f*rgb.y + 0.50000f*rgb.z + 128)
#define V(rgb) ( 0.50000f*rgb.x - 0.41900f*rgb.y - 0.08100f*rgb.z + 128)

/* Clamp (and round) a floating point value into legal 0-255 integer */
#define CLAMP_255(x) ((uint8_t)rintf(min(255.0f,max(0.0f,x))))

surface<void, cudaSurfaceType2D> yuvSurf;
surface<void, cudaSurfaceType2D> yuvSurfPreMedian;

/* Based on the define in .hpp, the implemented algorithms are spread
   into different files. Only one of these files can be included.
   They implement the same functions:
   allocateExtraBuffers(), performInitialPasses(), freeExtraBuffers(),
   getR/G1/G2/B() (device-functions)
   The algorithms can implement additional kernels, but the last pass is
   shared, merely calling different functions for retrieving the final pixelvalues.
   Because we use traditional textures, we cannot reference these if they aren't
   included in the same file (even by including the .cu file). bayerTex is therefore
   declared identically for all algorithms, bound to the initial input buffer.
 */
#if defined(DO_NN) || defined(DO_BILINEAR) || defined(DO_ADAPTIVE_BILINEAR) \
   	|| defined(DO_GRADIENT_CORRECTED)
#include "bayer/single_pass_algorithms.cu"
#elif defined(DO_SMOOTH_HUE)
#include "bayer/smooth_hue_algorithm.cu"
#elif defined(DO_EDGE_DIRECTED)
#include "bayer/edge_directed_algorithm.cu"
#elif defined(DO_WEIGHTED_ADAPTIVE)
#include "bayer/weighted_adaptive_algorithm.cu"
#elif defined(DO_FULL_WEIGHTED_ADAPTIVE)
#include "bayer/full_weighted_adaptive_algorithm.cu"
#elif defined(DO_HOMOGENEITY_BASED_EDGE_DIRECTED)
#include "bayer/homogeneity_based_edge_directed_algorithm.cu"
#elif defined(DO_GRADIENT_BASED_EDGE_DIRECTED)
#include "bayer/gradient_based_edge_directed_algorithm.cu"
#else
#error "MUST USE A BAYER ALGORITHM DEFINE"
#endif

#ifdef DO_FAST_KERNEL
/* Since this was the final version, I tried making a different version for optimization.
   Halves the execution time of this pass or so. Bigger impact on the other kernels below. */
__global__ void convertBayer(int srcW, int srcH){
	for(int idx=2*(blockIdx.x*blockDim.x + threadIdx.x);
			idx < (srcW*(srcH/2)); idx += 2*(blockDim.x*gridDim.x)){

		int x = idx%srcW;
		int y = 2*(idx/srcW);
		
		float3 rgb;
		uchar4 yuv;

		uchar2 pixels[10];
		getPixels(pixels, x, y);

		//Green1
		rgb.y = pixels[3].x;
		rgb.x = (((float)pixels[2].y-pixels[2].x)+
				((float)pixels[4].y-pixels[4].x))*0.5f + rgb.y;
		rgb.z = (((float)pixels[0].y-pixels[0].x)+
					((float)pixels[5].y-pixels[5].x))*0.5f + rgb.y;
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*x,y);

		//Red
		rgb.x = pixels[4].y;
		rgb.y = pixels[4].x;
		rgb.z = rgb.y + 0.25f*(
				((float)pixels[0].y-pixels[0].x) + ((float)pixels[1].y-pixels[1].x) +
				((float)pixels[5].y-pixels[5].x) + ((float)pixels[7].y-pixels[7].x));
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*(x+1),y);

		//Blue
		rgb.y = pixels[5].x;
		rgb.z = pixels[5].y;
		rgb.x = rgb.y + 0.25f*(
				((float)pixels[2].y-pixels[2].x) + ((float)pixels[4].y-pixels[4].x) +
				((float)pixels[8].y-pixels[8].x) + ((float)pixels[9].y-pixels[9].x));
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*x,y+1);

		//Green2
		rgb.y = pixels[6].x;
		rgb.x = (((float)pixels[4].y-pixels[4].x)+
					((float)pixels[9].y-pixels[9].x))*0.5f + rgb.y;
		rgb.z = (((float)pixels[5].y-pixels[5].x)+
				((float)pixels[7].y-pixels[7].x))*0.5f + rgb.y;
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*(x+1),y+1);
	}
}
#elif defined(FAST_SMOOTH_HUE)
__global__ void convertBayer(int srcW, int srcH){
	for(int idx=2*(blockIdx.x*blockDim.x + threadIdx.x);
			idx < (srcW*(srcH/2)); idx += 2*(blockDim.x*gridDim.x)){

		int x = idx%srcW;
		int y = 2*(idx/srcW);
		
		float3 rgb;
		uchar4 yuv;

		uchar2 pix[14];
		getPixels(pix, x, y);
// #define sdiv(i) ((float)pix[i].y/pix[i].x)
#define sdiv(i) (__fdividef(pix[i].y,pix[i].x))
		//Green1
		rgb.y = pix[4].x;
		rgb.x = 0.5f*rgb.y*( sdiv(3) + sdiv(5));
		rgb.z = 0.5f*rgb.y*( sdiv(0) + sdiv(8));
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*x,y);

		//Red
		rgb.y = pix[5].x;
		rgb.x = pix[5].y;
		rgb.z = 0.25f*rgb.y*( sdiv(0) + sdiv(3) + sdiv(8) + sdiv(10));
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*(x+1),y);

		//Blue
		rgb.y = pix[8].x;
		rgb.z = pix[8].y;
		rgb.x = 0.25f*rgb.y*( sdiv(3) + sdiv(5) + sdiv(11) + sdiv(13));
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*x,y+1);

		//Green2
		rgb.y = pix[9].x;
		rgb.x = 0.5f*rgb.y*( sdiv(5) + sdiv(13));
		rgb.z = 0.5f*rgb.y*( sdiv(8) + sdiv(10));
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*(x+1),y+1);
#undef sdiv
	}
}
#elif defined(FAST_GRADIENT_CORRECTED)
__global__ void convertBayer(int srcW, int srcH){
	for(int idx=2*(blockIdx.x*blockDim.x + threadIdx.x);
			idx < (srcW*(srcH/2)); idx += 2*(blockDim.x*gridDim.x)){

		int x = idx%srcW;
		int y = 2*(idx/srcW);
		
		float3 rgb;
		uchar4 yuv;
		float p, grad1, grad2;

		uint8_t pix[24];
		getPixels(pix, x, y);

		//Green1
		rgb = make_float3(0,0,0);
		rgb.y = pix[8];
		p = (float)pix[2]+pix[4]+pix[13]+pix[15];
		grad1 = rgb.y - 0.2f * (p+pix[6]+pix[10] - 0.5f*((float)pix[0]+pix[19]));
		grad2 = rgb.y - 0.2f * (p-0.5f*((float)pix[6]+pix[10])+pix[0]+pix[19]);
		rgb.x = 0.5f*((float)pix[7] + pix[9]) + BETA_B*grad1;
		rgb.z = 0.5f*((float)pix[3] + pix[14]) + BETA_B*grad2;
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*x,y);

		//Red
		rgb.x = pix[9];
		p = rgb.x - 0.25f*((float)pix[1]+pix[7]+pix[11]+pix[20]);
		rgb.y = 0.25f*((float)pix[4]+pix[8]+pix[10]+pix[15]) + ALPHA_B*p;
		rgb.z = 0.25f*((float)pix[3]+pix[5]+pix[14]+pix[16]) + GAMMA_B*p;
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*(x+1),y);

		//Blue
		rgb.z = pix[14];
		p = rgb.z - 0.25f*((float)pix[3]+pix[12]+pix[16]+pix[22]);
		rgb.x = 0.25f*((float)pix[7]+pix[9]+pix[18]+pix[20]) + GAMMA_B*p;
		rgb.y = 0.25f*((float)pix[8]+pix[13]+pix[15]+pix[19]) + ALPHA_B*p;
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*x,y+1);

		//Green2
		rgb.y = pix[15];
		p = (float)pix[8]+pix[10]+pix[19]+pix[21];
		grad1 = rgb.y - 0.2f * (p+pix[13]+pix[17] - 0.5f*((float)pix[4]+pix[23]));
		grad2 = rgb.y - 0.2f * (p-0.5f*((float)pix[13]+pix[17])+pix[4]+pix[23]);
		rgb.x = 0.5f*((float)pix[9] + pix[20]) + BETA_B*grad1;
		rgb.z = 0.5f*((float)pix[14] + pix[16]) + BETA_B*grad2;
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*(x+1),y+1);
	}
}
#elif defined(FAST_NN)
__global__ void convertBayer(int srcW, int srcH){
	for(int idx=2*(blockIdx.x*blockDim.x + threadIdx.x);
			idx < (srcW*(srcH/2)); idx += 2*(blockDim.x*gridDim.x)){

		int x = idx%srcW;
		int y = 2*(idx/srcW);
		
		float3 rgb;
		uchar4 yuv;

		uint8_t pixels[7];
		getPixels(pixels, x, y);
		//Green1
		rgb.x = pixels[1];
		rgb.y = pixels[2];
		rgb.z = pixels[0];
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*x,y);
		//Red
		rgb.x = pixels[3];
		rgb.y = pixels[2];
		rgb.z = pixels[0];
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*(x+1),y);
		//Blue
		rgb.x = pixels[1];
		rgb.y = pixels[4];
		rgb.z = pixels[5];
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*x,y+1);
		//Green2
		rgb.x = pixels[3];
		rgb.y = pixels[6];
		rgb.z = pixels[5];
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*(x+1),y+1);
	}
}
#elif defined(FAST_BILINEAR)
__global__ void convertBayer(int srcW, int srcH){
	for(int idx=2*(blockIdx.x*blockDim.x + threadIdx.x);
			idx < (srcW*(srcH/2)); idx += 2*(blockDim.x*gridDim.x)){

		int x = idx%srcW;
		int y = 2*(idx/srcW);
		
		float3 rgb;
		uchar4 yuv;

		uint8_t pixels[14];
		getPixels(pixels, x, y);

		//Green1
		rgb.x = ((float)pixels[3]+pixels[5])*0.5f;
		rgb.y = pixels[4];
		rgb.z = ((float)pixels[0]+pixels[8])*0.5f;
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*x,y);
		//Red
		rgb.x = pixels[5];
		rgb.y = ((float)pixels[1]+pixels[4]+pixels[6]+pixels[9])*0.25f;
		rgb.z = ((float)pixels[0]+pixels[2]+pixels[8]+pixels[10])*0.25f;
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*(x+1),y);
		//Blue
		rgb.x = ((float)pixels[3]+pixels[5]+pixels[11]+pixels[13])*0.25f;
		rgb.y = ((float)pixels[4]+pixels[7]+pixels[9]+pixels[12])*0.25f;
		rgb.z = pixels[8];
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*x,y+1);
		//Green2
		rgb.x = ((float)pixels[5]+pixels[13])*0.5f;
		rgb.y = pixels[9];
		rgb.z = ((float)pixels[8]+pixels[10])*0.5f;
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*(x+1),y+1);
	}
}
#elif defined(FAST_A_BILINEAR)
__global__ void convertBayer(int srcW, int srcH){
	for(int idx=2*(blockIdx.x*blockDim.x + threadIdx.x);
			idx < (srcW*(srcH/2)); idx += 2*(blockDim.x*gridDim.x)){

		int x = idx%srcW;
		int y = 2*(idx/srcW);
		
		float3 rgb;
		uchar4 yuv;
		float g_diff;

		uint8_t pixels[14];
		getPixels(pixels, x, y);

		//Green1
		rgb.x = ((float)pixels[3]+pixels[5])*0.5f;
		rgb.y = pixels[4];
		rgb.z = ((float)pixels[0]+pixels[8])*0.5f;
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*x,y);
		//Red
		rgb = make_float3(0,0,0);
		rgb.x = pixels[5];
		g_diff = fabsf((float)pixels[1]-pixels[9]) - fabsf((float)pixels[4]-pixels[6]);
		rgb.y = fabsf(g_diff) > EDGE_THRESH ?
			(g_diff < 0) ?
			0.5f*(pixels[1]+pixels[9]) : 0.5f*(pixels[4]+pixels[6]) :
			((float)pixels[1]+pixels[4]+pixels[6]+pixels[9])*0.25f;
		rgb.z = ((float)pixels[0]+pixels[2]+pixels[8]+pixels[10])*0.25f;
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*(x+1),y);
		//Blue
		rgb.x = ((float)pixels[3]+pixels[5]+pixels[11]+pixels[13])*0.25f;
		g_diff = fabsf((float)pixels[4]-pixels[12]) - fabsf((float)pixels[7]-pixels[9]);
		rgb.y = fabsf(g_diff) > EDGE_THRESH ?
			(g_diff < 0) ?
			0.5f*(pixels[4]+pixels[12]) : 0.5f*(pixels[7]+pixels[9]) :
			((float)pixels[4]+pixels[7]+pixels[9]+pixels[12])*0.25f;
		rgb.z = pixels[8];
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*x,y+1);
		//Green2
		rgb.x = ((float)pixels[5]+pixels[13])*0.5f;
		rgb.y = pixels[9];
		rgb.z = ((float)pixels[8]+pixels[10])*0.5f;
		yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*(x+1),y+1);
	}
}
#else
__global__ void convertBayer(int srcW, int srcH){

	for(int idx=2*(blockIdx.x*blockDim.x + threadIdx.x);
			idx < (srcW*srcH); idx += 2*(blockDim.x*gridDim.x)){

		int x = idx%srcW;
		int y = idx/srcW;
		uchar3 rgb;
		uchar4 yuv;
		if(y&1){ //BG row
			rgb = getBlue(x,y);
			yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
			surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*x,y);
			x++;
			rgb = getGreen2(x,y);
			yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
			surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*x,y);
		}else{ //GR row
			rgb = getGreen1(x,y);
			yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
			surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*x,y);
			x++;
			rgb = getRed(x,y);
			yuv = make_uchar4( CLAMP_255(Y(rgb)), CLAMP_255(U(rgb)), CLAMP_255(V(rgb)), 0);
			surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*x,y);
		}
	}
}

#endif

__device__ uint8_t getFilteredChroma(uint8_t * chrom, int x, int y){
#define SWAP(i,j) { \
	const uint8_t a = min(chrom[i], chrom[j]); \
	const uint8_t b = max(chrom[j], chrom[i]); \
	chrom[i] = a; chrom[j] = b; \
}
	SWAP(0,1); SWAP(0,2); SWAP(0,3); SWAP(0,4); SWAP(0,5); SWAP(0,6); SWAP(0,7); SWAP(0,8);
	SWAP(1,2); SWAP(1,3); SWAP(1,4); SWAP(1,5); SWAP(1,6); SWAP(1,7); SWAP(1,8);
	SWAP(2,3); SWAP(2,4); SWAP(2,5); SWAP(2,6); SWAP(2,7); SWAP(2,8);
	SWAP(3,4); SWAP(3,5); SWAP(3,6); SWAP(3,7); SWAP(3,8);
	SWAP(4,5); SWAP(4,6); SWAP(4,7); SWAP(4,8);
	return chrom[4];
#undef SWAP
}

__global__ void chromaMedianFiltering(int srcW, int srcH){
	for(int idx=(blockIdx.x*blockDim.x + threadIdx.x);
			idx < (srcW*srcH); idx += (blockDim.x*gridDim.x)){
		int x = idx%srcW;
		int y = idx/srcW;
		uint8_t uArr[9];
		uint8_t vArr[9];
		uchar4 yuv; 
		for(int i=0; i<3; ++i){
			for(int j=0; j<3; ++j){
				surf2Dread(&yuv, yuvSurfPreMedian, sizeof(uchar4)*(x+i-1),
						y+j-1, cudaBoundaryModeClamp);
				uArr[j+i*3] = yuv.y;
				vArr[j+i*3] = yuv.z;
			}
		}
		surf2Dread(&yuv, yuvSurfPreMedian, sizeof(uchar4)*x,y);
		yuv.y = getFilteredChroma(uArr, x, y);
		yuv.z = getFilteredChroma(vArr, x, y);
		surf2Dwrite(yuv, yuvSurf, sizeof(uchar4)*x,y);
	}
}

/* Debug funcion */
__global__ void toPlanar422(uint8_t * output, int dstW, int dstH){
	for(int idx=2*(blockIdx.x*blockDim.x + threadIdx.x);
			idx < (dstW*dstH); idx += 2*(blockDim.x*gridDim.x)){
		float uVal, vVal;
		int x = idx%dstW;
		int y = idx/dstW;
		uchar4 yuv;
		surf2Dread(&yuv, yuvSurf, sizeof(uchar4)*x, y);
		uVal = 0.5f*yuv.y;
		vVal = 0.5f*yuv.z;
		output[idx] = yuv.x;
		x++;
		surf2Dread(&yuv, yuvSurf, sizeof(uchar4)*x, y);
		uVal += 0.5f*yuv.y;
		vVal += 0.5f*yuv.z;
		output[idx+1] = yuv.x;
		output[dstW*dstH + (idx>>1)] = rintf(uVal);
		output[(dstW*dstH*3 + idx)>>1] = rintf(vVal);
	}
}

#ifdef GPU_BAYER_PRINT_DEBUGTIME
static float totalTime = 0;
static int totalFrames = 0;
#endif

void CudaBayerConverter::convertImage(cudaArray * input, cudaArray * output){

#ifdef GPU_BAYER_PRINT_DEBUGTIME
	struct timeval start_time, stop_time;
	gettimeofday(&start_time, 0);
#endif

	cudaSafe(cudaBindTextureToArray(bayerTex, input));

	/* Perform algorithm specific initial passes, for many agorithms this
	   is most of the work */
	performInitialPasses();

	/* Based on median filtering, we swap the buffers around a bit to ensure
	   correct input / output */
#if(MEDIAN_FILTER_ITERATIONS > 0)
	cudaArray * medianBuffers[2];
#if((MEDIAN_FILTER_ITERATIONS % 2)==0)
	medianBuffers[0] = output;
	medianBuffers[1] = medianFilterBuffer;
#else
	medianBuffers[0] = medianFilterBuffer;
	medianBuffers[1] = output;
#endif

	cudaSafe(cudaBindSurfaceToArray(yuvSurf, medianBuffers[0]));
	convertBayer<<< grid, block, 0, stream >>>(width, height);

	for(int i=0; i<MEDIAN_FILTER_ITERATIONS; ++i){
		cudaSafe(cudaStreamSynchronize(stream));
		cudaSafe(cudaBindSurfaceToArray(yuvSurfPreMedian, medianBuffers[(i&1)]));
		cudaSafe(cudaBindSurfaceToArray(yuvSurf, medianBuffers[(i&1)^1]));

		chromaMedianFiltering<<< grid, block, 0, stream >>>(width, height);
	}
#else
	cudaSafe(cudaBindSurfaceToArray(yuvSurf, output));
#if defined(DO_FAST_KERNEL) || defined(FAST_BILINEAR) || defined(FAST_A_BILINEAR) || \
	defined(FAST_GRADIENT_CORRECTED) || defined(FAST_SMOOTH_HUE) || defined(FAST_NN)
	convertBayer<<< 1+(width*height)/1024, 128, 0, stream >>>(width, height);
#else
	convertBayer<<< 1+(width*height)/4096, 128, 0, stream >>>(width, height);
// 	convertBayer<<< 1+(width*height)/2048, 128, 0, stream >>>(width, height);
#endif
	cudaSafe(cudaStreamSynchronize(stream));
#endif

#ifdef GPU_BAYER_PRINT_DEBUGTIME
	gettimeofday(&stop_time, 0);
 	float time;
	time = (stop_time.tv_sec * 1.0e3 + stop_time.tv_usec / 1.0e3)
		- (start_time.tv_sec * 1.0e3 + start_time.tv_usec / 1.0e3);
	totalFrames++;
	if(totalFrames>20) totalTime += time;
#endif
}

/* DEBUG FUNCTION */
void CudaBayerConverter::convertToPlanar(cudaArray * input, void * output){
	cudaSafe(cudaBindSurfaceToArray(yuvSurf, input));
	toPlanar422<<< grid, block, 0, stream >>>((uint8_t*)output, width, height/numSources);
	cudaSafe(cudaStreamSynchronize(stream));
}

void CudaBayerConverter::initCudaArrays(){

	block = dim3(128);
	grid = dim3(1 + ((width*height)/(block.x*2)));
#ifdef GPU_BAYER_PRINT_DEBUGTIME
	totalTime = 0;
	totalFrames = 0;
#endif

	cudaSafe(cudaStreamCreate(&stream));
	cudaChannelFormatDesc byteChannel = cudaCreateChannelDesc<uint8_t>();

#if(MEDIAN_FILTER_ITERATIONS > 0)
	cudaChannelFormatDesc u4byteChannel = cudaCreateChannelDesc<uchar4>();
	cudaSafe(cudaMallocArray(&medianFilterBuffer,&u4byteChannel, width, height,
				cudaArraySurfaceLoadStore));
#endif

	cudaSafe(cudaMallocArray(&inputData[0]->cuArr,&byteChannel, width, height,
				cudaArraySurfaceLoadStore));
	cudaSafe(cudaMallocArray(&inputData[1]->cuArr,&byteChannel, width, height,
				cudaArraySurfaceLoadStore));

	bayerTex.normalized = 0;
	bayerTex.addressMode[0] = cudaAddressModeClamp;
	bayerTex.addressMode[1] = cudaAddressModeClamp;
	bayerTex.filterMode = cudaFilterModePoint;

	allocateExtraBuffers();
}

void CudaBayerConverter::deleteCudaArrays(){
	cudaSafe(cudaStreamDestroy(stream));
	if(inputData[0]){
		cudaSafe(cudaFreeArray(inputData[0]->cuArr));
		free(inputData[0]);
	}
	if(inputData[1]){
		cudaSafe(cudaFreeArray(inputData[1]->cuArr));
		free(inputData[1]);
	}

	freeExtraBuffers();

#if(MEDIAN_FILTER_ITERATIONS > 0)
		cudaSafe(cudaFreeArray(medianFilterBuffer));
#endif

#ifdef GPU_BAYER_PRINT_DEBUGTIME
	if(totalFrames>20)
// 		printf("%7.3f ms\n", totalTime/(totalFrames-20));
		LOG_I("Bayer time   : %7.3fms", totalTime/(totalFrames-20));
#endif
}

