// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "CudaUploader.hpp"

#include "Helpers/logging.h"

#include <sys/time.h>

using namespace CudaUpload;


CudaUploader::CudaUploader(){
	initialized = false;
}

CudaUploader::~CudaUploader(){
	if(!initialized) return;

	pthread_join(consumer, NULL);
	cudaSafe(cudaStreamDestroy(stream));
	if(inputData[0].ptr) cudaSafe(cudaFreeHost(inputData[0].ptr));
	if(inputData[1].ptr) cudaSafe(cudaFreeHost(inputData[1].ptr));
	if(totalFrames>20)
		LOG_I("Upload time  : %7.3fms", totalTime/(totalFrames-20));
}

void CudaUploader::init(FetchModule *producer, size_t frameSize){

	inputIdx = 0;
	this->frameSize = frameSize;
	this->producer = producer;

	cudaSafe(cudaStreamCreate(&stream));
	cudaSafe(cudaMallocHost(&inputData[0].ptr,frameSize));
	cudaSafe(cudaMallocHost(&inputData[1].ptr,frameSize));
	pthread_barrier_init(&startWork, NULL, 2);


#define HAS_CUDA
	if(pthread_create(&consumer, NULL, consumerStarter, (void*)this)){
		LOG_E("Failed to start uploader consumer thread");
		perror("pthread_create");
		exit(EXIT_FAILURE);
	}
	totalTime = 0;
	totalFrames = 0;

	initialized = true;
}

struct frame * CudaUploader::getCudaFrame(struct frame *buffer){

	pthread_barrier_wait(&startWork);
	inputIdx ^= 1;
	if(!inputData[inputIdx].ptr) return NULL;

	struct timeval start_time, stop_time;
	gettimeofday(&start_time, 0);

	buffer->hdr = inputData[inputIdx].hdr;
	uint8_t * data = (uint8_t*) inputData[inputIdx].ptr;
	cudaSafe(cudaMemcpyAsync(buffer->cuArr,
				data, frameSize, cudaMemcpyHostToDevice, stream));
	cudaSafe(cudaStreamSynchronize(stream));

	gettimeofday(&stop_time, 0);
 	float time;
	time = (stop_time.tv_sec * 1.0e3 + stop_time.tv_usec / 1.0e3)
		- (start_time.tv_sec * 1.0e3 + start_time.tv_usec / 1.0e3);
	totalFrames++;
	if(totalFrames>20) totalTime += time;

	return buffer;
}

struct frame * CudaUploader::getCudaArrayFrame(struct frame *buffer) {

	pthread_barrier_wait(&startWork);
	inputIdx ^= 1;
	if(!inputData[inputIdx].ptr) return NULL;

	struct timeval start_time, stop_time;
	gettimeofday(&start_time, 0);

	buffer->hdr = inputData[inputIdx].hdr;
	uint8_t * data = (uint8_t *)inputData[inputIdx].ptr;
	cudaSafe(cudaMemcpyToArrayAsync(buffer->cuArr, 0, 0,
				data, frameSize, cudaMemcpyHostToDevice, stream));
	cudaSafe(cudaStreamSynchronize(stream));

	gettimeofday(&stop_time, 0);
 	float time;
	time = (stop_time.tv_sec * 1.0e3 + stop_time.tv_usec / 1.0e3)
		- (start_time.tv_sec * 1.0e3 + start_time.tv_usec / 1.0e3);
	totalFrames++;
	if(totalFrames>20) totalTime += time;

	return buffer;
}

void CudaUploader::runConsumer(){
	int inIdx = inputIdx^1;
	while(true){
		struct frame * ret = producer->getFrame(&inputData[inIdx]);
		if(!ret){
			cudaSafe(cudaFreeHost(inputData[inIdx].ptr));
			inputData[inIdx].ptr = NULL;
			break;
		}
		pthread_barrier_wait(&startWork);
		inIdx ^= 1;
	}
	pthread_barrier_wait(&startWork);
}

void *CudaUpload::consumerStarter(void * context){
	((CudaUploader *)context)->runConsumer();
	pthread_exit(0);
	return NULL;
}
