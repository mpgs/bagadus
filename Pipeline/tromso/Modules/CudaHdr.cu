// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "CudaHdr.hpp"
#include <cuda.h>
#include <cuda_runtime.h>

#include <sys/time.h>

#include "./hdr/utils/utils.cu"
#include "./hdr/radiancemapper/radiancemapper_debevec.cu"
#include "./hdr/radiancemapper/radiancemapper_tocci.cu"
#include "./hdr/radiancemapper/radiancemapper_robertson.cu"

#include "./hdr/tonemapper/tonemapper_ward.cu"
#include "./hdr/tonemapper/tonemapper_histward.cu"
#include "./hdr/tonemapper/tonemapper_reinhard.cu"



#ifdef GPU_HDR_PRINT_DEBUGTIME
static float totalTime = 0;
static int totalFrames = 0;
#endif

void CudaHdr::runKernels(cudaArray *output, cudaArray *input,
        struct header *header, float scene_key)
{
    // Called per frame

#ifdef GPU_HDR_PRINT_DEBUGTIME
    struct timeval start_time, stop_time;
    gettimeofday(&start_time, 0);
#endif

    //run both member


    //	metadata->expoFirst = 1002;
    //	metadata->expoSecond = 2338;


    radianceMapper->run(stream, input, output, header, numSources, 2, width,
            height);
    cudaStreamSynchronize(stream);
    toneMapper->run(stream, output, header, numSources, width, height,
            scene_key);




    cudaSafe(cudaStreamSynchronize(stream));
#ifdef GPU_HDR_PRINT_DEBUGTIME
    gettimeofday(&stop_time, 0);
    float time;
    time = (stop_time.tv_sec * 1.0e3 + stop_time.tv_usec / 1.0e3)
        - (start_time.tv_sec * 1.0e3 + start_time.tv_usec / 1.0e3);
    totalFrames++;
    if(totalFrames>20) totalTime += time;
#endif
}

void CudaHdr::initCudaArrays(){
    //Called at init
    block = dim3(128);
    grid = dim3(1 + ((width*height)/(block.x*2)));

    cudaSafe(cudaStreamCreate(&stream));

    cudaChannelFormatDesc byteChannel = cudaCreateChannelDesc<uchar4>();
    cudaSafe(cudaMallocArray(&inputData[0].cuArr ,&byteChannel, width,
                height*numSources*2, cudaArraySurfaceLoadStore));
    cudaSafe(cudaMallocArray(&inputData[1].cuArr, &byteChannel, width,
                height*numSources*2, cudaArraySurfaceLoadStore));
#ifdef GPU_HDR_PRINT_DEBUGTIME
    totalTime = 0;
    totalFrames = 0;
#endif
}

void CudaHdr::deleteCudaArrays(){
    //Called at termination
    cudaSafe(cudaStreamDestroy(stream));
    cudaSafe(cudaFreeArray(inputData[0].cuArr));
    cudaSafe(cudaFreeArray(inputData[1].cuArr));

#ifdef GPU_HDR_PRINT_DEBUGTIME
    if(totalFrames>20)
        LOG_I("Hdr time     : %7.3fms", totalTime/(totalFrames-20));
#endif
}

