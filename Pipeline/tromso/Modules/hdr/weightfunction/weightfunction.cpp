// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "weightfunction.h"
#include "../utils/utils.h"

#include <stdlib.h>
#include <sstream>
#include <string>
#include <fstream>
#include <cmath>


WeightFunction::WeightFunction()
{

}

WeightFunction::~WeightFunction()
{

}

#ifdef DEBUG
void WeightFunction::printWeightFunction()
{
	if(weightFunction != 0)
	{
		std::stringstream ss;
		ss << "response function: ";
		for(size_t i = 0; i < weightFunction->size(); ++i)
		{
				if(i != 0)
			
				ss << weightFunction->at(i) << ", ";
		}

		std::string s = ss.str();
		printDebug(s.c_str());
	}
}
#endif 

int WeightFunction::readWeightFromFile(const char* path)
{
    int error = ERROR_OK;
    if(path == 0)
    {
        error = ERROR_INVALID_ARGUMENTS;
        printError("readResponseWeightFromFile: path is null");
        return error;
    }
    std::ifstream fin;
    fin.open(path); // open a file
    if (!fin.good()) // exit if file not found 
    {
        error = ERROR_FILE;
        printError("readResponseWeightFromFile: couldn't open file");
        return error;
    }
         

    //char oneline[MAX_LINE_LENGTH];
    if(!weightFunction)
        weightFunction = new std::vector<double>();
    weightFunction->erase(weightFunction->begin(), weightFunction->end());
    float tmp_val;
   while (fin)
   {
       fin >> tmp_val;
        weightFunction->push_back(tmp_val);      
       if(error)
            return error;
    }

   fin.close();

    return error;
}

int WeightFunction::writeWeightToFile(const char* path)
{
    int error = ERROR_OK;
    if(path == 0)
    {
        error = ERROR_INVALID_ARGUMENTS;
        printError("writeResponseWeightToFile: path is null");
        return error;
    }

    if(weightFunction == 0)
    {
        error = ERROR_UNINITIALIZED_MEMBER;
        printError("writeResponseWeightToFile: no response function to print");
        return error;
    }


    std::ofstream outputFile;
    outputFile.open(path);
    if(outputFile.is_open())
    {
        for(unsigned int resp_it = 0; resp_it < weightFunction->size(); ++resp_it)
        {
            outputFile << weightFunction->at(resp_it) << std::endl;
        }
    }
    else
    {
        error = ERROR_FILE;
        printError("writeResponseFunctionToFile: couldn't create file");

    }
    return error;
}

std::vector<double>* WeightFunction::getWeightFunction()
{
    if(!weightFunction)
    {
        printError("WeightFunction::getWeightFunction: no weight function to return");
        return NULL;
    }

    return weightFunction;
}