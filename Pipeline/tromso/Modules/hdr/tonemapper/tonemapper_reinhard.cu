// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <cufft.h>



cufftHandle plan_image;
cufftHandle plan_gauss_filter;
cufftHandle plan_reverse;

cufftComplex* gauss_kernels;
cufftComplex* complex_convolved_Images;
cufftComplex* complex_input_image;
cufftReal* real_convolved_Images;

#ifdef DEBUG
//forward declarations
//__device__ float debug_get_real_max(cset_tone* settings, cufftReal* image);
//__global__ void debug_print_image_real(cset_tone* settings, cufftReal* image);
//__device__ float debug_get_complex_max(cset_tone* settings, cufftComplex* image);
//__global__ void debug_print_image_complex(cset_tone* settings, cufftComplex* image);
//__global__ void debug_print_complex_image_complex(cset_tone* settings, cufftComplex* image);
//__global__ void debug_print_gauss(cset_tone* settings, cufftComplex* _kernels, int num_kernel);
//__global__ void debug_count_values_gauss(cset_tone* settings, cufftComplex* _kernels, int num_kernel);
//__device__ void debug_write_v1_image(cset_tone* settings, int x_pos, int y_pos, float v1);
//__device__ void debug_write_preferred_scale_image(cset_tone* settings, int pref_scale, int x_pos, int y_pos);
//void test_gaussfft(cset_tone* settings, cset_tone* setaddr);
//void test_radianceimage_fft(cset_tone* settings, cset_tone* setdaddr);
#endif


/**
 * multiplies two complex numbers and then multiplies a real number with the result
 * @param settings pointer to the settings struct
 * @param[out] d_dest   result of operation is stored here
 * @param d_SrcA   first complex number
 * @param d_SrcB   second complex number that is multiplied with first
 * @param c        real value that is then multiplied with the result
 */
__global__ void kernel_simple_process2D(int width, int height, cufftComplex* d_dest, cufftComplex* d_SrcA, cufftComplex* d_SrcB, float c)
{
    int x_pos = blockIdx.x * blockDim.x + threadIdx.x, y_pos = blockIdx.y * blockDim.y + threadIdx.y;
    if(x_pos < width  && y_pos < height)
    {
        cufftComplex input_image_pixel = d_SrcA[y_pos * width + x_pos];
        cufftComplex gauss_kernels_pixel = d_SrcB[y_pos * width + x_pos];
        cufftComplex result = {0.0f, 0.0f};
        result.x = c * (input_image_pixel.x * gauss_kernels_pixel.x - input_image_pixel.y * gauss_kernels_pixel.y);
        result.y = c * (input_image_pixel.y * gauss_kernels_pixel.x + input_image_pixel.x * gauss_kernels_pixel.y);
        d_dest[y_pos * width + x_pos].x = result.x;
        d_dest[y_pos * width + x_pos].y = result.y;
    }
}

/**
 * multiplies two complex images pixel wise
 * @param settings pointer to settings struct
 * @param setaddr  pointer to pointer of settings
 * @param[out] d_dest   image to store result in
 * @param d_SrcA   first complex image
 * @param d_SrcB   second complex image
 */
void simple_process2D(int width, int height, cufftComplex* d_dest, cufftComplex* d_SrcA, cufftComplex* d_SrcB, cudaStream_t stream)
{
    float c = 0.5f/(width * height);
    dim3 threadsPerBlock(16, 16);
    dim3 numBlocks(divup(width, threadsPerBlock.x), divup(height, threadsPerBlock.y));
    kernel_simple_process2D<<<numBlocks, threadsPerBlock, 0, stream>>>(width, height, d_dest, d_SrcA, d_SrcB, c);
    
}


/**
 * gets the value of two pixels that were blurred with different gauss filters. The filters are always next to each other in size.
 * These values are referred to as v1 and v2 in the paper, hence the name
 * @param  settings              pointer to the settings struct
 * @param  x_pos                 x position of the pixel in the image
 * @param  y_pos                 y position of the pixel in the image
 * @param  gauss_it              number of the smaller of the two gauss kernels to look up
 * @param  real_convolved_Images pointer to the images that were blurred with different gauss filter sizes
 * @return                       two values of the pixel at different gauss sizes
 */
__device__ float2 get_v1_and_v2(int width, int height, int x_pos, int y_pos, int gauss_it, cufftReal* real_convolved_Images)
{
    float2 return_value = {0.0f, 0.0f};
    //int pixels_per_image = settings->d_image_dimensions.x * settings->d_image_dimensions.y;
    // return_value.x = real_convolved_Images[gauss_it * pixels_per_image + y_pos * settings->d_image_dimensions.x + x_pos];
    // return_value.y = real_convolved_Images[(gauss_it + 1) * pixels_per_image + y_pos * settings->d_image_dimensions.x + x_pos];
    return_value.x = read_pxl_float((float*)real_convolved_Images, x_pos, y_pos, width, height, gauss_it);
    return_value.y = read_pxl_float((float*)real_convolved_Images, x_pos, y_pos, width, height, gauss_it + 1);
    return return_value;
}

/**
 * implements equation 7 in the paper 
 * @param  settings              pointer to the settings struct
 * @param  x_pos                 x position of the current pixel in the image
 * @param  y_pos                 y position of the current pixel in the image
 * @param  gauss_it              number of the gauss kernel that is the smaller of the two used
 * @param  real_convolved_Images images that were blurred with different gauss kernel sizes
 * @param  key                   desired key of the scene (key is used as midtone in an image)
 * @param  gauss_size            the size of the smaller gauss kernel in pixels
 * @return                       result of equation 7 in the paper (used to determine the size of the correct gauss kernel)
 */
__device__ float calculate_center_surround(int width, int height, int x_pos, int y_pos, int gauss_it, cufftReal* real_convolved_Images, float key, float gauss_size)
{
    float return_value = 0.0f;
    float2 v1_and_v2 = get_v1_and_v2(width, height, x_pos, y_pos, gauss_it, real_convolved_Images);
    return_value = (v1_and_v2.x - v1_and_v2.y) / ( ( (key * powf(2.0, PHI_SHARPENING)) / (expf(gauss_size) * expf(gauss_size)) )  + v1_and_v2.x);
    return return_value;
}

/**
 * creates gauss kernels of increasing sizes starting at width of 1 pixel up to NUM_GAUSSKERNELS. Each successive kernels is 1.6 times the
 * size of the previous one. The kernels are stored in an image the same size as the input image (needed for fft)
 * @param settings pointer to the settings struct
 * @param[out] _kernels pointer to where the kernels will be written to
 */
__global__ void create_gauss(int width, int height, cufftComplex* _kernels)
{
    int x_pos = blockIdx.x * blockDim.x + threadIdx.x, y_pos = blockIdx.y * blockDim.y + threadIdx.y;
    if(x_pos < width  && y_pos < height)
    {

        float scale = 1.0f, x = 0.0f, y = 0.0f, c = 1.0f / 4.0f, a = 0.0f, s = 0.0f, t = 0.0f;
        //int pixels_per_image = settings->d_image_dimensions.x * settings->d_image_dimensions.y;
        x = (x_pos >= (width / 2)) ? x_pos - width : x_pos;
        y = (y_pos >= (height / 2)) ? y_pos - height : y_pos;
        for(int gauss_it = 0; gauss_it < NUM_GAUSSKERNELS; ++gauss_it)
        {
            a = 1.0f / (scale * 1.0f / (2.0f * 1.4142136f));
            s = erf(a * (y - 0.5f)) - erf(a * (y + 0.5f));
            t = erf(a * (x - 0.5f)) - erf(a * (x + 0.5f));
            _kernels[calc_pxl_pos(x_pos, y_pos, width, height, gauss_it)].x = (s * t * c);
            _kernels[calc_pxl_pos(x_pos, y_pos, width, height, gauss_it)].y = 0.0f;
            scale *= 1.6f;
        }
    }
}


/**
 * unfortunate hack needed. Converts the input rgb image into a luminance image that is already stored in its complex format.
 * This function also performs the same functionality as scale_to_midtone, so no need to call that anymore (for performance reasons)    
 * @param settings            pointer to the settings struct
 * @param complex_input_image complex image that will be created by this function. Assumes memory is already allocated
 * @param avg                 average luminance of the input image
 */
__global__ void create_radiance_image(cudaTextureObject_t input_tex, cudaSurfaceObject_t output_tex, cufftComplex* complex_input_image, int width, int height, float avg, float scene_key_brightness)
{
    int x_pos = blockIdx.x * blockDim.x + threadIdx.x, y_pos = blockIdx.y * blockDim.y + threadIdx.y;
    if(x_pos < width  && y_pos < height)
    {

        
        uchar4 in_pixel = read_pxl(input_tex, x_pos, y_pos, width);
        float3 converted_in_pixel = {0.0f, 0.0f, 0.0f};
        rp_to_flt3(&in_pixel, &converted_in_pixel);

        //if(x_pos == 0 && y_pos == 0)
            //printf("converted_in_pixel.x: %f, y: %f, z: %f\n", converted_in_pixel.x, converted_in_pixel.y, converted_in_pixel.z);
            //printf("bang\n");
        #ifdef INPUT_COLORSPACE_RGB
            PixelRGBToYUV(&converted_in_pixel);
        #endif
        converted_in_pixel.x *= ((1.0f / (avg)) * scene_key_brightness * 3.0f);
        converted_in_pixel.y *= ((1.0f / (avg)) * scene_key_brightness * 3.0f);
        converted_in_pixel.z *= ((1.0f / (avg)) * scene_key_brightness * 3.0f);
        #ifdef INPUT_COLORSPACE_RGB
            PixelYUVToRGB(&converted_in_pixel);
        #endif
        
        double luminance = (0.27 * (double)converted_in_pixel.x + 0.67 * (double)converted_in_pixel.y + 0.06 * (double)converted_in_pixel.z);
        complex_input_image[calc_pxl_pos(x_pos, y_pos, width)].x = luminance;
        complex_input_image[calc_pxl_pos(x_pos, y_pos, width)].y = 0;

        //if(x_pos == 1 && y_pos == 1)
        //    printf("create_radiance_image: luminance: %f\n", luminance);

        flt3_to_rp(&converted_in_pixel, &in_pixel);
        write_pxl(output_tex, x_pos, y_pos, width, in_pixel);
        
    }
}

/**
 * this performs the actual tonemapping. First a good scale for the gauss filter is selected and then equation 9 is applied to each
 * pixel. THRESHOLD_CONSTANT has influence on size of chosen Gausskernel. 
 * @param settings              pointer to the struct of settings
 * @param real_convolved_Images the input image convolved with several gauss filters of different sizes
 * @param key                   desired key of the whole image
 */
__global__ void tone_mapper_reinhard(cudaTextureObject_t input_tex, cudaSurfaceObject_t output_tex, int width, int height, cufftReal* real_convolved_Images, double key)
{
    int x_pos = blockIdx.x * blockDim.x + threadIdx.x, y_pos = blockIdx.y * blockDim.y + threadIdx.y;
    if(x_pos < width  && y_pos < height)
    {
    int preferred_scale = 0;

    float v1 = 0.0f;
    //iterate over scales
    float size = 1;
    for(int scale_it = 0; scale_it < NUM_GAUSSKERNELS - 1; ++scale_it)
    {
        //get value at that scale for pixel
        float value = calculate_center_surround(width, height, x_pos, y_pos, scale_it, real_convolved_Images, key, size);
        //compare
        if (fabs(value) > THRESHOLD_CONSTANT)
        {
            preferred_scale = scale_it;
            break;
        }
        size *= 1.6f;
    }

    // v1 = d_clip(get_v1_and_v2(settings, x_pos, y_pos, preferred_scale, real_convolved_Images).x, 0.0f, 1.0f);
    v1 = get_v1_and_v2(width, height, x_pos, y_pos, preferred_scale, real_convolved_Images).x;

    //debug_write_preferred_scale_image(settings, preferred_scale, x_pos, y_pos); 
//    debug_write_v1_image(settings, x_pos, y_pos, v1);
    // if(x_pos % 100 ==  0 && y_pos % 100 == 0)
    // {
    //     printf("out_luminance: %f, preferred_scale: %i \n", v1, preferred_scale);
    // }
    //convert to rgb
    uchar4 in_pixel = read_pxl(input_tex, x_pos, y_pos, width);

    float3 converted_in_pixel = {0.0f, 0.0f, 0.0f};
    rp_to_flt3(&in_pixel, &converted_in_pixel);
   

    // float luminosity = (0.27f * converted_in_pixel.x + 0.67f * converted_in_pixel.y + 0.06f * converted_in_pixel.z);
    
    // // float out01 = (v1 >= DISPLAY_MIN_LUMINANCE)?(v1 - DISPLAY_MIN_LUMINANCE) / (DISPLAY_MAX_LUMINANCE - DISPLAY_MIN_LUMINANCE):DISPLAY_MIN_LUMINANCE ;
    // // float scaling = out01 / luminosity;
    // //float scaling = (v1) / luminosity;
    // float scaling = luminosity / (1.0f + v1);
    // // scaling = expf(scaling);
    // converted_in_pixel.x *= scaling;
    // converted_in_pixel.y *= scaling;
    // converted_in_pixel.z *= scaling;

    float luminosity = ((0.27f * converted_in_pixel.x + 0.67f * converted_in_pixel.y + 0.06f * converted_in_pixel.z));
    // v1 = expf(v1);
    //PixelRGBToYUV(&converted_in_pixel);
    //float scaling = luminosity / (1.0f + v1);
    //converted_in_pixel.x = scaling;
     // converted_in_pixel.y *= scaling;
     // converted_in_pixel.z *= scaling;
    //PixelYUVToRGB(&converted_in_pixel);
    
    float factor = luminosity / (1.0f + v1);

    float scaling = factor/luminosity;
    converted_in_pixel.x *= scaling;
    converted_in_pixel.y *= scaling;
    converted_in_pixel.z *= scaling;


    #ifdef USE_GAMMA_CORRECTION
        converted_in_pixel.x = powf(converted_in_pixel.x, 1.0f/GAMMA_VALUE);
        converted_in_pixel.y = powf(converted_in_pixel.y, 1.0f/GAMMA_VALUE);
        converted_in_pixel.z = powf(converted_in_pixel.z, 1.0f/GAMMA_VALUE);
    #endif
   // if(x_pos == 325 && y_pos == 175)
   //     printf("___converted_in_pixel: r: %f, g: %f, b: %f, scaling: %f, luminosity: %f, v1: %f\n", converted_in_pixel.x, converted_in_pixel.y, converted_in_pixel.z, scaling, luminosity, v1);
    // if(x_pos % 100 == 0 && y_pos % 100 == 0)
    //     printf("converted_in_pixel: r: %f, g: %f, b: %f, scaling: %f, luminosity: %f, v1: %f\n", converted_in_pixel.x, converted_in_pixel.y, converted_in_pixel.z, scaling, luminosity, v1);
        


    float3 out_pixel = {0.0f, 0.0f, 0.0f};
    out_pixel.x = d_clip((converted_in_pixel.x * 255.0f), 0.0f, 255.0f);
    out_pixel.y = d_clip((converted_in_pixel.y * 255.0f), 0.0f, 255.0f);
    out_pixel.z = d_clip((converted_in_pixel.z * 255.0f), 0.0f, 255.0f);
    
    write_pxl_rgb(output_tex, x_pos, y_pos, width, out_pixel);

    }
}

/**
 * hack to circumvent memory garbage created by inverse FFT and in the end saving memory. Simply copies the real part 
 * of a complex image to a real image
 * @param settings   pointer to the settings struct
 * @param in_complex complex input image
 * @param[out] out_real   real output image
 */
__global__ void copy_complex_to_real(int width, int height, cufftComplex* in_complex, cufftReal* out_real)
{
    int x_pos = blockIdx.x * blockDim.x + threadIdx.x, y_pos = blockIdx.y * blockDim.y + threadIdx.y;
    if(x_pos < width  && y_pos < height)
    {
        int curr_pos = 0;
        for(int gauss_it = 0; gauss_it < NUM_GAUSSKERNELS; ++gauss_it)
        {
            curr_pos = calc_pxl_pos(x_pos, y_pos, width, height, gauss_it);
            out_real[curr_pos] = in_complex[curr_pos].x;
        }
    }   
}

/**
 * creates the plans nescessary for all fft operations, creates gauss kernels and performs a forward fft on them
 * This only needs to be called once on startup of the tone mapper
 * @param settings pointer to the settings struct
 * @param setaddr  pointer to pointer of settings struct
 */
//extern "C" void cu_tone_mapper_reinhard_init(int num_sources, int width, int height, cufftHandle plan_image, cufftHandle plan_gauss_filter, cufftHandle plan_reverse, cufftComplex* gauss_kernels,  cufftComplex* complex_input_image)
extern "C" void cu_tone_mapper_reinhard_init(int num_sources, int width, int height)
{
    // test_gaussfft(settings, setaddr);

    height = height * num_sources;
    //create gauss images
    cufftResult cufft_error;
    cudaError_t cuda_error;

    dim3 threadsPerBlock(16, 16);
    dim3 numBlocks(divup(width, threadsPerBlock.x), divup(height, threadsPerBlock.y));

    //create plan for the image
    cufft_error = cufftPlan2d(&plan_image, height, width, CUFFT_C2C);
    if(cufft_error != CUFFT_SUCCESS)
        printError("tonemapper_reinhard: init: cufft make plan image failed");
    
    //create plan for the gauss
    int dimensions[2] = {height, width};
    cufft_error = cufftPlanMany(&plan_gauss_filter, 2, dimensions, NULL, 1, 0, NULL, 1, 0, CUFFT_C2C, NUM_GAUSSKERNELS);
    if(cufft_error != CUFFT_SUCCESS)
        printError("tonemapper_reinhard: init: cufft make plan for gauss kernels failed");

    //create plan for the reverse transform of the combined images
    //cufft_error = cufftPlan2d(&plan_reverse, height, width, CUFFT_C2R);
    cufft_error = cufftPlanMany(&plan_reverse, 2, dimensions, NULL, 1, 0, NULL, 1, 0, CUFFT_C2C, NUM_GAUSSKERNELS);
    //cufft_error = cufftPlanMany(&plan_reverse, 2, dimensions, NULL, 1, 0, NULL, 1, 0, CUFFT_C2R, NUM_GAUSSKERNELS);
    
    if(cufft_error != CUFFT_SUCCESS)
        printError("tonemapper_reinhard: init: cufft make plan image failed");

    //allocate the input image
    //cuda_check(cudaMalloc(&complex_input_image, sizeof(cufftComplex) * dimensions[0] * dimensions[1]), "tonemapper_reinhard: init: faild allocating complex input image");
    
    //allocate gauss kernels
    cuda_check(cudaMalloc(&gauss_kernels, sizeof(cufftComplex) * dimensions[0] * dimensions[1] * NUM_GAUSSKERNELS), "tonemapper_reinhard: init: faild allocating gauss kernels");
    
    //create the gauss kernels
    create_gauss<<<numBlocks, threadsPerBlock>>>(width, height, gauss_kernels);
        cuda_error = cudaGetLastError();
    if(cuda_error != cudaSuccess)
        printError("tonemapper_reinhard: init: create_gauss kernel launch failed");
    
    //print_image_complex<<<1,1>>>(setaddr, gauss_kernels + dimensions[0] * dimensions[1] * 7);

    //transform them to be used later
    cufft_error = cufftExecC2C(plan_gauss_filter, gauss_kernels, gauss_kernels, CUFFT_FORWARD);
    if(cufft_error != CUFFT_SUCCESS)
        printError("tonemapper_reinhard: init: executing fft of gauss filters failed");

    //cuda_check(cudaMalloc(&complex_convolved_Images, sizeof(cufftComplex) * width * height * num_sources * NUM_GAUSSKERNELS), "tonemapper_reinhard: tone_mapper_reinhard: failed to allocate complex convolved image");
    //cuda_check(cudaMalloc(&real_convolved_Images, sizeof(cufftReal) * width * height * num_sources * NUM_GAUSSKERNELS), "tonemapper_reinhard: tone_mapper_reinhard: fail allocating real convolved images");


    //print_image_complex<<<1,1>>>(setaddr, gauss_kernels + dimensions[0] * dimensions[1] * 7);


}

//extern "C" void cu_tone_mapper_reinhard(cudaStream_t stream, cudaArray* output, double* d_tmp_image, resourceHdr* metadata, int numSources, int width, int height, cufftHandle plan_image, cufftHandle plan_gauss_filter, cufftHandle plan_reverse, cufftComplex* gauss_kernels, cufftComplex* complex_input_image, cufftComplex* complex_convolved_Images, cufftReal* real_convolved_Images)
extern "C" void cu_tone_mapper_reinhard(cudaStream_t stream, cudaArray* output, double* d_tmp_image, struct header * metadata, int numSources, int width, int height, float scene_key_brightness)

{
    //std::cout << "starting tone mapper" << std::endl;
    cudaError_t cuda_error;
    cufftResult cufft_error;
    height = height * numSources;

    dim3 threadsPerBlock(16, 16);
    dim3 numBlocks(divup(width, threadsPerBlock.x), divup(height, threadsPerBlock.y));
    int pixels_per_image = width * height;

	//Small edit added by ragnar
	static bool first = true;
	if(first){
		first = false;
		cufft_error = cufftSetStream(plan_image, stream);
		if(cufft_error != CUFFT_SUCCESS)
			std::cout << "tonemapper_reinhard: Attaching stream to cufft failed: " << cufft_error << std::endl;
		cufft_error = cufftSetStream(plan_reverse, stream);
		if(cufft_error != CUFFT_SUCCESS)
			std::cout << "tonemapper_reinhard: Attaching stream to cufft failed: " << cufft_error << std::endl;
	}

   
  //  cudaEvent_t start, stop;
  //  cudaEventCreate(&start);
  //  cudaEventCreate(&stop);

  //  cudaEventRecord(start);


    cudaTextureObject_t input_tex = 0;
    cudaSurfaceObject_t output_tex = 0;
    create_tex_obj(output, &input_tex);
    create_surf_obj(output, &output_tex);
    

    cuda_check(cudaMalloc(&d_tmp_image, sizeof(double) * width * height), "tonemapper_reinhard: init: failed to allocate temp image");
    

    //get key value of whole scene
    double accumulated_luminance = 0.0f;
    accumulated_luminance = run_reduce<double>(width * height, width, input_tex, d_tmp_image, stream);

   // std::cout << "accumulated_luminance: " << accumulated_luminance << std::endl;


    // scale_to_midtone<<<numBlocks, threadsPerBlock>>>(setaddr, 0.18, accumulated_luminance);
    // cuda_error = cudaGetLastError();
    // if(cuda_error != cudaSuccess)
    //     std::cout << "tonemapper_reinhard: scale to midtone: kernel launch failed: " << cudaGetErrorString(cuda_error) << std::endl;    

    cuda_check(cudaFree(d_tmp_image), "tonemapper_reinhard: failed to free temp image"); 
    cuda_check(cudaMalloc(&complex_input_image, sizeof(cufftComplex) * width * height), "tonemapper_reinhard: init: faild allocating complex input image");
       

    create_radiance_image<<<numBlocks, threadsPerBlock, 0, stream>>>(input_tex, output_tex, complex_input_image, width, height, accumulated_luminance, scene_key_brightness);
    cuda_error = cudaGetLastError();
    if(cuda_error != cudaSuccess)
        std::cout << "tonemapper_reinhard: create radiance image: kernel launch failed: " << cudaGetErrorString(cuda_error) << std::endl;    


   //  //print_image_complex<<<1,1>>>(setaddr, complex_input_image);


    //create fft of input image
    cufft_error = cufftExecC2C(plan_image, complex_input_image, complex_input_image, CUFFT_FORWARD);
    if(cufft_error != CUFFT_SUCCESS)
        std::cout << "tonemapper_reinhard: fft of input image failed: " << cufft_error << std::endl;

    
    cuda_check(cudaMalloc(&complex_convolved_Images, sizeof(cufftComplex) * pixels_per_image * NUM_GAUSSKERNELS), "tonemapper_reinhard: tone_mapper_reinhard: failed to allocate complex convolved image");
    
    
    //simple_process2D(settings, setaddr, complex_convolved_Images, complex_input_image, gauss_kernels);

    for(int gauss_it = 0; gauss_it < NUM_GAUSSKERNELS; ++gauss_it)
    {
        simple_process2D(width, height, complex_convolved_Images + (gauss_it * pixels_per_image), complex_input_image, gauss_kernels + (gauss_it * pixels_per_image), stream);
    }

    cufft_error = cufftExecC2C(plan_reverse, complex_convolved_Images, complex_convolved_Images, CUFFT_INVERSE);
    if(cufft_error != CUFFT_SUCCESS)
        std::cout << "tonemapper_reinhard: executing plan reverse failed: " << cufft_error << std::endl;
    
    cuda_check(cudaFree(complex_input_image), "tonemapper_reinhard: tone_mapper_reinhard: failed to free complex_input_image");
    

    cuda_check(cudaMalloc(&real_convolved_Images, sizeof(cufftReal) * pixels_per_image * NUM_GAUSSKERNELS), "tonemapper_reinhard: tone_mapper_reinhard: fail allocating real convolved images");
    
    //call kernel to copy from complex data type to real datatype
    copy_complex_to_real<<<numBlocks, threadsPerBlock, 0, stream>>>(width, height, complex_convolved_Images, real_convolved_Images);
    cuda_error = cudaGetLastError();
    if(cuda_error != cudaSuccess)
        std::cout << "tonemapper_reinhard: could not launch kernel copy_complex_to_real: " << cudaGetErrorString(cuda_error) << std::endl;    
    
    
    cuda_check(cudaFree(complex_convolved_Images), "tonemapper_reinhard: tone_mapper_reinhard: failed to free complex_convolved_Images");
           
   
   //call the kernel that picks the best pixel
    tone_mapper_reinhard<<<numBlocks, threadsPerBlock, 0, stream>>>(input_tex, output_tex, width, height, real_convolved_Images, accumulated_luminance);
    
//    cudaEventRecord(stop);
    
//    cudaEventSynchronize(stop);
//    float milliseconds = 0;
//    cudaEventElapsedTime(&milliseconds, start, stop);

//    std::cout << "elapsed time tonemapper reinhard " << milliseconds << std::endl;

    cuda_check(cudaFree(real_convolved_Images), "tonemapper_reinhard: cu_tone_mapper_reinhard: failed to free real_convolved_Images");

    cuda_error = cudaGetLastError();
    if(cuda_error != cudaSuccess)
        std::cout << "tone mapper kernel launch fail: " << cudaGetErrorString(cuda_error) << std::endl;

   // std::cout << "came to end of tonemapper" << std::endl;

    cuda_check(cudaDestroyTextureObject(input_tex), "tonemapper_reinhard: failed destroying texture object"); 
    cuda_check(cudaDestroySurfaceObject(output_tex), "tonemapper_reinhard: failed destroying surface object"); 
}




#ifdef DEBUG
//#####################################################################
//############# DEBUG ##################################################
//#####################################################################

/**
 * calculates maximum value of given real image
 * @param  settings pointer to settings struct
 * @param  image    image to get maximum of
 * @return          maximum value in given image
 */
/*__device__ float debug_get_real_max(cset_tone* settings, cufftReal* image)
{
    if(image == NULL)
        return 0.0f;
    int pixels_per_image = width * height;
    float max = image[0];
    for(int pixel_it = 1; pixel_it < pixels_per_image; ++pixel_it)
    {
        if(image[pixel_it] > max)
            max = image[pixel_it];
    }

    return max;    
}*/

/**
 * writes the given real image to output
 * @param settings pointer to settings struct
 * @param image    image that needs to be written
 */
/*__global__ void debug_print_image_real(cset_tone* settings, cufftReal* image)
{
    float max = debug_get_real_max(settings, image);
    for(int y_pos = 0; y_pos < settings->d_image_dimensions.y; ++y_pos)
    {
        for(int x_pos = 0; x_pos < settings->d_image_dimensions.x; ++x_pos)
        {

            uchar4& out_pixel = *(uchar4*)&(settings->d_out_image[(y_pos * settings->d_image_dimensions.x + x_pos)<<2]);        
            unsigned char value = (unsigned char)d_clip((int)(((float)image[y_pos * settings->d_image_dimensions.x + x_pos] /max) * 255.0f), 0, 255);
            out_pixel.x = value;
            out_pixel.y = value;
            out_pixel.z = value;
            out_pixel.w = 255;
            }
    }

}*/

/**
 * finds the max in the imaginary part of a given image
 * @param  settings pointer to settings struct
 * @param  image    image to find imaginary max of
 * @return          max value in the imaginary part of the given image
 */
/*__device__ float debug_get_imaginary_max(cset_tone* settings, cufftComplex* image)
{
    if(image == NULL)
        return 0.0f;
    int pixels_per_image = settings->d_image_dimensions.x * settings->d_image_dimensions.y;
    float max = image[0].y;
    for(int pixel_it = 1; pixel_it < pixels_per_image; ++pixel_it)
    {
        if(image[pixel_it].y > max)
            max = image[pixel_it].y;
    }

    return max;    
}*/

/**
 * get the real maximum of a complex image
 * @param  settings pointer to the settings struct
 * @param  image    image to find real max of 
 * @return          max value in the real part of the complex image
 */
/*__device__ float debug_get_complex_max(cset_tone* settings, cufftComplex* image)
{
    if(image == NULL)
        return 0.0f;
    int pixels_per_image = settings->d_image_dimensions.x * settings->d_image_dimensions.y;
    float max = image[0].x;
    for(int pixel_it = 1; pixel_it < pixels_per_image; ++pixel_it)
    {
        if(image[pixel_it].x > max)
            max = image[pixel_it].x;
    }

    return max;
}*/

/**
 * writes the real part of a complex image to a file
 * @param settings pointer to the settings struct
 * @param image    complex image of which to write the real part
 */
/*__global__ void debug_print_image_complex(cset_tone* settings, cufftComplex* image)
{
    float max = debug_get_complex_max(settings, image);
    for(int y_pos = 0; y_pos < settings->d_image_dimensions.y; ++y_pos)
    {
        for(int x_pos = 0; x_pos < settings->d_image_dimensions.x; ++x_pos)
        {

            uchar4 out_pixel = {0, 0, 0, 0};
            unsigned char value = (unsigned char)d_clip((int)((image[y_pos * settings->d_image_dimensions.x + x_pos].x /max) * 255.0f), 0, 255);
            out_pixel.x = value;
            out_pixel.y = value;
            out_pixel.z = value;
            out_pixel.w = 255;
            #ifdef DEBUG_IMAGE
                write_pxl_rgb(settings->d_debug, x_pos, y_pos, settings->d_image_dimensions.x, out_pixel);
            #else
                write_pxl_rgb(settings->d_out_image, x_pos, y_pos, settings->d_image_dimensions.x, out_pixel);
            #endif
        }
    }
 
}*/

/**
 * write the imaginary part of a complex image to file
 * @param settings pointer to the settings struct
 * @param image    complex image of which to write the imaginary part
 */
/*__global__ void debug_print_complex_image_complex(cset_tone* settings, cufftComplex* image)
{
    float max = debug_get_imaginary_max(settings, image);
    for(int y_pos = 0; y_pos < settings->d_image_dimensions.y; ++y_pos)
    {
        for(int x_pos = 0; x_pos < settings->d_image_dimensions.x; ++x_pos)
        {

            uchar4 out_pixel = {0, 0, 0, 0};
            unsigned char value = (unsigned char)d_clip((int)((image[y_pos * settings->d_image_dimensions.x + x_pos].y /max) * 255.0f), 0, 255);
            out_pixel.x = value;
            out_pixel.y = value;
            out_pixel.z = value;
            out_pixel.w = 255;
            #ifdef DEBUG_IMAGE
                write_pxl_rgb(settings->d_debug, x_pos, y_pos, settings->d_image_dimensions.x, out_pixel);
            #else
                write_pxl_rgb(settings->d_out_image, x_pos, y_pos, settings->d_image_dimensions.x, out_pixel);
            #endif
            }
    }
    
}*/

/**
 * write the values of a given gauss kernel to an image
 * @param settings   pointer to the settings struct
 * @param _kernels   image in which all the gauss kernels reside
 * @param num_kernel number of the gauss kernel to write to file (only one can be written at a time)
 */
/*__global__ void debug_print_gauss(cset_tone* settings, cufftComplex* _kernels, int num_kernel)
{
    
    printf("inside print gauss kernels\n");
    //print the first 10 gauss kernel values
    
    for(int i = settings->d_image_dimensions.x * settings->d_image_dimensions.y * num_kernel; i < settings->d_image_dimensions.x * settings->d_image_dimensions.y * (num_kernel + 1); ++i)
    {
        if(_kernels[i].x != 0)
            printf("[%i]: %f\n", i, _kernels[i].x);
    }
}
*/

/**
 * iterate over a given gauss kerne and count the number of pixels that are non zero
 * @param settings   pointer to the settings struct
 * @param _kernels   pointer to the image in which the gauss kernels are stored
 * @param num_kernel number of the kerne for whicht to perform the operation
 */
/*__global__ void debug_count_values_gauss(cset_tone* settings, cufftComplex* _kernels, int num_kernel)
{
    int num_non_zero = 0;
    for (int i = settings->d_image_dimensions.x * settings->d_image_dimensions.y * num_kernel; i < settings->d_image_dimensions.x * settings->d_image_dimensions.y * (num_kernel + 1); ++i)
    {
        if(_kernels[i].x >= 0.0004f || _kernels[i].x <= -0.0004f)
        {    
            ++num_non_zero;
            printf("[%i]: %f\n", i, _kernels[i].x);
        }
    }
    printf("number of non 0 elements: %i\n", num_non_zero);
}
*/
/**
 * simple test case that creates the gauss kernels, performs one forward and one inverse fft on them and then writes 
 * the result to a file
 * @param settings pointer to the settings struct
 * @param setaddr  pointer to a pointer of the settings struct
 */
/*void test_gaussfft(cset_tone* settings, cset_tone* setaddr)
{
    //create gauss images
    cufftResult cufft_error;
    cudaError_t cuda_error;

    dim3 threadsPerBlock(16, 16);
    dim3 numBlocks(divup(settings->d_image_dimensions.x, threadsPerBlock.x), divup(settings->d_image_dimensions.y, threadsPerBlock.y));

    //create plan for the gauss
    int dimensions[2] = {settings->d_image_dimensions.x, settings->d_image_dimensions.y};
    cufft_error = cufftPlanMany(&plan_gauss_filter, 2, dimensions, NULL, 1, 0, NULL, 1, 0, CUFFT_C2C, NUM_GAUSSKERNELS);
    //cufft_error = cufftPlan2d(&plan_gauss_filter, dimensions[0], dimensions[1], CUFFT_C2C);
    

    if(cufft_error != CUFFT_SUCCESS)
        std::cout << "tonemapper_reinhard: make plan for gauss kernel failed: " << cufft_error << std::endl;


    //allocate gauss kernels
    // cuda_error = cudaMalloc(&(settings->d_misc), sizeof(cufftComplex) * dimensions[0] * dimensions[1] * NUM_GAUSSKERNELS);
    cuda_error = cudaMalloc(&gauss_kernels, sizeof(cufftComplex) * dimensions[0] * dimensions[1] * NUM_GAUSSKERNELS);
    if(cuda_error != cudaSuccess)
        std::cout << "tonemapper_reinhard: could not allocate memory for gauss kernels" << cudaGetErrorString(cuda_error) << std::endl;


    //create the gauss kernels
    create_gauss<<<numBlocks, threadsPerBlock>>>(setaddr, gauss_kernels);
    cuda_error = cudaGetLastError();
    if(cuda_error != cudaSuccess)
        std::cout << "tonemapper_reinhard: create gausse kernel launch failed" << cudaGetErrorString(cuda_error) << std::endl;


    //print result
    //count_values_gauss<<<1,1>>>(setaddr,gauss_kernels, num_kernel);
    
    //transform them to be used later
    //cufft_error = cufftExecC2C(plan_gauss_filter, kernels + (settings->d_image_dimensions.x * settings->d_image_dimensions.y * num_kernel), kernels  + (settings->d_image_dimensions.x * settings->d_image_dimensions.y * num_kernel), CUFFT_FORWARD);
    cufft_error = cufftExecC2C(plan_gauss_filter, gauss_kernels, gauss_kernels, CUFFT_FORWARD);
    if(cufft_error != CUFFT_SUCCESS)
        std::cout << "tonemapper_reinhard: exec of plan failed" << cudaGetErrorString(cuda_error) << std::endl;

    //cufft_error = cufftExecC2C(plan_gauss_filter, gauss_kernels + (settings->d_image_dimensions.x * settings->d_image_dimensions.y * num_kernel), gauss_kernels + (settings->d_image_dimensions.x * settings->d_image_dimensions.y * num_kernel), CUFFT_INVERSE);
    cufft_error = cufftExecC2C(plan_gauss_filter, gauss_kernels, gauss_kernels, CUFFT_INVERSE);
    if(cufft_error != CUFFT_SUCCESS)
        std::cout << "tonemapper_reinhard: exec of plan failed" << cudaGetErrorString(cuda_error) << std::endl;

    debug_print_image_complex<<<1,1>>>(setaddr, gauss_kernels + dimensions[0] * dimensions[1] * 7);

    
    
}*/

/**
 * simple test case that creates a complex input image and then performs one forward and one inverse fft transformation. 
 * The result is then written to a debug-image. The result should be the same as the input image
 * @param settings pointer to the settings struct
 * @param setaddr  pointer to a pointer of the settings struct
 */
/*void test_radianceimage_fft(cset_tone* settings, cset_tone* setaddr)
{
    //create gauss images
    cufftResult cufft_error;
    cudaError_t cuda_error;

    dim3 threadsPerBlock(16, 16);
    dim3 numBlocks(divup(settings->d_image_dimensions.x, threadsPerBlock.x), divup(settings->d_image_dimensions.y, threadsPerBlock.y)); 
     
    create_radiance_image<<<numBlocks, threadsPerBlock>>>(setaddr, complex_input_image, 0.16);
    cuda_error = cudaGetLastError();
    if(cuda_error != cudaSuccess)
        std::cout << "tonemapper_reinhard: create radiance image: kernel launch failed: " << cudaGetErrorString(cuda_error) << std::endl;    

    //create fft of input image
    cufft_error = cufftExecC2C(plan_image, complex_input_image, complex_input_image, CUFFT_FORWARD);
    if(cufft_error != CUFFT_SUCCESS)
        std::cout << "tonemapper_reinhard: fft of input image failed: " << cufft_error << std::endl;

    cufft_error = cufftExecC2C(plan_image, complex_input_image, complex_input_image, CUFFT_INVERSE);
    if(cufft_error != CUFFT_SUCCESS)
        std::cout << "tonemapper_reinhard: fft of input image failed: " << cufft_error << std::endl;

    debug_print_image_complex<<<1,1>>>(setaddr, complex_input_image);

 }
*/
/**
 * writes the chosen v1 value for each pixel to an image
 * @param settings pointer to settings struct
 * @param x_pos    x position of the pixel in the image
 * @param y_pos    y position of the pixel in the image
 * @param v1       value of v1 to write
 */
/*__device__ void debug_write_v1_image(cset_tone* settings, int x_pos, int y_pos, float v1)
{
    uchar4 out_value = {0, 0, 0, 255};
    out_value.x = (unsigned char)d_clip((int)(v1 * 255.0f), 0, 255);
    out_value.y = (unsigned char)d_clip((int)(v1 * 255.0f), 0, 255);
    out_value.z = (unsigned char)d_clip((int)(v1 * 255.0f), 0, 255);
    #ifdef DEBUG_IMAGE
        write_pxl_rgb(settings->d_debug, x_pos, y_pos, settings->d_image_dimensions.x, out_value);
    #else
        write_pxl_rgb(settings->d_out_image, x_pos, y_pos, settings->d_image_dimensions.x, out_value);
    #endif
}
*/
/**
 * writes the preferred scale for the gauss kernel of each pixel in the image
 * for colors see comments below
 * @param settings   pointer to the settings struct
 * @param pref_scale preferred gauss kernel size to be written
 * @param x_pos      x position of the pixel in the image
 * @param y_pos      y position of the pixel in the image
 */
 /*__device__ void debug_write_preferred_scale_image(cset_tone* settings, int pref_scale, int x_pos, int y_pos)
 {
    uchar4 out_value = {0, 0, 0, 255};
    switch(pref_scale)
    {
        case 0: out_value.x = 255; break;                                       // 0 = red
        case 1: out_value.y = 255; break;                                       // 1 = green
        case 2: out_value.z = 255; break;                                       // 2 = blue
        case 3: out_value.x = 255; out_value.y = 255; break;                    // 3 = yellow
        case 4: out_value.x = 255; out_value.z = 255; break;                    // 4 = purple
        case 5: out_value.y = 255; out_value.z = 255; break;                    // 5 = turquise
        case 6: out_value.x = 255; out_value.y = 255; out_value.z = 255; break; // 6 = white
        case 7: out_value.x = 127; out_value.y = 127; out_value.z = 127; break; // 7 = grey
    }
    #ifdef DEBUG_IMAGE
        write_pxl_rgb(settings->d_debug, x_pos, y_pos, settings->d_image_dimensions.x, out_value);
    #else
        write_pxl_rgb(settings->d_out_image, x_pos, y_pos, settings->d_image_dimensions.x, out_value);
    #endif
 }*/
#endif
