// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "tonemapper_histward.h"
#include "../utils/utils.h"
#include "../utils/defines.h"
#include <cmath>

extern "C" void cu_tone_mapper_histogram_ward(cudaStream_t stream, cudaArray* output, double* d_tmp_image, struct header* metadata, int numSources, int width, int height, float scene_key_brightness, float3* d_foveal_image, int2 foveal_img_dim, unsigned int* d_histogram, float* d_cdf);


ToneMapperHistogramWard::ToneMapperHistogramWard()
{
	//so far do nothing
	initialized = false;
}


ToneMapperHistogramWard::~ToneMapperHistogramWard()
{
	if(initialized)
	{
		cuda_check(cudaFree(d_foveal_image), "ToneMapperHistogramWard::~ToneMapperHistogramWard: free of foveal image failed");
		cuda_check(cudaFree(d_histogram), "ToneMapperHistogramWard::~ToneMapperHistogramWard: free of histogram failed");
		cuda_check(cudaFree(d_cdf), "ToneMapperHistogramWard::~ToneMapperHistogramWard: free of cummulative distributive function image failed");
	}	

}

void ToneMapperHistogramWard::init(int numSources, int width, int height)
{
	//calculate size of foveal image
	
    foveal_img_dim.x =  2 * tan(deg_to_rad(HORIZONTAL_ANGLE) / 2.0f) / 0.01745f; 
    foveal_img_dim.y =  2 * tan(deg_to_rad(VERTICAL_ANGLE) / 2.0f) / 0.01745f;
    //allocate foveal image
	cuda_check(cudaMalloc(&d_foveal_image, sizeof(float3) * foveal_img_dim.x * foveal_img_dim.y), "ToneMapperHistogramWard::init: failed to allocate foveal image");
	cuda_check(cudaMemset(d_foveal_image, 0, sizeof(float3) * foveal_img_dim.x * foveal_img_dim.y), "ToneMapperHistogramWard::init: failed to set foveal image to zero");
  
	//allocate histogram
	cuda_check(cudaMalloc(&d_histogram, sizeof(unsigned int) * NUM_HISTOGRAM_BINS), "ToneMapperHistogramWard::init: failed to allocate histogram");
    cuda_check(cudaMemset(d_histogram, 0, sizeof(unsigned int) * NUM_HISTOGRAM_BINS), "ToneMapperHistogramWard::init: failed to copy histogram");
  
  	//allocate cummulative distribution function
  	cuda_check(cudaMalloc(&d_cdf, sizeof(float) * NUM_HISTOGRAM_BINS), "tonemapper_histward: tonemapper_histward: failed to allocate cummulative density function");
    cuda_check(cudaMemset(d_cdf, 0, sizeof(float) * NUM_HISTOGRAM_BINS), "tonemapper_histward: tonemapper_histward: failed to copy cummulative density function");

	cuda_check(cudaMalloc(&d_tmp_image, sizeof(double) * width * height * numSources), "tonemapper_ward: init: failed to allocate temp image");
	
	initialized = true;
}


float ToneMapperHistogramWard::deg_to_rad(float _degrees)
{
    return (_degrees * (M_PI / 180.0f));
}


/**
 * performs a single exectuion of the tone mapper e.g. upload everything, execute, download result
 */
void ToneMapperHistogramWard::run(cudaStream_t stream, cudaArray* output, struct header* metadata, int numSources, int width, int height, float scene_key_brightness)
{
	cu_tone_mapper_histogram_ward(stream, output, d_tmp_image, metadata, numSources, width, height, scene_key_brightness, d_foveal_image, foveal_img_dim, d_histogram, d_cdf);
}
