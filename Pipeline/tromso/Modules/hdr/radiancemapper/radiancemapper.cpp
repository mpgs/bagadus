// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "radiancemapper.h"
#include "../utils/defines.h"
#include "../utils/utils.h"
#include <cuda.h>
#include <cuda_runtime.h>
#include <math.h> /* sqrt */
#include <stdlib.h> /* srand, rand */
#include <time.h> /* time */
#include <fstream> /* files */
#include <iostream>
#include <sstream>/*stringstream*/

#ifdef PLATFORM_LINUX

// Ignore deprecation warnings
#if defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-register"
#elif defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-register"
#endif

#include "../Eigen/SVD"
#include "../Eigen/Dense"

#if defined(__GNUC__)
#pragma GCC diagnostic pop
#elif defined(__clang__)
#pragma clang diagnostic pop
#endif

#endif

#ifdef PLATFORM_WINDOWS
#include "Eigen\SVD"
#include "Eigen\Dense"
#endif





RadianceMapper::RadianceMapper()
{
	numImages = 0;


	wf = NULL;
}

RadianceMapper::~RadianceMapper()
{
	// if(h_out)
	// 	delete h_out;
	// if(numImages > 0 && h_Data)
	// {
	// 	for (int del_it; del_it < numImages; ++del_it)
	// 	{
	// 		delete h_Data[del_it];
	// 	}
	// }
}

/*
int RadianceMapper::addImage(Image* img)
{
	int error = ERROR_OK;
	if(img != 0){
		if(numImages == 0)
		{
			h_Data = (Image**)malloc(sizeof(Image*));
			if(!h_Data)
			{
				error = ERROR_MEMORY;
				printError("RadianceMapper::addImage: could not allocate memory");
				return error;
			}

		}
		else
		{
			h_Data = (Image**)realloc(h_Data, sizeof(Image*)*(numImages+1));
			if(!h_Data)
			{
				error = ERROR_MEMORY;
				printError("RadianceMapper::addImage: could not allocate memory");
				if(h_Data)
					free(h_Data);
				return error;
			}

		}
		h_Data[numImages] = img;
		++numImages;
	}

	return error;
}*/

int RadianceMapper::setWeightFunction(WeightFunction* _wf)
{
	int error = ERROR_OK;
	if(_wf)
		wf = _wf;
	else
	{
		printError("RadianceMapper::setWeightFunction: no Weighfunction given");
		error = ERROR_INVALID_ARGUMENTS;
	}
	return error;
}
