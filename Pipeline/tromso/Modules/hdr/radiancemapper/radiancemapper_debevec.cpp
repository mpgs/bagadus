// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "radiancemapper_debevec.h"
#include "../utils/defines.h"
#include "../utils/utils.h"
#include <cuda.h>
#include <cuda_runtime.h>
#include <math.h> /* sqrt */
#include <stdlib.h> /* srand, rand */
#include <time.h> /* time */
#include <fstream> /* files */
#include <iostream>
#include <sstream>/*stringstream*/
#include <cassert>

// Ignore deprecation warnings
#if defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-register"
#elif defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-register"
#endif

#include "../Eigen/SVD"
#include "../Eigen/Dense"

#if defined(__GNUC__)
#pragma GCC diagnostic pop
#elif defined(__clang__)
#pragma clang diagnostic pop
#endif



#ifdef DEBUG
/**
 * can set single values in the response function
 * @param pos position in the response function to be changed
 * @param val value that will be set at given position
 */
void RadianceMapperDebevec::setResponseFunction(unsigned int pos, float val)
{
	if (responseFunction == 0)
	{
		printError("setResponseFunction: responseFunction is null");
	}

	if(pos < responseFunction->size())
	{
		responseFunction->at(pos) = val;
	}
	else
	{
		printError("setResponseFunction: pos out of bounds");
	}
}

/**
 * prints the response function to console
 */
void RadianceMapperDebevec::printResponseFunction()
{
	if(responseFunction != 0)
	{
		std::stringstream ss;
		ss << "response function: ";
		for(size_t i = 0; i < responseFunction->size(); ++i)
		{
				if(i != 0)
			
				ss << responseFunction->at(i) << ", ";
		}

		std::string s = ss.str();
		printDebug(s.c_str());
	}

}


void RadianceMapperDebevec::printExposureTimes()
{
	if(exposureTimes != NULL)
	{
		std::stringstream ss;
		ss << "exposure times: ";
		for(unsigned int exp_it = 0; exp_it < exposureTimes->size(); ++exp_it)
		{
			ss << exposureTimes->at(exp_it) << ", ";
		}

		std::string s = ss.str();
		printDebug(s.c_str());
	}
	else
	{
		printError("RadianceMapperDebevec::printExposureTimes: no exposure times to print");
	}
}

#endif 

RadianceMapperDebevec::RadianceMapperDebevec()
{

	responseFunction = NULL;
	exposureTimes = NULL;
	initialized = false;

}


RadianceMapperDebevec::~RadianceMapperDebevec()
{
	if(initialized)
	{
		//first free CUDA stuff
		
		cuda_check(cudaFree(d_response_function), "radiancemapper_debevec:~radiancemapper_debevec: free out image failed");
		cuda_check(cudaFree(d_weight_function), "radiancemapper_debevec:~radiancemapper_debevec: free settings failed");
		
		//then delete CPU stuff

		//response function
		if(responseFunction != NULL) 
			delete responseFunction;

		if(exposureTimes != NULL)
			delete exposureTimes;
		//weight function
		if(wf)
			delete wf;
	}

}

/**
 * sets the weight function needed for multiple steps of this algorithm
 * @param  _wf valid pointer to a weight function
 * @return     ERROR_OK on success, else see defines.h
 */
int RadianceMapperDebevec::setWeightFunction(WeightFunction* _wf)
{
	int error = ERROR_OK;
	if(_wf)
		wf = _wf;
	else
	{
		error = ERROR_INVALID_ARGUMENTS;
		printError("RadianceMapperDebevec::setWeightFunction: no Weighfunction given");
	}
	return error;
}

/**
 * returns a vector with the current response function
 */
std::vector<float>* RadianceMapperDebevec::getResponseFunction()
{
	return responseFunction;
}





/**
 * tries to read the response function from a file. File has to be formatted as following:
 * one value per line, nothing else. Line-number of value corresponds to position in response function. Lower values first
 * @param  path path to the file containing the response function
 * @return      ERROR_OK on success, else see defines.h
 */
int RadianceMapperDebevec::readResponseFunctionFromFile(const char* path)
{
	int error = ERROR_OK;
	if(path == 0)
	{
		error = ERROR_INVALID_ARGUMENTS;
		printError("readResponseFunctionFromFile: path is null");
		return error;
	}
	std::ifstream fin;
  	fin.open(path); // open a file
  	if (!fin.good()) // exit if file not found 
  	{
  		error = ERROR_FILE;
  		printError("readResponseFunctionFromFile: couldn't open file");
  		return error;
  	}
   	 	 

	//char oneline[MAX_LINE_LENGTH];
	int num_lines_read = 0;
	if(responseFunction == NULL)
		responseFunction = new std::vector<float>();
	responseFunction->erase(responseFunction->begin(), responseFunction->end());
	float tmp_val;
   while (fin)
   {
       fin >> tmp_val;
		responseFunction->push_back(tmp_val);      
       if(error)
       		return error;
       ++num_lines_read;
    }

   fin.close();


	return error;

}



/**
 * calculate exposure time in seconds
 * @param  _exp_time exposure time given as exposure value (see: http://en.wikipedia.org/wiki/Exposure_value)
 * @param  _aperture aperture of the used camera
 * @return           exposure time in seconds
 */
float RadianceMapperDebevec::calculateExposureTime(float _exp_time, float _aperture)
{
	return (float) (powf(_aperture,2.0f)/powf(2.0f, _exp_time));
}


void RadianceMapperDebevec::init(WeightFunction* wf)
{
	int error = ERROR_OK;
	//read response function from file
	error = readResponseFunctionFromFile(FILE_PATH_RESPONSE_FUNCTION);
	//if not possible calibrate
	if(error != ERROR_OK)
	{
		std::cout << "couldn't read response function from file -> run CudaHDRCalibrate first" << std::endl;
		assert(false);
	}

	//set weight function
	if(wf != NULL)
	{
		setWeightFunction(wf);
	}

	//upload stuff to gpu
	
	//upload response function
	//-------------------------
	cuda_check(cudaMalloc(&d_response_function, sizeof(float)* responseFunction->size()), "radiancemapper_debevec: init: failed to allocate response function");
	cuda_check(cudaMemcpy(d_response_function, responseFunction->data(), responseFunction->size() * sizeof(float), cudaMemcpyHostToDevice), "radiancemapper_debevec: init: failed to copy response function");

	//upload weight function
	//------------------------
	cuda_check(cudaMalloc(&d_weight_function,  wf->getWeightFunction()->size() * sizeof(double) ), "radiancemapper_debevec: init: failed to allocate weight function");
	cuda_check(cudaMemcpy(d_weight_function, wf->getWeightFunction()->data(), wf->getWeightFunction()->size() * sizeof(double) , cudaMemcpyHostToDevice), "radiancemapper_debevec: init: failed to copy weight function");

	initialized = true;
}

/**
 * performs a single exectuion of the radiance mapper e.g. upload everything, execute, download result
 */

void RadianceMapperDebevec::debug_run()
{
	surface_test();		
}

void RadianceMapperDebevec::run(cudaStream_t stream, cudaArray* input, cudaArray* output, struct header* metadata, int num_sources, int num_images, int width, int height)
{	
	cu_radiance_mapper_debevec(stream, input, output, metadata, d_response_function, d_weight_function, num_sources, num_images, width, height);
	
}
