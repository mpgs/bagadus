// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 


#include <iostream>
#include <cstdio>
#include "../utils/utils.h"
#include "../utils/defines.h"
#include "Modules/Module.hpp"

__global__ void radiance_mapper_debevec_rad(cudaTextureObject_t input_tex, cudaSurfaceObject_t output_tex, float* response_function, double* weight_function, int num_images, int width, int height, float
        expo_first, float expo_second)
{



    int x_pos = blockIdx.x * blockDim.x + threadIdx.x, y_pos = blockIdx.y * blockDim.y + threadIdx.y;
    if(x_pos < width  && y_pos < height)
    {


        float3 v_sumWeightedZ = {0.0f, 0.0f, 0.0f};
        float3 v_sumWeights   = {0.0f, 0.0f, 0.0f};
        float3 v_rgb;

        float3 in_pixel = {0.0f, 0.0f, 0.0f};


        in_pixel = read_pxl_rgb(input_tex, x_pos, y_pos, width, height, 0);


        float z = 0.0f, w = 0.0f;

        float log_exposure = logf((float)expo_first);
        float luminance = 0.27f * in_pixel.x + 0.67f * in_pixel.y + 0.06f * in_pixel.z;
        z = in_pixel.x;
        w = calcWeight(weight_function, luminance);

        //if(x_pos == 1 && y_pos == 1)
        //	printf("expo_first: %f, expo_second: %f, log_exposure: %f, z: %f, w: %f\n", expo_first, expo_second, log_exposure, z, w);

        v_sumWeightedZ.x += (w + 1.0f) * ((calcResponse(response_function, z)) - log_exposure);
        v_sumWeights.x += (w + 1.0f);

        //g
        z = in_pixel.y;
        v_sumWeightedZ.y += (w + 1.0f) * ((calcResponse(response_function, z)) - log_exposure);
        v_sumWeights.y += (w + 1.0f);

        //b
        z = in_pixel.z;
        v_sumWeightedZ.z += (w + 1.0f) * ((calcResponse(response_function, z)) - log_exposure);
        v_sumWeights.z += (w + 1.0f);



        in_pixel = read_pxl_rgb(input_tex, x_pos, y_pos, width, height, 1);
        luminance = 0.27f * in_pixel.x + 0.67f * in_pixel.y + 0.06f * in_pixel.z;
        w = calcWeight(weight_function, luminance);
        log_exposure = logf((float)expo_second);
        //inner loop that accesses each channel unrolled

        //r
        z = in_pixel.x;
        v_sumWeightedZ.x += (w + 1.0f) * ((calcResponse(response_function, z)) - log_exposure);
        v_sumWeights.x += (w + 1.0f);

        //g
        z = in_pixel.y;
        v_sumWeightedZ.y += (w + 1.0f) * ((calcResponse(response_function, z)) - log_exposure);
        v_sumWeights.y += (w + 1.0f);

        //b
        z = in_pixel.z;
        v_sumWeightedZ.z += (w + 1.0f) * ((calcResponse(response_function, z)) - log_exposure);
        v_sumWeights.z += (w + 1.0f);






        //weighted pixel values are divided by used weights
        v_rgb.x = (float)expf(v_sumWeightedZ.x / v_sumWeights.x);
        v_rgb.y = (float)expf(v_sumWeightedZ.y / v_sumWeights.y);
        v_rgb.z = (float)expf(v_sumWeightedZ.z / v_sumWeights.z);


        //store result in hdr format
        uchar4 out_pixel = {0, 0, 0, 0};
        flt3_to_rp(&v_rgb, &out_pixel);

        write_pxl(output_tex, x_pos, y_pos, width, out_pixel);
    }
}

__global__ void radiance_mapper_debevec(cudaTextureObject_t input_tex, cudaSurfaceObject_t output_tex, float* response_function, double* weight_function, int num_images, int width, int height, float
        expo_first, float expo_second)
{



    int x_pos = blockIdx.x * blockDim.x + threadIdx.x, y_pos = blockIdx.y * blockDim.y + threadIdx.y;
    if(x_pos < width  && y_pos < height)
    {
        //	uchar4 in_pixel = {0, 0, 0, 0};

        //	in_pixel = read_pxl_rgb(input_tex, x_pos, y_pos, width, height, 1);

        //	if(x_pos == 1 && y_pos == 1)
        //		printf("in_pixel.x: %i, y: %i, z: %i\n", in_pixel.x, in_pixel.y, in_pixel.z);

        //	write_pxl_rgb(output_tex, x_pos, y_pos, width, in_pixel);


        float3 v_sumWeightedZ = {0.0f, 0.0f, 0.0f};
        float3 v_sumWeights   = {0.0f, 0.0f, 0.0f};
        float3 v_rgb;

        float3 in_pixel = {0.0f, 0.0f, 0.0f};

        //outer loop is now unrolled too


        in_pixel = read_pxl_rgb(input_tex, x_pos, y_pos, width, height, 0);
#ifdef USE_GAMMA_CORRECTION
        //in_pixel.x = powf(in_pixel.x, 1.0f/GAMMA);
        //in_pixel.y = powf(in_pixel.y, 1.0f/GAMMA);
        //in_pixel.z = powf(in_pixel.z, 1.0f/GAMMA);
#endif

        //	if(x_pos == 1 && y_pos == 1)
        //		printf("radiance mapper: in_pixel.x: %f, y: %f, z: %f\n", in_pixel.x, in_pixel.y, in_pixel.z);
        //inner loop that accesses each channel unrolled

        float z = 0.0f, w = 0.0f;

        float log_exposure = logf((float)expo_first);
        //r
        z = in_pixel.x;
        w = calcWeight(weight_function, z);

        //if(x_pos == 1 && y_pos == 1)
        //	printf("expo_first: %f, expo_second: %f, log_exposure: %f, z: %f, w: %f\n", expo_first, expo_second, log_exposure, z, w);

        v_sumWeightedZ.x += (w + 1.0f) * ((calcResponse(response_function, z)) - log_exposure);
        v_sumWeights.x += (w + 1.0f);

        //g
        z = in_pixel.y;
        w = calcWeight(weight_function, z);
        v_sumWeightedZ.y += (w + 1.0f) * ((calcResponse(response_function, z)) - log_exposure);
        v_sumWeights.y += (w + 1.0f);

        //b
        z = in_pixel.z;
        w = calcWeight(weight_function, z);
        v_sumWeightedZ.z += (w + 1.0f) * ((calcResponse(response_function, z)) - log_exposure);
        v_sumWeights.z += (w + 1.0f);



        in_pixel = read_pxl_rgb(input_tex, x_pos, y_pos, width, height, 1);
#ifdef USE_GAMMA_CORRECTION
        //in_pixel.x = powf(in_pixel.x, 1.0f/GAMMA);
        //in_pixel.y = powf(in_pixel.y, 1.0f/GAMMA);
        //in_pixel.z = powf(in_pixel.z, 1.0f/GAMMA);
#endif

        log_exposure = logf((float)expo_second);
        //inner loop that accesses each channel unrolled

        //r
        z = in_pixel.x;
        w = calcWeight(weight_function, z);

        v_sumWeightedZ.x += (w + 1.0f) * ((calcResponse(response_function, z)) - log_exposure);
        v_sumWeights.x += (w + 1.0f);

        //g
        z = in_pixel.y;
        w = calcWeight(weight_function, z);
        v_sumWeightedZ.y += (w + 1.0f) * ((calcResponse(response_function, z)) - log_exposure);
        v_sumWeights.y += (w + 1.0f);

        //b
        z = in_pixel.z;
        w = calcWeight(weight_function, z);
        v_sumWeightedZ.z += (w + 1.0f) * ((calcResponse(response_function, z)) - log_exposure);
        v_sumWeights.z += (w + 1.0f);






        //weighted pixel values are divided by used weights
        v_rgb.x = (float)expf(v_sumWeightedZ.x / v_sumWeights.x);
        v_rgb.y = (float)expf(v_sumWeightedZ.y / v_sumWeights.y);
        v_rgb.z = (float)expf(v_sumWeightedZ.z / v_sumWeights.z);


        //store result in hdr format
        uchar4 out_pixel = {0, 0, 0, 0};
        flt3_to_rp(&v_rgb, &out_pixel);

        //	if(x_pos == 1 && y_pos == 1)
        //	{
        //		printf("radiance mapper: v_rgb.x: %f, y:%f, z:%f\n", v_rgb.x, v_rgb.y, v_rgb.z);
        //printf("out_pixel.x: %i, out_pixel.y: %i, out_pixel.z: %i\n", out_pixel.x, out_pixel.y, out_pixel.z);
        //	}
        write_pxl(output_tex, x_pos, y_pos, width, out_pixel);

    }

}

__global__ void d_surface_test(cudaSurfaceObject_t test_surf, int size)
{
    uchar4 read_pixel = {0, 0, 0, 0};
    surf2Dread(&read_pixel, test_surf, 0, 0);
    printf("read pixel after read: x: %i, y: %i, z: %i, w: %i\n", read_pixel.x, read_pixel.y, read_pixel.z, read_pixel.w);
    uchar4 write_pixel = {100, 100, 100, 255};
    surf2Dwrite(write_pixel, test_surf, 0, 0);
    surf2Dread(&read_pixel, test_surf, 0, 0);
    printf("read pixel after second read: x: %i, y: %i, z: %i, w: %i\n", read_pixel.x, read_pixel.y, read_pixel.z, read_pixel.w);

}

extern "C" void surface_test()
{
    std::cout << "came here" << std::endl;
    int size = 100;
    cudaArray* test_arr;
    uchar4 value = {25, 50, 75, 255};

    uchar4* input = new uchar4[size * size];
    for (int i = 0; i < size * size; ++i)
    {
        input[i] = value;
    }




    //fill array
    cudaChannelFormatDesc byteChannel = cudaCreateChannelDesc(8, 8, 8, 8,  cudaChannelFormatKindUnsigned);
    cudaMallocArray(&test_arr,&byteChannel, size, size, cudaArraySurfaceLoadStore);

    cudaMemcpyToArray(test_arr, 0, 0, input, size * size * sizeof(uchar4), cudaMemcpyHostToDevice);


    //create surface object
    cudaSurfaceObject_t test_surf = 0;

    struct cudaResourceDesc out_res_desc;
    memset(&out_res_desc, 0, sizeof(out_res_desc));
    out_res_desc.resType = cudaResourceTypeArray;
    out_res_desc.res.array.array = test_arr;

    cudaCreateSurfaceObject(&test_surf, &out_res_desc);

    //run test kernel
    d_surface_test<<<1,1>>>(test_surf, size);
    cudaError_t error = cudaDeviceSynchronize();
    std::cout << "after kernel call" << std::endl;
    error = cudaGetLastError();
    std::cout << "error return: " << cudaGetErrorString(error) << std::endl;






}


extern "C" void cu_radiance_mapper_debevec(cudaStream_t stream,
        cudaArray* input, cudaArray* output,
        struct header * metadata, float* response_function,
        double* weight_function, int num_sources, int num_images, int width,
        int height)
{
    cudaError_t err = cudaGetLastError();

    height = height * num_sources; //treat all sources as one big image
    //	#ifdef DEBUG
    //		std::cout << "starting radiance mapper" << std::endl;
    //	#endif
    dim3 threadsPerBlock(16,16);
    dim3 numBlocks(divup(width, threadsPerBlock.x), divup(height, threadsPerBlock.y));


    //	printWeight<<<1,1>>>(setaddr);

    //	cudaEvent_t start, stop;
    //	cudaEventCreate(&start);
    //	cudaEventCreate(&stop);

    //	cudaEventRecord(start);


    //TODO: actually set response function
    // 	metadata->expoFirst = 7;
    // 	metadata->expoSecond = 31;

    //create texture object of input
    cudaTextureObject_t input_tex = 0;

    struct cudaResourceDesc in_res_desc;
    memset(&in_res_desc, 0, sizeof(in_res_desc));
    in_res_desc.resType = cudaResourceTypeArray;
    in_res_desc.res.array.array = input;

    struct cudaTextureDesc tex_desc;
    memset(&tex_desc, 0, sizeof(tex_desc));
    tex_desc.normalizedCoords = 0;
    tex_desc.readMode = cudaReadModeElementType;
    tex_desc.addressMode[0] = cudaAddressModeClamp;
    tex_desc.addressMode[1] = cudaAddressModeClamp;
    tex_desc.filterMode = cudaFilterModePoint;
    cuda_check(cudaCreateTextureObject(&input_tex, &in_res_desc, &tex_desc, NULL), "cu_radiance_mapper_debevec: failed to create texture object");

    //create surface for output
    cudaSurfaceObject_t output_tex = 0;

    struct cudaResourceDesc out_res_desc;
    memset(&out_res_desc, 0, sizeof(out_res_desc));
    out_res_desc.resType = cudaResourceTypeArray;
    out_res_desc.res.array.array = output;

    cuda_check(cudaCreateSurfaceObject(&output_tex, &out_res_desc), "cu_radiance_mapper_debevec: failed to create output surface object");

    // radiance_mapper_debevec<<<numBlocks, threadsPerBlock, 0, stream>>>(input_tex, output_tex, response_function, weight_function, num_images, width, height, metadata->expoFirst/1000000.0f,
    // 		metadata->expoSecond/1000000.0f);

    radiance_mapper_debevec_rad<<<numBlocks, threadsPerBlock, 0, stream>>>
        (input_tex, output_tex, response_function, weight_function, num_images,
         width, height, metadata->expoFirst/1000000.0f,
         metadata->expoSecond/1000000.0f);



    //	cudaEventRecord(stop);

    //	cudaEventSynchronize(stop);
    //	float milliseconds = 0;
    //	cudaEventElapsedTime(&milliseconds, start, stop);

    //	std::cout << "elapsed time radiance mapper debevec " << milliseconds << std::endl;

    err = cudaGetLastError();
    if(err != cudaSuccess)
        std::cout << "radiance mappper kenel launch fail: " << cudaGetErrorString(err) << std::endl;
}
