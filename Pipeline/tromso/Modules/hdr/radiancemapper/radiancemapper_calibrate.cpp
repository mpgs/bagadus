// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "radiancemapper_calibrate.h"
#include "../utils/defines.h"
#include "../utils/utils.h"

#include <math.h> /* sqrt */
#include <stdlib.h> /* srand, rand */
#include <time.h> /* time */
#include <fstream> /* files */
#include <iostream>
#include <sstream>/*stringstream*/

#ifdef PLATFORM_LINUX
#include "../Eigen/SVD"
#include "../Eigen/Dense"
#endif

#ifdef PLATFORM_WINDOWS
#include "Eigen\SVD"
#include "Eigen\Dense"
#endif


#ifdef DEBUG
/**
 * can set single values in the response function
 * @param pos position in the response function to be changed
 * @param val value that will be set at given position
 */
void RadianceMapperCalibrate::setResponseFunction(int pos, float val)
{
	if (responseFunction == 0)
	{
		printError("setResponseFunction: responseFunction is null");
	}

	if(pos < (int)responseFunction->size())
	{
		responseFunction->at(pos) = val;
	}
	else
	{
		printError("setResponseFunction: pos out of bounds");
	}
}

/**
 * prints the response function to console
 */
void RadianceMapperCalibrate::printResponseFunction()
{
	if(responseFunction != 0)
	{
		std::stringstream ss;
		ss << "response function: ";
		for(size_t i = 0; i < responseFunction->size(); ++i)
		{
				if(i != 0)
			
				ss << responseFunction->at(i) << ", ";
		}

		std::string s = ss.str();
		printDebug(s.c_str());
	}

}
/**
 * prints the sample points used for reconstructing the response function
 */
void RadianceMapperCalibrate::printSamplePoints()
{
	if(samplePoints == 0)
	{
		printError("printSamplePoints: no sample points set");
		return;
	}
	std::stringstream ss;
	int pixel;
	unsigned int r, g, b, a;
	//iterate over images
	for(int image_it = 0; image_it < numImages; ++image_it)
	{
		//iterate over sample points per image
		for(int point_it = 0; point_it < numSamplePoints; ++point_it)
		{
			pixel = samplePoints[image_it*numSamplePoints + point_it];
			r = (pixel & 0x000000ff);
			g = (pixel & 0x0000ff00) >> 8;
			b = (pixel & 0x00ff0000) >> 16;
			a = (pixel & 0xff000000) >> 24;
			//ss << samplePoints[image_it*numSamplePoints + point_it] << ", " << std::endl;
			ss << "Image" << image_it <<" Point no "<< point_it <<  "  r: " << r << ", g: " << g << ", b: " << b << ", a: " << a << std::endl;
		}
		ss << "--------------------------------------------------------------------" << std::endl;
	}

	std::string s = ss.str();
	printDebug(s.c_str());
}

/**
 * prints the exposure times associated with the input images
 */
void RadianceMapperCalibrate::printExposures()
{
	if(numImages > 0)
	{
		std::stringstream ss;

		for(int image_it = 0; image_it < numImages; ++image_it)
		{
			ss << "Image" << image_it << " exposure: " << log( h_Data[image_it]->expTime)<< std::endl ;
		}

		std::string s = ss.str();
		printDebug(s.c_str());
	}
}
#endif 

RadianceMapperCalibrate::RadianceMapperCalibrate()
{

	responseFunction = NULL;
	samplePoints = NULL;

}


RadianceMapperCalibrate::~RadianceMapperCalibrate()
{
	//first free CUDA stuff
	
	
	//then delete CPU stuff

	//h_Data
	if(h_Data != NULL)
	{
		for(int del_it = 0; del_it < numImages; ++del_it)
		{
			delete h_Data[del_it];
		}
		free(h_Data);
	}

	//sample points
	if(samplePoints)
	{
		delete samplePoints;
	}
	//response function
	if(responseFunction != NULL) 
		delete responseFunction;

	if(wf)
		delete wf;



}

/**
 * sets the weight function needed for multiple steps of this algorithm
 * @param  _wf valid pointer to a weight function
 * @return     ERROR_OK on success, else see defines.h
 */
int RadianceMapperCalibrate::setWeightFunction(WeightFunction* _wf)
{
	int error = ERROR_OK;
	if(_wf)
		wf = _wf;
	else
	{
		error = ERROR_INVALID_ARGUMENTS;
		printError("RadianceMapperCalibrate::setWeightFunction: no Weighfunction given");
	}
	return error;
}

/**
 * returns a vector with the current response function
 */
std::vector<float>* RadianceMapperCalibrate::getResponseFunction()
{
	return responseFunction;
}

/**
 * adds another image to the source images. 
 * @param  img pointer to a valid image with all values set. Does not clean up the image pointer, but does not need it afterwards
 * @return     ERROR_OK on success, ERROR_MEMORY if allocating space for that image failed
 */
int RadianceMapperCalibrate::addImage(Image* img)
{
	int error = ERROR_OK;
	if(img != 0){
		if(numImages == 0)
		{
			h_Data = (Image**)malloc(sizeof(Image*));
			if(!h_Data)
			{
				error = ERROR_MEMORY;
				printError("RadianceMapperCalibrate::addImage: could not allocate memory");
				return error;
			}

		}
		else
		{
			h_Data = (Image**)realloc(h_Data, sizeof(Image*)*(numImages+1));
			if(!h_Data)
			{
				error = ERROR_MEMORY;
				printError("RadianceMapperCalibrate::addImage: could not allocate memory");
				if(h_Data)
					free(h_Data);
				return error;
			}

		}
		h_Data[numImages] = img;
		++numImages;
	}

	return error;
}

// void RadianceMapperCalibrate::uploadImageToGPU()
// {
// 	if(h_Data)
// 	{
// 		//create alloc
// 		cudaMalloc(&d_Data,h_Data[0]->width * h_Data[0]->height * 4 * sizeof(unsigned char) * numImages);

// 		//copy images
// 		for (int image_it = 0; image_it < numImages; ++image_it)
// 		{
// 			cudaMemcpy(d_Data, h_Data[image_it]->image.data(), sizeof(unsigned char) * h_Data[image_it]->height * 4 * h_Data[image_it]->width , cudaMemcpyHostToDevice);
// 		}
// 	}
// }


/**
 * splits up the image into num_points regions and then picks a random pixel within each region. 
 * These are then added to the samplePoint array
 * @param  num_points how many random sample points to retrieve (>100 slows down the algorithm considerately)
 * @return            ERROR_OK on success, else see defines.h
 */
int RadianceMapperCalibrate::retrieveSamplePoints(int num_points)
{
	int error  = ERROR_OK;
	//allocate memory for sample points
	samplePoints = new int[num_points * numImages];

	//split up image into regions to enable even distribution of sample points
	int regions_per_axis = sqrt((float)num_points);
	int x_step = h_Data[0]->width/regions_per_axis;
	int y_step = h_Data[0]->height/regions_per_axis;
	int x_pos = 0, y_pos = 0;
	int pixel = 0;


	srand(time(NULL));
	for(int row_it = 0; row_it < regions_per_axis; ++row_it)
	{
		for(int col_it = 0; col_it < regions_per_axis; ++col_it)
		{
			//get random pixel pos within region
			x_pos = rand() % x_step;
			y_pos = rand() % y_step;
			//read each image at that pos
			for(int image_it = 0; image_it < numImages; ++image_it)
			{
				pixel = h_Data[image_it]->getPixel(x_step * col_it + x_pos, y_step * row_it + y_pos);
				//std::cout<<"retrieveSamplePoints: x: " << x_step * col_it + x_pos << ", y: " << y_step * row_it + y_pos << std::endl;				
				//convert to yuv color space (only luminosity is interesting for us)
				error = h_Data[image_it]->convertPixelRGBToYUV((unsigned char*)&pixel);
				
				//store in array
				samplePoints[image_it * num_points + col_it * regions_per_axis + row_it] = pixel;
			}
		}

	}
	
	return error;
}


/**
 * tries to read the response function from a file. File has to be formatted as following:
 * one value per line, nothing else. Line-number of value corresponds to position in response function. Lower values first
 * @param  path path to the file containing the response function
 * @return      ERROR_OK on success, else see defines.h
 */
int RadianceMapperCalibrate::readResponseFunctionFromFile(const char* path)
{
	int error = ERROR_OK;
	if(path == 0)
	{
		error = ERROR_INVALID_ARGUMENTS;
		printError("readResponseFunctionFromFile: path is null");
		return error;
	}
	std::ifstream fin;
  	fin.open(path); // open a file
  	if (!fin.good()) // exit if file not found 
  	{
  		error = ERROR_FILE;
  		printError("readResponseFunctionFromFile: couldn't open file");
  		return error;
  	}
   	 	 

	//char oneline[MAX_LINE_LENGTH];
	int num_lines_read = 0;
	if(responseFunction == NULL)
		responseFunction = new std::vector<float>();
	responseFunction->erase(responseFunction->begin(), responseFunction->end());
	float tmp_val;
   while (fin)
   {
       fin >> tmp_val;
		responseFunction->push_back(tmp_val);      
       if(error)
       		return error;
       ++num_lines_read;
    }

   fin.close();


	return error;

}

/**
 * writes the current response function to a file. Each value is written in a new line starting from the lower end
 * @param  path path including filename where to write the values
 * @return      ERROR_OK on Success, ERROR_FILE if there were problems with creating or writing the file
 */
int RadianceMapperCalibrate::writeResponseFunctionToFile(const char* path)
{
	int error = ERROR_OK;
	if(path == 0)
	{
		error = ERROR_INVALID_ARGUMENTS;
		printError("writeResponseFunctionToFile: path is null");
		return error;
	}

	if(responseFunction == 0)
	{
		error = ERROR_UNINITIALIZED_MEMBER;
		printError("writeResponseFunctionToFile: no response function to print");
		return error;
	}


	std::ofstream outputFile;
	outputFile.open(path);
	if(outputFile.is_open())
	{
		for(unsigned int resp_it = 0; resp_it < responseFunction->size(); ++resp_it)
		{
			outputFile << responseFunction->at(resp_it) << std::endl;
		}
	}
	else
	{
		error = ERROR_FILE;
		printError("writeResponseFunctionToFile: couldn't create file");

	}
	return error; 
}

/**
 * uses Gauss-Seidl iteration to reconstruct the camera's response function
 * requires input images to be set
 * @param  _nr_point_samples how many random pixels should be sampled for the reconstruction (more increased accuracy, but O(n²))
 * @return                   ERROR_OK on success or ERROR_UNITIALIZED_MEMBER if no input images have been added yet
 */
int RadianceMapperCalibrate::calculateResponseFunction(int _nr_point_samples)
{
	int error = ERROR_OK;
	int n = 256; //number of different intensities = length(char)
	float l = 255.0f; //some weighting factor

	int luminosity = 0;
	if(h_Data == 0){
		printError("RadianceMapperCalibrate::calculateResponseFunction: no images found");
		return error = ERROR_UNINITIALIZED_MEMBER;
	}
	numSamplePoints = _nr_point_samples;
	retrieveSamplePoints(_nr_point_samples);

	//create nescessary matrixes
	//WeightFunction* wf = new WeightFunction(0.0, 255.0);

	//std::cout << "numImages: " << numImages << ", numSamplePoints: " << numSamplePoints << std::endl;
	Eigen::MatrixXf A;
	A = Eigen::MatrixXf::Zero(numSamplePoints * numImages + n + 1, numSamplePoints + n);

	std::cout << "sizeof A: rows" << A.rows() << ", cols: " << A.cols() << std::endl;
	Eigen::VectorXf b;
	b = Eigen::VectorXf::Zero(A.rows());
	//Eigen::MatrixXf b(A.rows(), 1);

	//fill A matrix with sample points
	int k = 0;

	for(int point_it = 0; point_it < numSamplePoints; ++point_it)
	{
		for(int image_it = 0; image_it < numImages; ++image_it)
		{
			luminosity = (samplePoints[image_it*numSamplePoints + point_it] & 0x000000ff);
		//	std::cout << "image" << image_it << " samplePoint" << point_it << " luminosity: " << luminosity << std::endl;
			
			//std::cout << "luminosity: " << luminosity << std::endl;
			double weighted_pixel = wf->calc((double)luminosity, h_Data[image_it]->expTime); 
			//luminosity = log(luminosity);
			//std::cout << "Matrix size: cols: " << A.cols() << ", rows: " << A.rows() << ", index 1: " << k << ", index 2: " << n+point_it << std::endl;
			A(k, luminosity) = (float)weighted_pixel;
			A(k, n+point_it) = -(float)weighted_pixel;
			b(k) = (float)weighted_pixel * logf(h_Data[image_it]->expTime) ;
			++k;
		}
	}

	//set middle to zero as a pivot point
	A(k, 127) = 1.0f;
	++k;

	int default_exposure = h_Data[numImages/2]->expTime;

	//make sure function is smooth
	for(int i = 0; i < n -2; ++i)
	{
		A(k, i) = l * (float)wf->calc((double)i+1.0,  default_exposure);
		A(k, i+1) = -2.0f * l * (float)wf->calc((double)i+1.0,  default_exposure);
		A(k, i+2) = l * (float)wf->calc((double)i+1.0,  default_exposure);
		++k; 
	}

	Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");
	std::string sep = "\n----------------------------------------------------------\n";
	//std::cout << A.format(CleanFmt) << sep;

	//use SVD on the matrices
	Eigen::JacobiSVD<Eigen::MatrixXf> svd(A, Eigen::ComputeFullU | Eigen::ComputeFullV);

	Eigen::MatrixXf result = svd.solve(b);
	

	if(!responseFunction)
		responseFunction = new std::vector<float>();
	
	responseFunction->erase(responseFunction->begin(), responseFunction->end());

	for(int i = 0; i < 256; ++i)
	{
		responseFunction->push_back(result(i));

	}
	
	
	//free allocated memory
	// if(wf)
	// 	delete wf;
	return error;

}

/**
 * calculate exposure time in seconds
 * @param  _exp_time exposure time given as exposure value (see: http://en.wikipedia.org/wiki/Exposure_value)
 * @param  _aperture aperture of the used camera
 * @return           exposure time in seconds
 */
//float RadianceMapperCalibrate::calculateExposureTime(float _exp_time, float _aperture)
//{
//	return (float) (powf(_aperture,2.0f)/powf(2.0f, _exp_time));
//}


