// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "radiancemapper_robertson.h"
#include "../utils/utils.h"

#include <fstream> /* files */
#include <sstream>
#include <string.h>
#include <cassert>


#ifdef DEBUG
void RadianceMapperRobertson::printResponseFunction()
{
	if(responseFunction != 0)
	{
		std::stringstream ss;
		ss << "response function (length " << responseFunction->size() <<") : ";
		for(size_t i = 0; i < responseFunction->size(); ++i)
		{
				if(i != 0)
			
				ss << "[" << i << "]: " << responseFunction->at(i) << ", " << std::endl;
		}

		std::string s = ss.str();
		printDebug(s.c_str());
	}

}
#endif

RadianceMapperRobertson::RadianceMapperRobertson()
{
	numImages = 0;
}


RadianceMapperRobertson::~RadianceMapperRobertson()
{
	std::cout << "called destructor: ~RadianceMapperRobertson" << std::endl;

	if(initialized)
	{
		delete wf;
		delete responseFunction;

		cuda_check(cudaFree(d_response_function), "RadianceMapperRobertson::~RadianceMapperRobertson: failed to free d_response_function");
		cuda_check(cudaFree(d_weight_function), "RadianceMapperRobertson::~RadianceMapperRobertson: failed to free d_weight_function");
			
	}
}


/**
 * compares two float values and returns which is bigger
 * @param  a first value to be compared
 * @param  b second value to be compared
 * @return   0 if b is bigger, 1 if a is bigger
 */
int comp_floats( const void *a, const void *b )
{
  return ( (*((float*)a))< (*((float*)b)) ) ;
}



/**
 * tries to read the response function from a file. File has to be formatted as following:
 * one value per line, nothing else. Line-number of value corresponds to position in response function. Lower values first
 * @param  path path to the file containing the response function
 * @return      ERROR_OK on success, else see defines.h
 */
int RadianceMapperRobertson::readResponseFunctionFromFile(const char* path)
{
	int error = ERROR_OK;
	if(path == 0)
	{
		error = ERROR_INVALID_ARGUMENTS;
		printError("readResponseFunctionFromFile: path is null");
		return error;
	}
	std::ifstream fin;
  	fin.open(path); // open a file
  	if (!fin.good()) // exit if file not found 
  	{
  		error = ERROR_FILE;
  		printError("readResponseFunctionFromFile: couldn't open file");
  		return error;
  	}
   	 	 

	//char oneline[MAX_LINE_LENGTH];
	int num_lines_read = 0;
	if(!responseFunction)
		responseFunction = new std::vector<float>();
	responseFunction->erase(responseFunction->begin(), responseFunction->end());
	float tmp_val;
   while (fin)
   {
       fin >> tmp_val;
		responseFunction->push_back(tmp_val);      
       if(error)
       		return error;
       ++num_lines_read;
    }

   fin.close();


	return error;

}

int RadianceMapperRobertson::setWeightFunction(WeightFunction* _wf)
{
	int error = ERROR_OK;
	if(_wf)
		wf = _wf;
	else
	{
		error = ERROR_INVALID_ARGUMENTS;
		printError("RadianceMapperRobertson::setWeightFunction: no Weighfunction given");
	}
	return error;
}


std::vector<float>* RadianceMapperRobertson::getResponseFunction()
{
	return responseFunction;
}

void RadianceMapperRobertson::init(WeightFunction* wf)
{
	int error = ERROR_OK;
	error = readResponseFunctionFromFile(FILE_PATH_RESPONSE_FUNCTION);

	if(error != ERROR_OK)
	{
		std::cout << "couldn't read response function from file -> run CudaHDRCalibrate first" << std::endl;
		assert(false);
	}

	if(wf != NULL)
	{
		setWeightFunction(wf);
	}

	//upload response function
	//-------------------------
	cuda_check(cudaMalloc(&d_response_function, sizeof(float)* responseFunction->size()), "radiancemapper_robertson: init: failed to allocate response function");
	cuda_check(cudaMemcpy(d_response_function, responseFunction->data(), responseFunction->size() * sizeof(float), cudaMemcpyHostToDevice), "radiancemapper_robertson: init: failed to copy response function");

	//upload weight function
	//-------------------------
	cuda_check(cudaMalloc(&d_weight_function,  wf->getWeightFunction()->size() * sizeof(double) ), "radiancemapper_robertson: init: failed to allocate weight function");
	cuda_check(cudaMemcpy(d_weight_function, wf->getWeightFunction()->data(), wf->getWeightFunction()->size() * sizeof(double) , cudaMemcpyHostToDevice), "radiancemapper_robertson: init: failed to copy weight function");


	initialized = true;
}

/**
 * performs a single exectuion of the radiance mapper e.g. upload everything, execute, download result
 */
void RadianceMapperRobertson::run(cudaStream_t stream, cudaArray* input, cudaArray* output, struct header * metadata, int num_sources, int num_images, int width, int height)
{

	cu_radiance_mapper_robertson(stream, input, output, metadata, d_response_function, d_weight_function, num_sources, num_images, width, height);
}

void RadianceMapperRobertson::debug_run()
{
	
}
