// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

// __device__ float calcResponse(cset_radiancemapper* settings, float z) {
//     const float r = settings->d_response_function[(unsigned int) z];
//     return r;
// }

// __device__ double calcWeight(cset_radiancemapper* settings, float z){
// 	const float r = settings->d_weight_function[(unsigned int)z];
// 	return r;
// }

/**
 * this version works on the single radiance channel
 * CURRENTY BROKEN!!!
 * @param settings pointer to the settings array
 */
/*__global__ void radiance_mapper_robertson_radiance(cset_radiancemapper* settings)
  {
  int x_pos = blockIdx.x * blockDim.x + threadIdx.x, y_pos = blockIdx.y * blockDim.y + threadIdx.y;
  if(x_pos < settings->d_image_dimensions.x  && y_pos < settings->d_image_dimensions.y)
  {
  float sum = 0.0f;
  float divider = 0.0f;
  float3 result = {0.0f, 0.0f, 0.0f};
  uchar4 in_pixel = {0, 0, 0, 0};
  for(int img_it = 0; img_it < settings->d_num_images; ++img_it)
  {
  in_pixel = read_pxl_yuv(settings->d_image_data, x_pos, y_pos, settings->d_image_dimensions.x, settings->d_image_dimensions.y, img_it);

  sum += calcWeight(settings, in_pixel.x) * settings->d_exposure_times[img_it] * calcResponse(settings, in_pixel.x);
  divider += calcWeight(settings, in_pixel.x) * powf(settings->d_exposure_times[img_it], 2.0f);
  }

  if(divider != 0.0f)
  {
  result.x = sum / divider;
  result.y = in_pixel.y / 255; //TODO: find out by what to divide
  result.z = in_pixel.z / 255;
  }
  else
  {
  result.x = 0;
  result.y = in_pixel.y;
  result.z = in_pixel.z;
  }

  PixelYUVToRGB(&result);
  uchar4 out_pixel = {0, 0, 0, 0};
  flt3_to_rp(&result, &out_pixel);
  write_pxl(settings->d_out, x_pos, y_pos, settings->d_image_dimensions.x, out_pixel);
  }
  }
  */

__global__ void radiance_mapper_robertson(cudaTextureObject_t input_tex, cudaSurfaceObject_t output_tex, float* d_response_function, double* d_weight_function, int num_images, int width, int height, float expo_first, float expo_second)
{
    int x_pos = blockIdx.x * blockDim.x + threadIdx.x, y_pos = blockIdx.y * blockDim.y + threadIdx.y;
    if(x_pos < width  && y_pos < height)
    {

        float3 sum = {0.0f, 0.0f, 0.0f};
        float3 divider   = {0.0f, 0.0f, 0.0f};
        float3 result = {0.0f, 0.0f, 0.0f};
        float3 in_pixel = {0.0f, 0.0f, 0.0f};

        in_pixel = read_pxl_rgb(input_tex, x_pos, y_pos, width, height, 0);
        float luminance = 0.27f * in_pixel.x + 0.67f * in_pixel.y + 0.06f * in_pixel.z;
        float weight = calcWeight(d_weight_function, luminance);
        // float weight = 127.5f;
        //innermost loop unrolled
        //R
        sum.x += weight * expo_first * expf(calcResponse(d_response_function, in_pixel.x));
        divider.x += weight * pow(expo_first, 2.0f);

        //G
        sum.y += weight * expo_first * expf(calcResponse(d_response_function, in_pixel.y));
        divider.y += weight  * pow(expo_first, 2.0f);

        //B
        sum.z += weight * expo_first * expf(calcResponse(d_response_function, in_pixel.z));
        divider.z += weight  * pow(expo_first, 2.0f);


        in_pixel = read_pxl_rgb(input_tex, x_pos, y_pos, width, height, 1);
        luminance = 0.27f * in_pixel.x + 0.67f * in_pixel.y + 0.06f * in_pixel.z;
        weight = calcWeight(d_weight_function, luminance);

        //innermost loop unrolled
        //R
        sum.x += weight * expo_second * expf(calcResponse(d_response_function, in_pixel.x));
        divider.x += weight * pow(expo_second, 2.0f);

        //G
        sum.y += weight * expo_second * expf(calcResponse(d_response_function, in_pixel.y));
        divider.y += weight  * pow(expo_second, 2.0f);

        //B
        sum.z += weight * expo_second * expf(calcResponse(d_response_function, in_pixel.z));
        divider.z += weight  * pow(expo_second, 2.0f);




        if(divider.x != 0.0f)
        {
            result.x = sum.x / divider.x;
        }
        else
        {
            result.x = 0.0f;
        }

        if(divider.y != 0.0f)
        {
            result.y = sum.y / divider.y;
        }
        else
        {
            result.y = 0.0f;
        }

        if(divider.z != 0.0f)
        {
            result.z = sum.z / divider.z;
        }
        else
        {
            result.z = 0.0f;
        }


        uchar4 out_pixel = {0, 0, 0, 0};
        flt3_to_rp(&result, &out_pixel);
        write_pxl(output_tex, x_pos, y_pos, width, out_pixel);
    }

}


extern "C" void cu_radiance_mapper_robertson(cudaStream_t stream,
        cudaArray* input, cudaArray* output, struct header * metadata,
        float* d_response_function, double* d_weight_function, int num_sources,
        int num_images, int width, int height)
{

    height = height * num_sources;

    dim3 threadsPerBlock(16,16);
    dim3 numBlocks(divup(width, threadsPerBlock.x), divup(height, threadsPerBlock.y));

    cudaTextureObject_t input_tex = 0;
    cudaSurfaceObject_t output_tex = 0;
    create_tex_obj(input, &input_tex);
    create_surf_obj(output, &output_tex);

    radiance_mapper_robertson<<<numBlocks, threadsPerBlock, 0, stream>>>(input_tex, output_tex, d_response_function, d_weight_function, num_images, width, height, metadata->expoFirst/1000000.0f, metadata->expoSecond/1000000.0f);

    cudaError_t err = cudaGetLastError();
    if(err != cudaSuccess)
        std::cout << "radiance mappper kenel launch fail: " << cudaGetErrorString(err) << std::endl;

    cuda_check(cudaDestroyTextureObject(input_tex), "HDR: failed destroying texture object"); 
    cuda_check(cudaDestroySurfaceObject(output_tex), "HDR: failed destroying surface object"); 
}
