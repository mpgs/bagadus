// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#pragma once

//#define PLATFORM_WINDOWS
#define PLATFORM_LINUX

//#define DEBUG
//#define DEBUG_IMAGE

/*Input and output of the module is going to be in defined colorspace*/
//#define COLORSPACE_RGB
#define COLORSPACE_YUV

/*used by all tone mappers*/
#define USE_GAMMA_CORRECTION
#define GAMMA_VALUE 1.6f

#define SCENE_KEY_BRIGHTNESS 0.08f 	/** Value that the middle brightness of the scene should be mapped to
									 	should be between 0-1. 
										0.04 = very bright scene
										0.72 = very dark scene */



/* for reading files*/
#define MAX_LINE_LENGTH 256		//longest input value that will be read
#define FILE_PATH_RESPONSE_FUNCTION	"calibrated_response_function.txt" //init crashes if this file does not exist



/*for simple ward tonemapper*/
#define DISPLAY_LUMINANCE 250.0f  	//maximum real world luminance that can be displayed by the device that is showing the result
									// typically between 150-350


/*for ward histogram*/
#define HORIZONTAL_ANGLE 67.0f 		//Horizontal FOV of the camera in degrees
#define VERTICAL_ANGLE 160.0f 		/**	Vertical FOV of the camera in degrees 
										vertical is set higher than horizontal, because the images reside on top of each
										other in memory and the five input images are treated as one huge one*/

#define NUM_HISTOGRAM_BINS 100		/**	number of different bins in histogram should be between 100-255
										each bin represents one possible color-value. So <100 to few color values >255 useless
										since there are only 255 values per colorchannel */
#define DISPLAY_MIN_LUMINANCE 0.05f	//darkes value that can be displayed by a monitor divided by ten
#define DISPLAY_MAX_LUMINANCE 350.0f 	//as above brightest value of the display device but divided by ten
//#define HUMAN_CONTRAST	//enables a simulation of human contrast perception (unfortunately introduces a lot of noise)



/*for tocci radiancemapper*/
#define SATURATED_PIXEL 229.0f //90% of max pixel value is considered to be saturated
#define TOCCI_FILTER_SIZE 2 	//size of locals neighbourhood to be considered (actual size is 2*FILTER_SIZE + 1)


/*for reinhard radiancemapper*/
#define NUM_GAUSSKERNELS 2		/**	number of gauss kernels created. Should be between 4-10
								 	each additional kernel requires 5 * img-size * 4 bytes of memory
								 	ideal value is 8 but GPU can't handle that right now */
#define THRESHOLD_CONSTANT 0.05f 	//magic threshold constant from the paper
#define PHI_SHARPENING 8.0f 		//second magic constant from paper	(do not change unless you know what you are doing)
#define COLOR_BOOST 0.7f 			/** color channels are multiplied with that value, slightly dampens too bright colors
									 	0.4 = boring (nearly b&w) 1.0 = _very_ bright colors */


enum {
  ERROR_OK = 0,
  ERROR_UNKNOWN,
  ERROR_MEMORY,
  ERROR_INVALID_ARGUMENTS,
  ERROR_UNINITIALIZED_MEMBER,
  ERROR_EOF,
  ERROR_FILE,
  ERROR_CUDA
};
