// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "image.h"
#include "lodepng.h"
#include "utils.h"

#include <cuda.h>
#include <cuda_runtime.h>

#include <string>
#include <fstream> /* files */
#include <iostream>
#include <sstream>/*stringstream*/

#ifdef PLATFORM_WINDOWS
#include <SDL\SDL.h>
#include <Windows.h>
#endif

extern "C" void cu_rgb_to_yuv(int2 dim, unsigned char* data_addr);
extern "C" void cu_yuv_to_rgb(int2 dim, unsigned char* data_addr);

Image::Image()
{
	//so far do n
	//image = new std::vector<unsigned char>();
}

Image::Image(float _exp_time)
{
	expTime = _exp_time;
}

Image::Image(int x_dim, int y_dim)
{
	image.resize(x_dim * y_dim * 4, 1);
	width = x_dim;
	height = y_dim;
}

Image::~Image()
{

	// std::cout << "this destructor called: ~Image" << std::endl;	

}

void Image::setExposureTime(float _exp_time)
{
	expTime = _exp_time;
}

void Image::setAperture(float _aperture)
{
	aperture = _aperture;
}

int Image::loadHdr(const char* filename)
{
	int error = 0;

	return error;
}

int Image::saveHdr(const char* path)
{
	int error = 0;
	//create filehandle
	
	if(path == 0)
	{
		error = 1;
		printError("Image::saveHdr: path is null");
		return error;
	}


	std::ofstream outputFile;
	outputFile.open(path);
	if(outputFile.is_open())
	{
		//std::string header = "#?RADIANCE\n FORMAT=32-bit_rle_rgbe\n SOFTWARE=awk_hdr\n \n +Y " + height + "+X " + width + "\n";
		outputFile << "#?RADIANCE\nFORMAT=32-bit_rle_rgbe\nSOFTWARE=awk_hdr\n\n+Y ";
		outputFile << height;
		outputFile << "+X ";
		outputFile << width;
		outputFile << "\n";
		//write the raw data
		for(unsigned int y = 0; y < height; ++y)
		{
			for(unsigned int x = 0; x < width; ++x)
			{
				for(int chan = 0; chan < 4; chan++)
				outputFile << image[y * width*4 + x*4 + chan];
			}
		}
	}
	else
	{
		error = 1;
		printError("Image::saveHdr: couldn't create file");

	}

	//savePng("tmp.png");

	//write to file
	return error;
}

int Image::loadPng(const char* filename)
{

	
  //decode
  unsigned int error = lodepng::decode(image, width, height, filename);

  //if there's an error, display it
  //if(error) std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;
  
  if(error)printError(lodepng_error_text(error));
  //the pixels are now in the vector "image", 4 bytes per pixel, ordered RGBARGBA..., use it as texture, draw it, 
  isRGB = true;
  return error;

}
int Image::convertPixelRGBToYUV(unsigned char* pixel)
{
	int error = 0;
	if(pixel)
	{
		//read value
		unsigned int r = *pixel;
		unsigned int g = *(pixel+1);
		unsigned int b = *(pixel+2);

		//r *= .003921569f; g *= .003921569f; b *= .003921569f;
		//convert
		unsigned int y = ((66*r + 129*g + 25*b + 128) >> 8) + 16;
		unsigned int u = ((-38*r - 74*g + 112*b + 128) >> 8) + 128;
		unsigned int v = ( ( 112*r - 94*g - 18*b + 128) >> 8) + 128;
		
	/*	char buf[300];
		sprintf(buf,"y: %d, u: %d, v: %d", y, u, v);
		printMessage(buf);
	*/	

		//write value
		*pixel = (int)y;
		*(pixel+1) = (int)u;
		*(pixel+2) = (int)v;
		*(pixel+3) = 255;
	}
	else
	{
		printError("image.cpp:convertPixelRGBToYUV: pixel is NULL");
		error = 1;
	}
	return error;
}
int Image::RGBToYUV()
{
	int error = 0;
	if(isRGB)
	{

		unsigned char* tmp_pixel;
		//convert every pixel
		for(unsigned int y = 0; y < height; ++y)
		{
			for (unsigned int x = 0; x < width; ++x)
			{

				//read pixel
				//char buf[300];
				//sprintf(buf,"x: %d, y: %d, width: %d, vector size: %d", x, y, width, image.size());
				//printMessage(buf);
				tmp_pixel = &image[(y*width + x)*4];
				//unsigned char& pixel = *(unsigned char*)&image[(y*width + x)<<2];
				//call function
				error = convertPixelRGBToYUV(tmp_pixel);

			}

		}
	}
	else
	{
		printMessage("Image::RGBToYUV is already converted, dublicate call (penis!)");
	}

	if(error)
		printError("Image::RGBToYUV error when convert (fuck)");

	isRGB = false;

	return error;
}
int Image::convertPixelYUVToRGB(unsigned char* pixel)
{
	int error = 0;
	if(pixel)
	{
		//read pixel
		int y = *pixel - 16;
		int u = *(pixel+1) - 128;
		int v = *(pixel+2) - 128;
		//convert
		unsigned char r = (unsigned char)clip(( 298 * y           + 409 * v + 128) >> 8, 0, 255);
		unsigned char g = (unsigned char)clip(( 298 * y - 100 * u - 208 * v + 128) >> 8, 0, 255);
		unsigned char b = (unsigned char)clip(( 298 * y + 516 * u           + 128) >> 8, 0, 255);

	
		//write
		*pixel = r;
		*(pixel+1) = g;
		*(pixel+2) = b;
		*(pixel+3) = 255;
	}
	else
	{
		printError("image.cpp:convertPixelRGBToYUV: pixel is NULL");
		error = 1;
	}
	return error;
}
int Image::YUVToRGB()
{
	int error = 0;
	if(!isRGB)
	{

		unsigned char* tmp_pixel;
		//convert every pixel
		for (unsigned int x = 0; x < width; ++x)
		{
			for(unsigned int y = 0; y < height; ++y)
			{
				//read pixel
				tmp_pixel = &image[(y*width + x)*4];

				//call function
				error = convertPixelYUVToRGB(tmp_pixel);

			}

		}
	}
	else
	{
		printMessage("Image::YUVToRGB is already converted, dublicate call");
	}

	if(error)
		printError("Image::YUVToRGB error when convert");

	return error;	

}

/*int Image::cudaUploadImageToGPU()
{
	int error = 0;
	cudaMalloc(&d_Data,width * height * 4 * sizeof(unsigned char));
	cudaMemcpy(d_Data, image.data(), sizeof(unsigned char) * 4 * width * height, cudaMemcpyHostToDevice);
	if (cudaSuccess == cudaGetLastError())
	{
		onGPU = true;
		return error;
	}
	else
		return error = 1;
}
int Image::cudaDownloadImageFromGPU()
{
	int error = 0;
	if(onGPU)
	{
		cudaMemcpy(image.data(),d_Data,  sizeof(unsigned char) * 4 * width * height, cudaMemcpyDeviceToHost);
		cudaFree(d_Data);
	}	
	return error;
}

int Image::cudaRGBToYUV()
{
	int error = 0;
	int2 dim;
	dim.x = width; dim.y = height;
	cu_rgb_to_yuv(dim, d_Data);
	isRGB = false;
	return error;
}
int Image::cudaYUVToRGB()
{
	int error = 0;
	int2 dim;
	dim.x = width; dim.y = height;
	cu_yuv_to_rgb(dim, d_Data);
	isRGB = true;
	return error;
}

*/
int Image::savePng(const char* filename)
{
  //Encode the image
  unsigned int error = lodepng::encode(filename, image, width, height);

  //if there's an error, display it
  if(error)   printError(lodepng_error_text(error));;
  return error;
}
#ifdef PLATFORM_WINDOWS
int Image::displayImage()
{


  

	    SDL_Surface * surface = 0;
   
        Uint32 rmask, gmask, bmask, amask;

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
        rmask = 0xff000000;
        gmask = 0x00ff0000;
        bmask = 0x0000ff00;
        amask = 0x000000ff;
#else
        rmask = 0x000000ff;
        gmask = 0x0000ff00;
        bmask = 0x00ff0000;
        amask = 0xff000000;
#endif
        int depth = 32;
        surface = SDL_CreateRGBSurface(SDL_SWSURFACE, width, height, depth, rmask, gmask, bmask, amask);

        // Lock the surface, then store the pixel data.
        SDL_LockSurface(surface);

        unsigned char * pixelPointer = static_cast<unsigned char *>(surface->pixels);
        for (std::vector<unsigned char>::iterator iter = image.begin();
                    iter != image.end();
                    ++iter)
        {
            *pixelPointer = *iter;
            ++pixelPointer;
        }

        SDL_UnlockSurface(surface);

	SDL_Window *scr;
	SDL_Renderer *sdlRenderer;
  
	 SDL_CreateWindowAndRenderer(
                          width, height,
                          SDL_WINDOW_OPENGL, &scr, &sdlRenderer);

  
	if(!scr || !sdlRenderer)
	{
		//std::cout << "error, no SDL screen" << std::endl;
		printError("image.cpp::displayImage() : no SDL screen");
		return 0;
	}
		
	 SDL_Texture *sdlTexture;
	sdlTexture = SDL_CreateTextureFromSurface(sdlRenderer, surface);

	SDL_Event eve;
	int done = 0;
	//while(done == 0)
	//{

	//	while(SDL_PollEvent(&eve))
	//	{
	//		if(eve.type == SDL_QUIT) done = 2;
	//		else if(SDL_GetKeyboardState(NULL)[SDL_SCANCODE_ESCAPE]) done = 2;
	//		else if(eve.type == SDL_KEYDOWN) done = 1; //press any other key for next image
	//	}
		
		SDL_RenderClear(sdlRenderer);
		SDL_RenderCopy(sdlRenderer, sdlTexture, NULL, NULL);
		SDL_RenderPresent(sdlRenderer);
		SDL_Delay(5000); //pause 5 ms so it consumes less processing power
  //}

	SDL_Quit();
//  return done == 2 ? 1 : 0;

  


	return 0;
}
#endif //PLATFORM_WINDOWS

int Image::getPixel(int x_pos, int y_pos)
{
	int r = (int)image[(y_pos*width + x_pos)*4] ;
	int g = (int)image[(y_pos*width + x_pos)*4 + 1];
	int b = (int)image[(y_pos*width + x_pos)*4 + 2];
	int a = (int)image[(y_pos*width + x_pos)*4+ 3];
	return (r << 24) | (g << 16) | (b << 8) | a;
}

void Image::setPixel(int x_pos, int y_pos, unsigned char* pixel)
{
	image[(y_pos*width + x_pos)*4] = *pixel;
	image[(y_pos*width + x_pos)*4 + 1] = *(pixel + 1);
	image[(y_pos*width + x_pos)*4 + 2] = *(pixel + 2);
	image[(y_pos*width + x_pos)*4 + 3] = *(pixel + 3);
}
