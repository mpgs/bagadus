#include "../utils/defines.h"
#include "../utils/utils.h"

inline __device__ float calcResponse(float* response_function, float z) {
    z < 0 ? z = 0 : z;
    z > 254 ? z = 254 : z;
    return response_function[(unsigned int) z];
}

inline __device__ float calcWeight(double* weight_function, float z)
{
    z < 0 ? z = 0 : z;
    z > 254 ? z = 254 : z;
    return (float)weight_function[(int) z];

}

__global__ void printWeight(double* weight_function)
{
    for(int i = 0; i < 255; ++i)
    {
        printf("w%i: %f \n", i, weight_function[i]);
    }
}

inline __device__ bool is_this_value(float* d_response_function, float z, int curr_pos)
{
    bool is_value = false;
    //read nescessary values
    float lower = 0.0f, higher = 0.0f, curr_value = 0.0f;
    curr_value = d_response_function[curr_pos];
    if(curr_pos > 0)
    {
        lower = d_response_function[curr_pos - 1];
    }
    else
    {
        //extend lower to catch all overruns
        lower = d_response_function[curr_pos] - 100.0f * d_response_function[curr_pos];
    }

    if(curr_pos < 255)
    {
        higher =d_response_function[curr_pos + 1];
    }
    else
    {
        //extend much further out to catch all edge cases
        higher = d_response_function[curr_pos] + 100.0f * d_response_function[curr_pos];
    }

    //compare if within reach
    lower = curr_value - (curr_value - lower)/2;
    higher = curr_value + (higher - curr_value)/2;

    //values lies within half distance to lower and higher value    lower----|====curr====|----higher
    if(z >= lower && z <= higher)
    {
        is_value = true;
    }

    return is_value;
}

__device__ int calcInverseResponse(float* d_response_function, float z)
{
    //binary search for value
    int imin = 0, imax = 255, midpoint = 0;
    float curr_value = 0.0f;
    while(imax >= imin)
    {
        midpoint = imin + (imax - imin)/2;
        curr_value = d_response_function[midpoint];
        if(is_this_value(d_response_function, z, midpoint))
        {
            //found
            return midpoint;
        }
        else if (curr_value < z)
        {
            imin = midpoint + 1;
        }
        else
        {
            imax = midpoint - 1;
        }
    }

    printf("calcInverseResponse: failed to find value: %f\n", z);
    return -1;
}

__global__ void kernel()
{
	//printf(">>>CUDA Text<<<\n");

} 

/*
__global__ void testKernel(cset_radiancemapper* settings)
{
    int x_pos = blockIdx.x * blockDim.x + threadIdx.x, y_pos = blockIdx.y * blockDim.y + threadIdx.y;
     if(x_pos < settings->d_image_dimensions.x  && y_pos < settings->d_image_dimensions.y)
     {
     // for(int img_it = 0; img_it < settings->d_num_images; ++img_it)
     // {
        int img_it = 3;
        int pixels_per_image = settings->d_image_dimensions.x * settings->d_image_dimensions.y;
            uchar4& in_pixel = *(uchar4*)&(settings->d_image_data[(img_it * pixels_per_image + y_pos * settings->d_image_dimensions.x + x_pos)<<2]);
            uchar4& out_pixel = *(uchar4*)&(settings->d_out[(y_pos * settings->d_image_dimensions.x + x_pos)<<2]);
            
            

            out_pixel.x = in_pixel.x;
            out_pixel.y = in_pixel.y;
            out_pixel.z = in_pixel.z;
            out_pixel.w = in_pixel.w;
     // }
         
     }
}
*/

__device__ float3 operator*(float a, float3 b)
{ 
	b.x *= a;
	b.y *= a;
	b.z *= a;
	return b;
}

__device__ uchar4 operator*(float a, uchar4 b)
{
    b.x *= (unsigned char)a;
    b.y *= (unsigned char)a;
    b.z *= (unsigned char)a;
    return b;    
}

inline __device__ uchar4 operator+=(uchar4 a, uchar4 b)
{
    b.x = a.x + b.x;
    b.y = a.y + b.y;
    b.z = a.z + b.z;
    return b;
}





/*
 * Converts a vector of three floats into
 * a vector of 3 mantissa bytes and a one-byte exponent.
 *
 * Algorithm from James Arvo 1991, Graphic Gems II, p. 82
 */
__device__ void flt3_to_rp(const float3* v_in, uchar4* v_out) {
    float f = fmax(v_in->x, fmax(v_in->y, v_in->z));
    int exponent;

    if (f < 1e-32) {
        v_out->x = 0.0f;
        v_out->y = 0.0f;
        v_out->z = 0.0f;
        v_out->w = 0.0f;
        return;
    }

    f = frexpf(f, &exponent) * 256.0f / f;
    v_out->x = v_in->x * f;
    v_out->y = v_in->y * f;
    v_out->z = v_in->z * f;
    v_out->w = exponent + 128;
}

__device__ void rp_to_flt3(uchar4* v_in, float3* v_out) {
    const int e = v_in->w;

    if (e == 0) {
        v_out->x = 0.0f;
        v_out->y = 0.0f;
        v_out->z = 0.0f;
        return;
    }

    const float v = ldexpf(1.0f/256.0f, e - 128);
    v_out->x = ((float) v_in->x + .5f) * v;
    v_out->y = ((float) v_in->y + .5f) * v;
    v_out->z = ((float) v_in->z + .5f) * v;
}

__device__ void rp_to_flt3(int4* v_in, float3* v_out) {
    const int e = v_in->w;

    if (e == 0) {
        v_out->x = 0.0f;
        v_out->y = 0.0f;
        v_out->z = 0.0f;
        return;
    }

    const float v = ldexpf(1.0f/256.0f, e - 128);
    v_out->x = ((float) v_in->x + .5f) * v;
    v_out->y = ((float) v_in->y + .5f) * v;
    v_out->z = ((float) v_in->z + .5f) * v;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++ simple math
//*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// __device__  int d_clip(int value, int min, int max)
// {
//     return value>max? max : value<min ? min : value;
// }

template<typename T>
__device__ T d_clip(T value, T min, T max)
{
    return value>max? max : value<min ? min : value;   
}


//###########################################################
//### Color space conversions ###############################
//###########################################################
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
__device__ void PixelRGBToXYZ(float3* pixel)
{
    float3 read_pxl = *pixel;
    pixel->x = 0.4124564f * read_pxl.x + 0.3575761f * read_pxl.y + 0.1804375f * read_pxl.z;
    pixel->y = 0.2126729f * read_pxl.x + 0.7151522f * read_pxl.y + 0.0721750f * read_pxl.z;
    pixel->z = 0.0193339f * read_pxl.x + 0.1191920f * read_pxl.y + 0.9503041f * read_pxl.z;
}

__device__ void PixelXYZToRGB(float3* pixel)
{
    float3 read_pxl = *pixel;
    pixel->x = 3.2404542f * read_pxl.x - 1.5371385f * read_pxl.y - 0.4985314f * read_pxl.z;
    pixel->y = -0.9692660f * read_pxl.x + 1.8760108f * read_pxl.y + 0.0415560f * read_pxl.z;
    pixel->z = 0.0556434f * read_pxl.x - 0.2040259f * read_pxl.y + 1.0572252f * read_pxl.z;
}

/*__device__ void PixelRGBToYUV(uchar4* pixel)
{
    uchar4 read_pxl = *pixel;
    pixel->x =  ((66*read_pxl.x + 129*read_pxl.y + 25*read_pxl.z + 128) >> 8) + 16;
    pixel->y = ((-38*read_pxl.x - 74*read_pxl.y + 112*read_pxl.z + 128) >> 8) + 128;
    pixel->z = ( ( 112*read_pxl.x - 94*read_pxl.y - 18*read_pxl.z + 128) >> 8) + 128;

}*/

/*__device__ void PixelRGBToYUV(uchar4* pixel)
{
    uchar4 read_pxl = *pixel;
    pixel->x = d_clip( 0.299f * (float)read_pxl.x  + 0.587f * (float)read_pxl.y + 0.114f * (float)read_pxl.z ,0.0f, 255.0f);
    pixel->y = d_clip( -0.168735f * (float)read_pxl.x  - 0.331264f * (float)read_pxl.y + (0.5f) * (float)read_pxl.z + 128.0f , 0.0f, 255.0f);
    pixel->z = d_clip( 0.5f * (float)read_pxl.x - 0.418688f * (float)read_pxl.y - 0.081312f * (float)read_pxl.z + 128.0f , 0.0f, 255.0f);
}

*/

__device__ void PixelRGBToYUV(float3* pixel)
{
    float3 read_pxl = *pixel;
    pixel->x =  0.299f * read_pxl.x  + 0.587f * read_pxl.y + 0.114f * read_pxl.z;
    pixel->y =  -0.168735f * read_pxl.x  - 0.331264f * read_pxl.y + 0.5f * read_pxl.z + 128.0f;
    pixel->z =  0.5f * read_pxl.x - 0.418688f * read_pxl.y - 0.081312f * read_pxl.z + 128.0f;

/*    pixel->x =  ((0.299*read_pxl.x + 0.587*read_pxl.y + 0.114f*read_pxl.z));
    pixel->y = ((-0.14713f*read_pxl.x - 0.28886f*read_pxl.y + 0.436f*read_pxl.z));
    pixel->z = (( 0.615f*read_pxl.x - 0.51499f*read_pxl.y - 0.10001f*read_pxl.z));
*/
}

__device__ void PixelYUVToRGB(float3* pixel)
{
    float3 read_pxl = *pixel;
    pixel->x = read_pxl.x + 1.4075f * (read_pxl.z - 128.0f);
    pixel->y = read_pxl.x - 0.3455f * (read_pxl.y - 128.0f) - 0.7169f * (read_pxl.z - 128.0f);
    pixel->z = read_pxl.x + 1.7790f * (read_pxl.y - 128.0f);
    
 /*   pixel->x = d_clip( read_pxl.x + 1.4075f * (read_pxl.z - 128.0f)  , 0.0f, 255.0f);
    pixel->y = d_clip( read_pxl.x - 0.3455f * (read_pxl.y - 128.0f) - 0.7169f * (read_pxl.z - 128.0f), 0.0f, 255.0f);
    pixel->z = d_clip( read_pxl.x + 1.7790f * (read_pxl.y - 128.0f) , 0.0f, 255.0f);
*/
}

/*__device__ void PixelYUVToRGB(uchar4* pixel)
{
        uchar4 read_pxl = *pixel;
        pixel->x = d_clip( (float)read_pxl.x + 1.4075f * ((float)read_pxl.z - 128.0f)  , 0.0f, 255.0f);
        pixel->y = d_clip( (float)read_pxl.x - 0.3455f * ((float)read_pxl.y - 128.0f) - 0.7169f * ((float)read_pxl.z - 128.0f), 0.0f, 255.0f);
        pixel->z = d_clip( (float)read_pxl.x + 1.7790f * ((float)read_pxl.y - 128.0f) , 0.0f, 255.0f);

}
*/

__global__ void RGBToYUV(int2 dim, unsigned char* d_Data)
{
    int x_pos = blockIdx.x * blockDim.x + threadIdx.x, y_pos = blockIdx.y * blockDim.y + threadIdx.y;
    if(x_pos < dim.x  && y_pos < dim.y)
    {
        uchar4& pixel = *(uchar4*)&d_Data[(y_pos*dim.x + x_pos)<<2];
        uchar4 read_pxl = pixel;
        pixel.x =  ((66*read_pxl.x + 129*read_pxl.y + 25*read_pxl.z + 128) >> 8) + 16;
        pixel.y = ((-38*read_pxl.x - 74*read_pxl.y + 112*read_pxl.z + 128) >> 8) + 128;
        pixel.z = ( ( 112*read_pxl.x - 94*read_pxl.y - 18*read_pxl.z + 128) >> 8) + 128;
    }   

}

__global__ void YUVToRGB(int2 dim, unsigned char* d_Data)
{
    int x_pos = blockIdx.x * blockDim.x + threadIdx.x, y_pos = blockIdx.y * blockDim.y + threadIdx.y;
    if(x_pos < dim.x  && y_pos < dim.y)
    {
        uchar4& pixel = *(uchar4*)&d_Data[(y_pos*dim.x + x_pos)<<2];
        uchar4 read_pxl = (uchar4)pixel;
        pixel.x = (unsigned char)d_clip(( 298 * ( int)read_pxl.x           + 409 * ( int)read_pxl.z + 128) >> 8, 0, 255);
        pixel.y = (unsigned char)d_clip(( 298 * ( int)read_pxl.x - 100 * ( int)read_pxl.y - 208 * ( int)read_pxl.z + 128) >> 8, 0, 255);
        pixel.z = (unsigned char)d_clip(( 298 * ( int)read_pxl.x + 516 * ( int)read_pxl.y           + 128) >> 8, 0, 255);
    
    }
}


extern "C" void cu_rgb_to_yuv(int2 dim, unsigned char* data_addr)
{
    dim3 threadsPerBlock(16,16);
    dim3 numBlocks(divup(dim.x, threadsPerBlock.x), divup(dim.y, threadsPerBlock.y));

    RGBToYUV<<<numBlocks, threadsPerBlock>>>(dim, data_addr);
}

extern "C" void cu_yuv_to_rgb(int2 dim, unsigned char* data_addr)
{
    dim3 threadsPerBlock(16,16);
    dim3 numBlocks(divup(dim.x, threadsPerBlock.x), divup(dim.y, threadsPerBlock.y));

    YUVToRGB<<<numBlocks, threadsPerBlock>>>(dim, data_addr);
}

//*****************************************************************
//****   Reading/Writing Pixels
//*****************************************************************
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

inline __device__ int calc_pxl_pos(int x_pos, int y_pos, int width)
{
    return y_pos * width + x_pos;
}

inline __device__ int calc_pxl_pos(int x_pos, int y_pos, int width, int height, int num_img)
{
    return width * height * num_img + y_pos * width + x_pos;
}

inline __device__ uchar4 read_pxl(unsigned char* image, int x_pos, int y_pos, int width)
{
    return *(uchar4*)&(image[(calc_pxl_pos(x_pos, y_pos, width))<<2]);
}

inline __device__ uchar4 read_pxl(unsigned char* image, int x_pos, int y_pos, int width, int height, int num_img)
{

    return *(uchar4*)&(image[(calc_pxl_pos(x_pos, y_pos, width, height, num_img))<<2]);
}

inline __device__ float3 read_pxl(float3* image, int x_pos, int y_pos, int width)
{
    return *(float3*)&(image[calc_pxl_pos(x_pos, y_pos, width)]);
}

inline __device__ float3 read_pxl(float3* image, int x_pos, int y_pos, int width, int height, int num_img)
{
    return *(float3*)&(image[(calc_pxl_pos(x_pos, y_pos, width, height, num_img))]);
}

inline __device__ uchar4 read_pxl(cudaTextureObject_t image, int x_pos, int y_pos, int width)
{
    
    return tex2D<uchar4>(image, x_pos, y_pos);  
    
}

inline __device__ uchar4 read_pxl(cudaTextureObject_t image, int x_pos, int y_pos, int width, int height, int num_img)
{
    return tex2D<uchar4>(image, x_pos, y_pos  + (height * num_img ));
}

inline __device__ uchar4 read_pxl_surf(cudaSurfaceObject_t image, int x_pos, int y_pos)
{
    uchar4 return_value = {0, 0, 0, 0};
    surf2Dread(&return_value, image, x_pos * 4, y_pos);
    return return_value;
}

/*
inline __device__ uchar4 read_pxl(cudaSurfaceObject_t* image, int x_pos, int y_pos, int width)
{
    float4 return_value = {0.0f, 0.0f, 0.0f, 0.0f};
    //surf1Dread(&return_value, *image,calc_pxl_pos(x_pos, y_pos, width));
    //return_value = tex1D<float3>(*image, calc_pxl_pos(x_pos, y_pos, width));
    surf1Dread(&return_value, *image, calc_pxl_pos(x_pos, y_pos, width));
    return return_value;
}*/

inline __device__ float3 read_pxl_rgb(unsigned char* image, int x_pos, int y_pos, int width)
{
    uchar4 read_value = read_pxl(image, x_pos, y_pos, width);
    float3 return_value = {read_value.x, read_value.y, read_value.z};
    #ifdef COLORSPACE_YUV
        PixelYUVToRGB(&return_value);
    #endif
    return return_value;
}

inline __device__ float3 read_pxl_yuv(unsigned char* image, int x_pos, int y_pos, int width)
{
    uchar4 read_value = read_pxl(image, x_pos, y_pos, width);
    float3 return_value = {read_value.x, read_value.y, read_value.z};
    #ifdef COLORSPACE_RGB
        PixelRGBToYUV(&return_value);     
    #endif
    
    return return_value;    
}

inline __device__ float3 read_pxl_rgb(unsigned char* image, int x_pos,  int y_pos, int width, int height, int num_img)
{
    uchar4 read_value = read_pxl(image, x_pos, y_pos, width, height, num_img);
    float3 return_value = {read_value.x, read_value.y, read_value.z};
    #ifdef COLORSPACE_YUV
        PixelYUVToRGB(&return_value);
    #endif
    return return_value;
}

inline __device__ float3 read_pxl_yuv(unsigned char* image,  int x_pos,  int y_pos, int width, int height, int num_img)
{
    uchar4 read_value = read_pxl(image, x_pos, y_pos, width, height, num_img);
    float3 return_value = {read_value.x, read_value.y, read_value.z};
    #ifdef COLORSPACE_RGB
        PixelRGBToYUV(&return_value);
    #endif
    return return_value;
}

inline __device__ float3 read_pxl_rgb(cudaTextureObject_t image, int x_pos, int y_pos, int width)
{
    uchar4 read_value = read_pxl(image, x_pos, y_pos, width);
    float3 return_value = {read_value.x, read_value.y, read_value.z};
    #ifdef COLORSPACE_YUV
        PixelYUVToRGB(&return_value);
    #endif
    return return_value;   
}

inline __device__ float3 read_pxl_yuv(cudaTextureObject_t image, int x_pos, int y_pos, int width)
{
    uchar4 read_value = read_pxl(image, x_pos, y_pos, width);
    float3 return_value = {read_value.x, read_value.y, read_value.z};
    #ifdef COLORSPACE_RGB
        PixelRGBToYUV(&return_value);     
    #endif
    
    return return_value;    
}

inline __device__ float3 read_pxl_rgb(cudaTextureObject_t image, int x_pos,  int y_pos, int width, int height, int num_img)
{
    uchar4 read_value = read_pxl(image, x_pos, y_pos, width, height, num_img);
    float3 return_value = {read_value.x, read_value.y, read_value.z};
    #ifdef COLORSPACE_YUV
        PixelYUVToRGB(&return_value);
    #endif
    return return_value;
}

inline __device__ float3 read_pxl_yuv(cudaTextureObject_t image,  int x_pos,  int y_pos, int width, int height, int num_img)
{
    uchar4 read_value = read_pxl(image, x_pos, y_pos, width, height, num_img);
    float3 return_value = {read_value.x, read_value.y, read_value.z};
    #ifdef COLORSPACE_RGB
        PixelRGBToYUV(&return_value);
    #endif
    return return_value;
}


inline __device__ float read_pxl_float(float* image, int x_pos, int y_pos, int width)
{
    return image[calc_pxl_pos(x_pos, y_pos, width)];
}

inline __device__ float read_pxl_float(float* image, int x_pos, int y_pos, int width, int height, int num_img)
{
    return image[(calc_pxl_pos(x_pos, y_pos, width, height, num_img))];
}

inline __device__ float3 read_pxl_rgb(float3* image, int x_pos, int y_pos, int width)
{
    float3 return_value = read_pxl(image, x_pos, y_pos, width);
    #ifdef COLORSPACE_YUV
        PixelYUVToRGB(&return_value);
    #endif
    return return_value;
}

inline __device__ float3 read_pxl_rgb(float3* image, int x_pos, int y_pos, int width, int height, int num_img)
{
    float3 return_value = read_pxl(image, x_pos, y_pos, width, height, num_img);
    #ifdef COLORSPACE_YUV
        PixelYUVToRGB(&return_value);
    #endif
    return return_value;
}

inline __device__ float3 read_pxl_yuv(float3* image, int x_pos, int y_pos, int width)
{
    float3 return_value = read_pxl(image, x_pos, y_pos, width);
    #ifdef COLORSPACE_RGB
        PixelRGBToYUV(&return_value);
    #endif
    return return_value;
}

inline __device__ float3 read_pxl_yuv(float3* image, int x_pos, int y_pos, int width, int height, int num_img)
{
    float3 return_value = read_pxl(image, x_pos, y_pos, width, height, num_img);
    #ifdef COLORSPACE_RGB
        PixelRGBToYUV(&return_value);
    #endif
    return return_value;
}


inline __device__ void write_pxl(unsigned char* image, int x_pos, int y_pos, int width, uchar4 value)
{
    uchar4& out_pixel = *(uchar4*)&(image[(calc_pxl_pos(x_pos, y_pos, width))<<2]);
    out_pixel = value;
}

inline __device__ void write_pxl(unsigned char* image, int x_pos, int y_pos, int width, int height, int num_img, uchar4 value)
{
    uchar4& out_pixel = *(uchar4*)&(image[(calc_pxl_pos(x_pos, y_pos, width, height, num_img))<<2]);
    out_pixel = value;
}

inline __device__ void write_pxl(float3* image, int x_pos, int y_pos, int width, float3 value)
{
    float3& out_pixel = *(float3*)&(image[(calc_pxl_pos(x_pos, y_pos, width))]);
    out_pixel = value;
}

inline __device__ void write_pxl(float3* image, int x_pos, int y_pos, int width, int height, int num_img, float3 value)
{
    float3& out_pixel = *(float3*)&(image[(calc_pxl_pos(x_pos, y_pos, width, height, num_img))]);
    out_pixel = value;
}

inline __device__ void write_pxl(cudaSurfaceObject_t image, int x_pos, int y_pos, int width, uchar4 value)
{
    surf2Dwrite(value, image, x_pos * 4, y_pos);
}

inline __device__ void write_pxl(cudaSurfaceObject_t image, int x_pos, int y_pos, int width, int height, int num_img, uchar4 value)
{
    surf2Dwrite(value, image, x_pos * 4, y_pos + (height * num_img));
}


inline __device__ void write_pxl_rgb(unsigned char* image, int x_pos, int y_pos, int width, float3 value)
{
    #ifdef COLORSPACE_YUV
        PixelRGBToYUV(&value);
    #endif
    uchar4 write_value = {(unsigned char)d_clip(value.x, 0.0f, 255.0f), (unsigned char)d_clip(value.y, 0.0f, 255.0f), (unsigned char)d_clip(value.z, 0.0f, 255.0f), 255};
    write_pxl(image, x_pos, y_pos, width, write_value);
}

inline __device__ void write_pxl_yuv(unsigned char* image, int x_pos, int y_pos, int width, float3 value)
{
    #ifdef COLORSPACE_RGB
        PixelYUVToRGB(&value);
    #endif
    uchar4 write_value = {(unsigned char)d_clip(value.x, 0.0f, 255.0f), (unsigned char)d_clip(value.y, 0.0f, 255.0f), (unsigned char)d_clip(value.z, 0.0f, 255.0f), 255};
    write_pxl(image, x_pos, y_pos, width, write_value);
}

inline __device__ void write_pxl_yuv(unsigned char* image, int x_pos, int y_pos, int width, int height, int num_img, float3 value)
{
    #ifdef COLORSPACE_RGB
        PixelYUVToRGB(&value);
    #endif
    uchar4 write_value = {(unsigned char)d_clip(value.x, 0.0f, 255.0f), (unsigned char)d_clip(value.y, 0.0f, 255.0f), (unsigned char)d_clip(value.z, 0.0f, 255.0f), 255};
    write_pxl(image, x_pos, y_pos, width, height, num_img, write_value);
}

inline __device__ void write_pxl_rgb(unsigned char* image, int x_pos, int y_pos, int width, int height, int num_img, float3 value)
{
    #ifdef COLORSPACE_YUV
        PixelRGBToYUV(&value);
    #endif
    uchar4 write_value = {(unsigned char)d_clip(value.x, 0.0f, 255.0f), (unsigned char)d_clip(value.y, 0.0f, 255.0f), (unsigned char)d_clip(value.z, 0.0f, 255.0f), 255};
    write_pxl(image, x_pos, y_pos, width, height, num_img, write_value);
}


inline __device__ void write_pxl_rgb(float3* image, int x_pos, int y_pos, int width, float3 value)
{
    #ifdef COLORSPACE_YUV
        PixelRGBToYUV(&value);
    #endif
    write_pxl(image, x_pos, y_pos, width, value);
}

inline __device__ void write_pxl_yuv(float3* image, int x_pos, int y_pos, int width, float3 value)
{
    #ifdef COLORSPACE_RGB
        PixelYUVToRGB(&value);
    #endif
    write_pxl(image, x_pos, y_pos, width, value);
}

inline __device__ void write_pxl_yuv(float3* image, int x_pos, int y_pos, int width, int height, int num_img, float3 value)
{
    #ifdef COLORSPACE_RGB
        PixelYUVToRGB(&value);
    #endif
    write_pxl(image, x_pos, y_pos, width, height, num_img, value);
}

inline __device__ void write_pxl_rgb(float3* image, int x_pos, int y_pos, int width, int height, int num_img, float3 value)
{
    #ifdef COLORSPACE_YUV
        PixelRGBToYUV(&value);
    #endif
    write_pxl(image, x_pos, y_pos, width, height, num_img, value);
}


inline __device__ void write_pxl_rgb(cudaSurfaceObject_t image, int x_pos, int y_pos, int width, float3 value)
{
    #ifdef COLORSPACE_YUV
        PixelRGBToYUV(&value);
    #endif
    uchar4 write_value = {(unsigned char)d_clip(value.x, 0.0f, 255.0f), (unsigned char)d_clip(value.y, 0.0f, 255.0f), (unsigned char)d_clip(value.z, 0.0f, 255.0f), 255};
    write_pxl(image, x_pos, y_pos, width, write_value);
}

inline __device__ void write_pxl_yuv(cudaSurfaceObject_t image, int x_pos, int y_pos, int width, float3 value)
{
    #ifdef COLORSPACE_RGB
        PixelYUVToRGB(&value);
    #endif
    uchar4 write_value = {(unsigned char)d_clip(value.x, 0.0f, 255.0f), (unsigned char)d_clip(value.y, 0.0f, 255.0f), (unsigned char)d_clip(value.z, 0.0f, 255.0f), 255};
    write_pxl(image, x_pos, y_pos, width, write_value);
}

inline __device__ void write_pxl_yuv(cudaSurfaceObject_t image, int x_pos, int y_pos, int width, int height, int num_img, float3 value)
{
    #ifdef COLORSPACE_RGB
        PixelYUVToRGB(&value);
    #endif
    uchar4 write_value = {(unsigned char)d_clip(value.x, 0.0f, 255.0f), (unsigned char)d_clip(value.y, 0.0f, 255.0f), (unsigned char)d_clip(value.z, 0.0f, 255.0f), 255};
    write_pxl(image, x_pos, y_pos, width, height, num_img, write_value);
}

inline __device__ void write_pxl_rgb(cudaSurfaceObject_t image, int x_pos, int y_pos, int width, int height, int num_img, float3 value)
{
    #ifdef COLORSPACE_YUV
        PixelRGBToYUV(&value);
    #endif
    uchar4 write_value = {(unsigned char)d_clip(value.x, 0.0f, 255.0f), (unsigned char)d_clip(value.y, 0.0f, 255.0f), (unsigned char)d_clip(value.z, 0.0f, 255.0f), 255};
    write_pxl(image, x_pos, y_pos, width, height, num_img, write_value);
}




inline __device__ void write_pxl_float(float* image, int x_pos, int y_pos, int width, float value)
{
    image[(calc_pxl_pos(x_pos, y_pos, width))] = value;
}

inline __device__ void write_pxl_float(float* image, int x_pos, int y_pos, int width, int height, int num_img, float value)
{
    image[(calc_pxl_pos(x_pos, y_pos, width, height, num_img))] = value;
}


//#################################################################
//##########   Begin Nvidia  ######################################
//#################################################################

/*
 * Copyright 1993-2013 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */
// Utility class used to avoid linker errors with extern
// unsized shared memory arrays with templated type


template<class T>
struct SharedMemory
{
    __device__ inline operator       unsigned char *()
    {
        extern __shared__ int __smem[];
        return (T *)__smem;
    }

    __device__ inline operator const T *() const
    {
        extern __shared__ int __smem[];
        return (T *)__smem;
    }
};

// specialize for double to avoid unaligned memory
// access compile errors
template<>
struct SharedMemory<double>
{
    __device__ inline operator       double *()
    {
        extern __shared__ double __smem_d[];
        return (double *)__smem_d;
    }

    __device__ inline operator const double *() const
    {
        extern __shared__ double __smem_d[];
        return (double *)__smem_d;
    }
};

unsigned int nextPow2(unsigned int x)
{
    --x;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    return ++x;
}

template <class T, unsigned int blockSize, bool nIsPow2>
__global__ void
reduce6(cudaTextureObject_t g_idata, T *g_odata, int n, int width)
{


    double *sdata = SharedMemory<double>();
    double offset = 0.0000001;

    // perform first level of reduction,
    // reading from global memory, writing to shared memory
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x*blockSize*2 + threadIdx.x;
    unsigned int gridSize = blockSize*2*gridDim.x;

    int linear_pos = 0, x = 0, y = 0;

    float3 float_pixel = {0.0f, 0.0f, 0.0f};
    

    double mySum = 0.0;
   
    while (i < n)
    {
        linear_pos = i;
        x = linear_pos % width;
        y = linear_pos / width;
        uchar4 in_pixel = tex2D<uchar4>(g_idata, x, y);
        rp_to_flt3(&in_pixel, &float_pixel);
        mySum += (double) log(((double)float_pixel.x + offset)*0.27 + ((double)float_pixel.y + offset)*0.67 + ((double)float_pixel.z + offset)*0.06); 
   
        if (nIsPow2 || i + blockSize < n)
        {
            linear_pos = i + blockSize;
            x = linear_pos % width;
            y = linear_pos / width;
        
            uchar4 in_pixel = tex2D<uchar4>(g_idata, x, y);
            rp_to_flt3(&in_pixel, &float_pixel);
            mySum += log(((double)float_pixel.x + offset)*0.27 + ((double)float_pixel.y + offset)*0.67 + ((double)float_pixel.z + offset)*0.06); 
        }
            

        i += gridSize;
    }

    // each thread puts its local sum into shared memory
    sdata[tid] = mySum;
    __syncthreads();


    // do reduction in shared mem
    if (blockSize >= 512)
    {
        if (tid < 256)
        {
            sdata[tid] = mySum = mySum + sdata[tid + 256];
        }

        __syncthreads();
    }

    if (blockSize >= 256)
    {
        if (tid < 128)
        {
            sdata[tid] = mySum = mySum + sdata[tid + 128];
        }

        __syncthreads();
    }

    if (blockSize >= 128)
    {
        if (tid <  64)
        {
            sdata[tid] = mySum = mySum + sdata[tid +  64];
        }

        __syncthreads();
    }

    if (tid < 32)
    {
        // now that we are using warp-synchronous programming (below)
        // we need to declare our shared memory volatile so that the compiler
        // doesn't reorder stores to it and induce incorrect behavior.
        volatile double *smem = sdata;

        if (blockSize >=  64)
        {
            smem[tid] = mySum = mySum + smem[tid + 32];
        }

        if (blockSize >=  32)
        {
            smem[tid] = mySum = mySum + smem[tid + 16];
        }

        if (blockSize >=  16)
        {
            smem[tid] = mySum = mySum + smem[tid +  8];
        }

        if (blockSize >=   8)
        {
            smem[tid] = mySum = mySum + smem[tid +  4];
        }

        if (blockSize >=   4)
        {
            smem[tid] = mySum = mySum + smem[tid +  2];
        }

        if (blockSize >=   2)
        {
            smem[tid] = mySum = mySum + smem[tid +  1];
        }
    }

    // write result for this block to global mem
    if (tid == 0)
    {
        g_odata[blockIdx.x] = sdata[0]; 
        //printf("[%i, %i]Block result: %f\n", blockIdx.x, blockIdx.y, sdata[0]);
    }
}

extern "C" bool isPow2(unsigned int x);


// Wrapper function for kernel launch
template <class T>
void
reduce(int size, int width, int threads, int blocks, cudaTextureObject_t d_idata, T *d_odata, cudaStream_t stream)
{
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);

    // when there is only one warp per block, we need to allocate two warps
    // worth of shared memory so that we don't index shared memory out of bounds
    int smemSize = (threads <= 32) ? 2 * threads * sizeof(T) : threads * sizeof(T);
   if (isPow2(size))
    {

        switch (threads)
        {
            case 512:
                reduce6<T, 512, true><<< dimGrid, dimBlock, smemSize, stream>>>(d_idata, d_odata, size, width); break;
            case 256:
                reduce6<T, 256, true><<< dimGrid, dimBlock, smemSize, stream>>>(d_idata, d_odata, size, width); break;
            case 128:
                reduce6<T, 128, true><<< dimGrid, dimBlock, smemSize, stream>>>(d_idata, d_odata, size, width); break;
            case 64:
                reduce6<T,  64, true><<< dimGrid, dimBlock, smemSize, stream>>>(d_idata, d_odata, size, width); break;
            case 32:
                reduce6<T,  32, true><<< dimGrid, dimBlock, smemSize, stream>>>(d_idata, d_odata, size, width); break;
            case 16:
                reduce6<T,  16, true><<< dimGrid, dimBlock, smemSize, stream>>>(d_idata, d_odata, size, width); break;
            case  8:
                reduce6<T,   8, true><<< dimGrid, dimBlock, smemSize, stream>>>(d_idata, d_odata, size, width); break;
            case  4:
                reduce6<T,   4, true><<< dimGrid, dimBlock, smemSize, stream>>>(d_idata, d_odata, size, width); break;
            case  2:
                reduce6<T,   2, true><<< dimGrid, dimBlock, smemSize, stream>>>(d_idata, d_odata, size, width); break;
            case  1:
                reduce6<T,   1, true><<< dimGrid, dimBlock, smemSize, stream>>>(d_idata, d_odata, size, width); break;
        }
    }
    else
    {
        switch (threads)
        {
            case 512:
                reduce6<T, 512, false><<< dimGrid, dimBlock, smemSize, stream>>>(d_idata, d_odata, size, width); break;
            case 256:
                reduce6<T, 256, false><<< dimGrid, dimBlock, smemSize, stream>>>(d_idata, d_odata, size, width); break;
            case 128:
                reduce6<T, 128, false><<< dimGrid, dimBlock, smemSize, stream>>>(d_idata, d_odata, size, width); break;
            case 64:
                reduce6<T,  64, false><<< dimGrid, dimBlock, smemSize, stream>>>(d_idata, d_odata, size, width); break;
            case 32:
                reduce6<T,  32, false><<< dimGrid, dimBlock, smemSize, stream>>>(d_idata, d_odata, size, width); break;
            case 16:
                reduce6<T,  16, false><<< dimGrid, dimBlock, smemSize, stream>>>(d_idata, d_odata, size, width); break;
            case  8:
                reduce6<T,   8, false><<< dimGrid, dimBlock, smemSize, stream>>>(d_idata, d_odata, size, width); break;
            case  4:
                reduce6<T,   4, false><<< dimGrid, dimBlock, smemSize, stream>>>(d_idata, d_odata, size, width); break;
            case  2:
                reduce6<T,   2, false><<< dimGrid, dimBlock, smemSize, stream>>>(d_idata, d_odata, size, width); break;
            case  1:
                reduce6<T,   1, false><<< dimGrid, dimBlock, smemSize, stream>>>(d_idata, d_odata, size, width); break;
        }
    }
}

// Instantiate the reduction function for 3 types
// template void
// reduce<int>(int size, int threads, int blocks, int *d_idata, int *d_odata);

 template void
 reduce<float>(int size, int threads, int blocks, int width, cudaTextureObject_t d_idata, float *d_odata, cudaStream_t stream);

 template void
 reduce<double>(int size, int threads, int blocks, int width, cudaTextureObject_t d_idata, double *d_odata, cudaStream_t stream);

template void
reduce<unsigned char>(int size, int threads, int blocks, int width, cudaTextureObject_t d_idata, unsigned char *d_odata, cudaStream_t stream);



void getNumBlocksAndThreads(int n, int maxBlocks, int maxThreads, int &blocks, int &threads)
{

    //get device capability, to avoid block/grid size excceed the upbound
    cudaDeviceProp prop;
    int device;
    cudaGetDevice(&device);
    cudaGetDeviceProperties(&prop, device);


        threads = (n < maxThreads*2) ? nextPow2((n + 1)/ 2) : maxThreads;
        blocks = (n + (threads * 2 - 1)) / (threads * 2);
   

    if ((float)threads*blocks > (float)prop.maxGridSize[0] * prop.maxThreadsPerBlock)
    {
        printf("n is too large, please choose a smaller number!\n");
    }

    if (blocks > prop.maxGridSize[0])
    {
        printf("Grid size <%d> excceeds the device capability <%d>, set block size as %d (original %d)\n",
               blocks, prop.maxGridSize[0], threads*2, threads);

        blocks /= 2;
        threads *= 2;
    }

 
        blocks = min(maxBlocks, blocks);
  

}

template <class T>
T run_reduce(int num_pixels, int width, cudaTextureObject_t d_idata, T *d_odata, cudaStream_t stream)
{


    //n, numThreads, numBlocks
     int numBlocks = 0;
     int numThreads = 0;
     int maxBlocks = 64;
     int maxThreads = 256;

     getNumBlocksAndThreads(num_pixels, maxBlocks, maxThreads ,numBlocks, numThreads);

    cudaDeviceSynchronize();
//     cudaStreamSynchronize(stream); //Seems to achieve better scheduling if we wait for all others to end
    
    T *h_odata = (T *) malloc(numBlocks*sizeof(T));
        // execute the kernel
        reduce<T>(num_pixels, width, numThreads, numBlocks, d_idata, d_odata, stream);
        cudaError_t err;
        err = cudaGetLastError();
        if(err != cudaSuccess)
            std::cout << "tone mapper kernel launch fail: " << cudaGetErrorString(err) << std::endl;
        // check if kernel execution generated an error


            // sum partial sums from each block on CPU
            // copy result from device to host
            cudaMemcpy(h_odata, d_odata, numBlocks*sizeof(T), cudaMemcpyDeviceToHost);
            T gpu_result = h_odata[0];
            for (int i=1; i<numBlocks; i++)
            {
                gpu_result += h_odata[i];
            }
    
    //std::cout << "num Blocks: " << numBlocks << "gpu_result: " << gpu_result << std::endl;
    //return gpu_result;
    return exp(gpu_result/num_pixels);
    //return 348.0;
    //return 300.0f;

}


//#################################################################
//##########   End Nvidia  ########################################
//#################################################################
