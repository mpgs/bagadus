// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#pragma once
#include <vector>
#include "defines.h"



class Image
{
public:
	std::vector<unsigned char> image;
	unsigned char* d_Data;
	unsigned int width;
	unsigned int height;
	float expTime;
	float aperture;

	int loadPng(const char* filename);
	int savePng(const char* filename);

	int loadHdr(const char* filename);
	int saveHdr(const char* filename);
	#ifdef PLATFORM_WINDOWS
	int displayImage();	
	#endif
	void setExposureTime(float _exp_ime);
	void setAperture(float _aperture);
	int RGBToYUV();
	int YUVToRGB();
	
	int getPixel(int x_pos, int y_pos);
	void setPixel(int x_pos, int y_pos, unsigned char* pixel);

	int convertPixelRGBToYUV(unsigned char* pixel);
	int convertPixelYUVToRGB(unsigned char* pixel);
//	int cudaRGBToYUV();
//	int cudaYUVToRGB();
//	int cudaUploadImageToGPU();
//	int cudaDownloadImageFromGPU();

	Image();
	Image(float _exp_time);
	Image(int x_dim, int y_dim); //creates empty image with given size
	~Image();

private:
	bool isRGB;
	bool onGPU;


	
};
