// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#define CLAMP_255(x) ( (x<0) ? 0 : (x > 255) ? 255 : x)

texture<uint8_t, 2, cudaReadModeNormalizedFloat> yuvDstTex;
surface<void, cudaSurfaceType2D> edgeSurface;
surface<void, cudaSurfaceType2D> dynamicSurface;

__global__ void showEdgeMap(uint8_t * output, int dstW, int dstH){
	for(int idx=2*(blockIdx.x*blockDim.x + threadIdx.x);
			idx < (dstW*dstH); idx += 2*(blockDim.x*gridDim.x)){

		float value, gradient;
		int x, y;
		x = idx % dstW;
		y = idx / dstW;
		surf2Dread(&value, edgeSurface, sizeof(float)*x,
				y, cudaBoundaryModeClamp);
		gradient = rintf( value * 255.0f );
		output[idx] = CLAMP_255(gradient);

		x++;
		surf2Dread(&value, edgeSurface, sizeof(float)*x,
				y, cudaBoundaryModeClamp);
		gradient = rintf( value * 255.0f );
		output[idx+1] = CLAMP_255(gradient);
		output[dstW*dstH + (idx>>1)] = 128;
		output[(dstW*dstH*3 + idx)>>1] = 128;
	}
}

__global__ void createEdgeMap(int srcW, int srcH){

	for(int idx = blockIdx.x*blockDim.x + threadIdx.x;
			idx < (srcW*srcH); idx += blockDim.x*gridDim.x){
		int x, y;

		x = (idx%srcW);
		y = (idx/srcW);
		float3 arr[3];
		arr[0] = make_float3(
				tex2D(yuvDstTex, x-1, y-1),
				tex2D(yuvDstTex, x  , y-1),
				tex2D(yuvDstTex, x+1, y-1));
		arr[1] = make_float3(
				tex2D(yuvDstTex, x-1, y  ),
				tex2D(yuvDstTex, x  , y  ),
				tex2D(yuvDstTex, x+1, y  ));
		arr[2] = make_float3(
				tex2D(yuvDstTex, x-1, y+1),
				tex2D(yuvDstTex, x  , y+1),
				tex2D(yuvDstTex, x+1, y+1));

		float2 gradient;
		gradient.x = arr[0].x + 1*arr[1].x +arr[2].x - arr[0].z - 1*arr[1].z - arr[2].z;
		gradient.y = arr[0].x + 1*arr[0].y +arr[0].z - arr[2].x - 1*arr[2].y - arr[2].z;
		gradient.y = gradient.y + abs(arr[1].y - tex2D(yuvDstTex, x, y-2))
			- abs(arr[1].y - tex2D(yuvDstTex, x, y+2));
		gradient.x = gradient.x + abs(arr[1].y - tex2D(yuvDstTex, x-2, y))
			- abs(arr[1].y - tex2D(yuvDstTex, x+2, y));

		float result = sqrtf(gradient.x * gradient.x + gradient.y * gradient.y);
		surf2Dwrite(result, edgeSurface, sizeof(float)*(int)x,
				(int)y, cudaBoundaryModeClamp);
	}
}

__device__ float getEdgeCost(int x, int y){
	float3 arr[3];
	arr[0] = make_float3(
			tex2D(yuvTex, x-1, y-1).x,
			tex2D(yuvTex, x  , y-1).x,
			tex2D(yuvTex, x+1, y-1).x);
	arr[1] = make_float3(
			tex2D(yuvTex, x-1, y  ).x,
			tex2D(yuvTex, x  , y  ).x,
			tex2D(yuvTex, x+1, y  ).x);
	arr[2] = make_float3(
			tex2D(yuvTex, x-1, y+1).x,
			tex2D(yuvTex, x  , y+1).x,
			tex2D(yuvTex, x+1, y+1).x);

	float2 gradient;
	gradient.x = arr[0].x + 1*arr[1].x +arr[2].x - arr[0].z - 1*arr[1].z - arr[2].z;
	gradient.y = arr[0].x + 1*arr[0].y +arr[0].z - arr[2].x - 1*arr[2].y - arr[2].z;
	gradient.y = gradient.y + abs(arr[1].y - tex2D(yuvTex, x, y-2).x)
		- abs(arr[1].y - tex2D(yuvTex, x, y+2).x);
	gradient.x = gradient.x + abs(arr[1].y - tex2D(yuvTex, x-2, y).x)
		- abs(arr[1].y - tex2D(yuvTex, x+2, y).x);

	float result = sqrtf(gradient.x * gradient.x + gradient.y * gradient.y);
	return result;
}

// /* TODO This is currently incorrect, as it uses stitches in dstImg, not srcImg. I think */
// __global__ void createDynamicSeam(int *stitches, Node * nodes, int length){
// 	/* Grid(4), block(DYNAMIC_SEAM_AREA,3) */
// 	int nodeIdx = blockIdx.x*DYNAMIC_SEAM_AREA + threadIdx.x;
// 	int nextNodeIdx = blockIdx.x*DYNAMIC_SEAM_AREA + threadIdx.x + threadIdx.y - 1;
// 	int stride = gridDim.x*DYNAMIC_SEAM_AREA;
// 	bool outOfBounds = ((!threadIdx.x && !threadIdx.y) ||
// 			(threadIdx.x == DYNAMIC_SEAM_AREA-1 && threadIdx.y == 2));
// 	
// 	int imgIdx = stitches[blockIdx.x] - (DYNAMIC_SEAM_AREA>>1) + threadIdx.x;
// 	/* Calculate self cost */
// 	for(int i=threadIdx.y; i<length; i+=3){
// 		nodes[nodeIdx + stride*i].selfCost = getEdgeCost(i, imgIdx);
// 		if(i){
// 			nodes[nodeIdx + stride*i].childCost[0] = 100000;
// 			nodes[nodeIdx + stride*i].childCost[1] = 100000;
// 			nodes[nodeIdx + stride*i].childCost[2] = 100000;
// 		}
// 	}
// 	__syncthreads();
// 
// 	for(int i=0; i<length-1; ++i){
// 		Node n = nodes[nodeIdx + stride*i];
// 		int best = (n.childCost[0]<n.childCost[1])?0:1;
// 		if(n.childCost[2]<n.childCost[best]) best = 2;
// 		float cost = n.selfCost + n.childCost[best];
// 		if(!threadIdx.y)
// 			nodes[nextNodeIdx + stride*i].bestChild = best;
// 		if(!outOfBounds)
// 			nodes[nextNodeIdx + stride*(i+1)].childCost[threadIdx.y] = cost;
// 
// 		__syncthreads();
// 	}
// 	if(!threadIdx.x && !threadIdx.y){
// 		/* Find best end-node */
// 		float best = 100000;
// 		int bestIdx = 0;
// 		for(int i=0; i<DYNAMIC_SEAM_AREA; ++i){
// 			Node n = nodes[blockIdx.x*DYNAMIC_SEAM_AREA + i + stride*(length-1)];
// 			if(n.childCost[n.bestChild]<best){
// 				bestIdx = i;
// 				best = n.childCost[n.bestChild];
// 			}
// 		}
// 		printf("BEST: %d : %10.2f\n", bestIdx, best);
// 	}
// }
// 
