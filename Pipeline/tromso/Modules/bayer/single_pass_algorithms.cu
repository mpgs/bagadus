// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

texture<uint8_t, 2, cudaReadModeElementType> bayerTex;

#define ALPHA_B 0.5f
#define BETA_B 0.625f
#define GAMMA_B 0.75f

#define EDGE_THRESH 8

#ifdef DO_NN
#define FAST_NN
inline __device__ void getPixels(uint8_t *array, int x, int y){
#define setp(p,i,j) (array[p] = tex2D(bayerTex,x+i,y+j))
	setp( 0, 0,-1);
	setp( 1,-1, 0);
	setp( 2, 0, 0);
	setp( 3, 1, 0);
	setp( 4,-1, 1);
	setp( 5, 0, 1);
	setp( 6, 1, 1);
#undef setp
}
#elif defined(DO_ADAPTIVE_BILINEAR)
#define FAST_A_BILINEAR
inline __device__ void getPixels(uint8_t *array, int x, int y){
	array[0] = tex2D(bayerTex,x,y-1);
	array[1] = tex2D(bayerTex,x+1,y-1);
	array[2] = tex2D(bayerTex,x+2,y-1);
	array[3] = tex2D(bayerTex,x-1,y);
	array[4] = tex2D(bayerTex,x,y);
	array[5] = tex2D(bayerTex,x+1,y);
	array[6] = tex2D(bayerTex,x+2,y);
	array[7] = tex2D(bayerTex,x-1,y+1);
	array[8] = tex2D(bayerTex,x,y+1);
	array[9] = tex2D(bayerTex,x+1,y+1);
	array[10] = tex2D(bayerTex,x+2,y+1);
	array[11] = tex2D(bayerTex,x-1,y+2);
	array[12] = tex2D(bayerTex,x,y+2);
	array[13] = tex2D(bayerTex,x+1,y+2);
}
#elif defined(DO_BILINEAR)
#define FAST_BILINEAR
inline __device__ void getPixels(uint8_t *array, int x, int y){
	array[0] = tex2D(bayerTex,x,y-1);
	array[1] = tex2D(bayerTex,x+1,y-1);
	array[2] = tex2D(bayerTex,x+2,y-1);
	array[3] = tex2D(bayerTex,x-1,y);
	array[4] = tex2D(bayerTex,x,y);
	array[5] = tex2D(bayerTex,x+1,y);
	array[6] = tex2D(bayerTex,x+2,y);
	array[7] = tex2D(bayerTex,x-1,y+1);
	array[8] = tex2D(bayerTex,x,y+1);
	array[9] = tex2D(bayerTex,x+1,y+1);
	array[10] = tex2D(bayerTex,x+2,y+1);
	array[11] = tex2D(bayerTex,x-1,y+2);
	array[12] = tex2D(bayerTex,x,y+2);
	array[13] = tex2D(bayerTex,x+1,y+2);
}
#elif defined(DO_GRADIENT_CORRECTED)
#define FAST_GRADIENT_CORRECTED
inline __device__ void getPixels(uint8_t *array, int x, int y){
#define setp(p,i,j) (array[p] = tex2D(bayerTex,x+i,y+j))
	setp( 0, 0,-2);
	setp( 1, 1,-2);
	setp( 2,-1,-1);
	setp( 3, 0,-1);
	setp( 4, 1,-1);
	setp( 5, 2,-1);
	setp( 6,-2, 0);
	setp( 7,-1, 0);
	setp( 8, 0, 0);
	setp( 9, 1, 0);
	setp(10, 2, 0);
	setp(11, 3, 0);
	setp(12,-2, 1);
	setp(13,-1, 1);
	setp(14, 0, 1);
	setp(15, 1, 1);
	setp(16, 2, 1);
	setp(17, 3, 1);
	setp(18,-1, 2);
	setp(19, 0, 2);
	setp(20, 1, 2);
	setp(21, 2, 2);
	setp(22, 0, 3);
	setp(23, 1, 3);
#undef setp
}
#endif

inline __device__ uchar3 getRed(int x, int y){
	uchar3 rgb;

#if defined(DO_NN)
	
	rgb = make_uchar3(tex2D(bayerTex,x,y), tex2D(bayerTex,x-1,y), tex2D(bayerTex,x-1,y-1));

#elif defined(DO_BILINEAR)
	
	float4 tmp;
	rgb.x = tex2D(bayerTex, x, y);
	tmp = make_float4(
			tex2D(bayerTex,x,y-1), tex2D(bayerTex,x-1,y),
			tex2D(bayerTex,x+1,y), tex2D(bayerTex,x,y+1));
	rgb.y = 0.25f*(tmp.x + tmp.y + tmp.z + tmp.w);
	tmp = make_float4(
			tex2D(bayerTex,x-1,y-1), tex2D(bayerTex,x+1,y-1),
			tex2D(bayerTex,x-1,y+1), tex2D(bayerTex,x+1,y+1));
	rgb.z = 0.25f*(tmp.x + tmp.y + tmp.z + tmp.w);

#elif defined(DO_ADAPTIVE_BILINEAR)

	rgb = make_uchar3(0,0,0);
	float4 tmp;
	rgb.x = tex2D(bayerTex, x, y);
	tmp = make_float4(
			tex2D(bayerTex,x,y-1), tex2D(bayerTex,x-1,y),
			tex2D(bayerTex,x+1,y), tex2D(bayerTex,x,y+1));
	float g_diff = fabsf(tmp.x-tmp.w) - fabsf(tmp.y-tmp.z);
	rgb.y = fabsf(g_diff) > EDGE_THRESH ?
		(g_diff < 0) ?
		0.5f*(tmp.x+tmp.w) : 0.5f*(tmp.y+tmp.z) :
	   	0.25f*(tmp.x + tmp.y + tmp.z + tmp.w);
	tmp = make_float4(
			tex2D(bayerTex,x-1,y-1), tex2D(bayerTex,x+1,y-1),
			tex2D(bayerTex,x-1,y+1), tex2D(bayerTex,x+1,y+1));
	rgb.z = 0.25f*(tmp.x + tmp.y + tmp.z + tmp.w);

#elif defined(DO_GRADIENT_CORRECTED)

	float4 tmp;
	float tmp2;

	rgb.x = tex2D(bayerTex, x, y);

	float p = tex2D(bayerTex,x,y-2)+tex2D(bayerTex,x-2,y)
		+tex2D(bayerTex,x+2,y)+tex2D(bayerTex,x,y+2);

	tmp = make_float4(
			tex2D(bayerTex,x,y-1), tex2D(bayerTex,x-1,y),
			tex2D(bayerTex,x+1,y), tex2D(bayerTex,x,y+1));
	float grad1 = rgb.x - 0.25f * p;
	tmp2 = 0.25f*(tmp.x + tmp.y + tmp.z + tmp.w) + ALPHA_B*grad1;
	rgb.y = CLAMP_255(tmp2);

	float grad2 = rgb.x - (3 * p / 12.0f);
	tmp = make_float4(
			tex2D(bayerTex,x-1,y-1), tex2D(bayerTex,x+1,y-1),
			tex2D(bayerTex,x-1,y+1), tex2D(bayerTex,x+1,y+1));
	tmp2 = 0.25f*(tmp.x + tmp.y + tmp.z + tmp.w) + GAMMA_B*grad2;
	rgb.z = CLAMP_255(tmp2);

#endif

	return rgb;
}

inline __device__ uchar3 getBlue(int x, int y){
	uchar3 rgb;

#if defined(DO_NN)

	rgb = make_uchar3(tex2D(bayerTex,x-1,y-1), tex2D(bayerTex,x-1,y), tex2D(bayerTex,x,y));

#elif defined(DO_BILINEAR)

	float4 tmp;
	tmp = make_float4(
			tex2D(bayerTex,x-1,y-1), tex2D(bayerTex,x+1,y-1),
			tex2D(bayerTex,x-1,y+1), tex2D(bayerTex,x+1,y+1));
	rgb.x = 0.25f*(tmp.x + tmp.y + tmp.z + tmp.w);
	tmp = make_float4(
			tex2D(bayerTex,x,y-1), tex2D(bayerTex,x-1,y),
			tex2D(bayerTex,x+1,y), tex2D(bayerTex,x,y+1));
	rgb.y = 0.25f*(tmp.x + tmp.y + tmp.z + tmp.w);
	rgb.z = tex2D(bayerTex, x, y);

#elif defined(DO_ADAPTIVE_BILINEAR)

	float4 tmp;
	tmp = make_float4(
			tex2D(bayerTex,x-1,y-1), tex2D(bayerTex,x+1,y-1),
			tex2D(bayerTex,x-1,y+1), tex2D(bayerTex,x+1,y+1));
	rgb.x = 0.25f*(tmp.x + tmp.y + tmp.z + tmp.w);
	tmp = make_float4(
			tex2D(bayerTex,x,y-1), tex2D(bayerTex,x-1,y),
			tex2D(bayerTex,x+1,y), tex2D(bayerTex,x,y+1));
	float g_diff = fabsf(tmp.x-tmp.w) - fabsf(tmp.y-tmp.z);
	rgb.y = fabsf(g_diff) > EDGE_THRESH ?
		(g_diff < 0) ?
		0.5f*(tmp.x+tmp.w) : 0.5f*(tmp.y+tmp.z) :
	   	0.25f*(tmp.x + tmp.y + tmp.z + tmp.w);
	rgb.z = tex2D(bayerTex, x, y);

#elif defined(DO_GRADIENT_CORRECTED)

	float4 tmp;
	float tmp2;

	rgb.z = tex2D(bayerTex, x, y);

	float p = tex2D(bayerTex,x,y-2)+tex2D(bayerTex,x-2,y)
		+tex2D(bayerTex,x+2,y)+tex2D(bayerTex,x,y+2);
	float grad2 = rgb.z - 0.25f * p;

	tmp = make_float4(
			tex2D(bayerTex,x-1,y-1), tex2D(bayerTex,x+1,y-1),
			tex2D(bayerTex,x-1,y+1), tex2D(bayerTex,x+1,y+1));
	tmp2 = 0.25f*(tmp.x + tmp.y + tmp.z + tmp.w) + GAMMA_B*grad2;
	rgb.x = CLAMP_255(tmp2);

	tmp = make_float4(
			tex2D(bayerTex,x,y-1), tex2D(bayerTex,x-1,y),
			tex2D(bayerTex,x+1,y), tex2D(bayerTex,x,y+1));
	float grad1 = rgb.z - 0.25f * p;
	tmp2 = 0.25f*(tmp.x + tmp.y + tmp.z + tmp.w) + ALPHA_B*grad1;
	rgb.y = CLAMP_255(tmp2);

#endif

	return rgb;
}

inline __device__ uchar3 getGreen1(int x, int y){
	uchar3 rgb;

#if defined(DO_NN)
	
	rgb = make_uchar3(tex2D(bayerTex,x-1,y), tex2D(bayerTex,x,y), tex2D(bayerTex,x,y-1));

#elif defined(DO_BILINEAR) || defined(DO_ADAPTIVE_BILINEAR)

	float2 tmp;
	tmp = make_float2(
			tex2D(bayerTex,x-1,y), tex2D(bayerTex,x+1,y));
	rgb.x = 0.5f*(tmp.x + tmp.y);
	rgb.y = tex2D(bayerTex, x, y);
	tmp = make_float2(
			tex2D(bayerTex,x,y-1), tex2D(bayerTex,x,y+1));
	rgb.z = 0.5f*(tmp.x + tmp.y);

#elif defined(DO_GRADIENT_CORRECTED)

	rgb.y = tex2D(bayerTex, x, y);

	float p = (float)tex2D(bayerTex,x-1,y-1)+tex2D(bayerTex,x+1,y-1)
		+tex2D(bayerTex,x-1,y+1)+tex2D(bayerTex,x+1,y+1);
	float grad1 = rgb.y - 0.2f * (p
			+tex2D(bayerTex,x-2,y)+tex2D(bayerTex,x+2,y)
			- 0.5f*((float)tex2D(bayerTex,x,y-2)+tex2D(bayerTex,x,y+2)));
	float grad2 = rgb.y - 0.2f * (p
			- 0.5f*((float)tex2D(bayerTex,x-2,y)+tex2D(bayerTex,x+2,y))
			+ tex2D(bayerTex,x,y-2)+tex2D(bayerTex,x,y+2));

	float tmp;
	tmp = 0.5f*((float)tex2D(bayerTex,x-1,y) + tex2D(bayerTex,x+1,y)) + BETA_B*grad1;
	rgb.x = CLAMP_255(tmp);
	tmp = 0.5f*((float)tex2D(bayerTex,x,y-1) + tex2D(bayerTex,x,y+1)) + BETA_B*grad2;
	rgb.z = CLAMP_255(tmp);

#endif

	return rgb;
}

inline __device__ uchar3 getGreen2(int x, int y){
	uchar3 rgb;

#if defined(DO_NN)

	rgb = make_uchar3(tex2D(bayerTex,x,y-1), tex2D(bayerTex,x,y), tex2D(bayerTex,x-1,y));

#elif defined(DO_BILINEAR) || defined(DO_ADAPTIVE_BILINEAR)

	float2 tmp;
	tmp = make_float2(
			tex2D(bayerTex,x,y-1), tex2D(bayerTex,x,y+1));
	rgb.x = 0.5f*(tmp.x + tmp.y);
	rgb.y = tex2D(bayerTex, x, y);
	tmp = make_float2(
			tex2D(bayerTex,x-1,y), tex2D(bayerTex,x+1,y));
	rgb.z = 0.5f*(tmp.x + tmp.y);

#elif defined(DO_GRADIENT_CORRECTED)

	rgb.y = tex2D(bayerTex, x, y);

	float p = (float)tex2D(bayerTex,x-1,y-1)+tex2D(bayerTex,x+1,y-1)
		+tex2D(bayerTex,x-1,y+1)+tex2D(bayerTex,x+1,y+1);
	float grad1 = rgb.y - 0.2f * (p
			+tex2D(bayerTex,x-2,y)+tex2D(bayerTex,x+2,y)
			- 0.5f*((float)tex2D(bayerTex,x,y-2)+tex2D(bayerTex,x,y+2)));
	float grad2 = rgb.y - 0.2f * (p
			- 0.5f*((float)tex2D(bayerTex,x-2,y)+tex2D(bayerTex,x+2,y))
			+ tex2D(bayerTex,x,y-2)+tex2D(bayerTex,x,y+2));

	float tmp;
	tmp = 0.5f*((float)tex2D(bayerTex,x,y-1) + tex2D(bayerTex,x,y+1)) + BETA_B*grad2;
	rgb.x = CLAMP_255(tmp);
	tmp = 0.5f*((float)tex2D(bayerTex,x-1,y) + tex2D(bayerTex,x+1,y)) + BETA_B*grad1;
	rgb.z = CLAMP_255(tmp);

#endif

	return rgb;
}

inline void CudaBayerConverter::freeExtraBuffers(){
	//Empty
}

inline void CudaBayerConverter::allocateExtraBuffers(){
	//Empty
}

inline void CudaBayerConverter::performInitialPasses(){
	//Empty
}

