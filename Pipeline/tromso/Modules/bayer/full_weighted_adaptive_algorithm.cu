// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

texture<uint8_t, 2, cudaReadModeElementType> bayerTex;
texture<uchar2, 2, cudaReadModeElementType> bayerTexG;
surface<void, cudaSurfaceType2D> bayerSurfG;
texture<uchar4, 2, cudaReadModeElementType> bayerTexGcr;
surface<void, cudaSurfaceType2D> bayerSurfGcr;

__device__ __inline float max(float4 a){
	return max(max(a.x,a.y),max(a.z,a.w));
}

__device__ __inline float min(float4 a){
	return min(min(a.x,a.y),min(a.z,a.w));
}

__device__ __inline float4 removeLower(float4 a, float thresh){
	if(a.x < thresh) a.x = 0;
	if(a.y < thresh) a.y = 0;
	if(a.z < thresh) a.z = 0;
	if(a.w < thresh) a.w = 0;
	return a;
}

__device__ float getWeightedGreen(int x, int y){
#define p(a,b) ((float)tex2D(bayerTex,x+a,y+b))
		float4 alphaW, dirG; 
		float tmp, tmp2;
#if DO_WEIGHTED_ADAPTIVE == 8
		float4 alphaW2, dirG2;
#endif
		/* West */
		dirG.x = p(-1,0)+0.5f*(p(0,0)-p(-2,0));
		alphaW.x = 1.0f/(1 + fabsf(p(1,0)-p(-1,0)) + fabsf(p(-1,0)-p(-3,0))
				+ fabsf(p(0,0)-p(-2,0)) + 0.5f*(
					fabsf(p(0,-1)-p(-2,-1)) + fabsf(p(0,1)-p(-2,1))));
		/* East */
		dirG.y = p(1,0)+0.5f*(p(0,0)-p(2,0));
		alphaW.y = 1.0f/(1 + fabsf(p(-1,0)-p(1,0)) + fabsf(p(1,0)-p(3,0))
				+ fabsf(p(0,0)-p(2,0)) + 0.5f*(
					fabsf(p(0,1)-p(2,1)) + fabsf(p(0,-1)-p(2,-1))));
		/* North */
		dirG.z = p(0,-1)+0.5f*(p(0,0)-p(0,-2));
		alphaW.z = 1.0f/(1 + fabsf(p(0,1)-p(0,-1)) + fabsf(p(0,-1)-p(0,-3))
				+ fabsf(p(0,0)-p(0,-2)) + 0.5f*(
					fabsf(p(-1,0)-p(-1,-2)) + fabsf(p(1,0)-p(1,-2))));
		/* South */
		dirG.w = p(0,1)+0.5f*(p(0,0)-p(0,2));
		alphaW.w = 1.0f/(1 + fabsf(p(0,-1)-p(0,1)) + fabsf(p(0,1)-p(0,3))
				+ fabsf(p(0,0)-p(0,2)) + 0.5f*(
					fabsf(p(1,0)-p(1,2)) + fabsf(p(-1,0)-p(-1,2))));

#if DO_WEIGHTED_ADAPTIVE == 8
		/* NW */
		dirG2.x = 0.5f*(p(0,-1)+p(-1,0)+p(0,0)-0.5f*(p(0,-2)+p(-2,0)));
		alphaW2.x = 1.0f/(1 + fabsf(p(1,1)-p(-1,-1)) + fabsf(p(-1,-1)-p(-3,-3))
				+ fabsf(p(0,0)-p(-2,-2)) + 0.5f*(
					fabsf(p(0,1)-p(-2,-1) + fabsf(p(1,0)-p(-1,-2)))));
		/* SE */
		dirG2.y = 0.5f*(p(0,1)+p(1,0)+p(0,0)-0.5f*(p(0,2)+p(2,0)));
		alphaW2.y = 1.0f/(1 + fabsf(p(-1,-1)-p(1,1)) + fabsf(p(1,1)-p(3,3))
				+ fabsf(p(0,0)-p(2,2)) + 0.5f*(
					fabsf(p(0,-1)-p(2,1) + fabsf(p(-1,0)-p(1,2)))));
		/* NE */
		dirG2.z = 0.5f*(p(0,-1)+p(1,0)+p(0,0)-0.5f*(p(0,-2)+p(2,0)));
		alphaW2.z = 1.0f/(1 + fabsf(p(1,-1)-p(-1,1)) + fabsf(p(-1,1)-p(-3,3))
				+ fabsf(p(0,0)-p(-2,2)) + 0.5f*(
					fabsf(p(0,-1)-p(-2,1) + fabsf(p(1,0)-p(-1,2)))));
		/* SW */
		dirG2.w = 0.5f*(p(0,1)+p(-1,0)+p(0,0)-0.5f*(p(0,2)+p(-2,0)));
		alphaW2.w = 1.0f/(1 + fabsf(p(-1,1)-p(1,-1)) + fabsf(p(1,-1)-p(3,-3))
				+ fabsf(p(0,0)-p(2,-2)) + 0.5f*(
					fabsf(p(0,1)-p(2,-1) + fabsf(p(-1,0)-p(1,-2)))));

		/* Uncomment these for VNG variant */
// 		float minAlpha = min(min(alphaW2),min(alphaW));
// 		float maxAlpha = max(max(alphaW2),max(alphaW));
// 		float thresh =  maxAlpha - 0.5f*(maxAlpha-minAlpha);
// 		alphaW2 = removeLower(alphaW2,thresh);
#else
// 		float minAlpha = min(alphaW);
// 		float maxAlpha = max(alphaW);
// 		float thresh =  maxAlpha - (1.5f*minAlpha + 0.5f*(maxAlpha-minAlpha));
#endif
// 		if(alphaW.x < thresh && alphaW.y < thresh && alphaW.z < thresh && alphaW.w < thresh)
// 			printf("min %8.3f\t max %8.3f\t t %8.3f\t x %8.3f\t y %8.3f\n",
// 					minAlpha, maxAlpha, thresh, alphaW.x, alphaW.y);
// 		alphaW = removeLower(alphaW,thresh);

		tmp = dirG.x*alphaW.x + dirG.y*alphaW.y + dirG.z*alphaW.z + dirG.w*alphaW.w;
		tmp2 = alphaW.x+alphaW.y+alphaW.z+alphaW.w;
#if DO_WEIGHTED_ADAPTIVE == 8
		tmp += dirG2.x*alphaW2.x + dirG2.y*alphaW2.y + dirG2.z*alphaW2.z + dirG2.w*alphaW2.w;
		tmp2 += alphaW2.x+alphaW2.y+alphaW2.z+alphaW2.w;
#endif
		return tmp / tmp2;
#undef p
}

__device__ float getWeightedRed(int x, int y){
#define c(a,b) ((float)tex2D(bayerTexGcr,x+a,y+b).x)
#define g(a,b) ((float)tex2D(bayerTexGcr,x+a,y+b).y)
	float4 alphaW, dirG; 
	float tmp, tmp2;

	/* North */
	dirG.x = c(0,-1)+(g(0,0)-g(0,-1));
	alphaW.x = 1.0f/(1 + fabsf(g(0,0)-g(0,-1)) + fabsf(g(0,-1)-g(0,-2))
			+ 0.5f*( fabsf(g(-1,0)-g(-1,-1)) + fabsf(g(-1,-1)-g(-1,-2))
					   + fabsf(g(1,0)-g(1,-1)) + fabsf(g(1,-1)-g(1,-2))));
	/* West */
	dirG.y = c(-1,0)+(g(0,0)-g(-1,0));
	alphaW.y = 1.0f/(1 + fabsf(g(0,0)-g(1,0)) + fabsf(g(1,0)-g(2,0))
			+ 0.5f*( fabsf(g(0,-1)-g(1,-1)) + fabsf(g(1,-1)-g(2,-1))
					   + fabsf(g(0,1)-g(1,1)) + fabsf(g(1,1)-g(2,1))));
	/* South */
	dirG.z = c(0,1)+(g(0,0)-g(0,1));
	alphaW.z = 1.0f/(1 + fabsf(g(0,0)-g(0,1)) + fabsf(g(0,1)-g(0,2))
			+ 0.5f*( fabsf(g(-1,0)-g(-1,1)) + fabsf(g(-1,1)-g(-1,2))
					   + fabsf(g(1,0)-g(1,1)) + fabsf(g(1,1)-g(1,2))));
	/* East */
	dirG.w = c(1,0)+(g(0,0)-g(1,0));
	alphaW.w = 1.0f/(1 + fabsf(g(0,0)-g(-1,0)) + fabsf(g(-1,0)-g(-2,0))
			+ 0.5f*( fabsf(g(0,-1)-g(-1,-1)) + fabsf(g(-1,-1)-g(-2,-1))
					   + fabsf(g(0,1)-g(-1,1)) + fabsf(g(-1,1)-g(-2,1))));

	tmp = dirG.x*alphaW.x + dirG.y*alphaW.y + dirG.z*alphaW.z + dirG.w*alphaW.w;
	tmp2 = alphaW.x+alphaW.y+alphaW.z+alphaW.w;

	return tmp / tmp2;
#undef g
#undef c
}

__device__ float getWeightedBlue(int x, int y){
#define c(a,b) ((float)tex2D(bayerTexGcr,x+a,y+b).z)
#define g(a,b) ((float)tex2D(bayerTexGcr,x+a,y+b).y)
	float4 alphaW, dirG; 
	float tmp, tmp2;

	/* North */
	dirG.x = c(0,-1)+(g(0,0)-g(0,-1));
	alphaW.x = 1.0f/(1 + fabsf(g(0,0)-g(0,-1)) + fabsf(g(0,-1)-g(0,-2))
			+ 0.5f*( fabsf(g(-1,0)-g(-1,-1)) + fabsf(g(-1,-1)-g(-1,-2))
					   + fabsf(g(1,0)-g(1,-1)) + fabsf(g(1,-1)-g(1,-2))));
	/* West */
	dirG.y = c(-1,0)+(g(0,0)-g(-1,0));
	alphaW.y = 1.0f/(1 + fabsf(g(0,0)-g(1,0)) + fabsf(g(1,0)-g(2,0))
			+ 0.5f*( fabsf(g(0,-1)-g(1,-1)) + fabsf(g(1,-1)-g(2,-1))
					   + fabsf(g(0,1)-g(1,1)) + fabsf(g(1,1)-g(2,1))));
	/* South */
	dirG.z = c(0,1)+(g(0,0)-g(0,1));
	alphaW.z = 1.0f/(1 + fabsf(g(0,0)-g(0,1)) + fabsf(g(0,1)-g(0,2))
			+ 0.5f*( fabsf(g(-1,0)-g(-1,1)) + fabsf(g(-1,1)-g(-1,2))
					   + fabsf(g(1,0)-g(1,1)) + fabsf(g(1,1)-g(1,2))));
	/* East */
	dirG.w = c(1,0)+(g(0,0)-g(1,0));
	alphaW.w = 1.0f/(1 + fabsf(g(0,0)-g(-1,0)) + fabsf(g(-1,0)-g(-2,0))
			+ 0.5f*( fabsf(g(0,-1)-g(-1,-1)) + fabsf(g(-1,-1)-g(-2,-1))
					   + fabsf(g(0,1)-g(-1,1)) + fabsf(g(-1,1)-g(-2,1))));

	tmp = dirG.x*alphaW.x + dirG.y*alphaW.y + dirG.z*alphaW.z + dirG.w*alphaW.w;
	tmp2 = alphaW.x+alphaW.y+alphaW.z+alphaW.w;

	return tmp / tmp2;
#undef g
#undef c
}

__device__ float getWeightedCr(int x, int y){
#define g(a,b) ((float)tex2D(bayerTexG,x+a,y+b).x)
#define c(a,b) ((float)tex2D(bayerTexG,x+a,y+b).y)
	float4 alphaW, dirG; 
	float tmp, tmp2;

	/* NW */
	dirG.x = c(-1,-1)+0.5f*(g(0,0)-g(-1,-1));
	alphaW.x = 1.0f/(1 + fabsf(g(-2,-2)-g(-1,-1)) + fabsf(g(-1,-1)-g(0,0))
			+ 0.5f*( fabsf(g(-2,-1)-g(-1,0) + fabsf(g(-1,-2)-g(0,-1)))));
	/* SW */
	dirG.y = c(-1,1)+0.5f*(g(0,0)-g(-1,1));
	alphaW.y = 1.0f/(1 + fabsf(g(-2,2)-g(-1,1)) + fabsf(g(-1,1)-g(0,0))
			+ 0.5f*( fabsf(g(-2,1)-g(-1,0) + fabsf(g(-1,2)-g(0,1)))));
	/* SE */
	dirG.z = c(1,1)+0.5f*(g(0,0)-g(1,1));
	alphaW.z = 1.0f/(1 + fabsf(g(2,2)-g(1,1)) + fabsf(g(1,1)-g(0,0))
			+ 0.5f*( fabsf(g(2,1)-g(1,0) + fabsf(g(1,2)-g(0,1)))));
	/* NE */
	dirG.w = c(1,-1)+0.5f*(g(0,0)-g(1,-1));
	alphaW.w = 1.0f/(1 + fabsf(g(2,-2)-g(1,-1)) + fabsf(g(1,-1)-g(0,0))
			+ 0.5f*( fabsf(g(2,-1)-g(1,0) + fabsf(g(1,-2)-g(0,-1)))));

	tmp = dirG.x*alphaW.x + dirG.y*alphaW.y + dirG.z*alphaW.z + dirG.w*alphaW.w;
	tmp2 = alphaW.x+alphaW.y+alphaW.z+alphaW.w;

	return tmp / tmp2;
#undef g
#undef c
}

__global__ void bayerWeightedAdaptiveSecondPass(int srcW, int srcH){
	for(int idx=2*(blockIdx.x*blockDim.x + threadIdx.x);
			idx < (srcW*srcH); idx += 2*(blockDim.x*gridDim.x)){

		int x = idx%srcW;
		int y = idx/srcW;

		float tmp;
		uchar4 final;
		uchar2 gc;
		if(y&1){ //BG row
			gc = tex2D(bayerTexG,x,y);
			final.y = gc.x;
			final.z = gc.y;
			tmp = getWeightedCr(x,y);
			final.x = CLAMP_255(tmp);
			surf2Dwrite(final, bayerSurfGcr, sizeof(uchar4)*x, y);

			++x;
			gc = tex2D(bayerTexG,x,y);
			final = make_uchar4(0, gc.x, 0, 0);
			surf2Dwrite(final, bayerSurfGcr, sizeof(uchar4)*x, y);
		}else{ //GR row
			gc = tex2D(bayerTexG,x,y);
			final = make_uchar4(0, gc.x, 0, 0);
			surf2Dwrite(final, bayerSurfGcr, sizeof(uchar4)*x, y);

			++x;
			gc = tex2D(bayerTexG,x,y);
			final.x = gc.y;
			final.y = gc.x;
			tmp = getWeightedCr(x,y);
			final.z = CLAMP_255(tmp);
			surf2Dwrite(final, bayerSurfGcr, sizeof(uchar4)*x, y);
		}
	}
}

__global__ void bayerWeightedAdaptiveGreenPass(int srcW, int srcH){
	for(int idx=2*(blockIdx.x*blockDim.x + threadIdx.x);
			idx < (srcW*srcH); idx += 2*(blockDim.x*gridDim.x)){

		int x = idx%srcW;
		int y = idx/srcW;

		float tmp;
		uchar2 gc;
		if(y&1){ //BG row
			gc = make_uchar2(0, tex2D(bayerTex,x,y));
			tmp = getWeightedGreen(x,y);
			gc.x = CLAMP_255(tmp);
			surf2Dwrite(gc, bayerSurfG, sizeof(uchar2)*x, y);

			++x;
			gc = make_uchar2(tex2D(bayerTex,x,y), 0);
			surf2Dwrite(gc, bayerSurfG, sizeof(uchar2)*x, y);
		}else{ //GR row
			gc = make_uchar2(tex2D(bayerTex,x,y), 0);
			surf2Dwrite(gc, bayerSurfG, sizeof(uchar2)*x, y);
			
			++x;
			gc = make_uchar2(0, tex2D(bayerTex,x,y));
			tmp = getWeightedGreen(x,y);
			gc.x = CLAMP_255(tmp);
			surf2Dwrite(gc, bayerSurfG, sizeof(uchar2)*x, y);
		}
	}
}

inline __device__ uchar3 getRed(int x, int y){
	uchar4 rgb = tex2D(bayerTexGcr,x,y);
	return make_uchar3(rgb.x,rgb.y,rgb.z);
}

inline __device__ uchar3 getBlue(int x, int y){
	uchar4 rgb = tex2D(bayerTexGcr,x,y);
	return make_uchar3(rgb.x,rgb.y,rgb.z);
}

inline __device__ uchar3 getGreen1(int x, int y){
	uchar4 rgb = tex2D(bayerTexGcr,x,y);
	float tmp = getWeightedRed(x,y);
	rgb.x = CLAMP_255(tmp);
	tmp = getWeightedBlue(x,y);
	rgb.z = CLAMP_255(tmp);
	return make_uchar3(rgb.x,rgb.y,rgb.z);
}

inline __device__ uchar3 getGreen2(int x, int y){
	uchar4 rgb = tex2D(bayerTexGcr,x,y);
	float tmp = getWeightedRed(x,y);
	rgb.x = CLAMP_255(tmp);
	tmp = getWeightedBlue(x,y);
	rgb.z = CLAMP_255(tmp);
	return make_uchar3(rgb.x,rgb.y,rgb.z);
}

void CudaBayerConverter::freeExtraBuffers(){
	cudaSafe(cudaFreeArray(bayerFirstPassBuffer));
	cudaSafe(cudaFreeArray(bayerSecondPassBuffer));
}

void CudaBayerConverter::allocateExtraBuffers(){
	cudaChannelFormatDesc u2byteChannel = cudaCreateChannelDesc<uchar2>();
	cudaSafe(cudaMallocArray(&bayerFirstPassBuffer, &u2byteChannel,
				 width, height, cudaArraySurfaceLoadStore));
	cudaSafe(cudaBindSurfaceToArray(bayerSurfG, bayerFirstPassBuffer));
	cudaSafe(cudaBindTextureToArray(bayerTexG, bayerFirstPassBuffer));
	bayerTexG.normalized = 0;
	bayerTexG.addressMode[0] = cudaAddressModeClamp;
	bayerTexG.addressMode[1] = cudaAddressModeClamp;
	bayerTexG.filterMode = cudaFilterModePoint;

	cudaChannelFormatDesc u4byteChannel = cudaCreateChannelDesc<uchar4>();
	cudaSafe(cudaMallocArray(&bayerSecondPassBuffer, &u4byteChannel,
				 width, height, cudaArraySurfaceLoadStore));
	cudaSafe(cudaBindSurfaceToArray(bayerSurfGcr, bayerSecondPassBuffer));
	cudaSafe(cudaBindTextureToArray(bayerTexGcr, bayerSecondPassBuffer));
	bayerTexGcr.normalized = 0;
	bayerTexGcr.addressMode[0] = cudaAddressModeClamp;
	bayerTexGcr.addressMode[1] = cudaAddressModeClamp;
	bayerTexGcr.filterMode = cudaFilterModePoint;
}

void CudaBayerConverter::performInitialPasses(){
	bayerWeightedAdaptiveGreenPass<<< grid, block, 0, stream >>>(width, height);
	cudaSafe(cudaStreamSynchronize(stream));
	bayerWeightedAdaptiveSecondPass<<< grid, block, 0, stream >>>(width, height);
	cudaSafe(cudaStreamSynchronize(stream));
}
