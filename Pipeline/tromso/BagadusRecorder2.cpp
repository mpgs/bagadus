// AUTHOR(s): Ragnar Langseth, Sigurd Ljodal,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdint.h>
#include <vector>
#include <string>
#include <sys/time.h>

#include <event2/event.h>

#include "Modules/FrameWriter.hpp"

#ifndef SIMULATE_CAMS
#include <PylGigE.hpp>
#else
#include "Modules/CameraSimulator.hpp"
#endif

//TODO
#ifdef SIMULATE_CAMS
#error TODO Implement simulate cams
#endif


#include "Helpers/client.h"
#include "Helpers/logging.h"

#include <sisci_api.h>

#include "config.h"
#if RECORDER_CONFIG_VERSION < 3
#error Outdated configuration, please se config_sample.h \
    and update your file.
#endif

class CameraWrapper : public FetchModule {
	public:
		CameraWrapper(PylonGigE::PylonStream * s, bool hdr){
			stream = s;
            hdrMode = hdr;
		}
		struct frame *getHdrFrame(struct frame *ret){
			PylonGigE::PylonFrame * f1, * f2;
            f1 = stream->getFrame();
			if(!f1) return NULL;
            while(!f1->darkExposure){
                delete f1;
                f1 = stream->getFrame();
                if(!f1) return NULL;
            }
            f2 = stream->getFrame();
            while(f2->darkExposure){
                delete f2;
                f2 = stream->getFrame();
                if(!f2) return NULL;
            }

			struct frame *target;
			if(ret){
				target = ret;
			}else{
				target = new struct frame;
				target->ptr = malloc(f1->size + f2->size);
			}
// 			LOG_I("F: %8ld.%6ld", f->timeStamp.tv_sec, f->timeStamp.tv_usec);
			target->hdr.timestamp = f2->timeStamp;
			target->hdr.frameNum = f2->frameCounter/2;
			target->hdr.expoFirst = f1->exposureTime;
			target->hdr.expoSecond = f2->exposureTime;
			target->hdr.flags = 0;
			target->hdr.size = f1->size + f2->size;
			memcpy(target->ptr, f1->pixels, f1->size);	
			memcpy((void*)((uint8_t*)target->ptr + f1->size), f2->pixels, f2->size);	
			delete f1;
			delete f2;
			return target;
		}

		struct frame *getFrame(struct frame *ret){
            
            if(hdrMode) return getHdrFrame(ret);

			PylonGigE::PylonFrame * f = stream->getFrame();
			if(!f) return NULL;
			struct frame *target;
			if(ret){
				target = ret;
			}else{
				target = new struct frame;
				target->ptr = malloc(f->size);
			}
// 			LOG_I("F: %8ld.%6ld", f->timeStamp.tv_sec, f->timeStamp.tv_usec);
			target->hdr.timestamp = f->timeStamp;
			target->hdr.frameNum = f->frameCounter;
			target->hdr.expoFirst = 0;
			target->hdr.expoSecond = 0;
			target->hdr.flags = 0;
			target->hdr.size = f->size;
			memcpy(target->ptr, f->pixels, f->size);	
			delete f;
			return target;
		}
	private:
		PylonGigE::PylonStream *stream;
        bool hdrMode;
};

// Static strings
static char str_hostname[] = RECORDER_HOSTNAME;

// Communication settings
static char *host = str_hostname;
static uint16_t port =  RECORDER_PORT;
static int adapter =    RECORDER_ADAPTER;
static unsigned int node =       RECORDER_NODE;

// Camera settings
static const int EXPOSURE_UPDATE_FREQUENCY = 12;//sec
static const int num_cams = RECORDER_NUM_CAMS;
static const int cam_ids[] =  RECORDER_CAM_IDS;
static const std::vector<std::string> cam_macs = RECORDER_CAM_MACS;

// Declare all modules so they are accessible from the callback
// functions in main
static int configured = 0;
static client_t *c = NULL;
static FrameWriter *writers[num_cams];
static struct pkg_config config;
static bool control_exposure = false;
static int hdr = 0;

static PylonGigE::PylGigE *camera_control;
static std::vector<PylonGigE::PylonStream*> pylon_streams;
static CameraWrapper *cameras[num_cams];

#ifndef FRAMEWRITER_KEEP_RUNNING
static bool running = false;
#endif

void print_help()
{
    fprintf(stdout, "Bagadus recorder usage:\n");
    fprintf(stdout, "[Optional] -a <dolphin adapter>\n");
    fprintf(stdout, "[Optional] -s <host name>\n");
    fprintf(stdout, "[Optional] -p <port number of host>\n");
    fprintf(stdout, "[help    ] -h <print this message>\n");
}

void shutdown_cd(const struct pkg_header *pkg, void *ctx)
{
    // TODO
    // Don't use this callback, shutdown packages are not
    // sent at the moment.

    struct event_base *base = (struct event_base *)ctx;

    fprintf(stderr, "Shutdown callback! \n");

    // TODO Perhaps disconnect in a better way

    event_base_loopbreak(base);
}

void record_cb(const struct pkg_header *pkg, void *ctx)
{
}

void whitebalance_cb(const struct pkg_header *pkg, void *ctx)
{
    struct pkg_whitebalance * p = (struct pkg_whitebalance *)pkg;
    
    for (int i = 0; i < num_cams; i++) {
        if(cam_ids[i] == p->cam_idx){
//             LOG_D("Whitebalance cam[%d]: [%5.3f,%5.3f,%5.3f]", p->cam_idx, p->red, p->green, p->blue);
            camera_control->adjustWhitebalance(i, p->red, p->green, p->blue);
        }
    }
}

void broadcast_expo(float exposure, float gain)
{
    //Called when new exposure is set on pilot machine

    // Set up the package
    struct pkg_exposure pkg;
    pkg.header.size = sizeof(struct pkg_exposure);
    pkg.header.type = pkg_type_exposure;
    pkg.exposure = exposure;
    pkg.gain = gain;

    // Send the package
    client_write(c, &pkg, pkg.header.size);
}

void config_cb(const struct pkg_header *pkg, void *ctx)
{
    // TODO
    // ctx is the event base

    config = *(struct pkg_config *)pkg;

    fprintf(stderr, "Received configuration\n");
    fprintf(stderr, "Cameras:         ");
    for(int i=0; i<num_cams; ++i){
        fprintf(stderr, "%d ",cam_ids[i]);
    }
    fprintf(stderr, "\n");
    fprintf(stderr, "Frame rate:      %3d\n", config.fps);
    fprintf(stderr, "Dimensions:      %4d:%4d\n", config.width, config.height);
    fprintf(stderr, "HDR active:      %s\n", (config.hdr_mode)?"yes":"no");
    fprintf(stderr, "Base exposure:   %4d\n", config.baseExposure);
    fprintf(stderr, "Dolphin remote:  %3d\n", config.node_id);
    fprintf(stderr, "\n");

    hdr = config.hdr_mode;

	PylonGigE::PylonConfig conf = PylonGigE::PylonConfig();
	conf.width = config.width;
	conf.height = config.height;
	conf.exposurePilotHandle = -1;
    for (int i = 0; i < num_cams; i++) {
		if(cam_ids[i] == config.pilot_cam){
			conf.exposurePilotHandle = i;
			control_exposure = true;
		}
	}
    conf.autoUpdateRegion = RECORDER_EXPOSURE_ROI;
	conf.fps = config.fps;
	conf.autoTargetValue = config.baseExposure;
	conf.exposureUpdateFrequency = config.fps * EXPOSURE_UPDATE_FREQUENCY;
	conf.exposureCallback = broadcast_expo;
    conf.hdrConf.toggle = hdr;
    conf.hdrConf.targetLight = 80;
    conf.hdrConf.targetDark = 30;
// 	conf.exposureUpdateFrequency = config.fps * 3;
    if(! camera_control->open(cam_macs,pylon_streams,conf)){
        LOG_E("Failed to open cameras");
		return;
	}

	if(!camera_control->startStreaming()){
		LOG_E("Cameras failed to start streaming");
		return;
	}
    for (int i = 0; i < num_cams; i++) {
		cameras[i] = new CameraWrapper(pylon_streams[i], hdr);
        writers[i] = new FrameWriter(cam_ids[i], config.buffer_size);
        writers[i]->setInput(cameras[i]);
        if (!writers[i]->initialize()) {
            LOG_E("Unable to initialize frame writer");
			camera_control->close();
            return;
        }
        LOG_D("Dolphin %d initialzed", cam_ids[i]);

        // Connect dolphin
        if (!writers[i]->connect(config.node_id)) {
            LOG_E("Unable to connect to frame syncer");
			camera_control->close();
            return;
        }
        LOG_D("Dolphin %d connected", cam_ids[i]);

        if (!writers[i]->threadStart()) {
            LOG_E("Error starting frame writer thread %d", cam_ids[i]);
			camera_control->close();
            return;
        }
	}

	configured = 1;
}

void expo_cb(const struct pkg_header *pkg, void *ctx)
{
    struct pkg_exposure *exp = (struct pkg_exposure *)pkg;

	if(control_exposure) return;

    if(!configured){
        LOG_W("Received a exposure callback before being configured. Ignoring.");
        return;
    }

    //Loop through cameras and set exposure. When set on pilot cam, nothing happens
	camera_control->setExposure(exp->exposure, exp->gain);
}

int main(int argc, char *argv[])
{
#ifdef LOG_TIMESTAMP
	logging_start_timer();
#endif
    // Specify options
    static struct option long_options[] = {
        {"adapter", required_argument, NULL, 'a'},
        {"host"   , required_argument, NULL, 's'},
        {"port"   , required_argument, NULL, 'p'},
        {"help"   , no_argument      , NULL, 'h'},
        {0,         0,                 NULL,  0 }
    };

    // Get options
    int opt = 0, long_index = 0;
    while ((opt = getopt_long(argc, argv,"a:s:p:h",
                   long_options, &long_index )) != -1) {
        switch (opt) {
             case 'p' : port = atoi(optarg);
                 break;
             case 's' : host = optarg;
                 break;
             case 'a' : adapter = atoi(optarg);
                 break;
             case 'h' :
             default  : print_help();
                        exit(EXIT_FAILURE);
        }
    }

    // Initialize sisci
    sci_error_t error;
    SCIInitialize(0, &error);
    if (error != SCI_ERR_OK) {
        LOG_E("Unable to initalize SISCI library. Error: %x %s:%d", error, __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }

    // Dynamically get node
    if (node == 0) {
        sci_error_t error;
        SCIGetLocalNodeId(adapter, &node, 0, &error);
        if (error != SCI_ERR_OK) {
            LOG_E("Unable to get dolphin node id. Error: %x %s:%d", error, __FILE__, __LINE__);
            SCITerminate();
            exit(EXIT_FAILURE);
        }
    }

	PylonGigE::initializePylon();

	camera_control = new PylonGigE::PylGigE();

    LOG_I("Connecting to %s:%d", host, port);
	
    // Create a new libevent base
    struct event_base *base = event_base_new();

    // Create a client for connecting to the server
    c = client_init(base, host, port);
    if (!c) {
        LOG_E("Unable to initialize network client");
        for (int i = 0; i < num_cams; i++) {
            delete cameras[i];
        }
        return EXIT_FAILURE;
    }

    // Client callbacks
    client_register_cb(c, pkg_type_config, config_cb, base);
    client_register_cb(c, pkg_type_record, record_cb, base);
    client_register_cb(c, pkg_type_exposure, expo_cb, NULL);
    client_register_cb(c, pkg_type_shutdown, shutdown_cd, base);
    client_register_cb(c, pkg_type_whitebalance, whitebalance_cb, NULL);

    // Start libevent runloop, evenrything should be configured before calling this
    event_base_dispatch(base);

    // TODO clear up memory
    client_free(c);

    // Disconnect and then free memory and resources held by the dolphin module
    if (configured) {
        for (int i = 0; i < num_cams; i++) {
            if (writers[i]->threadIsRunning()) {
                writers[i]->threadStop();
            }
            writers[i]->disconnect();
            writers[i]->terminate();
            delete writers[i];
            delete cameras[i];
        }
		camera_control->stopStreaming();
		camera_control->close();
    }
	delete camera_control;
	PylonGigE::terminatePylon();

    LOG_I("Ended up after the event base. Terminating.");

	return 0;
}

