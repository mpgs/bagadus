// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 


#include <cstdlib>
#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <pthread.h>

#include <PylGigE.hpp>
#include <logging.h>

void * readingThread(void * s){
	PylonGigE::PylonStream * stream = (PylonGigE::PylonStream *) s;

	PylonGigE::PylonFrame * f;
	while( (f = stream->getFrame()) != NULL){
// 		fprintf(stderr,"Frame: %8ld.%7ld :: %10dbytes addr: %p\n",
// 				f->timeStamp.tv_sec,f->timeStamp.tv_usec,
// 				(int)f->size, (void*)f->pixels);
		delete f;
	}

	pthread_exit(0);
	return NULL;
}

int main(int argc, char ** argv){

	PylonGigE::initializePylon();
	PylonGigE::printAvailableDevices();
	{
		PylonGigE::PylGigE cams;
		std::vector<PylonGigE::PylonStream*> streams;
// 		std::vector<std::string> macs = {"003053151E2C"};
// 		std::vector<std::string> macs = {"003053161421","003053151E2C", "003053161436"};
		std::vector<std::string> macs = {
            "003053161421","003053151E2C","003053161436",
            "00305317090D","003053170913","00305317090F","003053170914"
        };

		PylonGigE::PylonConfig conf;//Default parameters set

        conf.exposureUpdateFrequency = conf.fps * 2;
        conf.fps = 50;
//         conf.hdrConf.toggle = true;
//         conf.hdrConf.targetDark = 70;
//         conf.hdrConf.targetLight = 40;
		
        if(!cams.open(macs, streams, conf)){
			std::cerr << "Open went to hell\n";
			PylonGigE::terminatePylon();
			return EXIT_FAILURE;
		}

// 		std::vector<void *> bufs;
// 		int bufSize = 2040*1080*2 + 128; //Times 2 in case testing for yuv422
// 		for(int i=0; i<12; ++i){
// 			bufs.push_back( malloc(bufSize));
// 		}
// 
// 		if(!cams.useCustomBuffers(bufs, bufSize)){
// 			std::cerr << "Custom buffers failed\n";
// 			cams.close();
// 			PylonGigE::terminatePylon();
// 			return EXIT_FAILURE;
// 		}
		if(!cams.startStreaming()){
			std::cerr << "Start Streaming failed\n";
			cams.close();
			PylonGigE::terminatePylon();
			return EXIT_FAILURE;
		}

		pthread_t threads[streams.size()];
		for(size_t i=0; i<streams.size(); ++i){
			pthread_create(&threads[i], NULL, readingThread, (void*) streams[i]);
		}
		
		std::cerr << "Sleeping...\n";
        int timeToSleep = (argc > 1) ? atoi(argv[1]) : 15;
		for(int st=timeToSleep; st > 0; st = sleep(st));
		std::cerr << "Waking...\n";

		for(size_t i=0; i<streams.size(); ++i){
			streams[i]->terminateStream();
		}
		for(size_t i=0; i<streams.size(); ++i){
			pthread_join(threads[i], NULL);
		}

		cams.stopStreaming();
		cams.close();
	}
	PylonGigE::terminatePylon();

	return EXIT_SUCCESS;
}
