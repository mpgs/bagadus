
project (Bagadussi)
cmake_minimum_required(VERSION 2.8)

set (CMAKE_BUILD_TYPE release)       #  (debug), release
set (CMAKE_VERBOSE_MAKEFILE false)   #  on, (off)
set (BUILD_SHARED_LIBS false)        #  on, (off)
set (LINK_SEARCH_END_STATIC true)    #  (on), off

# extra path for CMake Find modules
set (CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_SOURCE_DIR}/CMake.modules)

# supported configurations
set (CMAKE_CONFIGURATION_TYPES "Debug;Release" CACHE STRING "Configs" FORCE)

#  prefix path for searching private libs
set (CMAKE_PREFIX_PATH $ENV{HOME} CACHE STRING "search prefix")

# build options
option (ENABLE_PYLON "Enable the PylonSDK (default: YES)" true)
option (ENABLE_CUDA "Enable the CUDA SDK (default: YES)" true)

## Release build
set (CMAKE_CXX_FLAGS_RELEASE "-O2 -g3 -Wall")
set (CMAKE_C_FLAGS_RELEASE ${CMAKE_CXX_FLAGS_RELEASE})

## PROFILING build option
if (ENABLE_PROFILING)
    set (CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -pg")
    set (CMAKE_C_FLAGS_DEBUG ${CMAKE_CXX_FLAGS_DEBUG})

    set (CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -pg")
    set (CMAKE_C_FLAGS_RELEASE ${CMAKE_CXX_FLAGS_RELEASE})
endif ()

if(APPLE)
    set (CMAKE_CXX_FLAGS_DEBUG "-g3 -ggdb3 -DDEBUG -Wall -stdlib=libstdc++")
else()
    set (CMAKE_CXX_FLAGS_DEBUG "-g3 -ggdb3 -DDEBUG -Wall")
endif()

# '.' in include paths
set (CMAKE_INCLUDE_CURRENT_DIR true)
set (CMAKE_INCLUDE_DIRECTORIES_PROJECT_BEFORE true)

# set output path for the finished libraries (cmake built-in)
set (LIBRARY_OUTPUT_PATH ${CMAKE_BINARY_DIR}/lib CACHE PATH
    "Output directory for libraries")

# set output path for executables (cmake built-in)
set (EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR}/bin CACHE PATH
     "Output directory for applications")

# set output path for extra data (see macro below)
set (EXTRA_DATA_OUTPUT_PATH ${CMAKE_BINARY_DIR}/data CACHE PATH
     "Output directory for data (e.g. shared)")

# check for 32/64-bit system
if (${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    set (IS_LINUX true)
    if (CMAKE_SIZEOF_VOID_P MATCHES "8")
        SET (IS_AMD64 true)
        set (IS_LINUX64 true)
    else ()
        SET (IS_AMD64 false)
        set (IS_LINUX32 true)
    endif ()
    set (CMAKE_FIND_LIBRARY_SUFFIXES ".a" ".so")
endif ()

if (${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
    set (IS_DARWIN true)
    if (CMAKE_SIZEOF_VOID_P MATCHES "8")
        set (IS_DARWIN64 true)
    else ()
        set (IS_DARWIN32 true)
    endif ()
    set (CMAKE_FIND_LIBRARY_SUFFIXES ".a" ".dylib")
endif ()


#--------------------------------------------------------------------------
# Macro: EXPAND_LIST (converts any number of Cmake lists to a single whitespace
#        delimited string)
#--------------------------------------------------------------------------
macro (EXPAND_LIST S)
  set (${S} "")
  foreach(ARGLIST ${ARGN})  # ARGN is the variable containing all remaining args
    foreach(ARG ${ARGLIST})
      set(${S} "${${S}} ${ARG}")
    endforeach()
  endforeach()
endmacro()



#--------------------------------------------------------------------------
# Macro: INSTALL_EXTRA_DATA (links extra data files into the DATA directry)
#        - SOURCE_DATA_DIR must be an existing directory
#--------------------------------------------------------------------------
function (LINK_DIRECTORY SOURCE_DATA_DIR DEST_DATA_DIR)

    get_filename_component(DIRNAME ${SOURCE_DATA_DIR} NAME)

    # create the directory
    file(MAKE_DIRECTORY ${DEST_DATA_DIR}/${DIRNAME})

    # recurse through dir and link all files, create sub-directories
    FILE(GLOB ALL_FILES ${SOURCE_DATA_DIR}/*)
    foreach(ENTRY_FULL ${ALL_FILES})
        get_filename_component(ENTRY ${ENTRY_FULL} NAME)

        if (IS_DIRECTORY ${ENTRY_FULL})
            LINK_DIRECTORY(${ENTRY_FULL} ${DEST_DATA_DIR}/${DIRNAME})
        endif()

        if (NOT IS_DIRECTORY ${ENTRY_FULL})
          execute_process(COMMAND ${CMAKE_COMMAND} -E create_symlink
                          ${ENTRY_FULL} ${DEST_DATA_DIR}/${DIRNAME}/${ENTRY})
        endif ()
    endforeach()
endfunction()

function(INSTALL_EXTRA_DATA SOURCE_DATA_DIR)
    # create the data directory
    file(MAKE_DIRECTORY ${EXTRA_DATA_OUTPUT_PATH})

    # also link it into the bin install directory
    get_filename_component(SFX ${EXTRA_DATA_OUTPUT_PATH} NAME)
    #if ( ${CMAKE_GENERATOR} MATCHES "Xcode")
    if ( NOT EXISTS ${EXECUTABLE_OUTPUT_PATH})
        # Xcode is a littel more complicated since it will create the
        # ${CMAKE_BUILD_TYPE} directory itself _after_ cmake finished
        # hence we try creating it first
        file(MAKE_DIRECTORY ${EXECUTABLE_OUTPUT_PATH}/${CMAKE_BUILD_TYPE})
        set(BIN_DATA_DIR ${EXECUTABLE_OUTPUT_PATH}/${CMAKE_BUILD_TYPE}/${SFX})
    else ()
        set(BIN_DATA_DIR ${EXECUTABLE_OUTPUT_PATH}/${SFX})
    endif()

    execute_process(COMMAND ${CMAKE_COMMAND} -E create_symlink
                    ${EXTRA_DATA_OUTPUT_PATH} ${BIN_DATA_DIR})

    # link all files and directories
    FILE(GLOB ALL_FILES ${SOURCE_DATA_DIR}/*)
    foreach(ENTRY_FULL ${ALL_FILES})
        get_filename_component(ENTRY ${ENTRY_FULL} NAME)

        # create directories and link contained files
        if (IS_DIRECTORY ${ENTRY_FULL})
            LINK_DIRECTORY(${ENTRY_FULL} ${EXTRA_DATA_OUTPUT_PATH})
        endif ()

        if (NOT IS_DIRECTORY ${ENTRY_FULL})
            execute_process(COMMAND ${CMAKE_COMMAND} -E create_symlink
                            ${ENTRY_FULL} ${EXTRA_DATA_OUTPUT_PATH}/${ENTRY})
        endif ()
    endforeach()
endfunction()


add_subdirectory(tromso)
