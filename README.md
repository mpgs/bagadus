# Bagadus!

This is the open source version of the Bagadus repository, focused on the Tromso stadium panorama.

Main required libraries for the core targets for this project are (install in this order):
```
[r:p] gcc 4.7+ (for c++11, we use 4.8 as of writing this)
Howto: http://askubuntu.com/a/117813

[r:p] yasm     (version 1.2+)
./configure
make -j8 && sudo make install

[r  ] PylonSDK (see readme in PylonSDK dir, custom installation only)

[r:p] OpenCV   (newest stable, or at least 2.4.6+)
mkdir build ; cd build ; ccmake .. (enable jasper, jpeg, png and disable with-cuda)
make -j8 && sudo make install

[r:p] Cuda     (v6.0, v6.5 does not support driver v334, see below)
Nvidia display driver v334, this seems to be the only version that currently
functions correctly with NVENC on linux, at least on both Kepler and Maxwell.

[r:p] sisci & dolphin drivers (custom installation only)
#TODO Include install information

r = camera recording targets
p = processing targets
```

Additionally, there are multiple libraries that should be installed first through aptitude:
```
linux-source build-essential software-properties-common python-software-properties pkg-config
cmake cmake-curses-gui gdb 'g++' gcc git libgstreamer0.10-dev
libudev-dev libboost-system1.48-dev libboost-filesystem1.48-dev isc-dhcp-server
libevent-dev postgresql libpq-dev libopenjpeg-dev libjasper-dev

Ubuntu 14.04 also requires: postgresql-server-dev-9.3
```

This list may be incomplete, some libraries may have higher available versions and not all
targets (and therefore machines) require all of these libraries.

In order to read from GigE-cameras, we use Pylon SDK. The utilized version of this library is
included in the repository in Pipeline/PylonSDK. Inside the tar-ball is another tar-ball, which contains
the source code, as well as documentation and samples.
Copy the source code, 'pylonX'-directory, into /opt/.
Once installed, you may use the 'ListCameras' target to print all available cameras,
or the 'MakeImage' to capture an image from all available cameras.
Machines that need to utilize Pylon require some setup,
which is described in readme of PylonSDK dir.

BagadusProcessor and BagadusRecorder are targets for the main panorama pipeline.
These two applications are meant to be used on different machines, with multiple recorders
and a single processor. These use a config.h file to control most of the important information.
SingleMachine is a simplified version of the pipeline, based on the same modules, that may support
fewer cameras, but uses only a single machine.

